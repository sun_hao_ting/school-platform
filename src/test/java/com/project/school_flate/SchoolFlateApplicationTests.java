package com.project.school_flate;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybatisflex.codegen.Generator;
import com.mybatisflex.codegen.config.EntityConfig;
import com.mybatisflex.codegen.config.GlobalConfig;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.update.UpdateChain;
import com.project.school_flate.dto.shop.ShopAccountDto;
import com.project.school_flate.dto.shop.ShopExamineDto;
import com.project.school_flate.entity.com.table.ComVariableTableDef;
import com.project.school_flate.entity.integral.IntegralExchangeSave;
import com.project.school_flate.entity.order.*;
import com.project.school_flate.mapper.integral.IntegralExchangeSaveMapper;
import com.project.school_flate.mapper.order.*;
import com.project.school_flate.service.delivery.DeliveryExamineService;
import com.project.school_flate.service.shop.ShopAccountService;
import com.project.school_flate.service.shop.ShopExamineService;
import com.project.school_flate.util.wechat.WxMessagesManager;
import com.project.school_flate.dto.order.OrderTakeawayDto;
import com.project.school_flate.entity.com.ComDistributionRule;
import com.project.school_flate.entity.com.ComFileDetail;
import com.project.school_flate.entity.com.ComVariable;
import com.project.school_flate.entity.delivery.DeliveryInfo;
import com.project.school_flate.entity.flow.FlowBusiness;
import com.project.school_flate.entity.flow.FlowMoney;
import com.project.school_flate.entity.flow.FlowTask;
import com.project.school_flate.entity.shop.*;
import com.project.school_flate.entity.table.shop.ShopCommodityTable;
import com.project.school_flate.entity.user.UserInfo;
import com.project.school_flate.mapper.com.ComDistributionRuleMapper;
import com.project.school_flate.mapper.com.ComFileDetailMapper;
import com.project.school_flate.mapper.com.ComVariableMapper;
import com.project.school_flate.mapper.delivery.DeliveryExamineMapper;
import com.project.school_flate.mapper.com.ComAdminMapper;
import com.project.school_flate.mapper.delivery.DeliveryInfoMapper;
import com.project.school_flate.mapper.flow.FlowBusinessMapper;
import com.project.school_flate.mapper.flow.FlowMoneyMapper;
import com.project.school_flate.mapper.flow.FlowTaskMapper;
import com.project.school_flate.mapper.integral.IntegralCommodityMapper;
import com.project.school_flate.mapper.shop.*;
import com.project.school_flate.mapper.user.UserInfoMapper;
import com.project.school_flate.service.com.ComFileDetailService;
import com.project.school_flate.service.flow.FlowTaskService;
import com.project.school_flate.service.order.OrderTakeawayService;
import com.project.school_flate.service.shop.ShopCommodityEvaluateService;
import com.project.school_flate.util.DateUtils;
import com.project.school_flate.util.PoToDTO;
import com.zaxxer.hikari.HikariDataSource;
import org.dromara.x.file.storage.core.FileInfo;
import org.dromara.x.file.storage.core.FileStorageService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.*;

@SpringBootTest
class SchoolFlateApplicationTests {

    @Test
    void contextLoads() {
//        System.out.println((1709217140060L - 1709217170520L)/1000);
        //配置数据源
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl("jdbc:mysql://cd-cdb-qqrkcl4e.sql.tencentcdb.com:63931/aaaa?useUnicode=true&&characterEncoding=utf-8&&useSSL=false");
        dataSource.setUsername("root");
        dataSource.setPassword("sht960522");
        //创建配置内容，两种风格都可以。
        GlobalConfig globalConfig = createGlobalConfigUseStyle1();
        //通过 datasource 和 globalConfig 创建代码生成器
        Generator generator = new Generator(dataSource, globalConfig);
        //生成代码
        generator.generate();
    }

    public static GlobalConfig createGlobalConfigUseStyle1() {
        //创建配置内容
        GlobalConfig globalConfig = new GlobalConfig();
        //设置表前缀和只生成哪些表
        globalConfig.setTablePrefix("t_");
        //设置根包
        globalConfig.setBasePackage("com.project.school_flate");
        //注释配置
        globalConfig.getJavadocConfig()
                .setAuthor("马维健")
                .setSince("0.0.1")
                .setSince("2024/1/2");
        globalConfig.setEntityWithLombok(true);
        //设置生成 entity
        globalConfig.setEntityGenerateEnable(true);
        //设置生成 controller
        globalConfig.setControllerGenerateEnable(true);
        //设置生成 Service
        globalConfig.setServiceGenerateEnable(true);
        //设置生成 ServiceImpl
        globalConfig.setServiceImplGenerateEnable(true);
        //设置生成 mapper
        globalConfig.setMapperGenerateEnable(true);
        //设置生成 mapperXml
        globalConfig.setMapperXmlGenerateEnable(true);
        //设置生成TableDef
        globalConfig.setTableDefGenerateEnable(true);
        globalConfig.enableEntity()
                .setWithLombok(true)
                .setOverwriteEnable(false)
                .setWithLombok(true)
                .setWithSwagger(true)
                .setSwaggerVersion(EntityConfig.SwaggerVersion.DOC);
        // 开启 Mapper 生成配置
        globalConfig.getMapperConfig()
                .setClassSuffix("Mapper")
                .setOverwriteEnable(false)
                .setMapperAnnotation(true);
        // 开启 Service 生成配置
        globalConfig.getServiceConfig()
                .setClassSuffix("Service")
                .setOverwriteEnable(false);
        // 开启 ServiceImpl  生成配置
        globalConfig.getServiceImplConfig()
                .setOverwriteEnable(false);
        //Controller 生成配置
        globalConfig.getControllerConfig()
                .setOverwriteEnable(false);
        // TableDef 生成配置
        globalConfig.getTableDefConfig()
                .setClassSuffix("Table")
                .setOverwriteEnable(true);
        // MapperXml 生成配置
        globalConfig.getMapperConfig()
                .setOverwriteEnable(false);
        return globalConfig;
    }

    @Autowired
    private FileStorageService fileStorageService;//注入实列

    @Autowired
    private ComFileDetailService oComFileDetailService;

    @Test
    void contextLoads2() throws Exception {
        //上传文件
        FileInfo fileInfo = fileStorageService.of(new File("C:\\Users\\78789\\Desktop\\图片\\轮播1.jpg")).setPath("publicImage/").upload();
//        oComFileDetailService.saveFileInfo(fileInfo,1);
//        oComFileDetailService.saveFileInfo(fileInfo);
//        fileInfo = fileStorageService.of(new File("C:\\Users\\Administrator\\Desktop\\图片\\2.jpg")).upload();
//        oComFileDetailService.saveFileInfo(fileInfo);


//       fileInfo.getUrl();
        // 获取文件信息
//        FileInfo fileInfo = fileStorageService.getFileInfoByUrl("http://106.55.255.177:9990/pipenetworkimage/test/6595322b4d195516fe4b23e4.png");
//        System.out.println(fileInfo);
//        ComFileDetail oComFileDetail = new ComFileDetail();
//        PoToDTO.poToDto(fileInfo,oComFileDetail);
//
//        System.out.println(oComFileDetail);
//        //生成 URL ，有效期为1小时
//        String presignedUrl = fileStorageService.generatePresignedUrl(fileInfo, DateUtil.offsetHour(new Date(),1));
//        System.out.println("文件授权访问地址：" + presignedUrl);
        // 下载到文件
//        fileStorageService.download(fileInfo).file("C:\\Users\\78789\\Desktop\\图片2.png");
//        String thPresignedUrl = fileStorageService.generateThPresignedUrl(fileInfo,DateUtil.offsetHour(new Date(),1));
//        System.out.println("缩略图文件授权访问地址：" + thPresignedUrl);

    }

    @Autowired
    private ComAdminMapper oComAdminMapper;

    @Autowired
    private DeliveryInfoMapper oDeliveryInfoMapper;

    @Autowired
    private DeliveryExamineMapper oDeliveryExamineMapper;

    @Autowired
    private ShopInfoMapper oShopInfoMapper;

    @Autowired
    private ShopExamineMapper oShopExamineMapper;

    @Test
    void contextLoads3() throws Exception {

        ShopInfo oShopInfo = new ShopInfo();
        oShopInfo.setTradeBeginHour(DateUtils.stringTurnDate("09:00","HH:mm"));
        oShopInfo.setTradeEndHour(DateUtils.stringTurnDate("18:00","HH:mm"));
        oShopInfoMapper.insert(oShopInfo);
//        ComAdmin oComAdmin = new ComAdmin();
//        oComAdmin.setLoginName("aaa");
//        oComAdminMapper.insert(oComAdmin);

//        List<ShopInfo> oShopInfoList = oShopInfoMapper.selectAll();
//        for(ShopInfo oShopInfo : oShopInfoList){
//            ShopExamine oShopExamine = new ShopExamine();
//            oShopExamine.setShopId(oShopInfo.getId());
//            oShopExamineMapper.insert(oShopExamine);
//        }
    }

    @Test
    void test667() throws Exception {
        ShopExamine oShopExamine = oShopExamineMapper.selectOneById("106970257835655168");
        oShopExamine.setIsPass(1);
        oShopExamine.setAuditTime(new Date());
        oShopExamine.setAdminId("54692271085273077");
        oShopExamineMapper.update(oShopExamine);
        //创建店铺
        ShopInfo oShopInfo = new ShopInfo();
        oShopInfo.setId(oShopExamine.getShopId());
        //判断是否通过
        if(oShopExamine.getIsPass() == 1){
            //修改店铺信息
            oShopInfo.setExamineState(1);
            oShopInfo.setName(oShopExamine.getName());
            oShopInfo.setAccountId(oShopExamine.getAccountId());
            oShopInfo.setAddress(oShopExamine.getAddress());
            oShopInfo.setMainBusiness(oShopExamine.getMainBusiness());
            oShopInfo.setBusinessLicense(oShopExamine.getBusinessLicense());
            oShopInfo.setOperatePermit(oShopExamine.getOperatePermit());
            oShopInfo.setSchoolId(oShopExamine.getSchoolId());
            oShopInfo.setFullName(oShopExamine.getFullName());
            oShopInfo.setIdCard(oShopExamine.getIdCard());
            oShopInfo.setIdFront(oShopExamine.getIdFront());
            oShopInfo.setIdOpposite(oShopExamine.getIdOpposite());
        }else{
            oShopInfo.setExamineState(0);
        }
        //修改店铺状态
        oShopInfoMapper.update(oShopInfo);
        test6667();
    }

    @Test
    void test6667() throws Exception {
        ShopInfo oShopInfo = oShopInfoMapper.selectOneById("106970257294589952");
        oShopInfo.setTradeBeginHour(DateUtils.stringTurnDate("09:00:00","HH:mm:ss"));
        oShopInfo.setTradeEndHour(DateUtils.stringTurnDate("21:00:00","HH:mm:ss"));
        oShopInfo.setIsTrade(1);
        oShopInfo.setStartPrice(10.0);
        oShopInfo.setDeliveryPrice(2.0);
        oShopInfo.setIntroduction("本店历史悠久、味道绝美、价格实惠、快来品尝！！！");
        oShopInfo.setImage("106970437876166656");
        oShopInfo.setShopTypeId("106863718009368578");
        oShopInfo.setNotice("收藏店铺、储蓄充值，优惠多多！！！");
        oShopInfo.setIsPerfect(1);
        oShopInfo.setLogoImage("106970437876166656");
        oShopInfoMapper.update(oShopInfo);
    }

    @Autowired
    private ShopTypeMapper oShopTypeMapper;

    @Autowired
    private IntegralCommodityMapper oIntegralCommodityMapper;

    @Autowired
    private OrderTakeawayMapper oOrderTakeawayMapper;

    @Autowired
    private ShopCommodityEvaluateService oShopCommodityEvaluateService;

    @Autowired
    private ShopCommoditySpecsMapper oShopCommoditySpecsMapper;

    @Autowired
    private OrderTakeawayCommodityMapper oOrderTakeawayCommodityMapper;

    @Test
    void test6671() throws Exception {
        OrderTakeaway oOrderTakeaway1 = new OrderTakeaway();
        oOrderTakeaway1.setCallNumber(1006);//pp
        oOrderTakeaway1.setUserId("100712216383434701");
        oOrderTakeaway1.setPrice(0.06);//pp
        oOrderTakeaway1.setUserAddress("606号");
        oOrderTakeaway1.setPaymentMethod(0);
        oOrderTakeaway1.setSchoolId("100692379580727296");
        oOrderTakeaway1.setSchoolBuildingId("100692379580727297");
        oOrderTakeaway1.setPhone("13000000000");
        oOrderTakeaway1.setContacts("王先生");
        oOrderTakeaway1.setShopId("106934825999642624");
        oOrderTakeaway1.setPayState(1);
        oOrderTakeaway1.setPayTime(new Date());
        oOrderTakeawayMapper.insert(oOrderTakeaway1);

        List<OrderTakeawayCommodity> list = new ArrayList<>();
        OrderTakeawayCommodity oOrderTakeawayCommodity1 = new OrderTakeawayCommodity();
        oOrderTakeawayCommodity1.setOrderTakeawayId(oOrderTakeaway1.getId());
        oOrderTakeawayCommodity1.setCommodityId("106942667984961536");//pp
        oOrderTakeawayCommodity1.setPrice(0.03);//pp
        oOrderTakeawayCommodity1.setNumber(3);//pp
        oOrderTakeawayCommodity1.setCommoditySpecs("107072998556602368");//pp

        OrderTakeawayCommodity oOrderTakeawayCommodity2 = new OrderTakeawayCommodity();
        oOrderTakeawayCommodity2.setOrderTakeawayId(oOrderTakeaway1.getId());
        oOrderTakeawayCommodity2.setCommodityId("106943093971058688");//pp
        oOrderTakeawayCommodity2.setPrice(0.03);//pp
        oOrderTakeawayCommodity2.setNumber(3);//pp
        oOrderTakeawayCommodity2.setCommoditySpecs("107072998556602374");//pp
        list.add(oOrderTakeawayCommodity1);
        list.add(oOrderTakeawayCommodity2);
        oOrderTakeawayCommodityMapper.insertBatch(list);


    }

    @Autowired
    private ComFileDetailMapper oComFileDetailMapper;

    @Test
    void test66712() throws Exception {
        List<ComFileDetail> comFileDetails = oComFileDetailMapper.selectAll();
        System.out.println(comFileDetails);
    }

    @Autowired
    private ComDistributionRuleMapper oComDistributionRuleMapper;

    @Test
    void test6671tt() throws Exception {
        ComDistributionRule oComDistributionRule1 = new ComDistributionRule();
        oComDistributionRule1.setGrade(1);
        oComDistributionRule1.setCommission(0.05);
        oComDistributionRule1.setIncome(1.0);
        oComDistributionRule1.setType(0);

        ComDistributionRule oComDistributionRule2 = new ComDistributionRule();
        oComDistributionRule2.setGrade(1);
        oComDistributionRule2.setCommission(0.15);
        oComDistributionRule2.setIncome(0.5);
        oComDistributionRule2.setType(1);

        ComDistributionRule oComDistributionRule3 = new ComDistributionRule();
        oComDistributionRule3.setGrade(2);
        oComDistributionRule3.setCommission(0.07);
        oComDistributionRule3.setIncome(100.0);
        oComDistributionRule3.setType(0);

        ComDistributionRule oComDistributionRule4 = new ComDistributionRule();
        oComDistributionRule4.setGrade(2);
        oComDistributionRule4.setCommission(0.17);
        oComDistributionRule4.setIncome(50.0);
        oComDistributionRule4.setType(1);

        List<ComDistributionRule> oComDistributionRuleList = new ArrayList<>();
        oComDistributionRuleList.add(oComDistributionRule1);
        oComDistributionRuleList.add(oComDistributionRule2);
        oComDistributionRuleList.add(oComDistributionRule3);
        oComDistributionRuleList.add(oComDistributionRule4);
        oComDistributionRuleMapper.insertBatch(oComDistributionRuleList);
    }

    @Test
    void test6671tt11() throws Exception {
        StpUtil.logout("100712216383434701");
    }

    @Autowired
    private FlowBusinessMapper oFlowBusinessMapper;

    @Autowired
    private FlowMoneyMapper oFlowMoneyMapper;

    @Autowired
    private FlowTaskMapper oFlowTaskMapper;

    @Autowired
    private ShopCommodityEvaluateMapper oShopCommodityEvaluateMapper;

    @Autowired
    private ShopCommodityMapper oShopCommodityMapper;

    @Autowired
    private UserInfoMapper oUserInfoMapper;

    @Autowired
    private OrderTakeawaySaveCacheMapper oOrderTakeawaySaveCacheMapper;

    @Autowired
    private OrderDeliveryMapper oOrderDeliveryMapper;

    @Autowired
    private OrderTakeawayCancellationMapper oOrderTakeawayCancellationMapper;

    //清理测试订单所有数据
    @Test
    void deleteOrder() throws Exception {
        List<OrderTakeaway> oOrderTakeawayList = oOrderTakeawayMapper.selectAll();
        for(OrderTakeaway oOrderTakeaway : oOrderTakeawayList){
            oOrderTakeawayMapper.delete(oOrderTakeaway);
        }

        List<OrderTakeawayCommodity> oOrderTakeawayCommodityList = oOrderTakeawayCommodityMapper.selectAll();
        for(OrderTakeawayCommodity oOrderTakeawayCommodity : oOrderTakeawayCommodityList){
            oOrderTakeawayCommodityMapper.delete(oOrderTakeawayCommodity);
        }

        List<FlowBusiness> oFlowBusinessList = oFlowBusinessMapper.selectAll();
        for(FlowBusiness oFlowBusiness : oFlowBusinessList){
            oFlowBusinessMapper.delete(oFlowBusiness);
        }

        List<FlowMoney> oFlowMoneyList = oFlowMoneyMapper.selectAll();
        for(FlowMoney oFlowMoney : oFlowMoneyList){
            oFlowMoneyMapper.delete(oFlowMoney);
        }

        List<FlowTask> oFlowTaskList = oFlowTaskMapper.selectAll();
        for(FlowTask oFlowTask : oFlowTaskList){
            oFlowTaskMapper.delete(oFlowTask);
        }

        List<ShopCommodityEvaluate> oShopCommodityEvaluateList = oShopCommodityEvaluateMapper.selectAll();
        for(ShopCommodityEvaluate oShopCommodityEvaluate : oShopCommodityEvaluateList){
            oShopCommodityEvaluateMapper.delete(oShopCommodityEvaluate);
        }

        ShopInfo oShopInfo = oShopInfoMapper.selectOneById("106934825999642624");
        oShopInfo.setSalesVolume(0);
        oShopInfo.setBalance(0.0);
        oShopInfo.setScore(4.7);
        oShopInfo.setEvaluateNum(0);
        oShopInfoMapper.update(oShopInfo);

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(ShopCommodityTable.SHOP_COMMODITY.SHOP_ID.eq("106934825999642624"));
        List<ShopCommodity> oShopCommodityList = oShopCommodityMapper.selectListByQuery(queryWrapper);
        for(ShopCommodity oShopCommodity : oShopCommodityList){
            oShopCommodity.setSalesVolume(0);
            oShopCommodity.setScore(4.7);
            oShopCommodity.setEvaluateNum(0);
            oShopCommodityMapper.update(oShopCommodity);
        }

        UserInfo oUserInfo1 = oUserInfoMapper.selectOneById("100712216383434701");
        oUserInfo1.setConsumptionPrice(0.0);
        oUserInfo1.setIntegral(0);
        oUserInfo1.setSavingsPrice(0.0);
        oUserInfo1.setBalance(0.0);
        oUserInfoMapper.update(oUserInfo1);
        UserInfo oUserInfo2 = oUserInfoMapper.selectOneById("100712216383434702");
        oUserInfo2.setDistributionPrice(0.0);
        oUserInfo2.setDistributionGrade(1);
        oUserInfo2.setBalance(0.0);
        oUserInfoMapper.update(oUserInfo2);

        DeliveryInfo oDeliveryInfo = oDeliveryInfoMapper.selectOneById("102238792552005632");
        oDeliveryInfo.setIncome(0.0);
        oDeliveryInfo.setBalance(0.0);
        oDeliveryInfo.setScore(0.0);
        oDeliveryInfo.setEvaluateNum(0);
        oDeliveryInfoMapper.update(oDeliveryInfo);

        UpdateChain.of(ShopSaveUser.class)
                .set(ShopSaveUser::getUserBalance, 200)
                .set(ShopSaveUser::getIsSpend, 0)
                .where(ShopSaveUser::getId).eq("106934825999642626")
                .update();

        List<OrderTakeawaySaveCache> oOrderTakeawaySaveCacheList = oOrderTakeawaySaveCacheMapper.selectAll();
        for(OrderTakeawaySaveCache oOrderTakeawaySaveCache : oOrderTakeawaySaveCacheList){
            oOrderTakeawaySaveCacheMapper.delete(oOrderTakeawaySaveCache);
        }

        List<OrderDelivery> oOrderDeliveryList = oOrderDeliveryMapper.selectAll();
        for(OrderDelivery oOrderDelivery : oOrderDeliveryList){
            oOrderDeliveryMapper.delete(oOrderDelivery);
        }

        List<OrderTakeawayCancellation> oOrderTakeawayCancellationList = oOrderTakeawayCancellationMapper.selectAll();
        for(OrderTakeawayCancellation oOrderTakeawayCancellation : oOrderTakeawayCancellationList){
            oOrderTakeawayCancellationMapper.delete(oOrderTakeawayCancellation);
        }

    }

    @Autowired
    private OrderTakeawayService oOrderTakeawayService;

    @Autowired
    private FlowTaskService oFlowTaskService;

    @Test
    void allOrder() throws Exception {
        OrderTakeaway oOrderTakeaway = oOrderTakeawayMapper.selectOneById("129860229998354432");
        OrderTakeawayDto oOrderTakeawayDto = new OrderTakeawayDto();
        PoToDTO.poToDto(oOrderTakeaway,oOrderTakeawayDto);

//        //配送员抢单
//        oOrderTakeawayDto.setDeliveryId("102238792552005632");
//        oOrderTakeawayService.robOrderTakeawayToDelivery(oOrderTakeawayDto);

//        //店铺出餐
//        oOrderTakeawayService.diningOrderTakeawayToShop(oOrderTakeawayDto);

//        //配送员取餐
//        oOrderTakeawayService.takeOrderTakeawayToDelivery(oOrderTakeawayDto);
//
//        //配送员确认完成
//        oOrderTakeawayService.okOrderTakeawayToDelivery(oOrderTakeawayDto);

        //用户自动评价
        oShopCommodityEvaluateService.automaticShopCommodityEvaluate(oOrderTakeawayDto.getId());
//
//        //走任务
//        oFlowTaskService.executeFlowTask();

    }

    @Test
    void OSSTest() throws Exception {
        FileInfo fileInfo1 = fileStorageService
                .of(new File("C:\\Users\\78789\\Desktop\\图片\\身份证.png"))
                .setPlatform("aliyun-oss-2")
                .upload();
        System.out.println("1--------------------------------------------");
        System.out.println(fileInfo1.getUrl());
        String url = fileStorageService.generatePresignedUrl(fileInfo1, DateUtil.offsetHour(new Date(), 1));
        System.out.println("2--------------------------------------------");
        System.out.println(url);
//        ComFileDetail oComFileDetail1 = BeanUtil.copyProperties(fileInfo1,ComFileDetail.class,"metadata","userMetadata","thMetadata","thUserMetadata","attr");
//        //这是手动获 元数据 并转成 json 字符串，方便存储在数据库中
//        oComFileDetail1.setMetadata(valueToJson(fileInfo1.getMetadata()));
//        oComFileDetail1.setUserMetadata(valueToJson(fileInfo1.getUserMetadata()));
//        oComFileDetail1.setThMetadata(valueToJson(fileInfo1.getThMetadata()));
//        oComFileDetail1.setThUserMetadata(valueToJson(fileInfo1.getThUserMetadata()));
//        //这是手动获 取附加属性字典 并转成 json 字符串，方便存储在数据库中
//        oComFileDetail1.setAttr(valueToJson(fileInfo1.getAttr()));
//        oComFileDetailMapper.insert(oComFileDetail1);
//
//        FileInfo fileInfo2 = fileStorageService
//                .of(new File("C:\\Users\\78789\\Desktop\\图片\\营业执照.jpg"))
//                .setPlatform("aliyun-oss-1")
//                .upload();
//        System.out.println("2--------------------------------------------");
//        System.out.println(fileInfo2.getUrl());
//        ComFileDetail oComFileDetail2 = BeanUtil.copyProperties(fileInfo2,ComFileDetail.class,"metadata","userMetadata","thMetadata","thUserMetadata","attr");
//        //这是手动获 元数据 并转成 json 字符串，方便存储在数据库中
//        oComFileDetail2.setMetadata(valueToJson(fileInfo2.getMetadata()));
//        oComFileDetail2.setUserMetadata(valueToJson(fileInfo2.getUserMetadata()));
//        oComFileDetail2.setThMetadata(valueToJson(fileInfo2.getThMetadata()));
//        oComFileDetail2.setThUserMetadata(valueToJson(fileInfo2.getThUserMetadata()));
//        //这是手动获 取附加属性字典 并转成 json 字符串，方便存储在数据库中
//        oComFileDetail2.setAttr(valueToJson(fileInfo2.getAttr()));
//        oComFileDetailMapper.insert(oComFileDetail2);

//        FileInfo fileInfo3 = fileStorageService
//                .of(new File("C:\\Users\\78789\\Desktop\\图片\\积分商品\\礼物玩具\\3.jpg"))
//                .setPlatform("aliyun-oss-1")
//                .upload();
//        System.out.println("3--------------------------------------------");
//        System.out.println(fileInfo3.getUrl());
//        ComFileDetail oComFileDetail3 = BeanUtil.copyProperties(fileInfo3,ComFileDetail.class,"metadata","userMetadata","thMetadata","thUserMetadata","attr");
//        //这是手动获 元数据 并转成 json 字符串，方便存储在数据库中
//        oComFileDetail3.setMetadata(valueToJson(fileInfo3.getMetadata()));
//        oComFileDetail3.setUserMetadata(valueToJson(fileInfo3.getUserMetadata()));
//        oComFileDetail3.setThMetadata(valueToJson(fileInfo3.getThMetadata()));
//        oComFileDetail3.setThUserMetadata(valueToJson(fileInfo3.getThUserMetadata()));
//        //这是手动获 取附加属性字典 并转成 json 字符串，方便存储在数据库中
//        oComFileDetail3.setAttr(valueToJson(fileInfo3.getAttr()));
//        oComFileDetailMapper.insert(oComFileDetail3);
//
//        FileInfo fileInfo4 = fileStorageService
//                .of(new File("C:\\Users\\78789\\Desktop\\图片\\积分商品\\礼物玩具\\4.jpg"))
//                .setPlatform("aliyun-oss-1")
//                .upload();
//        System.out.println("4--------------------------------------------");
//        System.out.println(fileInfo4.getUrl());
//        ComFileDetail oComFileDetail4 = BeanUtil.copyProperties(fileInfo4,ComFileDetail.class,"metadata","userMetadata","thMetadata","thUserMetadata","attr");
//        //这是手动获 元数据 并转成 json 字符串，方便存储在数据库中
//        oComFileDetail4.setMetadata(valueToJson(fileInfo4.getMetadata()));
//        oComFileDetail4.setUserMetadata(valueToJson(fileInfo4.getUserMetadata()));
//        oComFileDetail4.setThMetadata(valueToJson(fileInfo4.getThMetadata()));
//        oComFileDetail4.setThUserMetadata(valueToJson(fileInfo4.getThUserMetadata()));
//        //这是手动获 取附加属性字典 并转成 json 字符串，方便存储在数据库中
//        oComFileDetail4.setAttr(valueToJson(fileInfo4.getAttr()));
//        oComFileDetailMapper.insert(oComFileDetail4);
//
//        FileInfo fileInfo5 = fileStorageService
//                .of(new File("C:\\Users\\78789\\Desktop\\图片\\积分商品\\礼物玩具\\5.jpg"))
//                .setPlatform("aliyun-oss-1")
//                .upload();
//        System.out.println("5--------------------------------------------");
//        System.out.println(fileInfo5.getUrl());
//        ComFileDetail oComFileDetail5 = BeanUtil.copyProperties(fileInfo5,ComFileDetail.class,"metadata","userMetadata","thMetadata","thUserMetadata","attr");
//        //这是手动获 元数据 并转成 json 字符串，方便存储在数据库中
//        oComFileDetail5.setMetadata(valueToJson(fileInfo5.getMetadata()));
//        oComFileDetail5.setUserMetadata(valueToJson(fileInfo5.getUserMetadata()));
//        oComFileDetail5.setThMetadata(valueToJson(fileInfo5.getThMetadata()));
//        oComFileDetail5.setThUserMetadata(valueToJson(fileInfo5.getThUserMetadata()));
//        //这是手动获 取附加属性字典 并转成 json 字符串，方便存储在数据库中
//        oComFileDetail5.setAttr(valueToJson(fileInfo5.getAttr()));
//        oComFileDetailMapper.insert(oComFileDetail5);
//
//        FileInfo fileInfo6 = fileStorageService
//                .of(new File("C:\\Users\\78789\\Desktop\\图片\\积分商品\\礼物玩具\\6.jpg"))
//                .setPlatform("aliyun-oss-1")
//                .upload();
//        System.out.println("6--------------------------------------------");
//        System.out.println(fileInfo6.getUrl());
//        ComFileDetail oComFileDetail6 = BeanUtil.copyProperties(fileInfo6,ComFileDetail.class,"metadata","userMetadata","thMetadata","thUserMetadata","attr");
//        //这是手动获 元数据 并转成 json 字符串，方便存储在数据库中
//        oComFileDetail6.setMetadata(valueToJson(fileInfo6.getMetadata()));
//        oComFileDetail6.setUserMetadata(valueToJson(fileInfo6.getUserMetadata()));
//        oComFileDetail6.setThMetadata(valueToJson(fileInfo6.getThMetadata()));
//        oComFileDetail6.setThUserMetadata(valueToJson(fileInfo6.getThUserMetadata()));
//        //这是手动获 取附加属性字典 并转成 json 字符串，方便存储在数据库中
//        oComFileDetail6.setAttr(valueToJson(fileInfo6.getAttr()));
//        oComFileDetailMapper.insert(oComFileDetail6);

    }

    private ObjectMapper objectMapper = new ObjectMapper();

    public String valueToJson(Object value) throws JsonProcessingException {
        if (value == null){
            return null;
        }
        return objectMapper.writeValueAsString(value);
    }

    @Test
    void qaa() throws Exception {
        List<ShopType> oShopTypeList = oShopTypeMapper.selectAll();
        for(ShopType oShopType : oShopTypeList){
            System.out.println(oShopType);
        }
    }

    @Autowired
    private ComVariableMapper oComVariableMapper;

    @Test
    void qaaad() throws Exception {
//        ComVariable oComVariable = new ComVariable();
//        oComVariable.setSystemTitle("speciality_distribution_money");
//        oComVariable.setSystemValues("29.9");
//        oComVariable.setType("充值成为专业分销商金额");
//        oComVariableMapper.insert(oComVariable);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(ComVariableTableDef.COM_VARIABLE.SYSTEM_TITLE.eq("wx_mch_cert_resourceurl"));
        ComVariable oComVariable = oComVariableMapper.selectOneByQuery(queryWrapper);
        System.out.println(oComVariable);
    }

    @Autowired
    private WxMessagesManager wxMessagesManager;

    @Autowired
    private ShopSaveUserMapper oShopSaveUserMapper;

    @Test
    void dada() throws Exception {
//        System.out.println(Md5Util.code("fsm202307").substring(0,20));;
//        System.out.println(Md5Util.code("JoEHt5ItDAf7syMFbranXHKDiieUXoDtV2M8pIQCxmY"));;
//        Map<String,String> map = new HashMap<>();
//        map.put("oOrderTakeawayId","asdasdasdasdas");
//        map.put("oShopName","鸡毛店");
//        wxMessagesManager.payOrderTakeaway("ol0fe6X37Okm4v65MZrQ9YdabA5k",map);
//        ComVariable oComVariable1 = new ComVariable();
//        oComVariable1.setSystemTitle("speciality_distribution_money");
//        oComVariable1.setSystemValues("0.01");
//        oComVariableMapper.update(oComVariable1);
        ComVariable oComVariable2 = new ComVariable();
        oComVariable2.setSystemTitle("min_withdrawal");
        oComVariable2.setSystemValues("0.01");
        oComVariable2.setType("最低提现");
        oComVariableMapper.insert(oComVariable2);
        ComVariable oComVariable3 = new ComVariable();
        oComVariable3.setSystemTitle("withdrawal_fees");
        oComVariable3.setSystemValues("0.006");
        oComVariable3.setType("提现手续费");
        oComVariableMapper.insert(oComVariable3);
//        QueryWrapper queryWrapper = new QueryWrapper();
//        queryWrapper.where(ComVariableTableDef.COM_VARIABLE.SYSTEM_TITLE.eq("evaluateTemplate"));
//        ComVariable oComVariable = oComVariableMapper.selectOneByQuery(queryWrapper);
//        System.out.println(oComVariable);
    }

    @Autowired
    private IntegralExchangeSaveMapper oIntegralExchangeSaveMapper;

    @Test
    void addIntegralExchangeSave() throws Exception {
        IntegralExchangeSave oIntegralExchangeSave = new IntegralExchangeSave();
        oIntegralExchangeSave.setShopId("100692379580727296");
        oIntegralExchangeSave.setIntegral(100);
        oIntegralExchangeSave.setPrice(5.0);
        oIntegralExchangeSaveMapper.insert(oIntegralExchangeSave);
    }

    @Autowired
    private ShopExamineService oShopExamineService;

    @Autowired
    private DeliveryExamineService oDeliveryExamineService;

    @Test
    void shenhe() throws Exception {
        ShopExamine oShopExamine = oShopExamineMapper.selectOneById("144910599519920128");
        ShopExamineDto oShopExamineDto = new ShopExamineDto();
        PoToDTO.poToDto(oShopExamine,oShopExamineDto);
        oShopExamineDto.setIsPass(1);
        oShopExamineService.updateShopExamine(oShopExamineDto);
//        oOrderTakeawayService.wxPayOrderTakeawayCallback("140407616463572992");
//        DeliveryExamine oDeliveryExamine = oDeliveryExamineMapper.selectOneById("140775535919951872");
//        DeliveryExamineDto oDeliveryExamineDto = new DeliveryExamineDto();
//        PoToDTO.poToDto(oDeliveryExamine,oDeliveryExamineDto);
//        oDeliveryExamineDto.setIsPass(1);
//        oDeliveryExamineService.updateDeliveryExamine(oDeliveryExamineDto);

    }

    @Autowired
    private ShopAccountService oShopAccountService;

    @Autowired
    private ShopCommodityTypeMapper oShopCommodityTypeMapper;

    @Test
    void all() throws Exception {
//        //注册店铺账号
//        ShopAccountDto oShopAccountDto = new ShopAccountDto();
//        oShopAccountDto.setPhone("16623911354");
//        oShopAccountService.registerShopAccount(oShopAccountDto);

        FlowTask oFlowTask1 = new FlowTask();
        oFlowTask1.setDeliveryId("102238792552005632");
        oFlowTask1.setState(1);
        oFlowTask1.setType(0);
        oFlowTask1.setMoney(2.0);
        oFlowTask1.setExecuteTime(new Date());
        oFlowTaskMapper.insert(oFlowTask1);
        FlowTask oFlowTask2 = new FlowTask();
        oFlowTask2.setDeliveryId("102238792552005632");
        oFlowTask2.setState(1);
        oFlowTask2.setType(0);
        oFlowTask2.setMoney(2.0);
        oFlowTask2.setExecuteTime(new Date());
        oFlowTaskMapper.insert(oFlowTask2);
        FlowTask oFlowTask3 = new FlowTask();
        oFlowTask3.setDeliveryId("102238792552005632");
        oFlowTask3.setState(1);
        oFlowTask3.setType(0);
        oFlowTask3.setMoney(3.0);
        oFlowTask3.setExecuteTime(new Date());
        oFlowTaskMapper.insert(oFlowTask3);

    }

}
