package com.project.school_flate.entity.com;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.user.UserInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "反馈")
@Table(value = "t_com_feedback")
public class ComFeedback implements Serializable {

    private static final long serialVersionUID = -6770073509593481811L;

    /**
     * 反馈ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "反馈ID")
    private String id;

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    private String userId;

    /**
     * 用户信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "userId",targetField = "id")
    @Schema(title = "用户信息",name = "userInfo",type = "userInfo")
    private UserInfo userInfo;

    /**
     * 图片
     */
    @Schema(description = "图片")
    private String image;

    /**
     * 描述
     */
    @Schema(description = "描述")
    private String describe;

    /**
     * 管理员ID
     */
    @Schema(description = "管理员ID")
    private String adminId;

    /**
     * 管理员信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "adminId",targetField = "id")
    @Schema(title = "管理员信息",name = "comAdmin",type = "comAdmin")
    private ComAdmin comAdmin;

    /**
     * 回复
     */
    @Schema(description = "回复")
    private String reply;

    /**
     * 反馈有无用评分
     */
    @Schema(description = "反馈有用分数")
    private Integer score;

    /**
     * 状态（0：未处理；1：已处理）
     */
    @Column(value = "state",onInsertValue = "0")
    @Schema(description = "状态（0：未处理；1：已处理）")
    private Integer state;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

}
