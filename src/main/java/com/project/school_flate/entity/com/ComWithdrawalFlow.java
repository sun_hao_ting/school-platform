package com.project.school_flate.entity.com;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.delivery.DeliveryInfo;
import com.project.school_flate.entity.shop.ShopInfo;
import com.project.school_flate.entity.user.UserInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "微信转账流水")
@Table(value = "t_com_withdrawal_flow")
public class ComWithdrawalFlow implements Serializable {

    private static final long serialVersionUID = -3592869747849497667L;

    /**
     * 微信转账流水ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "微信转账流水ID")
    private String id;

    /**
     * 类型（0：用户转账；1：配送员转账；2：店铺转账）
     */
    @Schema(description = "类型（0：用户转账；1：配送员转账；2：店铺转账）")
    private Integer type;

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    private String userId;

    /**
     * 客户信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "userId",targetField = "id")
    @Schema(title = "客户信息",name = "userInfo",type = "UserInfo")
    private UserInfo userInfo;

    /**
     * 配送员ID
     */
    @Schema(description = "配送员ID")
    private String deliveryId;

    /**
     * 配送员信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "deliveryId",targetField = "id")
    @Schema(title = "配送员信息",name = "deliveryInfo",type = "DeliveryInfo")
    private DeliveryInfo deliveryInfo;

    /**
     * 店铺ID
     */
    @Schema(description = "店铺ID")
    private String shopId;

    /**
     * 店铺信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "shopId",targetField = "id")
    @Schema(title = "店铺信息",name = "shopInfo",type = "ShopInfo")
    private ShopInfo shopInfo;

    /**
     * 金额
     */
    @Schema(description = "金额")
    private Double total;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 状态（0：待转账；1：已转账；2：转账失败）
     */
    @Column(value = "state",onInsertValue = "0")
    @Schema(description = "状态（0：待转账；1：已转账；2：转账失败）")
    private Integer state;

    /**
     * 审核状态（0：未通过；1：已通过；2：审核中）
     */
    @Column(value = "examine_state",onInsertValue = "2")
    @Schema(description = "审核状态（0：未通过；1：已通过；2：审核中）")
    private Integer examineState;

    /**
     * 管理员ID
     */
    @Schema(description = "管理员ID")
    private String adminId;

    /**
     * 管理员信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "adminId",targetField = "id")
    @Schema(title = "管理员信息",name = "comAdmin",type = "comAdmin")
    private ComAdmin comAdmin;

    /**
     * 理由
     */
    @Schema(description = "理由")
    private String reason;

    /**
     * 审核时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "审核时间")
    private Date auditTime;

    /**
     * 总提现金额
     */
    @Schema(description = "总提现金额")
    private Double allTotal;

    /**
     * 提现手续费
     */
    @Schema(description = "提现手续费")
    private Double withdrawalFees;

}
