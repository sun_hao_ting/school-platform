package com.project.school_flate.entity.com;

import com.mybatisflex.annotation.*;

import java.io.Serializable;

import com.mybatisflex.core.keygen.KeyGenerators;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "系统图")
@Table(value = "t_com_image")
public class ComImage implements Serializable {

    private static final long serialVersionUID = 7747477869388337538L;

    /**
     * 系统图ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "系统图ID")
    private String id;

    /**
     * 图片
     */
    @Schema(description = "图片")
    private String image;

    /**
     * 类型（0：轮播图；1：会员图；2：推广图）
     */
    @Schema(description = "类型（0：轮播图；1：会员图；2：推广图）")
    private Integer type;

    /**
     * 链接
     */
    @Schema(description = "链接")
    private String link;

    /**
     * 链接类型（0：无参页面；1：有参页面；2：tab页；3：视频）
     */
    @Schema(description = "链接类型（0：无参页面；1：有参页面；2：tab页；3：视频）")
    private Integer linkType;

    /**
     * 参数
     */
    @Schema(description = "参数")
    private String parameter;

    /**
     * 颜色
     */
    @Schema(description = "颜色")
    private String color;

}
