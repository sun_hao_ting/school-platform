package com.project.school_flate.entity.com;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import com.project.school_flate.config.tableListener.ComVariableListener;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author sunht
 * @Date 2023/10/2 18:07
 * @Version v1.0.0
 * @Message
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "t_com_variable", onSet = ComVariableListener.class, onInsert = ComVariableListener.class, onUpdate = ComVariableListener.class)
@Tag(name = "系统变量", description = "系统变量实体类")
public class ComVariable implements Serializable {
    private static final long serialVersionUID = -140362668296438386L;

    @Id(value = "system_title")
    @Schema(name = "systemTitle",title = "系统变量名称",type = "String")
    private String systemTitle;

    @Column(value = "system_values")
    @Schema(name = "systemValues",title = "系统变量值",type = "String")
    private String systemValues;

    @Column(value = "type")
    @Schema(name = "type",title = "类型",type = "String")
    private String type;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

}
