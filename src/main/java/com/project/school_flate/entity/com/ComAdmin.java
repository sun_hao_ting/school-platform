package com.project.school_flate.entity.com;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;
import com.mybatisflex.core.keygen.KeyGenerators;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "t_com_admin")
@Tag(name = "管理员", description = "管理员实体类")
public class ComAdmin implements Serializable {

    private static final long serialVersionUID = 6076546330882085021L;

    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(title = "管理员ID",name = "id",type = "String")
    private String id;

    @Column(value = "name")
    @Schema(title = "名称",name = "name",type = "String")
    private String name;

    @Column(value = "login_name")
    @Schema(title = "登录名",name = "loginName",type = "String")
    private String loginName;

    @Column(value = "password")
    @Schema(title = "密码",name = "password",type = "String")
    private String password;

    @Column(value = "phone")
    @Schema(title = "手机号",name = "phone",type = "String")
    private String phone;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(title = "创建时间",name = "createTime",type = "date")
    private Date createTime;

    @Column(value = "state",onInsertValue = "1")
    @Schema(title = "状态（0、冻结；1、存在）",name = "status",type = "int")
    private Integer state;

    /**
     * 角色ID
     */
    @Column(value = "role_id")
    @Schema(title = "角色ID",name = "roleId",type = "String")
    private String roleId;

    /**
     * 角色信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "roleId",targetField = "id")
    @Schema(title = "角色信息",name = "role",type = "Role")
    private ComRole comRole;

    /**
     * 图片
     */
    @Column(value = "image")
    @Schema(description = "图片")
    private String image;

}
