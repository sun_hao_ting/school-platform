package com.project.school_flate.entity.com;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import java.io.Serializable;
import java.math.BigDecimal;

import com.mybatisflex.core.keygen.KeyGenerators;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "分销商等级规则")
@Table(value = "t_com_distribution_rule")
public class ComDistributionRule implements Serializable {

    private static final long serialVersionUID = 2782536631521429077L;

    /**
     * 分销商等级规则ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "分销商等级规则ID")
    private String id;

    /**
     * 等级
     */
    @Schema(description = "等级")
    private Integer grade;

    /**
     * 提成比例
     */
    @Schema(description = "提成比例")
    private Double commission;

    /**
     * 升级收益条件
     */
    @Schema(description = "升级收益条件")
    private Double income;

    /**
     * 类型（0：普通分销商；1：专业分销商）
     */
    @Schema(description = "类型（0：普通分销商；1：专业分销商）")
    private Integer type;

}
