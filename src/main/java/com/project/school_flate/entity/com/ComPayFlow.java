package com.project.school_flate.entity.com;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.keygen.KeyGenerators;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author sunht
 * @Date 2023/9/19 20:53
 * @Version v1.0.0
 * @Message
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "t_com_pay_flow")
public class ComPayFlow implements Serializable {
    private static final long serialVersionUID = -6902781606802397683L;
    /**
     * ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(title = "主键ID",name = "id")
    private String id;

    @Column("type")
    @Schema(title = "类型（0：外卖订单微信支付；1：储蓄充值；2：成为专业分销商；3：外卖订单退款）",name = "type")
    private Integer type;

    @Column("order_id")
    @Schema(title = "订单ID",name = "orderId")
    private String orderId;

    @Column("client_id")
    @Schema(title = "用户ID",name = "clientId")
    private String clientId;

    @Column("delivery_id")
    @Schema(title = "配送员ID",name = "deliveryId")
    private String deliveryId;

    @Column("shop_id")
    @Schema(title = "店铺ID",name = "shopId")
    private String shopId;

    @Column("total")
    @Schema(title = "金额",name = "total")
    private Double total;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

    @Column(value = "state",onInsertValue = "0")
    @Schema(title = "状态（0：待支付；1：已支付；2：支付失败）",name = "state")
    private Integer state;
}
