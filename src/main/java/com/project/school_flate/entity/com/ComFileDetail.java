package com.project.school_flate.entity.com;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.config.tableListener.ComFileDetailListener;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 文件记录表 实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "文件记录表")
@Table(value = "t_com_file_detail",onSet = ComFileDetailListener.class)
public class ComFileDetail implements Serializable {

    private static final long serialVersionUID = 4275934184846640436L;

    /**
     * 文件id
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "文件id")
    private String id;

    /**
     * 文件访问地址
     */
    @Schema(description = "文件访问地址")
    private String url;

    /**
     * 文件大小，单位字节
     */
    @Schema(description = "文件大小，单位字节")
    private Long size;

    /**
     * 文件名称
     */
    @Schema(description = "文件名称")
    private String filename;

    /**
     * 原始文件名
     */
    @Schema(description = "原始文件名")
    private String originalFilename;

    /**
     * 基础存储路径
     */
    @Schema(description = "基础存储路径")
    private String basePath;

    /**
     * 存储路径
     */
    @Schema(description = "存储路径")
    private String path;

    /**
     * 文件扩展名
     */
    @Schema(description = "文件扩展名")
    private String ext;

    /**
     * MIME类型
     */
    @Schema(description = "MIME类型")
    private String contentType;

    /**
     * 存储平台
     */
    @Schema(description = "存储平台")
    private String platform;

    /**
     * 缩略图访问路径
     */
    @Schema(description = "缩略图访问路径")
    private String thUrl;

    /**
     * 缩略图名称
     */
    @Schema(description = "缩略图名称")
    private String thFilename;

    /**
     * 缩略图大小，单位字节
     */
    @Schema(description = "缩略图大小，单位字节")
    private Long thSize;

    /**
     * 缩略图MIME类型
     */
    @Schema(description = "缩略图MIME类型")
    private String thContentType;

    /**
     * 文件所属对象id
     */
    @Schema(description = "文件所属对象id")
    private String objectId;

    /**
     * 文件所属对象类型，例如用户头像，评价图片
     */
    @Schema(description = "文件所属对象类型，例如用户头像，评价图片")
    private String objectType;

    /**
     * 文件元数据
     */
    @Schema(description = "文件元数据")
    private String metadata;

    /**
     * 文件用户元数据
     */
    @Schema(description = "文件用户元数据")
    private String userMetadata;

    /**
     * 缩略图元数据
     */
    @Schema(description = "缩略图元数据")
    private String thMetadata;

    /**
     * 缩略图用户元数据
     */
    @Schema(description = "缩略图用户元数据")
    private String thUserMetadata;

    /**
     * 附加属性
     */
    @Schema(description = "附加属性")
    private String attr;

    /**
     * 文件ACL
     */
    @Schema(description = "文件ACL")
    private String fileAcl;

    /**
     * 缩略图文件ACL
     */
    @Schema(description = "缩略图文件ACL")
    private String thFileAcl;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 生成URL
     */
    @Column(ignore = true)
    @Schema(description = "生成URL")
    private String generateUrl;

}
