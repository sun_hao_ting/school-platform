package com.project.school_flate.entity.com;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.keygen.KeyGenerators;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author sunht
 * @Date 2023/10/7 9:19
 * @Version v1.0.0
 * @Message
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "t_com_role")
public class ComRole implements Serializable {

    private static final long serialVersionUID = -5135591781240122211L;

    /**
     * ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(title = "主键ID",name = "id")
    private String id;

    /**
     * 角色名称
     */
    @Column(value = "name")
    @Schema(title = "角色名称",name = "name")
    private String name;

    /**
     * 角色描述
     */
    @Column(value = "describe")
    @Schema(title = "角色描述",name = "describe")
    private String describe;

    /**
     * 状态（0、不存在；1、存在）
     */
    @Column(value = "state",onInsertValue = "1")
    @Schema(title = "状态（0、不存在；1、存在）",name = "state")
    private Integer state;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

}
