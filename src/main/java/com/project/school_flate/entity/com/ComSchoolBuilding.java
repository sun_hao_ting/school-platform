package com.project.school_flate.entity.com;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.mybatisflex.core.keygen.KeyGenerators;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "校园楼栋")
@Table(value = "t_com_school_building")
public class ComSchoolBuilding implements Serializable {

    private static final long serialVersionUID = 4529662195924810386L;

    /**
     * 校园楼栋ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "校园楼栋ID")
    private String id;

    /**
     * 校园ID
     */
    @Schema(description = "校园ID")
    private String schoolId;

    /**
     * 校园信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "schoolId",targetField = "id")
    @Schema(title = "校园信息",name = "comSchool",type = "ComSchool")
    private ComSchool comSchool;

    /**
     * 名称
     */
    @Schema(description = "名称")
    private String name;

    /**
     * 位置
     */
    @Schema(description = "位置")
    private String address;

    /**
     * 类型（0：女寝；1：男寝；2：混寝）
     */
    @Schema(description = "类型（0：女寝；1：男寝；2：混寝）")
    private Integer type;

    /**
     * 状态（0、不存在；1、存在）
     */
    @Column(value = "state",onInsertValue = "1")
    @Schema(description = "状态（0、不存在；1、存在）")
    private Integer state;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private LocalDateTime createTime;

}
