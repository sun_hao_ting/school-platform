package com.project.school_flate.entity.shop;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.com.ComFileDetail;
import com.project.school_flate.entity.com.ComSchool;
import com.project.school_flate.entity.order.OrderTakeawayCommodity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "店铺")
@Table(value = "t_shop_info")
public class ShopInfo implements Serializable {

    private static final long serialVersionUID = -6289696816421005664L;

    /**
     * 店铺ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "店铺ID")
    private String id;

    /**
     * 名称
     */
    @Schema(description = "名称")
    private String name;

    /**
     * 店铺账号ID
     */
    @Schema(description = "店铺账号ID")
    private String accountId;

    /**
     * 店铺账号信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "accountId",targetField = "id")
    @Schema(title = "店铺账号信息",name = "shopAccount",type = "ShopAccount")
    private ShopAccount shopAccount;

    /**
     * 地址
     */
    @Schema(description = "地址")
    private String address;

    /**
     * 配送范围
     */
    @Schema(description = "配送范围")
    private String deliveryScope;

    /**
     * 营业开始小时
     */
    @JSONField(format = "HH:mm")
    @DateTimeFormat(pattern = "HH:mm")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", timezone = "GMT+8")
    @Schema(description = "营业开始小时")
    private Date tradeBeginHour;

    /**
     * 营业结束小时
     */
    @JSONField(format = "HH:mm")
    @DateTimeFormat(pattern = "HH:mm")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", timezone = "GMT+8")
    @Schema(description = "营业结束小时")
    private Date tradeEndHour;

    /**
     * 是否营业（0：否；1：是）
     */
    @Column(value = "is_trade",onInsertValue = "0")
    @Schema(description = "是否营业（0：否；1：是）")
    private Integer isTrade;

    /**
     * 销量
     */
    @Column(value = "sales_volume",onInsertValue = "0")
    @Schema(description = "销量")
    private Integer salesVolume;

    /**
     * 起价费
     */
    @Schema(description = "起价费")
    private Double startPrice;

    /**
     * 配送费
     */
    @Schema(description = "配送费")
    private Double deliveryPrice;

    /**
     * 主营业务
     */
    @Schema(description = "主营业务")
    private String mainBusiness;

    /**
     * 介绍
     */
    @Schema(description = "介绍")
    private String introduction;

    /**
     * 营业执照
     */
    @Schema(description = "营业执照")
    private String businessLicense;

    /**
     * 经营许可证
     */
    @Schema(description = "经营许可证")
    private String operatePermit;

    /**
     * 余额
     */
    @Column(value = "balance",onInsertValue = "0.00")
    @Schema(description = "余额")
    private Double balance;

    /**
     * 图片
     */
    @Schema(description = "图片")
    private String image;

    /**
     * 入驻校区ID
     */
    @Schema(description = "入驻校区ID")
    private String schoolId;

    /**
     * 入驻校区信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "schoolId",targetField = "id")
    @Schema(title = "入驻校区信息",name = "comSchool",type = "comSchool")
    private ComSchool comSchool;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 状态（0、冻结；1、存在）
     */
    @Column(value = "state",onInsertValue = "1")
    @Schema(description = "状态（0、冻结；1、存在）")
    private Integer state;

    /**
     * 审核状态（0：未通过；1：已通过；2：审核中）
     */
//    @Column(value = "examine_state",onInsertValue = "0")
    @Column(value = "examine_state")
    @Schema(description = "审核状态（0：未通过；1：已通过；2：审核中）")
    private Integer examineState;

    /**
     * 支付提现密码
     */
    @Schema(description = "支付提现密码")
    private String payPassword;

    /**
     * 姓名
     */
    @Schema(description = "姓名")
    private String fullName;

    /**
     * 身份证号
     */
    @Schema(description = "身份证号")
    private String idCard;

    /**
     * 身份证正面
     */
    @Schema(description = "身份证正面")
    private String idFront;

    /**
     * 身份证反面
     */
    @Schema(description = "身份证反面")
    private String idOpposite;

    /**
     * 提现总额
     */
    @Column(value = "withdrawal_total",onInsertValue = "0")
    @Schema(description = "提现总额")
    private Double withdrawalTotal;

    /**
     * 店铺类型ID
     */
    @Schema(description = "店铺类型ID")
    private String shopTypeId;

    /**
     * 店铺类型信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "shopTypeId",targetField = "id")
    @Schema(title = "店铺类型信息",name = "shopType",type = "ShopType")
    private ShopType shopType;

    /**
     * 评分
     */
    @Column(value = "score",onInsertValue = "0")
    @Schema(description = "评分")
    private Double score;

    /**
     * 评价模板
     */
    @Schema(description = "评价模板")
    private String evaluateTemplate;

    /**
     * 手机号
     */
    @Schema(description = "手机号")
    private String phone;

    /**
     * 公告
     */
    @Schema(description = "公告")
    private String notice;

    /**
     * 是否完善信息（0：否；1：是）
     */
    @Column(value = "is_perfect",onInsertValue = "0")
    @Schema(description = "是否完善信息（0：否；1：是）")
    private Integer isPerfect;

    /**
     * LOGO
     */
    @Schema(description = "LOGO")
    private String logoImage;

    /**
     * 是否推荐（0：否；1：是）
     */
    @Column(value = "is_recommend",onInsertValue = "0")
    @Schema(description = "是否推荐（0：否；1：是）")
    private Integer isRecommend;

    /**
     * 是否开通储蓄（0：否；1：是）
     */
    @Column(value = "is_open_save",onInsertValue = "0")
    @Schema(description = "是否开通储蓄（0：否；1：是）")
    private Integer isOpenSave;

    /**
     * 储蓄审核状态（0：未通过；1：已通过；2：审核中：3：未提交）
     */
    @Column(value = "save_examine_state",onInsertValue = "3")
    @Schema(description = "储蓄审核状态（0：未通过；1：已通过；2：审核中：3：未提交）")
    private Integer saveExamineState;

    /**
     * 是否开启自动出餐（0：否；1：是）
     */
    @Column(value = "is_dining",onInsertValue = "0")
    @Schema(description = "是否开启自动出餐（0：否；1：是）")
    private Integer isDining;

    /**
     * 店铺推荐商品列表
     */
    @Column(ignore = true)
    @RelationOneToMany(selfField = "id",targetField = "shopId", orderBy = "no asc", extraCondition = "is_shop_recommend = 1")
    @Schema(description = "店铺推荐商品列表")
    private List<ShopCommodity> recommendShopCommodityList;

    /**
     * 出餐时间
     */
    @Schema(description = "出餐时间")
    private Integer diningTime;

    /**
     * 打包费
     */
    @Schema(description = "打包费")
    private Double packingCharge;

    /**
     * 店铺规则列表
     */
    @Column(ignore = true)
    @RelationOneToMany(selfField = "id",targetField = "shopId")
    @Schema(description = "店铺规则列表")
    private List<ShopRule> shopRuleList;

    /**
     * 评价次数
     */
    @Column(value = "evaluate_num",onInsertValue = "0")
    @Schema(description = "评价次数")
    private Integer evaluateNum;

    /**
     * 提现次数
     */
    @Column(value = "withdrawal_num",onInsertValue = "0")
    @Schema(description = "提现次数")
    private Integer withdrawalNum;

}
