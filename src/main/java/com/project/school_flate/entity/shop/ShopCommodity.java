package com.project.school_flate.entity.shop;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.com.ComFileDetail;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "商品")
@Table(value = "t_shop_commodity")
public class ShopCommodity implements Serializable {

    private static final long serialVersionUID = 6013968965909763035L;

    /**
     * 商品ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "商品ID")
    private String id;

    /**
     * 名称
     */
    @Schema(description = "名称")
    private String name;

    /**
     * 店铺ID
     */
    @Schema(description = "店铺ID")
    private String shopId;

    /**
     * 店铺信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "shopId",targetField = "id")
    @Schema(title = "店铺信息",name = "shopInfo",type = "shopInfo")
    private ShopInfo shopInfo;

    /**
     * 商品分类ID
     */
    @Schema(description = "商品分类ID")
    private String commodityTypeId;

    /**
     * 状态（0：上架；1：下架；2：售完）
     */
    @Schema(description = "状态（0：上架；1：下架；2：售完）")
    private Integer state;

    /**
     * 销量
     */
    @Column(value = "sales_volume",onInsertValue = "0")
    @Schema(description = "销量")
    private Integer salesVolume;

    /**
     * 原价
     */
    @Schema(description = "原价")
    private Double originalPrice;

    /**
     * 会员价
     */
    @Schema(description = "会员价")
    private Double memberPrice;

    /**
     * 图片
     */
    @Schema(description = "图片")
    private String image;

    /**
     * 海报
     */
    @Schema(description = "海报")
    private String poster;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 评分
     */
    @Column(value = "score",onInsertValue = "0")
    @Schema(description = "评分")
    private Double score;

    /**
     * 评价模板
     */
    @Schema(description = "评价模板")
    private String evaluateTemplate;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Integer no;

    /**
     * 商品规格类型列表
     */
    @Column(ignore = true)
    @RelationOneToMany(selfField = "id",targetField = "commodityId", orderBy = "no asc")
    @Schema(description = "商品规格类型列表")
    private List<ShopCommoditySpecsType> shopCommoditySpecsTypeList;

    /**
     * 是否商家推荐（0：否；1：是）
     */
    @Schema(description = "是否商家推荐（0：否；1：是）")
    private Integer isShopRecommend;

    /**
     * 是否平台推荐（0：否；1：是）
     */
    @Column(value = "is_system_recommend",onInsertValue = "0")
    @Schema(description = "是否平台推荐（0：否；1：是）")
    private Integer isSystemRecommend;

    /**
     * 储蓄折扣比例
     */
    @Schema(description = "储蓄折扣比例")
    private Double saveDiscountRatio;

    /**
     * 是否参与储蓄折扣（0：否；1：是）
     */
    @Schema(description = "是否参与储蓄折扣（0：否；1：是）")
    private Integer isInSave;

    /**
     * 平台推荐价格
     */
    @Column(value = "system_recommend_price",onInsertValue = "0")
    @Schema(description = "平台推荐价格")
    private Double systemRecommendPrice;

    /**
     * 评价次数
     */
    @Column(value = "evaluate_num",onInsertValue = "0")
    @Schema(description = "评价次数")
    private Integer evaluateNum;

}
