package com.project.school_flate.entity.shop;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import java.io.Serializable;
import java.math.BigDecimal;

import com.mybatisflex.core.keygen.KeyGenerators;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "店铺规则")
@Table(value = "t_shop_rule")
public class ShopRule implements Serializable {

    private static final long serialVersionUID = 5016706081615991118L;

    /**
     * 店铺规则ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "店铺规则ID")
    private String id;

    /**
     * 店铺ID
     */
    @Schema(description = "店铺ID")
    private String shopId;

    /**
     * 类型（0：首单优惠；1：满减；2：储蓄）
     */
    @Schema(description = "类型（0：首单优惠；1：满减；2：储蓄）")
    private Integer type;

    /**
     * 达到金额
     */
    @Schema(description = "达到金额")
    private Double reachPrice;

    /**
     * 赠送金额
     */
    @Schema(description = "赠送金额")
    private Double givePrice;

}
