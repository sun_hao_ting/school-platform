package com.project.school_flate.entity.shop;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import com.mybatisflex.core.keygen.KeyGenerators;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "商品规格")
@Table(value = "t_shop_commodity_specs")
public class ShopCommoditySpecs implements Serializable {

    private static final long serialVersionUID = 5319866890462519777L;

    /**
     * 商品规格ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "商品规格ID")
    private String id;

    /**
     * 名称
     */
    @Schema(description = "名称")
    private String name;

    /**
     * 商品规格类型ID
     */
    @Schema(description = "商品规格类型ID")
    private String commoditySpecsTypeId;

    /**
     * 原价
     */
    @Schema(description = "原价")
    private Double originalPrice;

    /**
     * 会员价
     */
    @Schema(description = "会员价")
    private Double memberPrice;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 商品ID
     */
    @Schema(description = "商品ID")
    private String commodityId;

    /**
     * 是否还有库存（0：没有；1：有）
     */
    @Schema(description = "是否还有库存（0：没有；1：有）")
    private Integer isInventory;

    /**
     * 储蓄折扣比例
     */
    @Schema(description = "储蓄折扣比例")
    private Double saveDiscountRatio;

    /**
     * 是否参与储蓄折扣（0：否；1：是）
     */
    @Schema(description = "是否参与储蓄折扣（0：否；1：是）")
    private Integer isInSave;

}
