package com.project.school_flate.entity.shop;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.order.OrderTakeawayCommodity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "商品规格类型")
@Table(value = "t_shop_commodity_specs_type")
public class ShopCommoditySpecsType implements Serializable {

    private static final long serialVersionUID = -450955618111561788L;

    /**
     * 商品规格类型ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "商品规格类型ID")
    private String id;

    /**
     * 商品ID
     */
    @Schema(description = "商品ID")
    private String commodityId;

    /**
     * 名称
     */
    @Schema(description = "名称")
    private String name;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Integer no;

    /**
     * 商品规格列表
     */
    @Column(ignore = true)
    @RelationOneToMany(selfField = "id",targetField = "commoditySpecsTypeId")
    @Schema(description = "商品规格列表")
    private List<ShopCommoditySpecs> shopCommoditySpecsList;

}