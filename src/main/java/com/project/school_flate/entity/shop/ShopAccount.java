package com.project.school_flate.entity.shop;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.com.ComFileDetail;
import com.project.school_flate.entity.order.OrderIntegralCommodity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "店铺账号")
@Table(value = "t_shop_account")
public class ShopAccount implements Serializable {

    private static final long serialVersionUID = -3364453858051236089L;

    /**
     * 店铺账号ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "店铺账号ID")
    private String id;

    /**
     * 手机号
     */
    @Schema(description = "手机号")
    private String phone;

    /**
     * 密码
     */
    @Schema(description = "密码")
    private String password;

    @Column(value = "state",onInsertValue = "1")
    @Schema(title = "状态（0、冻结；1、存在）",name = "status",type = "int")
    private Integer state;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 店铺列表
     */
    @Column(ignore = true)
    @RelationOneToMany(selfField = "id",targetField = "accountId")
    @Schema(description = "店铺列表")
    private List<ShopInfo> shopInfoList;

    /**
     * 图片
     */
//    @Column(value = "image",onInsertValue = "'https://school-platform-public.oss-cn-chengdu.aliyuncs.com/65fc6357ad578537e8c8a74a.jpg'")
    @Column(value = "image")
    @Schema(description = "图片")
    private String image;

    /**
     * 名称
     */
    @Schema(description = "名称")
    private String name;

    /**
     * 微信optionid
     */
    @Schema(description = "微信optionid")
    private String wxOptionId;

    /**
     * 微信通用ID
     */
    @Schema(description = "微信通用ID")
    private String unionId;

}
