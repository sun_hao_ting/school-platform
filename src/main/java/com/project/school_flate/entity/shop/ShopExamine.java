package com.project.school_flate.entity.shop;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.com.ComAdmin;
import com.project.school_flate.entity.com.ComSchool;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "店铺审核")
@Table(value = "t_shop_examine")
public class ShopExamine implements Serializable {

    private static final long serialVersionUID = -5531298413540388526L;

    /**
     * 店铺审核ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "店铺审核ID")
    private String id;

    /**
     * 店铺ID
     */
    @Schema(description = "店铺ID")
    private String shopId;

    /**
     * 店铺信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "shopId",targetField = "id")
    @Schema(title = "店铺信息",name = "shopInfo",type = "shopInfo")
    private ShopInfo shopInfo;

    /**
     * 管理员ID
     */
    @Schema(description = "管理员ID")
    private String adminId;

    /**
     * 管理员信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "adminId",targetField = "id")
    @Schema(title = "管理员信息",name = "comAdmin",type = "comAdmin")
    private ComAdmin comAdmin;

    /**
     * 是否通过（0：未通过；1：已通过；2：审核中）
     */
    @Column(value = "is_pass",onInsertValue = "2")
    @Schema(description = "是否通过（0：未通过；1：已通过；2：审核中）")
    private Integer isPass;

    /**
     * 理由
     */
    @Schema(description = "理由")
    private String reason;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 审核时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "审核时间")
    private Date auditTime;

    /**
     * 名称
     */
    @Schema(description = "名称")
    private String name;

    /**
     * 店铺账号ID
     */
    @Schema(description = "店铺账号ID")
    private String accountId;

    /**
     * 店铺账号信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "accountId",targetField = "id")
    @Schema(title = "店铺账号信息",name = "shopAccount",type = "ShopAccount")
    private ShopAccount shopAccount;

    /**
     * 地址
     */
    @Schema(description = "地址")
    private String address;

    /**
     * 主营业务
     */
    @Schema(description = "主营业务")
    private String mainBusiness;

    /**
     * 营业执照
     */
    @Schema(description = "营业执照")
    private String businessLicense;

    /**
     * 经营许可证
     */
    @Schema(description = "经营许可证")
    private String operatePermit;

    /**
     * 入驻校区ID
     */
    @Schema(description = "入驻校区ID")
    private String schoolId;

    /**
     * 入驻校区信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "schoolId",targetField = "id")
    @Schema(title = "入驻校区信息",name = "comSchool",type = "comSchool")
    private ComSchool comSchool;

    /**
     * 姓名
     */
    @Schema(description = "姓名")
    private String fullName;

    /**
     * 身份证号
     */
    @Schema(description = "身份证号")
    private String idCard;

    /**
     * 身份证正面
     */
    @Schema(description = "身份证正面")
    private String idFront;

    /**
     * 身份证反面
     */
    @Schema(description = "身份证反面")
    private String idOpposite;

    /**
     * 手机号
     */
    @Schema(description = "手机号")
    private String phone;

}
