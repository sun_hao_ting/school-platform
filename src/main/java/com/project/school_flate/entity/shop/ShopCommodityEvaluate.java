package com.project.school_flate.entity.shop;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.com.ComFileDetail;
import com.project.school_flate.entity.user.UserInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "商品评价")
@Table(value = "t_shop_commodity_evaluate")
public class ShopCommodityEvaluate implements Serializable {

    private static final long serialVersionUID = 3094323327055314649L;

    /**
     * 商品评价ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "商品评价ID")
    private String id;

    /**
     * 商品ID
     */
    @Schema(description = "商品ID")
    private String commodityId;

    /**
     * 商品信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "commodityId",targetField = "id")
    @Schema(title = "商品信息",name = "shopCommodity",type = "shopCommodity")
    private ShopCommodity shopCommodity;

    /**
     * 服务态度评分
     */
    @Schema(description = "服务态度评分")
    private Double serviceRating;

    /**
     * 产品质量评分
     */
    @Schema(description = "产品质量评分")
    private Double qualityRating;

    /**
     * 送达速度评分
     */
    @Schema(description = "送达速度评分")
    private Double speedRating;

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    private String userId;

    /**
     * 用户信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "userId",targetField = "id")
    @Schema(title = "用户信息",name = "userInfo",type = "userInfo")
    private UserInfo userInfo;

    /**
     * 内容
     */
    @Schema(description = "内容")
    private String content;

    /**
     * 图片
     */
    @Schema(description = "图片")
    private String image;

    /**
     * 是否匿名（0：否；1：是）
     */
    @Schema(description = "是否匿名（0：否；1：是）")
    private Integer isAnonymous;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 综合评分
     */
    @Schema(description = "综合评分")
    private Double allRating;

    /**
     * 店铺ID
     */
    @Schema(description = "综合评分")
    private String shopId;

    /**
     * 评价模板
     */
    @Schema(description = "评价模板")
    private String evaluateTemplate;

}
