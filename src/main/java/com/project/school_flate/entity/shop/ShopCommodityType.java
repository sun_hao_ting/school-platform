package com.project.school_flate.entity.shop;

import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.util.List;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.order.OrderTakeawayCommodity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "商品分类")
@Table(value = "t_shop_commodity_type")
public class ShopCommodityType implements Serializable {

    private static final long serialVersionUID = 5099810782047671996L;

    /**
     * 商品分类ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "商品分类ID")
    private String id;

    /**
     * 名称
     */
    @Schema(description = "名称")
    private String name;

    /**
     * 店铺ID
     */
    @Schema(description = "店铺ID")
    private String shopId;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Integer no;

    /**
     * 商品列表
     */
    @Column(ignore = true)
    @RelationOneToMany(selfField = "id",targetField = "commodityTypeId")
    @Schema(description = "商品列表")
    private List<ShopCommodity> shopCommodities;

}
