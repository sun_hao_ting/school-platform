package com.project.school_flate.entity.shop;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.user.UserInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "店铺用户储蓄")
@Table(value = "t_shop_save_user")
public class ShopSaveUser implements Serializable {

    private static final long serialVersionUID = -8691863760121439993L;

    /**
     * 店铺用户储蓄ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "店铺用户储蓄ID")
    private String id;

    /**
     * 店铺ID
     */
    @Schema(description = "店铺ID")
    private String shopId;

    /**
     * 店铺信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "shopId",targetField = "id")
    @Schema(title = "店铺信息",name = "shopInfo",type = "shopInfo")
    private ShopInfo shopInfo;

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    private String userId;

    /**
     * 用户信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "userId",targetField = "id")
    @Schema(title = "用户信息",name = "userInfo",type = "userInfo")
    private UserInfo userInfo;

    /**
     * 用户余额
     */
    @Schema(description = "用户余额")
    private Double userBalance;

    /**
     * 充值金额
     */
    @Schema(description = "充值金额")
    private Double rechargePrice;

    /**
     * 赠送金额
     */
    @Schema(description = "赠送金额")
    private Double giftsPrice;

    /**
     * 折扣比例
     */
    @Schema(description = "折扣比例")
    private Double discountRatio;

    /**
     * 是否全部花完（0：否；1：是）
     */
    @Column(value = "is_spend",onInsertValue = "0")
    @Schema(description = "是否全部花完（0：否；1：是）")
    private Integer isSpend;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 总额
     */
    @Column(ignore = true)
    @Schema(description = "总额")
    private Double allPrice;

}
