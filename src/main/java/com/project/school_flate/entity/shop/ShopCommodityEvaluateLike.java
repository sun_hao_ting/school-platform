package com.project.school_flate.entity.shop;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.user.UserInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "商品评论点赞")
@Table(value = "t_shop_commodity_evaluate_like")
public class ShopCommodityEvaluateLike implements Serializable {

    private static final long serialVersionUID = -7563568554809373596L;

    /**
     * 商品评论点赞ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "商品评论点赞ID")
    private String id;

    /**
     * 商品ID
     */
    @Schema(description = "商品ID")
    private String commodityId;

    /**
     * 商品信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "commodityId",targetField = "id")
    @Schema(title = "商品信息",name = "shopCommodity",type = "shopCommodity")
    private ShopCommodity shopCommodity;

    /**
     * 商品评论ID
     */
    @Schema(description = "商品评论ID")
    private String evaluateId;

    /**
     * 商品评论信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "evaluateId",targetField = "id")
    @Schema(title = "商品评论信息",name = "shopCommodityEvaluate",type = "shopCommodityEvaluate")
    private ShopCommodityEvaluate shopCommodityEvaluate;

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    private String userId;

    /**
     * 用户信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "userId",targetField = "id")
    @Schema(title = "用户信息",name = "userInfo",type = "userInfo")
    private UserInfo userInfo;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(title = "创建时间",name = "createTime",type = "date")
    private Date createTime;

}
