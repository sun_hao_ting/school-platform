package com.project.school_flate.entity.order;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.com.ComSchool;
import com.project.school_flate.entity.com.ComSchoolBuilding;
import com.project.school_flate.entity.shop.ShopInfo;
import com.project.school_flate.entity.user.UserInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "外卖订单")
@Table(value = "t_order_takeaway")
public class OrderTakeaway implements Serializable {

    private static final long serialVersionUID = 7676737234160086703L;

    /**
     * 外卖订单ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "外卖订单ID")
    private String id;

    /**
     * 叫号
     */
    @Schema(description = "叫号")
    private Integer callNumber;

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    private String userId;

    /**
     * 用户信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "userId",targetField = "id")
    @Schema(title = "用户信息",name = "userInfo",type = "userInfo")
    private UserInfo userInfo;

    /**
     * 价格
     */
    @Schema(description = "价格")
    private Double price;

    /**
     * 接单时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "接单时间")
    private Date connectTime;

    /**
     * 外卖订单配送列表
     */
    @Column(ignore = true)
    @RelationOneToMany(selfField = "id",targetField = "orderTakeawayId")
    @Schema(description = "外卖订单配送列表")
    private List<OrderDelivery> orderDeliveryList;

    /**
     * 配送费
     */
    @Schema(description = "配送费")
    private Double deliveryPrice;

    /**
     * 配送地址
     */
    @Schema(description = "配送地址ID")
    private String userAddress;

    /**
     * 支付方式（0：微信支付；1：储蓄支付；2：支付宝支付）
     */
    @Schema(description = "支付方式（0：微信支付；1：储蓄支付；2：支付宝支付）")
    private Integer paymentMethod;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 校区ID
     */
    @Schema(description = "校区ID")
    private String schoolId;

    /**
     * 校区信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "schoolId",targetField = "id")
    @Schema(title = "校区信息",name = "comSchool",type = "ComSchool")
    private ComSchool comSchool;

    /**
     * 校区楼栋ID
     */
    @Schema(description = "校区楼栋ID")
    private String schoolBuildingId;

    /**
     * 校区楼栋信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "schoolBuildingId",targetField = "id")
    @Schema(title = "校区楼栋信息",name = "comSchoolBuilding",type = "ComSchoolBuilding")
    private ComSchoolBuilding comSchoolBuilding;

    /**
     * 手机号
     */
    @Schema(description = "手机号")
    private String phone;

    /**
     * 联系人
     */
    @Schema(description = "联系人")
    private String contacts;

    /**
     * 店铺ID
     */
    @Schema(description = "店铺ID")
    private String shopId;

    /**
     * 店铺信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "shopId",targetField = "id")
    @Schema(title = "店铺信息",name = "shopInfo",type = "ShopInfo")
    private ShopInfo shopInfo;

    /**
     * 支付状态（0、待支付；1、已支付；2、已完成；3、取消订单；4、取消完成；5、申请投诉；6、投诉完成）
     */
    @Column(value = "pay_state",onInsertValue = "0")
    @Schema(description = "支付状态（0、待支付；1、已支付；2、已完成；3、取消订单；4、取消完成；5、申请投诉；6、投诉完成）")
    private Integer payState;

    /**
     * 配送状态（0、未配送；1：已接单【取餐中】；2：已取餐【配送中】；3、已送达）
     */
    @Column(value = "delivery_state",onInsertValue = "0")
    @Schema(description = "配送状态（0、未配送；1：已接单【取餐中】；2：已取餐【配送中】；3、已送达）")
    private Integer deliveryState;

    /**
     * 出餐状态（0、出餐中；1、已出餐；2、已完成；3、取消订单；4、取消完成）
     */
    @Column(value = "shop_state",onInsertValue = "0")
    @Schema(description = "出餐状态（0、出餐中；1、已出餐；2、已完成；3、取消订单；4、取消完成）")
    private Integer shopState;

    /**
     * 支付时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "支付时间")
    private Date payTime;

    /**
     * 外卖订单商品列表
     */
    @Column(ignore = true)
    @RelationOneToMany(selfField = "id",targetField = "orderTakeawayId")
    @Schema(description = "外卖订单商品列表")
    private List<OrderTakeawayCommodity> orderTakeawayCommodityList;

    /**
     * 备注
     */
    @Schema(description = "备注")
    private String notes;

    /**
     * 客户确认时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "客户确认时间")
    private Date okTime;

    /**
     * 打包费
     */
    @Schema(description = "打包费")
    private Double packingCharge;

    /**
     * 折扣详情
     */
    @Schema(description = "折扣详情")
    private String discountDetails;

    /**
     * 出餐时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "出餐时间")
    private Date diningTime;

    /**
     * 是否评价（0：未评价；1：已评价）
     */
    @Column(value = "is_evaluate",onInsertValue = "0")
    @Schema(description = "是否评价（0：未评价；1：已评价）")
    private Integer isEvaluate;

    /**
     * 节省金额
     */
    @Column(value = "savings_price",onInsertValue = "0")
    @Schema(description = "节省金额")
    private Double savingsPrice;

    /**
     * 配送方式（0：多人；1：单人）
     */
    @Schema(description = "配送方式（0：多人；1：单人）")
    private Integer deliveryType;

    /**
     * 是否中转（0：未中转；1：已中转）
     */
    @Column(value = "is_transfer",onInsertValue = "0")
    @Schema(description = "是否中转（0：未中转；1：已中转）")
    private Integer isTransfer;

    /**
     * 平台抽取比例
     */
    @Schema(description = "存储金额比例过程")
    private String comDraw;

    /**
     * 取餐时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "取餐时间")
    private Date pickingTime;

    /**
     * 送达时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "送达时间")
    private Date reachTime;

}
