package com.project.school_flate.entity.order;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.keygen.KeyGenerators;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "订单储蓄支付缓存")
@Table(value = "t_order_takeaway_save_cache")
public class OrderTakeawaySaveCache implements Serializable {

    private static final long serialVersionUID = 8516944280777692640L;

    /**
     * 订单储蓄支付缓存ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "订单储蓄支付缓存ID")
    private String id;

    /**
     * 订单ID
     */
    @Schema(description = "外卖订单ID")
    private String orderTakeawayId;

    /**
     * 店铺用户储蓄ID
     */
    @Schema(description = "店铺用户储蓄ID")
    private String shopSaveUserId;

    /**
     * 用户消费价格
     */
    @Schema(description = "用户消费价格")
    private Double userPrice;

}
