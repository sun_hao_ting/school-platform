package com.project.school_flate.entity.order;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.com.ComSchool;
import com.project.school_flate.entity.shop.ShopInfo;
import com.project.school_flate.entity.user.UserInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "订单取消")
@Table(value = "t_order_takeaway_cancellation")
public class OrderTakeawayCancellation implements Serializable {

    private static final long serialVersionUID = 5249080577891433694L;

    /**
     * 订单取消ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "订单取消ID")
    private String id;

    /**
     * 订单ID
     */
    @Schema(description = "订单ID")
    private String orderTakeawayId;

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    private String userId;

    /**
     * 用户信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "userId",targetField = "id")
    @Schema(title = "用户信息",name = "userInfo",type = "UserInfo")
    private UserInfo userInfo;

    /**
     * 店铺ID
     */
    @Schema(description = "店铺ID")
    private String shopId;

    /**
     * 店铺信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "shopId",targetField = "id")
    @Schema(title = "店铺信息",name = "shopInfo",type = "ShopInfo")
    private ShopInfo shopInfo;

    /**
     * 理由
     */
    @Schema(description = "理由")
    private String reason;

    /**
     * 理由模板
     */
    @Schema(description = "理由模板")
    private String template;

    /**
     * 图片
     */
    @Schema(description = "图片")
    private String image;

    /**
     * 类型（0：用户；1：店铺）
     */
    @Schema(description = "类型（0：用户；1：店铺）")
    private Integer type;

    /**
     * 状态（0：审核中；1：通过；2：驳回）
     */
    @Column(value = "state",onInsertValue = "0")
    @Schema(description = "状态（0：审核中；1：通过；2：驳回）")
    private Integer state;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

}
