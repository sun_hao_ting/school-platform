package com.project.school_flate.entity.order;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.delivery.DeliveryInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "外卖订单配送")
@Table(value = "t_order_delivery")
public class OrderDelivery implements Serializable {

    private static final long serialVersionUID = 2122931313516498647L;

    /**
     * 外卖订单配送ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "外卖订单配送ID")
    private String id;

    /**
     * 订单ID
     */
    @Schema(description = "订单ID")
    private String orderTakeawayId;

    /**
     * 配送人ID
     */
    @Schema(description = "配送人ID")
    private String deliveryId;

    /**
     * 配送人信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "deliveryId",targetField = "id")
    @Schema(title = "配送人信息",name = "deliveryInfo",type = "DeliveryInfo")
    private DeliveryInfo deliveryInfo;

    /**
     * 配送状态（0：取餐中；1：配送中；2、已送达）
     */
    @Schema(description = "配送状态（0：取餐中；1：配送中；2、已送达）")
    private Integer deliveryState;

    /**
     * 取餐时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "取餐时间")
    private Date pickingTime;

    /**
     * 送达时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "送达时间")
    private Date reachTime;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

}
