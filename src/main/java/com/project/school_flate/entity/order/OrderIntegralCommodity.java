package com.project.school_flate.entity.order;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.integral.IntegralCommodity;
import com.project.school_flate.entity.user.UserInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "积分订单商品")
@Table(value = "t_order_integral_commodity")
public class OrderIntegralCommodity implements Serializable {

    /**
     * 积分订单商品ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "积分订单商品ID")
    private String id;

    /**
     * 订单ID
     */
    @Schema(description = "订单ID")
    private String orderTakeawayId;

//    /**
//     * 订单信息
//     */
//    @Column(ignore = true)
//    @RelationOneToOne(selfField = "orderTakeawayId",targetField = "id")
//    @Schema(title = "订单信息",name = "orderTakeaway",type = "OrderTakeaway")
//    private OrderTakeaway orderTakeaway;

    /**
     * 商品ID
     */
    @Schema(description = "商品ID")
    private String commodityId;

    /**
     * 商品信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "commodityId",targetField = "id")
    @Schema(title = "商品信息",name = "integralCommodity",type = "IntegralCommodity")
    private IntegralCommodity integralCommodity;

    /**
     * 积分
     */
    @Schema(description = "积分")
    private String integral;

    /**
     * 数量
     */
    @Schema(description = "数量")
    private Integer number;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

}
