package com.project.school_flate.entity.delivery;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.com.ComAdmin;
import com.project.school_flate.entity.com.ComFileDetail;
import com.project.school_flate.entity.com.ComSchool;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "配送员审核")
@Table(value = "t_delivery_examine")
public class DeliveryExamine implements Serializable {

    private static final long serialVersionUID = 7356363390639417787L;

    /**
     * 配送员审核ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "配送员审核ID")
    private String id;

    /**
     * 配送员ID
     */
    @Schema(description = "配送员ID")
    private String deliveryId;

    /**
     * 配送员信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "deliveryId",targetField = "id")
    @Schema(title = "配送员信息",name = "deliveryInfo",type = "deliveryInfo")
    private DeliveryInfo deliveryInfo;

    /**
     * 管理员ID
     */
    @Schema(description = "管理员ID")
    private String adminId;

    /**
     * 管理员信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "adminId",targetField = "id")
    @Schema(title = "管理员信息",name = "comAdmin",type = "comAdmin")
    private ComAdmin comAdmin;

    /**
     * 是否通过（0：未通过；1：已通过；2：审核中）
     */
//    @Column(value = "is_pass",onInsertValue = "0")
    @Column(value = "is_pass")
    @Schema(description = "是否通过（0：未通过；1：已通过；2：审核中）")
    private Integer isPass;

    /**
     * 理由
     */
    @Schema(description = "理由")
    private String reason;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 审核时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "审核时间")
    private Date auditTime;

    /**
     * 姓名
     */
    @Schema(description = "姓名")
    private String fullName;

    /**
     * 手机号
     */
    @Schema(description = "手机号")
    private String phone;

    /**
     * 性别
     */
    @Schema(description = "性别")
    private Integer sex;

    /**
     * 身份证号
     */
    @Schema(description = "身份证号")
    private String idCard;

    /**
     * 身份证正面
     */
    @Schema(description = "身份证正面")
    private String idFront;

    /**
     * 身份证反面
     */
    @Schema(description = "身份证反面")
    private String idOpposite;

    /**
     * 学生证
     */
    @Schema(description = "学生证")
    private String idStudent;

    /**
     * 入驻校区ID
     */
    @Schema(description = "入驻校区ID")
    private String schoolId;

    /**
     * 入驻校区信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "schoolId",targetField = "id")
    @Schema(title = "入驻校区信息",name = "comSchool",type = "comSchool")
    private ComSchool comSchool;

    /**
     * 工作类型（0：兼职；1：全职）
     */
    @Schema(description = "工作类型（0：兼职；1：全职）")
    private Integer workType;

}
