package com.project.school_flate.entity.delivery;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import com.mybatisflex.core.keygen.KeyGenerators;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "配送员评价")
@Table(value = "t_delivery_evaluate")
public class DeliveryEvaluate implements Serializable {

    private static final long serialVersionUID = -2483353300871175294L;

    /**
     * 配送员评价ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "配送员评价ID")
    private String id;

    /**
     * 配送员ID
     */
    @Schema(description = "配送员ID")
    private String deliveryId;

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    private String userId;

    /**
     * 订单ID
     */
    @Schema(description = "订单ID")
    private String orderTakeawayId;

    /**
     * 综合评分
     */
    @Schema(description = "综合评分")
    private BigDecimal allRating;

    /**
     * 内容
     */
    @Schema(description = "内容")
    private String content;

    /**
     * 图片
     */
    @Schema(description = "图片")
    private String image;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

}
