package com.project.school_flate.entity.delivery;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.com.ComFileDetail;
import com.project.school_flate.entity.com.ComSchool;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "配送员")
@Table(value = "t_delivery_info")
public class DeliveryInfo implements Serializable {

    private static final long serialVersionUID = 8260658464192848554L;

    /**
     * 配送员ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "配送员ID")
    private String id;

    /**
     * 名称
     */
    @Schema(description = "名称")
    private String name;

    /**
     * 手机号
     */
    @Schema(description = "手机号")
    private String phone;

    /**
     * 密码
     */
    @Schema(description = "密码")
    private String password;

    /**
     * 姓名
     */
    @Schema(description = "姓名")
    private String fullName;

    /**
     * 身份证号
     */
    @Schema(description = "身份证号")
    private String idCard;

    /**
     * 身份证正面
     */
    @Schema(description = "身份证正面")
    private String idFront;

    /**
     * 身份证反面
     */
    @Schema(description = "身份证反面")
    private String idOpposite;

    /**
     * 学生证
     */
    @Schema(description = "学生证")
    private String idStudent;

    /**
     * 性别（0：女；1：男）
     */
    @Schema(description = "性别（0：女；1：男）")
    private Integer sex;

    /**
     * 收益
     */
    @Column(value = "income",onInsertValue = "0")
    @Schema(description = "收益")
    private Double income;

    /**
     * 余额
     */
    @Column(value = "balance",onInsertValue = "0")
    @Schema(description = "余额")
    private Double balance;

    /**
     * 图片
     */
//    @Column(value = "image",onInsertValue = "'https://school-platform-public.oss-cn-chengdu.aliyuncs.com/65fc6296ad57692b29c57d74.jpg'")
    @Column(value = "image")
    @Schema(description = "图片")
    private String image;

    /**
     * 入驻校区ID
     */
    @Schema(description = "入驻校区ID")
    private String schoolId;

    /**
     * 入驻校区信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "schoolId",targetField = "id")
    @Schema(title = "入驻校区信息",name = "comSchool",type = "comSchool")
    private ComSchool comSchool;

    /**
     * 审核状态（0：未通过；1：已通过；2：审核中）
     */
    @Column(value = "examine_state",onInsertValue = "0")
    @Schema(description = "审核状态（0：未通过；1：已通过）")
    private Integer examineState;

    /**
     * 状态（0、冻结；1、存在）
     */
    @Column(value = "state",onInsertValue = "1")
    @Schema(description = "状态（0、冻结；1、存在）")
    private Integer state;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 支付提现密码
     */
    @Schema(description = "支付提现密码")
    private String payPassword;

    /**
     * 提现总额
     */
    @Column(value = "withdrawal_total",onInsertValue = "0")
    @Schema(description = "提现总额")
    private Double withdrawalTotal;

    /**
     * 工作类型（0：兼职；1：全职）
     */
    @Schema(description = "工作类型（0：兼职；1：全职）")
    private Integer workType;

    /**
     * 每单收益
     */
    @Schema(description = "每单收益")
    private Double unitPrice;

    /**
     * 每月收益
     */
    @Schema(description = "每月收益")
    private Double monthPrice;

    /**
     * 兼职等级
     */
    @Column(value = "part_grade",onInsertValue = "1")
    @Schema(description = "兼职等级")
    private Integer partGrade;

    /**
     * 全职等级
     */
    @Column(value = "full_grade",onInsertValue = "1")
    @Schema(description = "全职等级")
    private Integer fullGrade;

    /**
     * 评分
     */
    @Column(value = "score",onInsertValue = "0")
    @Schema(description = "评分")
    private Double score;

    /**
     * 评价次数
     */
    @Column(value = "evaluate_num",onInsertValue = "0")
    @Schema(description = "评价次数")
    private Integer evaluateNum;

    /**
     * 微信optionid
     */
    @Schema(description = "微信optionid")
    private String wxOptionId;

    /**
     * 微信通用ID
     */
    @Schema(description = "微信通用ID")
    private String unionId;

    /**
     * 提现次数
     */
    @Column(value = "withdrawal_num",onInsertValue = "0")
    @Schema(description = "提现次数")
    private Integer withdrawalNum;

}
