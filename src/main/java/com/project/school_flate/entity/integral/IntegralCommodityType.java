package com.project.school_flate.entity.integral;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import java.io.Serializable;

import com.mybatisflex.core.keygen.KeyGenerators;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "积分商品分类")
@Table(value = "t_integral_commodity_type")
public class IntegralCommodityType implements Serializable {

    private static final long serialVersionUID = -707099387175255301L;

    /**
     * 积分商品分类ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "积分商品分类ID")
    private String id;

    /**
     * 名称
     */
    @Schema(description = "名称")
    private String name;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Integer no;

    /**
     * 归属类型（0：普通商品；1：储蓄兑换）
     */
    @Schema(description = "归属类型（0：普通商品；1：储蓄兑换）")
    private Integer type;

}
