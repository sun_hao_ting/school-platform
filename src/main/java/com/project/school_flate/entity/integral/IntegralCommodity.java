package com.project.school_flate.entity.integral;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.com.ComFileDetail;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "积分商品")
@Table(value = "t_integral_commodity")
public class IntegralCommodity implements Serializable {

    private static final long serialVersionUID = 6242481853994944977L;

    /**
     * 积分商品ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "积分商品ID")
    private String id;

    /**
     * 名称
     */
    @Schema(description = "名称")
    private String name;

    /**
     * 商品分类ID
     */
    @Schema(description = "商品分类ID")
    private String commodityTypeId;

    /**
     * 商品分类信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "commodityTypeId",targetField = "id")
    @Schema(title = "商品分类信息",name = "integralCommodityType",type = "integralCommodityType")
    private IntegralCommodityType integralCommodityType;

    /**
     * 状态（1：上架；2：下架；3：售完）
     */
    @Schema(description = "状态（1：上架；2：下架；3：售完）")
    private Integer state;

    /**
     * 销量
     */
    @Column(value = "sales_volume",onInsertValue = "0")
    @Schema(description = "销量")
    private Integer salesVolume;

    /**
     * 积分
     */
    @Schema(description = "积分")
    private Integer integral;

    /**
     * 图片
     */
    @Schema(description = "图片")
    private String image;

    /**
     * 海报
     */
    @Schema(description = "海报")
    private String poster;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

}
