package com.project.school_flate.entity.flow;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;
import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.user.UserInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "流水任务表")
@Table(value = "t_flow_task")
public class FlowTask implements Serializable {

    private static final long serialVersionUID = -8997492284966397734L;

    /**
     * ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "id")
    private String id;

    /**
     * 客户ID
     */
    @Column("user_id")
    @Schema(description = "客户ID")
    private String userId;

    /**
     * 用户信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "userId",targetField = "id")
    @Schema(title = "用户信息",name = "userInfo",type = "userInfo")
    private UserInfo userInfo;

    /**
     * 商店ID
     */
    @Column("shop_id")
    @Schema(description = "商店ID")
    private String shopId;

    /**
     * 骑手ID
     */
    @Column("delivery_id")
    @Schema(description = "骑手ID")
    private String deliveryId;

    /**
     * 订单ID
     */
    @Column("order_id")
    @Schema(description = "订单ID")
    private String orderId;

    /**
     * 状态（0、未完成；1、已完成；）
     */
    @Column(value = "state",onInsertValue = "0")
    @Schema(description = "状态（0、未完成；1、已完成；）")
    private Integer state;

    /**
     * 类型（0、订单支付；1、充值储蓄；2、提现；3：分销商返利；4：成为分销商；5、推广储蓄返利；6：绑定手机号）
     */
    @Column("type")
    @Schema(description = "类型（0、订单支付；1、充值储蓄；2、提现；3：分销商返利；4：成为分销商；5、推广储蓄返利；6：绑定手机号）")
    private Integer type;

    /**
     * 金额
     */
    @Column("money")
    @Schema(description = "金额")
    private Double money;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 执行时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "执行时间")
    private Date executeTime;

}
