package com.project.school_flate.entity.flow;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;
import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.delivery.DeliveryInfo;
import com.project.school_flate.entity.order.OrderDelivery;
import com.project.school_flate.entity.shop.ShopInfo;
import com.project.school_flate.entity.user.UserInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "业务流水表")
@Table(value = "t_flow_business")
public class FlowBusiness implements Serializable {
    private static final long serialVersionUID = -4884847441176183938L;

    /**
     * ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "id")
    private String id;

    /**
     * 客户ID
     */
    @Column("user_id")
    @Schema(description = "客户ID")
    private String userId;

    /**
     * 客户信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "userId",targetField = "id")
    @Schema(title = "客户信息",name = "userInfo",type = "UserInfo")
    private UserInfo userInfo;

    /**
     * 店铺ID
     */
    @Column("shop_id")
    @Schema(description = "店铺ID")
    private String shopId;

    /**
     * 店铺信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "shopId",targetField = "id")
    @Schema(title = "店铺信息",name = "shopInfo",type = "ShopInfo")
    private ShopInfo shopInfo;

    /**
     * 配送员ID
     */
    @Column("delivery_id")
    @Schema(description = "配送员ID")
    private String deliveryId;

    /**
     * 配送员列表
     */
    @Column(ignore = true)
    @RelationOneToMany(selfField = "deliveryId",targetField = "id",selfValueSplitBy = ",")
    @Schema(description = "配送员列表")
    private List<DeliveryInfo> deliveryInfoList;

    /**
     * 订单ID
     */
    @Column("order_id")
    @Schema(description = "订单ID")
    private String orderId;

    /**
     * 状态（0、未完成；1、已完成；）
     */
    @Column(value = "state",onInsertValue = "0")
    @Schema(description = "状态（0、未完成；1、已完成；）")
    private Integer state;

    /**
     * 类型（0、订单支付；1、充值储蓄；2、提现；3：分销商返利；4：成为分销商；5、推广储蓄返利；6：绑定手机号）
     */
    @Column("type")
    @Schema(description = "类型（0、订单支付；1、充值储蓄；2、提现；3：分销商返利；4：成为分销商；5、推广储蓄返利；6：绑定手机号）")
    private Integer type;

    /**
     * 金额
     */
    @Column("money")
    @Schema(description = "金额")
    private Double money;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

    /***
     * 流入方向（0、流入；1、流出）
     */
    @Column("inflow_direction")
    @Schema(description = "流入方向（0、流入；1、流出）")
    private Integer inflowDirection;

    /***
     * 流入方式（0：支付；1：积分）
     */
    @Column("inflow_way")
    @Schema(description = "流入方式（0：支付；1：积分）")
    private Integer inflowWay;

    /***
     * 积分
     */
    @Column("integral")
    @Schema(description = "积分")
    private Integer integral;

}
