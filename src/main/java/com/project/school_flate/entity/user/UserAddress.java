package com.project.school_flate.entity.user;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.com.ComSchool;
import com.project.school_flate.entity.com.ComSchoolBuilding;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *  实体类。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "配送地址")
@Table(value = "t_user_address")
public class UserAddress implements Serializable {

    private static final long serialVersionUID = -1440999865647179012L;

    /**
     * 配送地址ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "配送地址ID")
    private String id;

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    private String userId;

    /**
     * 用户信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "userId",targetField = "id")
    @Schema(title = "用户信息",name = "userInfo",type = "userInfo")
    private UserInfo userInfo;

    /**
     * 地址
     */
    @Schema(description = "地址")
    private String address;

    /**
     * 手机号
     */
    @Schema(description = "手机号")
    private String phone;

    /**
     * 联系人
     */
    @Schema(description = "联系人")
    private String contacts;

    /**
     * 校区ID
     */
    @Schema(description = "校区ID")
    private String schoolId;

    /**
     * 校区信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "schoolId",targetField = "id")
    @Schema(title = "校区信息",name = "comSchool",type = "ComSchool")
    private ComSchool comSchool;

    /**
     * 校区楼栋ID
     */
    @Schema(description = "校区楼栋ID")
    private String schoolBuildingId;

    /**
     * 校区楼栋信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "schoolBuildingId",targetField = "id")
    @Schema(title = "校区楼栋信息",name = "comSchoolBuilding",type = "ComSchoolBuilding")
    private ComSchoolBuilding comSchoolBuilding;

    /**
     * 最近使用时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "最近使用时间")
    private Date recentlyTime;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

}
