package com.project.school_flate.entity.user;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.*;
import com.mybatisflex.core.keygen.KeyGenerators;
import com.project.school_flate.entity.com.ComFileDetail;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Author sunht
 * @Date 2023/10/2 18:07
 * @Version v1.0.0
 * @Message
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "t_user_info")
@Tag(name = "用户", description = "用户实体类")
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 5484177379145573493L;

    /**
     * 用户ID
     */
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @Schema(description = "用户ID")
    private String id;

    /**
     * 名称
     */
    @Schema(description = "名称")
    private String name;

    /**
     * 手机号
     */
    @Schema(description = "手机号")
    private String phone;

    /**
     * 微信optionid
     */
    @Schema(description = "微信optionid")
    private String wxOptionId;

    /**
     * 验证码
     */
    @Schema(description = "验证码")
    private String code;

    /**
     * 消费费用
     */
    @Column(value = "consumption_price",onInsertValue = "0")
    @Schema(description = "消费费用")
    private Double consumptionPrice;

    /**
     * 图片
     */
    @Schema(description = "图片")
    private String image;

    /**
     * 积分
     */
    @Column(value = "integral",onInsertValue = "0")
    @Schema(description = "积分")
    private Integer integral;

    /**
     * 储蓄累计节省
     */
    @Column(value = "savings_price",onInsertValue = "0")
    @Schema(description = "储蓄累计节省")
    private Double savingsPrice;

    /**
     * 是否返利（0：否；1：是）
     */
    @Column(value = "is_rebate",onInsertValue = "0")
    @Schema(description = "是否返利（0：否；1：是）")
    private Integer isRebate;

    /**
     * 分销金额
     */
    @Column(value = "distribution_price",onInsertValue = "0")
    @Schema(description = "分销金额")
    private Double distributionPrice;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(value = "create_time",onInsertValue = "now()")
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 支付提现密码
     */
    @Schema(description = "支付提现密码")
    private String payPassword;

    /**
     * 邀请码（推荐用户ID）
     */
    @Schema(description = "邀请码（推荐用户ID）")
    private String invitationCode;

    /**
     * 推荐用户信息
     */
    @Column(ignore = true)
    @RelationOneToOne(selfField = "invitationCode",targetField = "id")
    @Schema(title = "推荐用户信息",name = "codeUserInfo",type = "UserInfo")
    private UserInfo codeUserInfo;

    /**
     * 是否是分销商（0：普通分销商；1：专业分销商；3：不是）
     */
    @Column(value = "is_distribution",onInsertValue = "3")
    @Schema(description = "是否是分销商（0：普通分销商；1：专业分销商；3：不是）")
    private Integer isDistribution;

    /**
     * 分销商等级
     */
    @Schema(description = "分销商等级")
    private Integer distributionGrade;

    /**
     * 余额
     */
    @Column(value = "balance",onInsertValue = "0")
    @Schema(description = "余额")
    private Double balance;

    /**
     * 提现总额
     */
    @Column(value = "withdrawal_total",onInsertValue = "0")
    @Schema(description = "提现总额")
    private Double withdrawalTotal;

    /**
     * 状态（0、冻结；1、存在）
     */
    @Column(value = "state",onInsertValue = "1")
    @Schema(description = "状态（0、冻结；1、存在）")
    private Integer state;

    /**
     * 邀请人数
     */
    @Column(value = "invitation_num",onInsertValue = "0")
    @Schema(description = "邀请人数")
    private Integer invitationNum;

    /**
     * 邀请储蓄会员人数
     */
    @Column(value = "invitation_save_num",onInsertValue = "0")
    @Schema(description = "邀请储蓄会员人数")
    private Integer invitationSaveNum;

    /**
     * 微信通用ID
     */
    @Schema(description = "微信通用ID")
    private String unionId;

    /**
     * 暂存邀请码（只用作推广码识别）
     */
    @Schema(description = "暂存邀请码（只用作推广码识别）")
    private Integer tempCode;

    /**
     * 提现次数
     */
    @Column(value = "withdrawal_num",onInsertValue = "0")
    @Schema(description = "提现次数")
    private Integer withdrawalNum;

}
