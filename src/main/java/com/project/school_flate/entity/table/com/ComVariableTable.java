package com.project.school_flate.entity.table.com;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 * @Author sunht
 * @Date 2023/10/2 18:18
 * @Version v1.0.0
 * @Message
 **/
public class ComVariableTable extends TableDef {

    public static final ComVariableTable COM_VARIABLE = new ComVariableTable();

    /**
     * 系统变量名称
     */
    public final QueryColumn SYSTEM_TITLE = new QueryColumn(this, "system_title");

    /**
     * 系统变量值
     */
    public final QueryColumn SYSTEM_VALUES = new QueryColumn(this, "system_values");

    /**
     * 类型
     */
    public final QueryColumn TYPE = new QueryColumn(this, "type");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{SYSTEM_TITLE, SYSTEM_VALUES, TYPE};

    public ComVariableTable() {
        super("", "t_com_variable");
    }
}
