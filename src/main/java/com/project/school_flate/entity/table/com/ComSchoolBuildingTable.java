package com.project.school_flate.entity.table.com;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ComSchoolBuildingTable extends TableDef {

    /**
     * 
     */
    public static final ComSchoolBuildingTable COM_SCHOOL_BUILDING = new ComSchoolBuildingTable();

    /**
     * 校园楼栋ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 类型（0：女寝；1：男寝；2：混寝）
     */
    public final QueryColumn TYPE = new QueryColumn(this, "type");

    /**
     * 状态（0、不存在；1、存在）
     */
    public final QueryColumn STATE = new QueryColumn(this, "state");

    /**
     * 位置
     */
    public final QueryColumn ADDRESS = new QueryColumn(this, "address");

    /**
     * 校园ID
     */
    public final QueryColumn SCHOOL_ID = new QueryColumn(this, "school_id");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, SCHOOL_ID, NAME, ADDRESS, TYPE, STATE, CREATE_TIME};

    public ComSchoolBuildingTable() {
        super("", "t_com_school_building");
    }

}
