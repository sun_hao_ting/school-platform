package com.project.school_flate.entity.table.shop;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ShopTypeTable extends TableDef {

    /**
     *
     */
    public static final ShopTypeTable SHOP_TYPE = new ShopTypeTable();

    /**
     * 店铺类型ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 图片
     */
    public final QueryColumn IMAGE = new QueryColumn(this, "image");

    /**
     * 状态（0、冻结；1、存在）
     */
    public final QueryColumn STATE = new QueryColumn(this, "state");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, NAME, IMAGE, STATE, CREATE_TIME};

    public ShopTypeTable() {
        super("", "t_shop_type");
    }

}
