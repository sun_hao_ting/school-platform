package com.project.school_flate.entity.table.shop;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ShopRuleTable extends TableDef {

    /**
     * 
     */
    public static final ShopRuleTable SHOP_RULE = new ShopRuleTable();

    /**
     * 店铺规则ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 类型（0：首单优惠；1：满减；2：储蓄）
     */
    public final QueryColumn TYPE = new QueryColumn(this, "type");

    /**
     * 店铺ID
     */
    public final QueryColumn SHOP_ID = new QueryColumn(this, "shop_id");

    /**
     * 赠送金额
     */
    public final QueryColumn GIVE_PRICE = new QueryColumn(this, "give_price");

    /**
     * 达到金额
     */
    public final QueryColumn REACH_PRICE = new QueryColumn(this, "reach_price");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, SHOP_ID, TYPE, REACH_PRICE, GIVE_PRICE};

    public ShopRuleTable() {
        super("", "t_shop_rule");
    }

}
