package com.project.school_flate.entity.table.com;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

// Auto generate by mybatis-flex, do not modify it.
public class ComAdminTable extends TableDef {

    public static final ComAdminTable COM_ADMIN = new ComAdminTable();

    public final QueryColumn ID = new QueryColumn(this, "id");

    public final QueryColumn NAME = new QueryColumn(this, "name");

    public final QueryColumn STATE = new QueryColumn(this, "state");

    public final QueryColumn PASSWORD = new QueryColumn(this, "password");

    public final QueryColumn LOGIN_NAME = new QueryColumn(this, "login_name");

    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    public final QueryColumn IMAGE = new QueryColumn(this, "image");

    public final QueryColumn PHONE = new QueryColumn(this,"phone");
    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, NAME, STATE, PASSWORD, LOGIN_NAME, CREATE_TIME, PHONE, IMAGE};

    public ComAdminTable() {
        super("", "t_com_admin");
    }

}
