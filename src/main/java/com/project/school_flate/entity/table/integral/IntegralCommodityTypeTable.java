package com.project.school_flate.entity.table.integral;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class IntegralCommodityTypeTable extends TableDef {

    /**
     * 
     */
    public static final IntegralCommodityTypeTable INTEGRAL_COMMODITY_TYPE = new IntegralCommodityTypeTable();

    /**
     * 积分商品分类ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 排序
     */
    public final QueryColumn NO = new QueryColumn(this, "no");

    /**
     * 归属类型（0：普通商品；1：储蓄兑换）
     */
    public final QueryColumn TYPE = new QueryColumn(this, "type");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, NAME, NO, TYPE};

    public IntegralCommodityTypeTable() {
        super("", "t_integral_commodity_type");
    }

}
