package com.project.school_flate.entity.table.order;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class OrderTakeawaySaveCacheTable extends TableDef {

    /**
     *
     */
    public static final OrderTakeawaySaveCacheTable ORDER_TAKEAWAY_SAVE_CACHE = new OrderTakeawaySaveCacheTable();

    /**
     * 订单储蓄支付缓存ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 订单ID
     */
    public final QueryColumn ORDER_TAKEAWAY_ID = new QueryColumn(this, "order_takeaway_id");

    /**
     * 店铺用户储蓄ID
     */
    public final QueryColumn SHOP_SAVE_USER_ID = new QueryColumn(this, "shop_save_user_id");

    /**
     * 用户消费价格
     */
    public final QueryColumn USER_PRICE = new QueryColumn(this, "user_price");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, ORDER_TAKEAWAY_ID, SHOP_SAVE_USER_ID, USER_PRICE};

    public OrderTakeawaySaveCacheTable() {
        super("", "t_order_takeaway_save_cache");
    }

}
