package com.project.school_flate.entity.table.shop;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ShopCommodityTypeTable extends TableDef {

    /**
     * 
     */
    public static final ShopCommodityTypeTable SHOP_COMMODITY_TYPE = new ShopCommodityTypeTable();

    /**
     * 商品分类ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 店铺ID
     */
    public final QueryColumn SHOP_ID = new QueryColumn(this, "shop_id");

    /**
     * 排序
     */
    public final QueryColumn NO = new QueryColumn(this, "no");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, NAME, SHOP_ID, NO};

    public ShopCommodityTypeTable() {
        super("", "t_shop_commodity_type");
    }

}
