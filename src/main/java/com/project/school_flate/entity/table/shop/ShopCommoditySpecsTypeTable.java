package com.project.school_flate.entity.table.shop;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ShopCommoditySpecsTypeTable extends TableDef {

    /**
     * 
     */
    public static final ShopCommoditySpecsTypeTable SHOP_COMMODITY_SPECS_TYPE = new ShopCommoditySpecsTypeTable();

    /**
     * 商品规格类型ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 商品ID
     */
    public final QueryColumn COMMODITY_ID = new QueryColumn(this, "commodity_id");

    /**
     * 排序
     */
    public final QueryColumn NO = new QueryColumn(this, "no");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, COMMODITY_ID, NAME, CREATE_TIME, NO};

    public ShopCommoditySpecsTypeTable() {
        super("", "t_shop_commodity_specs_type");
    }

}
