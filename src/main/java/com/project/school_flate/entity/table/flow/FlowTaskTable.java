package com.project.school_flate.entity.table.flow;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

public class FlowTaskTable extends TableDef {
    public static final FlowTaskTable FLOW_TASK_TABLE = new FlowTaskTable();

    /**
     * ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 商店ID
     */
    public final QueryColumn SHOP_ID = new QueryColumn(this, "shop_id");

    /**
     * 骑手ID
     */
    public final QueryColumn DELIVERY_ID = new QueryColumn(this, "delivery_id");

    /**
     * 客户ID
     */
    public final QueryColumn USER_ID = new QueryColumn(this, "user_id");

    /**
     * 订单ID
     */
    public final QueryColumn ORDER_ID = new QueryColumn(this, "order_id");

    /**
     * 状态（0、未完成；1、已完成；）
     */
    public final QueryColumn STATE = new QueryColumn(this, "state");

    /**
     * 类型（0、订单支付；1、充值储蓄；2、提现；3：分销商返利；4：成为分销商；5、推广储蓄返利；6：绑定手机号）
     */
    public final QueryColumn TYPE = new QueryColumn(this, "type");

    /**
     * 金额
     */
    public final QueryColumn MOMEY = new QueryColumn(this, "money");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 执行时间
     */
    public final QueryColumn EXECUTE_TIME = new QueryColumn(this, "execute_time");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID,SHOP_ID,USER_ID,STATE, DELIVERY_ID, MOMEY,ORDER_ID, CREATE_TIME, EXECUTE_TIME};

    public FlowTaskTable() {
        super("", "t_flow_task");
    }
}
