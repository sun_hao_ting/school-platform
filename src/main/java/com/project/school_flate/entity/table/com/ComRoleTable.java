package com.project.school_flate.entity.table.com;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ComRoleTable extends TableDef {

    /**
     *
     */
    public static final ComRoleTable COM_ROLE = new ComRoleTable();

    /**
     * 角色ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 角色名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 角色描述
     */
    public final QueryColumn DESCRIBE = new QueryColumn(this, "describe");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 状态
     */
    public final QueryColumn STATE = new QueryColumn(this, "state");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, NAME, DESCRIBE, CREATE_TIME, STATE};

    public ComRoleTable() {
        super("", "t_com_role");
    }

}
