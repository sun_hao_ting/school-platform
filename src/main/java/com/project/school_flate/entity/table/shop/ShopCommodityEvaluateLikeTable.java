package com.project.school_flate.entity.table.shop;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ShopCommodityEvaluateLikeTable extends TableDef {

    /**
     * 
     */
    public static final ShopCommodityEvaluateLikeTable SHOP_COMMODITY_EVALUATE_LIKE = new ShopCommodityEvaluateLikeTable();

    /**
     * 商品评论点赞ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 用户ID
     */
    public final QueryColumn USER_ID = new QueryColumn(this, "user_id");

    /**
     * 商品评论ID
     */
    public final QueryColumn EVALUATE_ID = new QueryColumn(this, "evaluate_id");

    /**
     * 商品ID
     */
    public final QueryColumn COMMODITY_ID = new QueryColumn(this, "commodity_id");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, COMMODITY_ID, EVALUATE_ID, USER_ID, CREATE_TIME};

    public ShopCommodityEvaluateLikeTable() {
        super("", "t_shop_commodity_evaluate_like");
    }

}
