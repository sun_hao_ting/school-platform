package com.project.school_flate.entity.table.shop;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ShopCommodityEvaluateTable extends TableDef {

    /**
     * 
     */
    public static final ShopCommodityEvaluateTable SHOP_COMMODITY_EVALUATE = new ShopCommodityEvaluateTable();

    /**
     * 商品评价ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 图片
     */
    public final QueryColumn IMAGE = new QueryColumn(this, "image");

    /**
     * 用户ID
     */
    public final QueryColumn USER_ID = new QueryColumn(this, "user_id");

    /**
     * 内容
     */
    public final QueryColumn CONTENT = new QueryColumn(this, "content");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 商品ID
     */
    public final QueryColumn COMMODITY_ID = new QueryColumn(this, "commodity_id");

    /**
     * 是否匿名
     */
    public final QueryColumn IS_ANONYMOUS = new QueryColumn(this, "is_anonymous");

    /**
     * 送达速度评分
     */
    public final QueryColumn SPEED_RATING = new QueryColumn(this, "speed_rating");

    /**
     * 产品质量评分
     */
    public final QueryColumn QUALITY_RATING = new QueryColumn(this, "quality_rating");

    /**
     * 服务态度评分
     */
    public final QueryColumn SERVICE_RATING = new QueryColumn(this, "service_rating");

    /**
     * 综合评分
     */
    public final QueryColumn ALL_RATING = new QueryColumn(this, "all_rating");

    /**
     * 店铺ID
     */
    public final QueryColumn SHOP_ID = new QueryColumn(this, "shop_id");

    /**
     * 评价模板
     */
    public final QueryColumn EVALUATE_TEMPLATE = new QueryColumn(this, "evaluate_template");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, COMMODITY_ID, SERVICE_RATING, QUALITY_RATING, SPEED_RATING, USER_ID, CONTENT, IMAGE, IS_ANONYMOUS, CREATE_TIME, ALL_RATING, SHOP_ID, EVALUATE_TEMPLATE};

    public ShopCommodityEvaluateTable() {
        super("", "t_shop_commodity_evaluate");
    }

}
