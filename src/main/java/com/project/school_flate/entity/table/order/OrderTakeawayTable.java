package com.project.school_flate.entity.table.order;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class OrderTakeawayTable extends TableDef {

    /**
     *
     */
    public static final OrderTakeawayTable ORDER_TAKEAWAY = new OrderTakeawayTable();

    /**
     * 外卖订单ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 价格
     */
    public final QueryColumn PRICE = new QueryColumn(this, "price");

    /**
     * 用户ID
     */
    public final QueryColumn USER_ID = new QueryColumn(this, "user_id");

    /**
     * 叫号
     */
    public final QueryColumn CALL_NUMBER = new QueryColumn(this, "call_number");

    /**
     * 接单时间
     */
    public final QueryColumn CONNECT_TIME = new QueryColumn(this, "connect_time");

    /**
     * 配送费
     */
    public final QueryColumn DELIVERY_PRICE = new QueryColumn(this, "delivery_price");

    /**
     * 支付方式（0：微信支付；1：储蓄支付；2：支付宝支付）
     */
    public final QueryColumn PAYMENT_METHOD = new QueryColumn(this, "payment_method");

    /**
     * 配送地址ID
     */
    public final QueryColumn USER_ADDRESS = new QueryColumn(this, "user_address");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 校区ID
     */
    public final QueryColumn SCHOOL_ID = new QueryColumn(this, "school_id");

    /**
     * 校区楼栋ID
     */
    public final QueryColumn SCHOOL_BUILDING_ID = new QueryColumn(this, "school_building_id");

    /**
     * 手机号
     */
    public final QueryColumn PHONE = new QueryColumn(this, "phone");

    /**
     * 联系人
     */
    public final QueryColumn CONTACTS = new QueryColumn(this, "contacts");

    /**
     * 店铺ID
     */
    public final QueryColumn SHOP_ID = new QueryColumn(this, "shop_id");

    /**
     * 支付状态（0、待支付；1、已支付；2、已完成；3、取消订单；4、取消完成；5、申请投诉；6、投诉完成）
     */
    public final QueryColumn PAY_STATE = new QueryColumn(this, "pay_state");

    /**
     * 配送状态（0、未配送；1：已接单【取餐中】；2：已取餐【配送中】；3、已送达）
     */
    public final QueryColumn DELIVERY_STATE = new QueryColumn(this, "delivery_state");

    /**
     * 出餐状态（0、出餐中；1、已出餐；2、已完成；3、取消订单；4、取消完成）
     */
    public final QueryColumn SHOP_STATE = new QueryColumn(this, "shop_state");

    /**
     * 支付时间
     */
    public final QueryColumn PAY_TIME = new QueryColumn(this, "pay_time");

    /**
     * 备注
     */
    public final QueryColumn NOTES = new QueryColumn(this, "notes");

    /**
     * 客户确认时间
     */
    public final QueryColumn OK_TIME = new QueryColumn(this, "ok_time");

    /**
     * 打包费
     */
    public final QueryColumn PACKING_CHARGE = new QueryColumn(this, "packing_charge");

    /**
     * 折扣详情
     */
    public final QueryColumn DISCOUNT_DETAILS = new QueryColumn(this, "discount_details");

    /**
     * 出餐时间
     */
    public final QueryColumn DINING_TIME = new QueryColumn(this, "dining_time");

    /**
     * 是否评价（0：未评价；1：已评价）
     */
    public final QueryColumn IS_EVALUATE = new QueryColumn(this, "is_evaluate");

    /**
     * 节省金额
     */
    public final QueryColumn SAVINGS_PRICE = new QueryColumn(this, "savings_price");

    /**
     * 配送方式（0：多人；1：单人）
     */
    public final QueryColumn DELIVERY_TYPE = new QueryColumn(this, "delivery_type");

    /**
     * 是否中转（0：未中转；1：已中转）
     */
    public final QueryColumn IS_TRANSFER = new QueryColumn(this, "is_transfer");

    /**
     * 平台抽取比例
     */
    public final QueryColumn COM_DRAW = new QueryColumn(this, "com_draw");

    /**
     * 送达时间
     */
    public final QueryColumn REACH_TIME = new QueryColumn(this, "reach_time");

    /**
     * 取餐时间
     */
    public final QueryColumn PICKING_TIME = new QueryColumn(this, "picking_time");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, CALL_NUMBER, USER_ID, PRICE, DELIVERY_PRICE, USER_ADDRESS, PAYMENT_METHOD, CREATE_TIME, SCHOOL_ID, SCHOOL_BUILDING_ID, CONNECT_TIME, PHONE, CONTACTS, SHOP_ID, PAY_STATE, DELIVERY_STATE, SHOP_STATE, NOTES, OK_TIME, PACKING_CHARGE, DISCOUNT_DETAILS, DINING_TIME, IS_EVALUATE, SAVINGS_PRICE, DELIVERY_TYPE, COM_DRAW, REACH_TIME, PICKING_TIME};

    public OrderTakeawayTable() {
        super("", "t_order_takeaway");
    }

}
