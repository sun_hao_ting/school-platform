package com.project.school_flate.entity.table.user;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class UserAddressTable extends TableDef {

    /**
     * 
     */
    public static final UserAddressTable USER_ADDRESS = new UserAddressTable();

    /**
     * 配送地址ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 手机号
     */
    public final QueryColumn PHONE = new QueryColumn(this, "phone");

    /**
     * 用户ID
     */
    public final QueryColumn USER_ID = new QueryColumn(this, "user_id");

    /**
     * 地址
     */
    public final QueryColumn ADDRESS = new QueryColumn(this, "address");

    /**
     * 联系人
     */
    public final QueryColumn CONTACTS = new QueryColumn(this, "contacts");

    /**
     * 校区ID
     */
    public final QueryColumn SCHOOL_ID = new QueryColumn(this, "school_id");

    /**
     * 校区楼栋ID
     */
    public final QueryColumn SCHOOL_BUILDING_ID = new QueryColumn(this, "school_building_id");

    /**
     * 最近使用时间
     */
    public final QueryColumn RECENTLY_TIME = new QueryColumn(this, "recently_time");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, USER_ID, ADDRESS, PHONE, CONTACTS, SCHOOL_ID, SCHOOL_BUILDING_ID, RECENTLY_TIME, CREATE_TIME};

    public UserAddressTable() {
        super("", "t_user_address");
    }

}
