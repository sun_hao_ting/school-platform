package com.project.school_flate.entity.table.com;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ComFeedbackTable extends TableDef {

    /**
     * 
     */
    public static final ComFeedbackTable COM_FEEDBACK = new ComFeedbackTable();

    /**
     * 反馈ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 图片
     */
    public final QueryColumn IMAGE = new QueryColumn(this, "image");

    /**
     * 用户ID
     */
    public final QueryColumn USER_ID = new QueryColumn(this, "user_id");

    /**
     * 描述
     */
    public final QueryColumn DESCRIBE = new QueryColumn(this, "describe");

    /**
     * 管理员ID
     */
    public final QueryColumn ADMIN_ID = new QueryColumn(this, "admin_id");

    /**
     * 回复
     */
    public final QueryColumn REPLY = new QueryColumn(this, "reply");

    /**
     * 状态（0：未处理；1：已处理）
     */
    public final QueryColumn STATE = new QueryColumn(this, "state");

    /**
     * 反馈有无用评分
     */
    public final QueryColumn SCORE = new QueryColumn(this, "score");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, USER_ID, IMAGE, DESCRIBE, ADMIN_ID, REPLY, STATE, SCORE, CREATE_TIME};

    public ComFeedbackTable() {
        super("", "t_com_feedback");
    }

}
