package com.project.school_flate.entity.table.shop;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ShopInfoTable extends TableDef {

    /**
     *
     */
    public static final ShopInfoTable SHOP_INFO = new ShopInfoTable();

    /**
     * 店铺ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 图片
     */
    public final QueryColumn IMAGE = new QueryColumn(this, "image");

    /**
     * 店铺账号ID
     */
    public final QueryColumn ACCOUNT_ID = new QueryColumn(this, "account_id");

    /**
     * 状态（0、冻结；1、存在）
     */
    public final QueryColumn STATE = new QueryColumn(this, "state");

    /**
     * 地址
     */
    public final QueryColumn ADDRESS = new QueryColumn(this, "address");

    /**
     * 余额
     */
    public final QueryColumn BALANCE = new QueryColumn(this, "balance");

    /**
     * 是否营业
     */
    public final QueryColumn IS_TRADE = new QueryColumn(this, "is_trade");

    /**
     * 入驻校区ID
     */
    public final QueryColumn SCHOOL_ID = new QueryColumn(this, "school_id");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 起价费
     */
    public final QueryColumn START_PRICE = new QueryColumn(this, "start_price");

    /**
     * 销量
     */
    public final QueryColumn SALES_VOLUME = new QueryColumn(this, "sales_volume");

    /**
     * 介绍
     */
    public final QueryColumn INTRODUCTION = new QueryColumn(this, "introduction");

    /**
     * 主营业务
     */
    public final QueryColumn MAIN_BUSINESS = new QueryColumn(this, "main_business");

    /**
     * 营业结束小时
     */
    public final QueryColumn TRADE_END_HOUR = new QueryColumn(this, "trade_end_hour");

    /**
     * 配送费
     */
    public final QueryColumn DELIVERY_PRICE = new QueryColumn(this, "delivery_price");

    /**
     * 配送范围
     */
    public final QueryColumn DELIVERY_SCOPE = new QueryColumn(this, "delivery_scope");

    /**
     * 经营许可证
     */
    public final QueryColumn OPERATE_PERMIT = new QueryColumn(this, "operate_permit");

    /**
     * 营业开始小时
     */
    public final QueryColumn TRADE_BEGIN_HOUR = new QueryColumn(this, "trade_begin_hour");

    /**
     * 营业执照
     */
    public final QueryColumn BUSINESS_LICENSE = new QueryColumn(this, "business_license");

    /**
     * 审核状态（0：未通过；1：已通过）
     */
    public final QueryColumn EXAMINE_STATE = new QueryColumn(this, "examine_state");

    /**
     * 支付提现密码
     */
    public final QueryColumn PAY_PASSWORD = new QueryColumn(this, "pay_password");

    /**
     * 姓名
     */
    public final QueryColumn FULL_NAME = new QueryColumn(this, "full_name");

    /**
     * 身份证号
     */
    public final QueryColumn ID_CARD = new QueryColumn(this, "id_card");

    /**
     * 身份证正面
     */
    public final QueryColumn ID_FRONT = new QueryColumn(this, "id_front");

    /**
     * 身份证反面
     */
    public final QueryColumn ID_OPPOSITE = new QueryColumn(this, "id_opposite");

    /**
     * 提现总额
     */
    public final QueryColumn WITHDRAWAL_TOTAL = new QueryColumn(this, "withdrawal_total");

    /**
     * 店铺类型ID
     */
    public final QueryColumn SHOP_TYPE_ID = new QueryColumn(this, "shop_type_id");

    /**
     * 评分
     */
    public final QueryColumn SCORE = new QueryColumn(this, "score");

    /**
     * 评价模板
     */
    public final QueryColumn EVALUATE_TEMPLATE = new QueryColumn(this, "evaluate_template");

    /**
     * 手机号
     */
    public final QueryColumn PHONE = new QueryColumn(this, "phone");

    /**
     * 公告
     */
    public final QueryColumn NOTICE = new QueryColumn(this, "notice");

    /**
     * 是否完善信息（0：否；1：是）
     */
    public final QueryColumn IS_PERFECT = new QueryColumn(this, "is_perfect");

    /**
     * LOGO
     */
    public final QueryColumn LOGO_IMAGE = new QueryColumn(this, "logo_image");

    /**
     * 是否推荐（0：否；1：是）
     */
    public final QueryColumn IS_RECOMMEND = new QueryColumn(this, "is_recommend");

    /**
     * 是否开通储蓄（0：否；1：是）
     */
    public final QueryColumn IS_OPEN_SAVE = new QueryColumn(this, "is_open_save");

    /**
     * 储蓄审核状态（0：未通过；1：已通过；2：审核中：3：未提交）
     */
    public final QueryColumn SAVE_EXAMINE_STATE = new QueryColumn(this, "save_examine_state");

    /**
     * 是否开启自动出餐（0：否；1：是）
     */
    public final QueryColumn IS_DINING = new QueryColumn(this, "is_dining");

    /**
     * 出餐时间
     */
    public final QueryColumn DINING_TIME = new QueryColumn(this, "dining_time");

    /**
     * 打包费
     */
    public final QueryColumn PACKING_CHARGE = new QueryColumn(this, "packing_charge");

    /**
     * 评价次数
     */
    public final QueryColumn EVALUATE_NUM = new QueryColumn(this, "evaluate_num");

    /**
     * 提现次数
     */
    public final QueryColumn WITHDRAWAL_NUM = new QueryColumn(this, "withdrawal_num");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, NAME, ACCOUNT_ID, ADDRESS, DELIVERY_SCOPE, TRADE_BEGIN_HOUR, TRADE_END_HOUR, IS_TRADE, SALES_VOLUME, START_PRICE, DELIVERY_PRICE, MAIN_BUSINESS, INTRODUCTION, BUSINESS_LICENSE, OPERATE_PERMIT, BALANCE, IMAGE, SCHOOL_ID, CREATE_TIME, STATE, EXAMINE_STATE, PAY_PASSWORD, FULL_NAME, ID_CARD, ID_FRONT, ID_OPPOSITE, WITHDRAWAL_TOTAL, SHOP_TYPE_ID, SCORE, EVALUATE_TEMPLATE, PHONE, NOTICE, IS_PERFECT, LOGO_IMAGE, IS_RECOMMEND, IS_OPEN_SAVE, SAVE_EXAMINE_STATE, IS_DINING, DINING_TIME, PACKING_CHARGE, EVALUATE_NUM, WITHDRAWAL_NUM};

    public ShopInfoTable() {
        super("", "t_shop_info");
    }

}
