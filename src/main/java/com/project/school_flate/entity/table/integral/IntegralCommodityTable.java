package com.project.school_flate.entity.table.integral;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class IntegralCommodityTable extends TableDef {

    /**
     * 
     */
    public static final IntegralCommodityTable INTEGRAL_COMMODITY = new IntegralCommodityTable();

    /**
     * 积分商品ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 图片
     */
    public final QueryColumn IMAGE = new QueryColumn(this, "image");

    /**
     * 状态（1：上架；2：下架；3：售完）
     */
    public final QueryColumn STATE = new QueryColumn(this, "state");

    /**
     * 海报
     */
    public final QueryColumn POSTER = new QueryColumn(this, "poster");

    /**
     * 积分
     */
    public final QueryColumn INTEGRAL = new QueryColumn(this, "integral");

    /**
     * 销量
     */
    public final QueryColumn SALES_VOLUME = new QueryColumn(this, "sales_volume");

    /**
     * 商品分类ID
     */
    public final QueryColumn COMMODITY_TYPE_ID = new QueryColumn(this, "commodity_type_id");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, NAME, COMMODITY_TYPE_ID, SALES_VOLUME, INTEGRAL, IMAGE, POSTER,STATE, CREATE_TIME};

    public IntegralCommodityTable() {
        super("", "t_integral_commodity");
    }

}
