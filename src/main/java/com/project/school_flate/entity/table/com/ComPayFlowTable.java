package com.project.school_flate.entity.table.com;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 * @Author sunht
 * @Date 2023/9/19 20:57
 * @Version v1.0.0
 * @Message
 **/
public class ComPayFlowTable extends TableDef {

    public static final ComPayFlowTable COM_PAY_FLOW = new ComPayFlowTable();

    public final QueryColumn ID = new QueryColumn(this, "id");

    public final QueryColumn TYPE = new QueryColumn(this, "type");

    public final QueryColumn ORDER_ID = new QueryColumn(this, "order_id");

    public final QueryColumn CLIENT_ID = new QueryColumn(this, "client_id");

    public final QueryColumn DELIVERY_ID = new QueryColumn(this, "delivery_id");

    public final QueryColumn SHOP_ID = new QueryColumn(this, "shop_id");

    public final QueryColumn TOTAL = new QueryColumn(this, "total");

    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    public final QueryColumn STATE = new QueryColumn(this, "state");
    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, TYPE, ORDER_ID, CLIENT_ID, TOTAL, CREATE_TIME,STATE};

    public ComPayFlowTable() {
        super("", "t_com_pay_flow");
    }
}
