package com.project.school_flate.entity.table.delivery;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class DeliveryEvaluateTable extends TableDef {

    /**
     * 
     */
    public static final DeliveryEvaluateTable DELIVERY_EVALUATE = new DeliveryEvaluateTable();

    /**
     * 配送员评价ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 图片
     */
    public final QueryColumn IMAGE = new QueryColumn(this, "image");

    /**
     * 用户ID
     */
    public final QueryColumn USER_ID = new QueryColumn(this, "user_id");

    /**
     * 内容
     */
    public final QueryColumn CONTENT = new QueryColumn(this, "content");

    /**
     * 综合评分
     */
    public final QueryColumn ALL_RATING = new QueryColumn(this, "all_rating");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 配送员ID
     */
    public final QueryColumn DELIVERY_ID = new QueryColumn(this, "delivery_id");

    /**
     * 订单ID
     */
    public final QueryColumn ORDER_TAKEAWAY_ID = new QueryColumn(this, "order_takeaway_id");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, DELIVERY_ID, USER_ID, ORDER_TAKEAWAY_ID, ALL_RATING, CONTENT, IMAGE, CREATE_TIME};

    public DeliveryEvaluateTable() {
        super("", "t_delivery_evaluate");
    }

}
