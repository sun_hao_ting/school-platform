package com.project.school_flate.entity.table.shop;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ShopSaveUserTable extends TableDef {

    /**
     *
     */
    public static final ShopSaveUserTable SHOP_SAVE_USER = new ShopSaveUserTable();

    /**
     * 店铺用户储蓄ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 店铺ID
     */
    public final QueryColumn SHOP_ID = new QueryColumn(this, "shop_id");

    /**
     * 用户ID
     */
    public final QueryColumn USER_ID = new QueryColumn(this, "user_id");

    /**
     * 用户余额
     */
    public final QueryColumn USER_BALANCE = new QueryColumn(this, "user_balance");

    /**
     * 充值金额
     */
    public final QueryColumn RECHARGE_PRICE = new QueryColumn(this, "recharge_price");

    /**
     * 赠送金额
     */
    public final QueryColumn GIFTS_PRICE = new QueryColumn(this, "gifts_price");

    /**
     * 折扣比例
     */
    public final QueryColumn DISCOUNT_RATIO = new QueryColumn(this, "discount_ratio");

    /**
     * 是否全部花完（0：否；1：是）
     */
    public final QueryColumn IS_SPEND = new QueryColumn(this, "is_spend");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, SHOP_ID, USER_ID, USER_BALANCE, RECHARGE_PRICE, GIFTS_PRICE, DISCOUNT_RATIO, IS_SPEND, CREATE_TIME};

    public ShopSaveUserTable() {
        super("", "t_shop_save_user");
    }

}
