package com.project.school_flate.entity.table.com;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ComMemberTable extends TableDef {

    /**
     * 
     */
    public static final ComMemberTable COM_MEMBER = new ComMemberTable();

    /**
     * 会员设置ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 价格
     */
    public final QueryColumn PRICE = new QueryColumn(this, "price");

    /**
     * 返利金额
     */
    public final QueryColumn REBATE_PRICE = new QueryColumn(this, "rebate_price");

    /**
     * 返利人数
     */
    public final QueryColumn REBATE_NUMBER = new QueryColumn(this, "rebate_number");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, PRICE, REBATE_NUMBER, REBATE_PRICE};

    public ComMemberTable() {
        super("", "t_com_member");
    }

}
