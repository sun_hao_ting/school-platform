package com.project.school_flate.entity.table.order;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class OrderDeliveryTable extends TableDef {

    /**
     *
     */
    public static final OrderDeliveryTable ORDER_DELIVERY = new OrderDeliveryTable();

    /**
     * 外卖订单配送ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 送达时间
     */
    public final QueryColumn REACH_TIME = new QueryColumn(this, "reach_time");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 配送人ID
     */
    public final QueryColumn DELIVERY_ID = new QueryColumn(this, "delivery_id");

    /**
     * 取餐时间
     */
    public final QueryColumn PICKING_TIME = new QueryColumn(this, "picking_time");

    /**
     * 配送状态（0：取餐中；1：配送中；2、已送达）
     */
    public final QueryColumn DELIVERY_STATE = new QueryColumn(this, "delivery_state");

    /**
     * 订单ID
     */
    public final QueryColumn ORDER_TAKEAWAY_ID = new QueryColumn(this, "order_takeaway_id");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, ORDER_TAKEAWAY_ID, DELIVERY_ID, DELIVERY_STATE, PICKING_TIME, REACH_TIME, CREATE_TIME};

    public OrderDeliveryTable() {
        super("", "t_order_delivery");
    }

}
