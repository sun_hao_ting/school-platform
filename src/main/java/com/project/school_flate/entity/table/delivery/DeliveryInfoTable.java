package com.project.school_flate.entity.table.delivery;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class DeliveryInfoTable extends TableDef {

    /**
     *
     */
    public static final DeliveryInfoTable DELIVERY_INFO = new DeliveryInfoTable();

    /**
     * 配送员ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 性别（0：女；1：男）
     */
    public final QueryColumn SEX = new QueryColumn(this, "sex");

    /**
     * 名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 图片
     */
    public final QueryColumn IMAGE = new QueryColumn(this, "image");

    /**
     * 手机号
     */
    public final QueryColumn PHONE = new QueryColumn(this, "phone");

    /**
     * 状态（0、冻结；1、存在）
     */
    public final QueryColumn STATE = new QueryColumn(this, "state");

    /**
     * 收益
     */
    public final QueryColumn INCOME = new QueryColumn(this, "income");

    /**
     * 余额
     */
    public final QueryColumn BALANCE = new QueryColumn(this, "balance");

    /**
     * 姓名
     */
    public final QueryColumn FULL_NAME = new QueryColumn(this, "full_name");

    /**
     * 身份证号
     */
    public final QueryColumn ID_CARD = new QueryColumn(this, "id_card");

    /**
     * 身份证正面
     */
    public final QueryColumn ID_FRONT = new QueryColumn(this, "id_front");

    /**
     * 密码
     */
    public final QueryColumn PASSWORD = new QueryColumn(this, "password");

    /**
     * 入驻校区ID
     */
    public final QueryColumn SCHOOL_ID = new QueryColumn(this, "school_id");

    /**
     * 身份证反面
     */
    public final QueryColumn ID_OPPOSITE = new QueryColumn(this, "id_opposite");

    /**
     * 学生证
     */
    public final QueryColumn ID_STUDENT = new QueryColumn(this, "id_student");

    /**
     * 审核状态（0：未通过；1：已通过；2：审核中）
     */
    public final QueryColumn EXAMINE_STATE = new QueryColumn(this, "examine_state");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 支付提现密码
     */
    public final QueryColumn PAY_PASSWORD = new QueryColumn(this, "pay_password");

    /**
     * 提现总额
     */
    public final QueryColumn WITHDRAWAL_TOTAL = new QueryColumn(this, "withdrawal_total");

    /**
     * 工作类型（0：兼职；1：全职）
     */
    public final QueryColumn WORK_TYPE = new QueryColumn(this, "work_type");

    /**
     * 每单收益
     */
    public final QueryColumn UNIT_PRICE = new QueryColumn(this, "unit_price");

    /**
     * 每月收益
     */
    public final QueryColumn MONTH_PRICE = new QueryColumn(this, "month_price");

    /**
     * 兼职等级
     */
    public final QueryColumn PART_GRADE = new QueryColumn(this, "part_grade");

    /**
     * 全职等级
     */
    public final QueryColumn FULL_GRADE = new QueryColumn(this, "full_grade");

    /**
     * 评分
     */
    public final QueryColumn SCORE = new QueryColumn(this, "score");

    /**
     * 评价次数
     */
    public final QueryColumn EVALUATE_NUM = new QueryColumn(this, "evaluate_num");

    /**
     * 微信optionid
     */
    public final QueryColumn WX_OPTION_ID = new QueryColumn(this, "wx_option_id");

    /**
     * 微信通用ID
     */
    public final QueryColumn UNION_ID = new QueryColumn(this, "union_id");

    /**
     * 提现次数
     */
    public final QueryColumn WITHDRAWAL_NUM = new QueryColumn(this, "withdrawal_num");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, NAME, PHONE, PASSWORD, ID_FRONT, ID_OPPOSITE, SEX, INCOME, BALANCE, IMAGE, SCHOOL_ID, EXAMINE_STATE, STATE, CREATE_TIME, PAY_PASSWORD, ID_STUDENT, FULL_NAME, ID_CARD, WITHDRAWAL_TOTAL, WORK_TYPE, UNIT_PRICE, MONTH_PRICE, PART_GRADE ,FULL_GRADE, SCORE, EVALUATE_NUM, WX_OPTION_ID, UNION_ID, WITHDRAWAL_NUM};

    public DeliveryInfoTable() {
        super("", "t_delivery_info");
    }

}
