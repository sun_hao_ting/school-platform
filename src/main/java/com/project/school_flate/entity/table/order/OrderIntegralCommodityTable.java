package com.project.school_flate.entity.table.order;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class OrderIntegralCommodityTable extends TableDef {

    /**
     *
     */
    public static final OrderIntegralCommodityTable ORDER_INTEGRAL_COMMODITY = new OrderIntegralCommodityTable();

    /**
     * 积分订单商品ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 数量
     */
    public final QueryColumn NUMBER = new QueryColumn(this, "number");

    /**
     * 积分
     */
    public final QueryColumn INTEGRAL = new QueryColumn(this, "integral");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 商品ID
     */
    public final QueryColumn COMMODITY_ID = new QueryColumn(this, "commodity_id");

    /**
     * 订单ID
     */
    public final QueryColumn ORDER_TAKEAWAY_ID = new QueryColumn(this, "order_takeaway_id");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, ORDER_TAKEAWAY_ID, COMMODITY_ID, INTEGRAL, NUMBER, CREATE_TIME};

    public OrderIntegralCommodityTable() {
        super("", "t_order_integral_commodity");
    }

}
