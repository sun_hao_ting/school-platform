package com.project.school_flate.entity.table.order;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class OrderTakeawayCancellationTable extends TableDef {

    /**
     * 
     */
    public static final OrderTakeawayCancellationTable ORDER_TAKEAWAY_CANCELLATION = new OrderTakeawayCancellationTable();

    /**
     * 订单取消ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 图片
     */
    public final QueryColumn IMAGE = new QueryColumn(this, "image");

    /**
     * 状态（0：审核中；1：通过；2：驳回）
     */
    public final QueryColumn STATE = new QueryColumn(this, "state");

    /**
     * 理由
     */
    public final QueryColumn REASON = new QueryColumn(this, "reason");

    /**
     * 店铺ID
     */
    public final QueryColumn SHOP_ID = new QueryColumn(this, "shop_id");

    /**
     * 用户ID
     */
    public final QueryColumn USER_ID = new QueryColumn(this, "user_id");

    /**
     * 理由模板
     */
    public final QueryColumn TEMPLATE = new QueryColumn(this, "template");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 订单ID
     */
    public final QueryColumn ORDER_TAKEAWAY_ID = new QueryColumn(this, "order_takeaway_id");

    /**
     * 类型（0：用户；1：店铺）
     */
    public final QueryColumn TYPE = new QueryColumn(this, "type");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, ORDER_TAKEAWAY_ID, USER_ID, SHOP_ID, REASON, TEMPLATE, IMAGE, STATE, CREATE_TIME, TYPE};

    public OrderTakeawayCancellationTable() {
        super("", "t_order_takeaway_cancellation");
    }

}
