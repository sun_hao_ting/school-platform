package com.project.school_flate.entity.table.com;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ComWithdrawalFlowTable extends TableDef {

    /**
     * 
     */
    public static final ComWithdrawalFlowTable COM_WITHDRAWAL_FLOW = new ComWithdrawalFlowTable();

    /**
     * 微信转账流水ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 类型（0：用户转账；1：配送员转账；2：店铺转账）
     */
    public final QueryColumn TYPE = new QueryColumn(this, "type");

    /**
     * 状态（0：待转账；1：已转账；2：转账失败）
     */
    public final QueryColumn STATE = new QueryColumn(this, "state");

    /**
     * 金额
     */
    public final QueryColumn TOTAL = new QueryColumn(this, "total");

    /**
     * 店铺ID
     */
    public final QueryColumn SHOP_ID = new QueryColumn(this, "shop_id");

    /**
     * 用户ID
     */
    public final QueryColumn USER_ID = new QueryColumn(this, "user_id");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 配送员ID
     */
    public final QueryColumn DELIVERY_ID = new QueryColumn(this, "delivery_id");

    /**
     * 审核状态（0：未通过；1：已通过；2：审核中）
     */
    public final QueryColumn EXAMINE_STATE = new QueryColumn(this, "examine_state");

    /**
     * 理由
     */
    public final QueryColumn REASON = new QueryColumn(this, "reason");

    /**
     * 管理员ID
     */
    public final QueryColumn ADMIN_ID = new QueryColumn(this, "admin_id");

    /**
     * 审核时间
     */
    public final QueryColumn AUDIT_TIME = new QueryColumn(this, "audit_time");

    /**
     * 总提现金额
     */
    public final QueryColumn ALL_TOTAL = new QueryColumn(this, "all_total");

    /**
     * 提现手续费
     */
    public final QueryColumn WITHDRAWAL_FEES = new QueryColumn(this, "withdrawal_fees");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, TYPE, USER_ID, DELIVERY_ID, SHOP_ID, TOTAL, CREATE_TIME, STATE, EXAMINE_STATE, REASON, ADMIN_ID, AUDIT_TIME, ALL_TOTAL, WITHDRAWAL_FEES};

    public ComWithdrawalFlowTable() {
        super("", "t_com_withdrawal_flow");
    }

}
