package com.project.school_flate.entity.table.delivery;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class DeliveryExamineTable extends TableDef {

    /**
     * 
     */
    public static final DeliveryExamineTable DELIVERY_EXAMINE = new DeliveryExamineTable();

    /**
     * 配送员审核ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 是否通过（0：未通过；1：已通过；2：审核中）
     */
    public final QueryColumn IS_PASS = new QueryColumn(this, "is_pass");

    /**
     * 理由
     */
    public final QueryColumn REASON = new QueryColumn(this, "reason");

    /**
     * 配送员ID
     */
    public final QueryColumn DELIVERY_ID = new QueryColumn(this, "delivery_id");

    /**
     * 管理员ID
     */
    public final QueryColumn ADMIN_ID = new QueryColumn(this, "admin_id");

    /**
     * 审核时间
     */
    public final QueryColumn AUDIT_TIME = new QueryColumn(this, "audit_time");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 性别（0：女；1：男）
     */
    public final QueryColumn SEX = new QueryColumn(this, "sex");

    /**
     * 手机号
     */
    public final QueryColumn PHONE = new QueryColumn(this, "phone");

    /**
     * 姓名
     */
    public final QueryColumn FULL_NAME = new QueryColumn(this, "full_name");

    /**
     * 身份证号
     */
    public final QueryColumn ID_CARD = new QueryColumn(this, "id_card");

    /**
     * 身份证正面
     */
    public final QueryColumn ID_FRONT = new QueryColumn(this, "id_front");

    /**
     * 入驻校区ID
     */
    public final QueryColumn SCHOOL_ID = new QueryColumn(this, "school_id");

    /**
     * 身份证反面
     */
    public final QueryColumn ID_OPPOSITE = new QueryColumn(this, "id_opposite");

    /**
     * 学生证
     */
    public final QueryColumn ID_STUDENT = new QueryColumn(this, "id_student");

    /**
     * 工作类型（0：兼职；1：全职）
     */
    public final QueryColumn WORK_TYPE = new QueryColumn(this, "work_type");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, DELIVERY_ID, ADMIN_ID, IS_PASS, REASON, CREATE_TIME, AUDIT_TIME, SEX, PHONE, ID_FRONT, SCHOOL_ID, ID_OPPOSITE, ID_STUDENT, FULL_NAME, WORK_TYPE};

    public DeliveryExamineTable() {
        super("", "t_delivery_examine");
    }

}
