package com.project.school_flate.entity.table.com;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ComImageTable extends TableDef {

    /**
     *
     */
    public static final ComImageTable COM_IMAGE = new ComImageTable();

    /**
     * 系统图ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 图片
     */
    public final QueryColumn IMAGE = new QueryColumn(this, "image");

    /**
     * 类型（0：轮播图；1：会员图；2：推广图）
     */
    public final QueryColumn TYPE = new QueryColumn(this, "type");

    /**
     * 链接
     */
    public final QueryColumn LINK = new QueryColumn(this, "link");

    /**
     * 链接类型（0：无参页面；1：有参页面；2：tab页；3：视频）
     */
    public final QueryColumn LINK_TYPE = new QueryColumn(this, "link_type");

    /**
     * 参数
     */
    public final QueryColumn PARAMETER = new QueryColumn(this, "parameter");

    /**
     * 颜色
     */
    public final QueryColumn COLOR = new QueryColumn(this, "color");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, IMAGE, TYPE, LINK, LINK_TYPE, PARAMETER, COLOR};

    public ComImageTable() {
        super("", "t_com_image");
    }

}
