package com.project.school_flate.entity.table.integral;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class IntegralExchangeSaveTable extends TableDef {

    /**
     * 
     */
    public static final IntegralExchangeSaveTable INTEGRAL_EXCHANGE_SAVE = new IntegralExchangeSaveTable();

    /**
     * 积分兑换储蓄ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 金额
     */
    public final QueryColumn PRICE = new QueryColumn(this, "price");

    /**
     * 店铺ID
     */
    public final QueryColumn SHOP_ID = new QueryColumn(this, "shop_id");

    /**
     * 积分
     */
    public final QueryColumn INTEGRAL = new QueryColumn(this, "integral");

    /**
     * 是否推荐（0：否；1：是）
     */
    public final QueryColumn IS_RECOMMEND = new QueryColumn(this, "is_recommend");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, SHOP_ID, INTEGRAL, PRICE, IS_RECOMMEND};

    public IntegralExchangeSaveTable() {
        super("", "t_integral_exchange_save");
    }

}
