package com.project.school_flate.entity.table.com;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ComSchoolTable extends TableDef {

    /**
     * 
     */
    public static final ComSchoolTable COM_SCHOOL = new ComSchoolTable();

    /**
     * 校园ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 位置
     */
    public final QueryColumn ADDRESS = new QueryColumn(this, "address");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 状态
     */
    public final QueryColumn STATE = new QueryColumn(this, "state");

    /**
     * 经度
     */
    public final QueryColumn LONGITUDE = new QueryColumn(this, "longitude");

    /**
     * 纬度
     */
    public final QueryColumn LATITUDE = new QueryColumn(this, "latitude");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, NAME, ADDRESS, CREATE_TIME, STATE, LONGITUDE, LATITUDE};

    public ComSchoolTable() {
        super("", "t_com_school");
    }

}
