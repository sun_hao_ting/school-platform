package com.project.school_flate.entity.table.shop;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ShopCommodityTable extends TableDef {

    /**
     *
     */
    public static final ShopCommodityTable SHOP_COMMODITY = new ShopCommodityTable();

    /**
     * 商品ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 图片
     */
    public final QueryColumn IMAGE = new QueryColumn(this, "image");

    /**
     * 状态（0：上架；1：下架；2：售完）
     */
    public final QueryColumn STATE = new QueryColumn(this, "state");

    /**
     * 海报
     */
    public final QueryColumn POSTER = new QueryColumn(this, "poster");

    /**
     * 店铺ID
     */
    public final QueryColumn SHOP_ID = new QueryColumn(this, "shop_id");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 会员价
     */
    public final QueryColumn MEMBER_PRICE = new QueryColumn(this, "member_price");

    /**
     * 销量
     */
    public final QueryColumn SALES_VOLUME = new QueryColumn(this, "sales_volume");

    /**
     * 原价
     */
    public final QueryColumn ORIGINAL_PRICE = new QueryColumn(this, "original_price");

    /**
     * 商品分类ID
     */
    public final QueryColumn COMMODITY_TYPE_ID = new QueryColumn(this, "commodity_type_id");

    /**
     * 评分
     */
    public final QueryColumn SCORE = new QueryColumn(this, "score");

    /**
     * 评价模板
     */
    public final QueryColumn EVALUATE_TEMPLATE = new QueryColumn(this, "evaluate_template");

    /**
     * 排序
     */
    public final QueryColumn NO = new QueryColumn(this, "no");

    /**
     * 是否商家推荐（0：否；1：是）
     */
    public final QueryColumn IS_SHOP_RECOMMEND = new QueryColumn(this, "is_shop_recommend");

    /**
     * 是否平台推荐（0：否；1：是）
     */
    public final QueryColumn IS_SYSTEM_RECOMMEND = new QueryColumn(this, "is_system_recommend");

    /**
     * 储蓄折扣比例
     */
    public final QueryColumn SAVE_DISCOUNT_RATIO = new QueryColumn(this, "save_discount_ratio");

    /**
     * 是否参与储蓄折扣（0：否；1：是）
     */
    public final QueryColumn IS_IN_SAVE = new QueryColumn(this, "is_in_save");

    /**
     * 平台推荐价格
     */
    public final QueryColumn SYSTEM_RECOMMEND_PRICE = new QueryColumn(this, "system_recommend_price");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 评价次数
     */
    public final QueryColumn EVALUATE_NUM = new QueryColumn(this, "evaluate_num");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, NAME, SHOP_ID, COMMODITY_TYPE_ID, STATE, SALES_VOLUME, ORIGINAL_PRICE, MEMBER_PRICE, IMAGE, POSTER, CREATE_TIME, SCORE, EVALUATE_TEMPLATE, NO, IS_SHOP_RECOMMEND, IS_SYSTEM_RECOMMEND, SAVE_DISCOUNT_RATIO, IS_IN_SAVE, EVALUATE_NUM};

    public ShopCommodityTable() {
        super("", "t_shop_commodity");
    }

}
