package com.project.school_flate.entity.table.shop;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ShopExamineTable extends TableDef {

    /**
     * 
     */
    public static final ShopExamineTable SHOP_EXAMINE = new ShopExamineTable();

    /**
     * 店铺审核ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 是否通过（0：未通过；1：已通过）
     */
    public final QueryColumn IS_PASS = new QueryColumn(this, "is_pass");

    /**
     * 理由
     */
    public final QueryColumn REASON = new QueryColumn(this, "reason");

    /**
     * 店铺ID
     */
    public final QueryColumn SHOP_ID = new QueryColumn(this, "shop_id");

    /**
     * 管理员ID
     */
    public final QueryColumn ADMIN_ID = new QueryColumn(this, "admin_id");

    /**
     * 审核时间
     */
    public final QueryColumn AUDIT_TIME = new QueryColumn(this, "audit_time");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 店铺账号ID
     */
    public final QueryColumn ACCOUNT_ID = new QueryColumn(this, "account_id");

    /**
     * 地址
     */
    public final QueryColumn ADDRESS = new QueryColumn(this, "address");

    /**
     * 主营业务
     */
    public final QueryColumn MAIN_BUSINESS = new QueryColumn(this, "main_business");

    /**
     * 营业执照
     */
    public final QueryColumn BUSINESS_LICENSE = new QueryColumn(this, "business_license");

    /**
     * 经营许可证
     */
    public final QueryColumn OPERATE_PERMIT = new QueryColumn(this, "operate_permit");

    /**
     * 入驻校区ID
     */
    public final QueryColumn SCHOOL_ID = new QueryColumn(this, "school_id");

    /**
     * 姓名
     */
    public final QueryColumn FULL_NAME = new QueryColumn(this, "full_name");

    /**
     * 身份证号
     */
    public final QueryColumn ID_CARD = new QueryColumn(this, "id_card");

    /**
     * 身份证正面
     */
    public final QueryColumn ID_FRONT = new QueryColumn(this, "id_front");

    /**
     * 身份证反面
     */
    public final QueryColumn ID_OPPOSITE = new QueryColumn(this, "id_opposite");

    /**
     * 手机号
     */
    public final QueryColumn PHONE = new QueryColumn(this, "phone");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, SHOP_ID, ADMIN_ID, IS_PASS, REASON, CREATE_TIME, AUDIT_TIME, NAME, ACCOUNT_ID, ADDRESS, MAIN_BUSINESS, BUSINESS_LICENSE, OPERATE_PERMIT, SCHOOL_ID, FULL_NAME, ID_CARD, ID_FRONT, ID_OPPOSITE, PHONE};

    public ShopExamineTable() {
        super("", "t_shop_examine");
    }

}
