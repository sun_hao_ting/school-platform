package com.project.school_flate.entity.table.shop;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ShopCommoditySpecsTable extends TableDef {

    /**
     *
     */
    public static final ShopCommoditySpecsTable SHOP_COMMODITY_SPECS = new ShopCommoditySpecsTable();

    /**
     * 商品规格ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 商品规格类型ID
     */
    public final QueryColumn COMMODITY_SPECS_TYPE_ID = new QueryColumn(this, "commodity_specs_type_id");

    /**
     * 会员价
     */
    public final QueryColumn MEMBER_PRICE = new QueryColumn(this, "member_price");

    /**
     * 原价
     */
    public final QueryColumn ORIGINAL_PRICE = new QueryColumn(this, "original_price");

    /**
     * 商品ID
     */
    public final QueryColumn COMMODITY_ID = new QueryColumn(this, "commodity_id");

    /**
     * 是否还有库存（0：没有；1：有）
     */
    public final QueryColumn IS_INVENTORY = new QueryColumn(this, "is_inventory");

    /**
     * 储蓄折扣比例
     */
    public final QueryColumn SAVE_DISCOUNT_RATIO = new QueryColumn(this, "save_discount_ratio");

    /**
     * 是否参与储蓄折扣（0：否；1：是）
     */
    public final QueryColumn IS_IN_SAVE = new QueryColumn(this, "is_in_save");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, NAME, COMMODITY_SPECS_TYPE_ID, ORIGINAL_PRICE, MEMBER_PRICE, CREATE_TIME, COMMODITY_ID, SAVE_DISCOUNT_RATIO, IS_IN_SAVE};

    public ShopCommoditySpecsTable() {
        super("", "t_shop_commodity_specs");
    }

}
