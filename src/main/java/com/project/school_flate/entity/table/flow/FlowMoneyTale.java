package com.project.school_flate.entity.table.flow;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

public class FlowMoneyTale extends TableDef {
    public static final FlowMoneyTale FLOW_MONEY_TALE = new FlowMoneyTale();
    /**
     * ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 商店ID
     */
    public final QueryColumn SHOP_ID = new QueryColumn(this, "shop_id");

    /**
     * 骑手ID
     */
    public final QueryColumn DELIVERY_ID = new QueryColumn(this, "delivery_id");

    /**
     * 客户ID
     */
    public final QueryColumn USER_ID = new QueryColumn(this, "user_id");

    /**
     * 订单ID
     */
    public final QueryColumn ORDER_ID = new QueryColumn(this, "order_id");

    /**
     * 状态（0、未完成；1、已完成；）
     */
    public final QueryColumn STATE = new QueryColumn(this, "state");

    /**
     * 类型（0、订单支付；1、充值储蓄；2、提现；3：分销商返利；4：成为分销商；5、推广储蓄返利；6：绑定手机号）
     */
    public final QueryColumn TYPE = new QueryColumn(this, "type");

    /**
     * 金额
     */
    public final QueryColumn MOMEY = new QueryColumn(this, "money");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 创建时间
     */
    public final QueryColumn INFLOW_DIRECTION = new QueryColumn(this, "inflow_direction");

    /**
     * 流入方式（0：支付；1：积分）
     */
    public final QueryColumn INFLOW_WAY = new QueryColumn(this, "inflow_way");

    /**
     * 积分
     */
    public final QueryColumn INTEGRAL = new QueryColumn(this, "integral");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID,SHOP_ID,USER_ID,STATE, DELIVERY_ID, MOMEY,ORDER_ID, INFLOW_DIRECTION, CREATE_TIME, INFLOW_WAY, INTEGRAL};

    public FlowMoneyTale() {
        super("", "t_flow_money");
    }
}
