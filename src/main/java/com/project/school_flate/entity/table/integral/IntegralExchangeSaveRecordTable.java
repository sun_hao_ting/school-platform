package com.project.school_flate.entity.table.integral;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class IntegralExchangeSaveRecordTable extends TableDef {

    /**
     * 
     */
    public static final IntegralExchangeSaveRecordTable INTEGRAL_EXCHANGE_SAVE_RECORD = new IntegralExchangeSaveRecordTable();

    /**
     * 积分兑换储蓄记录ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 金额
     */
    public final QueryColumn PRICE = new QueryColumn(this, "price");

    /**
     * 商铺ID
     */
    public final QueryColumn SHOP_ID = new QueryColumn(this, "shop_id");

    /**
     * 用户ID
     */
    public final QueryColumn USER_ID = new QueryColumn(this, "user_id");

    /**
     * 积分
     */
    public final QueryColumn INTEGRAL = new QueryColumn(this, "integral");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, USER_ID, SHOP_ID, INTEGRAL, PRICE, CREATE_TIME};

    public IntegralExchangeSaveRecordTable() {
        super("", "t_integral_exchange_save_record");
    }

}
