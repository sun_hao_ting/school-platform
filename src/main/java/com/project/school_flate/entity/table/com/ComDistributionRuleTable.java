package com.project.school_flate.entity.table.com;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ComDistributionRuleTable extends TableDef {

    /**
     * 
     */
    public static final ComDistributionRuleTable COM_DISTRIBUTION_RULE = new ComDistributionRuleTable();

    /**
     * 分销商等级规则ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 类型（0：普通分销商；1：专业分销商）
     */
    public final QueryColumn TYPE = new QueryColumn(this, "type");

    /**
     * 等级
     */
    public final QueryColumn GRADE = new QueryColumn(this, "grade");

    /**
     * 升级收益条件
     */
    public final QueryColumn INCOME = new QueryColumn(this, "income");

    /**
     * 提成比例
     */
    public final QueryColumn COMMISSION = new QueryColumn(this, "commission");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, GRADE, COMMISSION, INCOME, TYPE};

    public ComDistributionRuleTable() {
        super("", "t_com_distribution_rule");
    }

}
