package com.project.school_flate.entity.table.user;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class UserInfoTable extends TableDef {

    /**
     *
     */
    public static final UserInfoTable USER_INFO = new UserInfoTable();

    /**
     * 用户ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 验证码
     */
    public final QueryColumn CODE = new QueryColumn(this, "code");

    /**
     * 名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 图片
     */
    public final QueryColumn IMAGE = new QueryColumn(this, "image");

    /**
     * 手机号
     */
    public final QueryColumn PHONE = new QueryColumn(this, "phone");

    /**
     * 积分
     */
    public final QueryColumn INTEGRAL = new QueryColumn(this, "integral");

    /**
     * 储蓄累计节省
     */
    public final QueryColumn SAVINGS_PRICE = new QueryColumn(this, "savings_price");

    /**
     * 微信optionid
     */
    public final QueryColumn WX_OPTION_ID = new QueryColumn(this, "wx_option_id");

    /**
     * 消费费用
     */
    public final QueryColumn CONSUMPTION_PRICE = new QueryColumn(this, "consumption_price");

    /**
     * 是否返利（0：否；1：是）
     */
    public final QueryColumn IS_REBATE = new QueryColumn(this, "is_rebate");

    /**
     * 分销金额
     */
    public final QueryColumn DISTRIBUTION_PRICE = new QueryColumn(this, "distribution_price");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 支付提现密码
     */
    public final QueryColumn PAY_PASSWORD = new QueryColumn(this, "pay_password");

    /**
     * 邀请码（推荐用户ID）
     */
    public final QueryColumn INVITATION_CODE = new QueryColumn(this, "invitation_code");

    /**
     * 是否是分销商（0：普通分销商；1：专业分销商；3：不是）
     */
    public final QueryColumn IS_DISTRIBUTION = new QueryColumn(this, "is_distribution");

    /**
     * 分销商等级
     */
    public final QueryColumn DISTRIBUTION_GRADE = new QueryColumn(this, "distribution_grade");

    /**
     * 余额
     */
    public final QueryColumn BALANCE = new QueryColumn(this, "balance");

    /**
     * 提现总额
     */
    public final QueryColumn WITHDRAWAL_TOTAL = new QueryColumn(this, "withdrawal_total");

    /**
     * 状态（0、冻结；1、存在）
     */
    public final QueryColumn STATE = new QueryColumn(this, "state");

    /**
     * 邀请人数
     */
    public final QueryColumn INVITATION_NUM = new QueryColumn(this, "invitation_num");

    /**
     * 邀请储蓄会员人数
     */
    public final QueryColumn INVITATION_SAVE_NUM = new QueryColumn(this, "invitation_save_num");

    /**
     * 微信通用ID
     */
    public final QueryColumn UNION_ID = new QueryColumn(this, "union_id");

    /**
     * 暂存邀请码（只用作推广码识别）
     */
    public final QueryColumn TEMP_CODE = new QueryColumn(this, "temp_code");

    /**
     * 提现次数
     */
    public final QueryColumn WITHDRAWAL_NUM = new QueryColumn(this, "withdrawal_num");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, NAME, PHONE, WX_OPTION_ID, CODE, CONSUMPTION_PRICE, IMAGE, INTEGRAL, SAVINGS_PRICE, IS_REBATE, DISTRIBUTION_PRICE, CREATE_TIME, PAY_PASSWORD, INVITATION_CODE, IS_DISTRIBUTION, DISTRIBUTION_GRADE, BALANCE, WITHDRAWAL_TOTAL, STATE, INVITATION_NUM, INVITATION_SAVE_NUM, UNION_ID, TEMP_CODE, WITHDRAWAL_NUM};

    public UserInfoTable() {
        super("", "t_user_info");
    }

}
