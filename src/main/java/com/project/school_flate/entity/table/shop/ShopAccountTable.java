package com.project.school_flate.entity.table.shop;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 *  表定义层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public class ShopAccountTable extends TableDef {

    /**
     * 
     */
    public static final ShopAccountTable SHOP_ACCOUNT = new ShopAccountTable();

    /**
     * 店铺账号ID
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 手机号
     */
    public final QueryColumn PHONE = new QueryColumn(this, "phone");

    /**
     * 密码
     */
    public final QueryColumn PASSWORD = new QueryColumn(this, "password");

    /**
     * 状态（0、冻结；1、存在）
     */
    public final QueryColumn STATE = new QueryColumn(this, "state");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 图片
     */
    public final QueryColumn IMAGE = new QueryColumn(this, "image");

    /**
     * 名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 微信optionid
     */
    public final QueryColumn WX_OPTION_ID = new QueryColumn(this, "wx_option_id");

    /**
     * 微信通用ID
     */
    public final QueryColumn UNION_ID = new QueryColumn(this, "union_id");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, PHONE, PASSWORD, STATE, CREATE_TIME, WX_OPTION_ID, UNION_ID};

    public ShopAccountTable() {
        super("", "t_shop_account");
    }

}
