package com.project.school_flate.util.wechat;

import com.mybatisflex.annotation.Column;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PayDto implements Serializable {

    private static final long serialVersionUID = 7153892323333569602L;

    @Column(ignore = true)
    @Schema(title = "订单描述",name = "body")
    private String body;

    @Column(ignore = true)
    @Schema(title = "订单号",name = "outTradeNo")
    private String outTradeNo;

    @Column(ignore = true)
    @Schema(title = "价格（分）",name = "totalFee")
    private Double totalFee;

    @Column(ignore = true)
    @Schema(title = "用户ID",name = "userId")
    private String userId;

    @Column(ignore = true)
    @Schema(title = "支付ID",name = "prepayId")
    private String prepayId;

    @Column(ignore = true)
    @Schema(title = "用户终端IP",name = "payerClientIp")
    private String payerClientIp;

    @Column(ignore = true)
    @Schema(title = "支付终端",name = "h5InfoType")
    private String h5InfoType;

    @Column(ignore = true)
    @Schema(title = "场景类型（0：外卖订单微信支付；1：储蓄充值；2：成为专业分销商；3：外卖订单退款）",name = "sceneType")
    private Integer sceneType;

    @Column(ignore = true)
    @Schema(title = "附加数据",name = "attach")
    private String attach;

    @Column(ignore = true)
    @Schema(title = "退款结果回调",name = "refundsNotifyUrl")
    private String refundsNotifyUrl;

    @Column(ignore = true)
    @Schema(title = "退款金额",name = "refundsRefund")
    private Double refundsRefund;

    @Column(ignore = true)
    @Schema(title = "退款原订单金额",name = "refundsTotal")
    private Double refundsTotal;

    @Column(ignore = true)
    @Schema(title = "退款用户ID",name = "refundsUserId")
    private String refundsUserId;

    @Column(ignore = true)
    @Schema(title = "当前网页的URL",name = "thisUrl")
    private String thisUrl;

    @Column(ignore = true)
    @Schema(title = "转账金额",name = "totalAmount")
    private Double totalAmount;

    @Column(ignore = true)
    @Schema(title = "商家提现批次单号",name = "outBatchNo")
    private String outBatchNo;

    @Column(ignore = true)
    @Schema(title = "商户系统内部区分转账批次单下不同转账明细单的唯一标识",name = "outDetailNo")
    private String outDetailNo;

    @Column(ignore = true)
    @Schema(title = "该笔批量转账的名称",name = "batchName")
    private String batchName;

    @Column(ignore = true)
    @Schema(title = "异步接收微信支付结果通知的回调地址",name = "notifyUrl")
    private String notifyUrl;

    @Column(ignore = true)
    @Schema(title = "收款用户openid",name = "notifyUrl")
    private String openid;


}
