package com.project.school_flate.util.system;

import com.mybatisflex.core.query.QueryWrapper;
import com.project.school_flate.entity.com.ComVariable;
import com.project.school_flate.entity.table.com.ComVariableTable;
import com.project.school_flate.mapper.com.ComVariableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ComVariableUtil {
    @Autowired
    private ComVariableMapper comVariableMapper;

    private static ComVariableUtil comVariableUtil;

    //第四步创建一个初始化方法，用于注入实体类，使用 @PostConstruct
    @PostConstruct
    public void init(){
        comVariableUtil = this;
        //第五步，调用你需要使用的mapper
        comVariableUtil.comVariableMapper = this.comVariableMapper;
    }

    public static String getSystemValue(String key){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(ComVariableTable.COM_VARIABLE.SYSTEM_TITLE.eq(key));
        ComVariable variable = comVariableUtil.comVariableMapper.selectOneByQuery(queryWrapper);
        return variable.getSystemValues();
    }

}
