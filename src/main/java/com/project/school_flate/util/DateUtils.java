package com.project.school_flate.util;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DateUtils {

    //date转String
    public static String dateTurnString(Date date, String type) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat(type);
        return formatter.format(date);
    }

    //String转date   "yyyy-MM-dd HH:mm:ss"
    public static Date stringTurnDate(String dateStr, String type) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat(type);
        return formatter.parse(dateStr);
    }

    /**
     * 比较两个时间的大小
     *
     * @param start 开始时间
     * @param end   结束时间
     * @return
     */
    public static boolean checkTwoDate(Date start, Date end) {
        if (start.before(end)) {
            //表示start小于end
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取当前时间之前几分钟的时间
     *
     * @param minute 分钟
     * @return
     */
    public static Date getNowMinute(Integer minute) {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MINUTE, -minute);
        return now.getTime();
    }

    /**
     * 获取昨天的开始时间和结束时间
     *
     * @param minute 分钟
     * @return
     */
    public static Map<String,Date> getYesterdayTime() {
        // 创建Calendar实例
        Calendar calendar = Calendar.getInstance();
        // 获取当前日期
        calendar.setTimeInMillis(System.currentTimeMillis());
        // 将日期减去1天
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        // 获取昨天的开始时间
        Date startTime = calendar.getTime();
        // 设置时间为23:59:59
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        // 获取昨天的结束时间
        Date endTime = calendar.getTime();
        Map<String,Date> map = new HashMap<>();
        map.put("startTime",startTime);
        map.put("endTime",endTime);
        return map;
    }

    /**
     * 获取今天的开始时间和结束时间
     *
     * @param minute 分钟
     * @return
     */
    public static Map<String,Date> getTodayTime() {
        // 创建Calendar实例
        Calendar calendar = Calendar.getInstance();
        // 获取当前日期
        calendar.setTimeInMillis(System.currentTimeMillis());
        // 将日期减去1天
        calendar.add(Calendar.DAY_OF_MONTH, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        // 获取昨天的开始时间
        Date startTime = calendar.getTime();
        // 设置时间为23:59:59
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        // 获取昨天的结束时间
        Date endTime = calendar.getTime();
        Map<String,Date> map = new HashMap<>();
        map.put("startTime",startTime);
        map.put("endTime",endTime);
        return map;
    }

    /**
     * 获取这个月的开始时间和结束时间
     *
     * @param minute 分钟
     * @return
     */
    public static Map<String,Date> getMonthdayTime() throws Exception {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Calendar.DAY_OF_MONTH, 1);
        calendar1.set(Calendar.HOUR_OF_DAY, 0);
        calendar1.set(Calendar.MINUTE, 0);
        calendar1.set(Calendar.SECOND, 0);
        calendar1.set(Calendar.MILLISECOND, 0);

        Calendar calendar2 = Calendar.getInstance();
        calendar2.set(Calendar.DAY_OF_MONTH, calendar2.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar2.set(Calendar.HOUR_OF_DAY, 23);
        calendar2.set(Calendar.MINUTE, 59);
        calendar2.set(Calendar.SECOND, 59);
        calendar2.set(Calendar.MILLISECOND, 999);
        Map<String,Date> map = new HashMap<>();
        map.put("startTime",calendar1.getTime());
        map.put("endTime",calendar2.getTime());
        return map;
    }

    /**
     * 判断两个时间是否大于一天
     *
     * @param minute 分钟
     * @return
     */
    public static Boolean getTimeGeDay(Date date1,Date date2) throws Exception {
        long between = date2.getTime() - date1.getTime();
        if(between >= (24* 3600000)){
            return true;
        }
        return false;
    }

    public static void main(String[] args) throws Exception {
        String date1 = "2024-04-01 11:11:11";
        String date2 = "2024-04-02 11:11:12";
        System.out.println(getTimeGeDay(stringTurnDate(date1,"yyyy-MM-dd HH:mm:ss"),stringTurnDate(date2,"yyyy-MM-dd HH:mm:ss")));
    }

}
