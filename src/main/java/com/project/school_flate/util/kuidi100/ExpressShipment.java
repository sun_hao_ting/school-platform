package com.project.school_flate.util.kuidi100;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data
public class ExpressShipment implements Serializable {
    private static final long serialVersionUID = 3421356668548846991L;
    @Schema(title = "订单编号",name = "orderId")
    private String orderId;
    //递公司的编码，一律用小写字母，见《快递公司编码》
    @Schema(title = "递公司的编码，一律用小写字母，见《快递公司编码》",name = "kuaidicom")
    private String kuaidicom;
    //收件人姓名
    @Schema(title = "收件人姓名",name = "recManName")
    private String recManName;
    //收件人的手机号，手机号和电话号二者其一必填
    @Schema(title = "收件人的手机号，手机号和电话号二者其一必填",name = "recManMobile")
    private String recManMobile;
    //收件人所在完整地址，如广东深圳市深圳市南山区科技南十二路2号金蝶软件园
    @Schema(title = "收件人所在完整地址，如广东深圳市深圳市南山区科技南十二路2号金蝶软件园",name = "recManPrintAddr")
    private String recManPrintAddr;
    //寄件人姓名
    @Schema(title = "寄件人姓名",name = "sendManName")
    private String sendManName;
    @Schema(title = "寄件人的手机号，手机号和电话号二者其一必填",name = "sendManMobile")
    private String sendManMobile;
    @Schema(title = "寄件人所在的完整地址，如广东深圳市深圳市南山区科技南十二路2号金蝶软件园B10",name = "sendManPrintAddr")
    private String sendManPrintAddr;
    @Schema(title = "callBackUrl订单信息回调地址",name = "callBackUrl")
    private String callBackUrl;

}
