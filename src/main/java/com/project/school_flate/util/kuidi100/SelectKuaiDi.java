package com.project.school_flate.util.kuidi100;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data
public class SelectKuaiDi implements Serializable {
    private static final long serialVersionUID = 3899915075084088344L;
    @Schema(title = "快递商编码",name = "com")
    private String com;
    @Schema(title = "快递编码",name = "num")
    private String num;
    @Schema(title = "电话号码",name = "phone")
    private String phone;
}
