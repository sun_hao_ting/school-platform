package com.project.school_flate.util.kuidi100;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 快递100 智能识别返回结果 List<KuaiDiCode 按相似度排序（高的在前）
 * @author wangsong
 * @mail 1720696548@qq.com
 * @date 2020/9/16 0016 13:45
 * @version 1.0.0
 */
@Data
@ToString
public class KuaiDiCode implements Serializable {

    private static final long serialVersionUID = 465402578016234428L;
    private String comCode;
    private String id;
    private Integer noCount;
    private String noPre;
    private LocalDateTime startTime;

}
