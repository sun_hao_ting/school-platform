package com.project.school_flate.util.Result;

/**
 * @Author sunht
 * @Date 2023/5/29 21:35
 * @Version v1.0.0
 * @Message
 **/
public interface BaseErrorInfoInterface {
    /**
     * 错误码
     */
    Integer getResultCode();
    /**
     * 错误描述
     */
    String getResultMsg();
}
