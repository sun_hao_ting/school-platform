package com.project.school_flate.util.Result;

import lombok.Getter;

/**
 * @Author sunht
 * @Date 2023/5/29 21:35
 * @Version v1.0.0
 * @Message
 **/
@Getter
public enum ResultCodeEnum implements BaseErrorInfoInterface{
    SUCCESS(true, 200, "成功"),
    NULL_POINTER(false, 500, "空指针异常"),
    UN_KNOW(false, 500, "服务器未知内部错误"),
    COMMON_FAIL(false, 500, "失败"),
    NOT_LOGIN(false, 509, "请先登录");
    private Boolean success;
    private Integer code;
    private String message;
    private ResultCodeEnum(Boolean success, Integer code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }
    //重写父接口的抽象方法，返回错误码
    @Override
    public Integer getResultCode() {
        return code;
    }
    //重写父接口的抽象方法，返回错误信息
    @Override
    public String getResultMsg() {
        return message;
    }
}
