package com.project.school_flate.util.Result;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * @Author sunht
 * @Date 2023/5/29 21:36
 * @Version v1.0.0
 * @Message
 **/
@Tag(name = "返回信息", description = "返回信息实体")
@Data
public class Result implements Serializable {
    private static final long serialVersionUID = 4138564106859106097L;
    @Schema(name = "成功",type = "boolean")
    private Boolean success;
    @Schema(name = "接口返回码",type = "int")
    private Integer code;
    @Schema(name = "接口返回消息",type = "String")
    private String message;
    @Schema(name = "接口返回数据",type = "data")
    private Object data;
    @Schema(name = "接口返回数据(List)类型",type = "dataList")
    private List dataList;
    @Schema(name = "总数",type = "total")
    private Long total;

    //请求成功时的返回值
    public static Result ok() {
        Result r = new Result();
        r.setSuccess(ResultCodeEnum.SUCCESS.getSuccess());
        r.setCode(ResultCodeEnum.SUCCESS.getCode());
        r.setMessage(ResultCodeEnum.SUCCESS.getMessage());
        return r;
    }

    //请求成功时的返回值
    public static Result ok(Object data) {
        Result r = new Result();
        r.setSuccess(ResultCodeEnum.SUCCESS.getSuccess());
        r.setCode(ResultCodeEnum.SUCCESS.getCode());
        r.setMessage(ResultCodeEnum.SUCCESS.getMessage());
        r.setData(data);
        return r;
    }
    //请求成功时的返回值
    public static Result ok(List dataList,Long total) {
        Result r = new Result();
        r.setSuccess(ResultCodeEnum.SUCCESS.getSuccess());
        r.setCode(ResultCodeEnum.SUCCESS.getCode());
        r.setMessage(ResultCodeEnum.SUCCESS.getMessage());
        r.setDataList(dataList);
        r.setTotal(total);
        return r;
    }
    //请求成功时的返回值
    public static Result ok(List dataList,Integer total) {
        Result r = new Result();
        r.setSuccess(ResultCodeEnum.SUCCESS.getSuccess());
        r.setCode(ResultCodeEnum.SUCCESS.getCode());
        r.setMessage(ResultCodeEnum.SUCCESS.getMessage());
        r.setDataList(dataList);
        r.setTotal(total.longValue());
        return r;
    }
    //请求失败时的返回值
    public static Result fail() {
        Result r = new Result();
        r.setSuccess(ResultCodeEnum.COMMON_FAIL.getSuccess());
        r.setCode(ResultCodeEnum.COMMON_FAIL.getCode());
        r.setMessage(ResultCodeEnum.COMMON_FAIL.getMessage());
        return r;
    }

    //请求失败时的返回值
    public static Result fail(String strMessage) {
        Result r = new Result();
        r.setSuccess(ResultCodeEnum.COMMON_FAIL.getSuccess());
        r.setCode(ResultCodeEnum.COMMON_FAIL.getCode());
        r.setMessage(strMessage);
        return r;
    }

    //请求失败时的返回值
    public static Result notLogin() {
        Result r = new Result();
        r.setSuccess(ResultCodeEnum.NOT_LOGIN.getSuccess());
        r.setCode(ResultCodeEnum.NOT_LOGIN.getCode());
        r.setMessage(ResultCodeEnum.NOT_LOGIN.getMessage());
        return r;
    }

    //请求失败时的返回值
    public static Result notLogin(String message) {
        Result r = new Result();
        r.setSuccess(ResultCodeEnum.NOT_LOGIN.getSuccess());
        r.setCode(ResultCodeEnum.NOT_LOGIN.getCode());
        r.setMessage(message);
        return r;
    }

    public static Result setResult(ResultCodeEnum resultCodeEnum) {
        Result r = new Result();
        r.setSuccess(resultCodeEnum.getSuccess());
        r.setCode(resultCodeEnum.getCode());
        r.setMessage(resultCodeEnum.getMessage());
        return r;
    }

    public Result success(Boolean success) {
        this.setSuccess(success);
        return this;
    }

    public Result message(String message) {
        this.setMessage(message);
        return this;
    }

    public Result code(Integer code) {
        this.setCode(code);
        return this;
    }

    public Result data(Object value) {
        this.data = value;
        return this;
    }

    /**
     * @作者 sunhaoteng
     * @日期 2023/5/29
     * @描述 异常时调用
     */
    public static Result error(BaseErrorInfoInterface errorInfo) {
        Result r = new Result();
        r.setSuccess(false);
        r.setCode(errorInfo.getResultCode());
        r.setMessage(errorInfo.getResultMsg());
        return r;
    }

    /**
     * @作者 sunhaoteng
     * @日期 2023/5/29
     * @描述 异常时调用
     */
    public static Result error(Integer code, String message) {
        Result r = new Result();
        r.setSuccess(false);
        r.setCode(code);
        r.setMessage(message);
        return r;
    }

    /**
     * @作者 sunhaoteng
     * @日期 2023/5/29
     * @描述 异常时调用
     */
    public static Result error(String message) {
        Result r = new Result();
        r.setSuccess(false);
        r.setCode(500);
        r.setMessage(message);
        return r;
    }

    private Result() {
    }

    public Result(Boolean success, Integer code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    /**
     * @作者 sunhaoteng
     * @日期 2023/5/29
     * @描述 基于全局异常处理类封装的返回值
     */
    private Result(BaseErrorInfoInterface errorInfo) {
        this.code = errorInfo.getResultCode();
        this.message = errorInfo.getResultMsg();
    }
}
