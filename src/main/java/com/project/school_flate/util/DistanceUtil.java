package com.project.school_flate.util;

import org.gavaghan.geodesy.Ellipsoid;
import org.gavaghan.geodesy.GeodeticCalculator;
import org.gavaghan.geodesy.GeodeticCurve;
import org.gavaghan.geodesy.GlobalCoordinates;

import java.math.BigDecimal;

public class DistanceUtil {

    /**
     *
     * @param lat1 纬度1
     * @param lon1 经度1
     * @param lat2 纬度2
     * @param lon2 经度2
     * @return
     */
    public static double getLatAndLon(Double lat1, Double lon1, Double lat2, Double lon2) {
        GlobalCoordinates source = new GlobalCoordinates(lat1, lon1);
        GlobalCoordinates target = new GlobalCoordinates(lat2, lon2);
        double meter1 = getDistanceMeter(source, target, Ellipsoid.Sphere);
//        double meter2 = getDistanceMeter(source, target, Ellipsoid.WGS84);
//        System.out.println("Sphere坐标系计算结果：" + meter1 + "米");
//        System.out.println("WGS84坐标系计算结果：" + meter2 + "米");
        return meter1;
    }

    private static double getDistanceMeter(GlobalCoordinates gpsFrom, GlobalCoordinates gpsTo, Ellipsoid ellipsoid) {
        //创建GeodeticCalculator，调用计算方法，传入坐标系、经纬度用于计算距离
        GeodeticCurve geoCurve = new GeodeticCalculator().calculateGeodeticCurve(ellipsoid, gpsFrom, gpsTo);
        return geoCurve.getEllipsoidalDistance();
    }

    public static void main(String[] args) {
        System.out.println(getLatAndLon(30.552712, 104.075296, 30.55625, 104.052951));
        double latAndLon = getLatAndLon(30.552712, 104.075296, 30.55625, 104.052951);
        BigDecimal two1 = new BigDecimal(latAndLon / 1000);
        double three1 = two1.setScale(1,BigDecimal.ROUND_HALF_UP).doubleValue();
        System.out .println(three1);
    }

}
