package com.project.school_flate.config.tableListener;

import com.mybatisflex.annotation.InsertListener;
import com.mybatisflex.annotation.SetListener;
import com.mybatisflex.annotation.UpdateListener;
import com.project.school_flate.entity.com.ComVariable;
import com.project.school_flate.util.AesEncryptUtils;
import org.apache.commons.lang3.StringUtils;

public class ComVariableListener  implements SetListener, InsertListener, UpdateListener {

    @Override
    public Object onSet(Object entity, String property, Object value) {
        if("systemValues".equals(property)){
            try {
                return AesEncryptUtils.decrypt(value.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return value;
    }

    @Override
    public void onInsert(Object entity) {
        ComVariable oComVariable = (ComVariable) entity;
        try{
            if(StringUtils.isNotBlank(oComVariable.getSystemValues())){
                String VariableData = AesEncryptUtils.encrypt(oComVariable.getSystemValues());
                oComVariable.setSystemValues(VariableData);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onUpdate(Object entity) {
        ComVariable oComVariable = (ComVariable) entity;
        try{
            if(StringUtils.isNotBlank(oComVariable.getSystemValues())){
                String VariableData = AesEncryptUtils.encrypt(oComVariable.getSystemValues());
                oComVariable.setSystemValues(VariableData);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println(AesEncryptUtils.encrypt("量真的多,包装精良,精选好店,下次再来,超高折扣,口味很棒"));
    }

}
