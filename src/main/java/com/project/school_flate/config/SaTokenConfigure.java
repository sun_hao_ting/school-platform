package com.project.school_flate.config;

import cn.dev33.satoken.config.SaTokenConfig;
import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.dev33.satoken.stp.StpUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class SaTokenConfigure implements WebMvcConfigurer {

    @Bean
    public CorsInterceptor getCorsInterceptor(){
        return new CorsInterceptor();
    }

    // 注册拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getCorsInterceptor());
        // 注册 Sa-Token 拦截器，校验规则为 StpUtil.checkLogin() 登录校验。
        registry.addInterceptor(new SaInterceptor(handle -> StpUtil.checkLogin()))
                .addPathPatterns("/**")
                .excludePathPatterns("/login/wxClientLogin","/login/systemLogin",
                        "/shopType/getShopTypeUser",
                        "/tokenVerification",
                        "/comImage/getComImage",
                        "/comAdmin/loginComAdmin",
                        "/shopCommoditySpecs/getShopCommoditySpecsUser",
                        "/shopCommoditySpecsType/getShopCommoditySpecsUserType",
                        "/shopCommodityEvaluateLike/getShopCommodityEvaluateLikeUser",
                        "/shopCommodityType/getShopCommodityTypeUser",
                        "/shopCommodity/getShopCommodityUser","/shopCommodity/getRecommendShopCommodityUser",
                        "/shopCommodityEvaluate/getShopCommodityEvaluateUser",
                        "/deliveryInfo/registerDeliveryInfo","/deliveryInfo/loginDeliveryInfo","/deliveryInfo/exitDeliveryInfo","/deliveryInfo/loginDeliveryInfoOfficial",
                        "/shopInfo/registerShopInfo","/shopInfo/loginShopInfo","/shopInfo/exitShopInfo","/shopInfo/getShopInfoUser","/shopInfo/getRecommendShopInfoUser",
                        "/userInfo/loginUserInfoOfficial","/userInfo/becomeUserInfoSpecialityDistributionCallback",
                        "/orderTakeaway/wxPayOrderTakeawayCallback",
                        "/shopSaveUser/rechargeShopSaveUserCallback",
                        "/orderTakeawayCancellation/cancellationOrderTakeawayCallback",
                        "/comSchool/getComSchool",
                        "/comWithdrawalFlow/comWithdrawalFlowUserCallback",
                        "/shopAccount/registerShopAccount","/shopAccount/loginShopAccount","/shopAccount/loginShopAccountOfficial",
                        "/integralCommodityType/getIntegralCommodityTypeUser",
                        "/integralCommodity/getIntegralCommodityUser",
                        "/integralExchange/getIntegralExchangeSaveUser",
                        "/templates/**","/webjars/**","/swagger-ui.html","/doc.html","/v3/api-docs/**","/swagger-resources","/favicon.ico","/error",
                        "/userDocumentDown/**","/deliveryDocumentDown/**","/shopDocumentDown/**",
                        "/kafka/send1","/kafka/send2","/kafka/sendOut","/kafka/sendTopic");

    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        System.out.println("进入拦截器");
        registry.addResourceHandler( "/userDocumentDown/**").addResourceLocations("file:/home/school-platform/document/user/");
        registry.addResourceHandler( "/deliveryDocumentDown/**").addResourceLocations("file:/home/school-platform/document/delivery/");
        registry.addResourceHandler( "/shopDocumentDown/**").addResourceLocations("file:/home/school-platform/document/shop/");
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/static/")
                .addResourceLocations("classpath:/resources/");
        registry.addResourceHandler("/templates/**").addResourceLocations("classpath:/templates/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
        registry.addResourceHandler("swagger-ui.html","doc.html").addResourceLocations("classpath:/META-INF/resources/");
    }

    /**
     * 跨域支持
     * @author sunhaoteng
     * @date 2023/06/30
     *
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
//                .allowedOrigins("*")
                .allowedOriginPatterns("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "DELETE", "PUT", "PATCH", "OPTIONS", "HEAD")
                .maxAge(3600 * 24);
    }

}


