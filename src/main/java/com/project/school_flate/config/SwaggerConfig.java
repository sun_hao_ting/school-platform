package com.project.school_flate.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {
    // 设置 openapi 基础参数
    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .info(new Info()
                        .title("公共API管理")
                        .contact(new Contact().name("孙浩腾").email("sht0522@sina.com"))
                        .version("1.0")
                        .description( "公共API管理")
                        .license(new License().name("Java")));
    }
}

