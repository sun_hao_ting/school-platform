package com.project.school_flate.config;

import com.mybatisflex.core.FlexGlobalConfig;
import com.mybatisflex.core.audit.AuditManager;
import com.mybatisflex.core.logicdelete.LogicDeleteManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;
import java.util.Date;

@Configuration
public class MyBatisFlexConfiguration {

    private static final Logger logger = LoggerFactory
            .getLogger("mybatis-flex-sql");

    public MyBatisFlexConfiguration() {
        FlexGlobalConfig globalConfig = FlexGlobalConfig.getDefaultConfig();
        //设置数据库正常时的值
        globalConfig.setNormalValueOfLogicDelete("1");
        //设置数据已被删除时的值
        globalConfig.setDeletedValueOfLogicDelete("0");
        globalConfig.setLogicDeleteColumn("status");
        LogicDeleteManager.setProcessor(new DefaultLogicDeleteProcessor());
        //开启审计功能
        AuditManager.setAuditEnable(true);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //设置 SQL 审计收集器
        AuditManager.setMessageCollector(auditMessage ->
                logger.info("{},耗时：{}ms, 执行时间点：{},数据库IP:{}", auditMessage.getFullSql(), auditMessage.getElapsedTime(),sdf.format(new Date(auditMessage.getQueryTime())),auditMessage.getHostIp()));
    }
}
