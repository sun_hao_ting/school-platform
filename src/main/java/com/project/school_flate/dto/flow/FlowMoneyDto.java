package com.project.school_flate.dto.flow;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.flow.FlowMoney;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data
public class FlowMoneyDto extends FlowMoney implements Serializable {

    private static final long serialVersionUID = 2520116776951192220L;

    @Column(ignore = true)
    @Schema(description = "页数")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "条数")
    private Integer limit;

    /**
     * 配送人名称
     */
    @Column(ignore = true)
    @Schema(description = "配送人名称")
    private String deliveryName;

    /**
     * 用户名称
     */
    @Column(ignore = true)
    @Schema(description = "用户名称")
    private String userName;

    /**
     * 店铺名称
     */
    @Column(ignore = true)
    @Schema(description = "店铺名称")
    private String shopName;

    /**
     * 校区ID
     */
    @Column(ignore = true)
    @Schema(description = "校区ID")
    private String schoolId;

}
