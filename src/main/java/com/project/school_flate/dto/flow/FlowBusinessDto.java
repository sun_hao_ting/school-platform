package com.project.school_flate.dto.flow;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.flow.FlowBusiness;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data
public class FlowBusinessDto extends FlowBusiness implements Serializable {

    private static final long serialVersionUID = 1353564798594174816L;

    @Column(ignore = true)
    @Schema(description = "页数")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "条数")
    private Integer limit;

    /**
     * 配送人名称
     */
    @Column(ignore = true)
    @Schema(description = "配送人名称")
    private String deliveryName;

    /**
     * 用户名称
     */
    @Column(ignore = true)
    @Schema(description = "用户名称")
    private String userName;

    /**
     * 店铺名称
     */
    @Column(ignore = true)
    @Schema(description = "店铺名称")
    private String shopName;

    /**
     * 校区ID
     */
    @Column(ignore = true)
    @Schema(description = "校区ID")
    private String schoolId;

}
