package com.project.school_flate.dto.flow;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.flow.FlowTask;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data
public class FlowTaskDto extends FlowTask implements Serializable {

    private static final long serialVersionUID = -140781178864099836L;

    @Column(ignore = true)
    @Schema(description = "页数")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "条数")
    private Integer limit;

}
