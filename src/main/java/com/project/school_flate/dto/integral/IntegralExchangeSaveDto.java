package com.project.school_flate.dto.integral;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.integral.IntegralExchangeSave;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
@Data
public class IntegralExchangeSaveDto extends IntegralExchangeSave implements Serializable {

    private static final long serialVersionUID = -4409432309809626809L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

    @Column(ignore = true)
    @Schema(description = "校区ID",name = "schoolId")
    private String schoolId;

    /**
     * 店铺名称
     */
    @Column(ignore = true)
    @Schema(description = "店铺名称")
    private String shopName;

}
