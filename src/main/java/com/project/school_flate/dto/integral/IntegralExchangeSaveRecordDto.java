package com.project.school_flate.dto.integral;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.integral.IntegralExchangeSaveRecord;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data
public class IntegralExchangeSaveRecordDto extends IntegralExchangeSaveRecord implements Serializable {

    private static final long serialVersionUID = 4621105069303648663L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

    /**
     * 用户名称
     */
    @Column(ignore = true)
    @Schema(description = "用户名称")
    private String userName;

    /**
     * 店铺名称
     */
    @Column(ignore = true)
    @Schema(description = "店铺名称")
    private String shopName;

    /**
     * 校区ID
     */
    @Column(ignore = true)
    @Schema(description = "校区ID")
    private String schoolId;

}
