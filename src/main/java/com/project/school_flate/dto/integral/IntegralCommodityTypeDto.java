package com.project.school_flate.dto.integral;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.integral.IntegralCommodityType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
@Data
public class IntegralCommodityTypeDto extends IntegralCommodityType implements Serializable {

    private static final long serialVersionUID = 6590672893785938882L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

}
