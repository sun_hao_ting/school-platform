package com.project.school_flate.dto.delivery;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.delivery.DeliveryEvaluate;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data
public class DeliveryEvaluateDto extends DeliveryEvaluate implements Serializable {

    private static final long serialVersionUID = -7047768929246247656L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

}
