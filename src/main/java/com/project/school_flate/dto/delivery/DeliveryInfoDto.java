package com.project.school_flate.dto.delivery;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.delivery.DeliveryInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class DeliveryInfoDto extends DeliveryInfo implements Serializable {

    private static final long serialVersionUID = 8793286420283240869L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

    @Column(ignore = true)
    @Schema(description = "token",name = "token")
    private String token;

    @Column(ignore = true)
    @Schema(description = "老密码",name = "oldPassword")
    private String oldPassword;

    @Column(ignore = true)
    @Schema(description = "code",name = "code")
    private String code;

    @Column(ignore = true)
    @Schema(description = "获取收入明细l类型（0：本月收入；1：今日收入；2：昨日收入）",name = "code")
    private Integer incomeType;

}
