package com.project.school_flate.dto.delivery;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.delivery.DeliveryExamine;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class DeliveryExamineDto extends DeliveryExamine implements Serializable {

    private static final long serialVersionUID = 5507893989259591486L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

}
