package com.project.school_flate.dto.user;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.user.UserAddress;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserAddressDto extends UserAddress implements Serializable {

    private static final long serialVersionUID = -6665680312001284164L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

}
