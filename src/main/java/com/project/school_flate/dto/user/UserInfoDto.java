package com.project.school_flate.dto.user;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.user.UserInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserInfoDto extends UserInfo implements Serializable {

    private static final long serialVersionUID = 5598104836960168076L;

    @Column(ignore = true)
    @Schema(title = "token",name = "token",type = "String")
    private String token;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

    @Column(ignore = true)
    @Schema(description = "code",name = "code")
    private String code;

    @Column(ignore = true)
    @Schema(description = "充值成为专业分销商金额",name = "specialityDistributionMoney")
    private Double specialityDistributionMoney;

    @Column(ignore = true)
    @Schema(description = "获取收入明细l类型（0：本月收入；1：今日收入；2：昨日收入）",name = "code")
    private Integer incomeType;

}
