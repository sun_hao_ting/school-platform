package com.project.school_flate.dto.order;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.order.OrderIntegralCommodity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data
public class OrderIntegralCommodityDto extends OrderIntegralCommodity implements Serializable {

    private static final long serialVersionUID = 8725481535320564903L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

}
