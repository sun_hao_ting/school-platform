package com.project.school_flate.dto.order;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.RelationOneToOne;
import com.project.school_flate.entity.order.OrderTakeawayCommodity;
import com.project.school_flate.entity.shop.ShopCommodity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class OrderTakeawayCommodityDto extends OrderTakeawayCommodity implements Serializable {

    private static final long serialVersionUID = -1842648565940302679L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

    /**
     * 购物车外卖订单商品列表
     */
    @Column(ignore = true)
    @Schema(description = "购物车外卖订单商品列表")
    private List<OrderTakeawayCommodityDto> carOrderTakeawayCommodityDtoList;

    /**
     * 购物车外卖订单商品规格ID集合
     */
    @Column(ignore = true)
    @Schema(description = "购物车外卖订单商品规格ID集合")
    private List<String> carShopCommoditySpecsIdList;

    /**
     * 购物车外卖订单店铺ID
     */
    @Column(ignore = true)
    @Schema(description = "购物车外卖订单店铺ID")
    private String carShopId;

}
