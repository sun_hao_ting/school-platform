package com.project.school_flate.dto.order;

import com.alibaba.fastjson2.JSONObject;
import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.order.OrderTakeaway;
import com.project.school_flate.entity.order.OrderTakeawayCommodity;
import com.project.school_flate.util.wechat.PayDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class OrderTakeawayDto extends OrderTakeaway implements Serializable {

    private static final long serialVersionUID = 4005655981210224086L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

    /**
     * 查询开始时间
     */
    @Column(ignore = true)
    @Schema(description = "查询开始时间")
    private Date queryBeginTime;

    /**
     * 查询结束时间
     */
    @Column(ignore = true)
    @Schema(description = "查询结束时间")
    private Date queryEndTime;

    /**
     * 原价
     */
    @Column(ignore = true)
    @Schema(description = "原价")
    private Double originalPrice;

    /**
     * 会员价
     */
    @Column(ignore = true)
    @Schema(description = "会员价")
    private Double memberPrice;

    /**
     * 配送地址ID
     */
    @Column(ignore = true)
    @Schema(description = "配送地址ID")
    private String userAddressId;

    /**
     * 整体用户状态（0：待支付；1：待完成、2：待评价、3：退款/售后）
     */
    @Column(ignore = true)
    @Schema(description = "整体状态")
    private Integer wholeUserState;

    /**
     * 配送人ID
     */
    @Column(ignore = true)
    @Schema(description = "配送人ID")
    private String deliveryId;

    /**
     * 中转前配送员ID
     */
    @Column(ignore = true)
    @Schema(description = "中转前配送员ID")
    private String transferDeliveryId;

    /**
     * 配送人名称
     */
    @Column(ignore = true)
    @Schema(description = "配送人名称")
    private String deliveryName;

    /**
     * 用户名称
     */
    @Column(ignore = true)
    @Schema(description = "用户名称")
    private String userName;

    /**
     * 店铺名称
     */
    @Column(ignore = true)
    @Schema(description = "店铺名称")
    private String shopName;

}
