package com.project.school_flate.dto.login;

import com.mybatisflex.annotation.Column;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author sunht
 * @Date 2023/10/2 17:39
 * @Version v1.0.0
 * @Message
 **/
@Data
public class Login implements Serializable {
    private static final long serialVersionUID = 8039072439888721248L;

    @Column(ignore = true)
    @Schema(name = "loginName",title = "登录名")
    private String loginName;

    @Column(ignore = true)
    @Schema(name = "password",title = "密码")
    private String password;

    @Column(ignore = true)
    @Schema(name = "phoneNumber",title = "电话号码")
    private String phoneNumber;

    @Column(ignore = true)
    @Schema(name = "name",title = "名称")
    private String name;

    @Column(ignore = true)
    @Schema(name = "verificationCode",title = "校验")
    private String verificationCode;

    @Column(ignore = true)
    @Schema(name = "code",title = "微信code值",type = "String")
    private String code;
}
