package com.project.school_flate.dto.shop;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.shop.ShopCommodityEvaluate;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ShopCommodityEvaluateDto extends ShopCommodityEvaluate implements Serializable {

    private static final long serialVersionUID = 8445526622814096021L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

    /**
     * 订单ID
     */
    @Schema(description = "外卖订单ID")
    private String orderTakeawayId;

    /**
     * 配送员综合评分
     */
    @Schema(description = "配送员综合评分")
    private Double deliveryAllRating;

}
