package com.project.school_flate.dto.shop;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.shop.ShopCommoditySpecsType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ShopCommoditySpecsTypeDto extends ShopCommoditySpecsType implements Serializable {

    private static final long serialVersionUID = -4851823784843798019L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

    @Column(ignore = true)
    @Schema(description = "商品规格数组",name = "shopCommoditySpecsTypeList")
    private List<ShopCommoditySpecsType> shopCommoditySpecsTypeList;

    @Column(ignore = true)
    @Schema(description = "店铺ID",name = "shopId")
    private String shopId;

}
