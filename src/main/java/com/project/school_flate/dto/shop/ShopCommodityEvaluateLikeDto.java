package com.project.school_flate.dto.shop;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.shop.ShopCommodityEvaluateLike;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
@Data
public class ShopCommodityEvaluateLikeDto extends ShopCommodityEvaluateLike implements Serializable {

    private static final long serialVersionUID = -6367355308578410173L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

}
