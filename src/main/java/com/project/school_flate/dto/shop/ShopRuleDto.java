package com.project.school_flate.dto.shop;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.shop.ShopRule;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ShopRuleDto extends ShopRule implements Serializable {

    private static final long serialVersionUID = 1142592907918603798L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

    @Column(ignore = true)
    @Schema(description = "店铺规则数组",name = "shopRuleList")
    private List<ShopRule> shopRuleList;

}
