package com.project.school_flate.dto.shop;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.RelationOneToMany;
import com.project.school_flate.entity.shop.ShopCommodity;
import com.project.school_flate.entity.shop.ShopInfo;
import com.sun.org.apache.xpath.internal.operations.Bool;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ShopInfoDto extends ShopInfo implements Serializable {

    private static final long serialVersionUID = -4896648609699451565L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

    @Column(ignore = true)
    @Schema(description = "用户是否店铺储蓄",name = "isSave")
    private Boolean isUserSave = false;

    /**
     * 是否前往完善信息
     */
    @Column(ignore = true)
    @Schema(description = "是否前往完善信息")
    private Boolean isGoPerfect = false;

    /**
     * 用户ID
     */
    @Column(ignore = true)
    @Schema(description = "用户ID")
    private String userId;

    @Column(ignore = true)
    @Schema(description = "获取收入明细类型（0：本月收入；1：今日收入；2：昨日收入）",name = "code")
    private Integer incomeType;

}
