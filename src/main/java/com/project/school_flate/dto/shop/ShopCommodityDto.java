package com.project.school_flate.dto.shop;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.shop.ShopCommodity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ShopCommodityDto extends ShopCommodity implements Serializable {

    private static final long serialVersionUID = 3852477163569134518L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

    @Column(ignore = true)
    @Schema(description = "校区ID",name = "schoolId")
    private String schoolId;

}
