package com.project.school_flate.dto.shop;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.shop.ShopAccount;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ShopAccountDto extends ShopAccount implements Serializable {

    private static final long serialVersionUID = 2669964666822845217L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

    @Column(ignore = true)
    @Schema(description = "token",name = "token")
    private String token;

    @Column(ignore = true)
    @Schema(description = "原密码",name = "oldPassword")
    private String oldPassword;

    @Column(ignore = true)
    @Schema(description = "code",name = "code")
    private String code;

}
