package com.project.school_flate.dto.com;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.com.ComAdmin;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ComAdminDto extends ComAdmin implements Serializable {

    private static final long serialVersionUID = -3488042034317515200L;

    @Column(ignore = true)
    @Schema(title = "token",name = "token",type = "String")
    private String token;

    @Column(ignore = true)
    @Schema(title = "页数",name = "page",type = "int")
    private Integer page;

    @Column(ignore = true)
    @Schema(title = "条数",name = "limit",type = "int")
    private Integer limit;

}
