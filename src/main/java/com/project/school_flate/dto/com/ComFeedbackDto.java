package com.project.school_flate.dto.com;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.com.ComFeedback;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ComFeedbackDto extends ComFeedback implements Serializable {

    private static final long serialVersionUID = 8541112153690893588L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

    @Column(ignore = true)
    @Schema(description = "回复内容",name = "limit")
    private String replyContent;

    /**
     * 用户名称
     */
    @Column(ignore = true)
    @Schema(description = "用户名称")
    private String userName;

}
