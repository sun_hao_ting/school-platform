package com.project.school_flate.dto.com;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.com.ComRole;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data
public class ComRoleDto extends ComRole implements Serializable {

    private static final long serialVersionUID = 5771961201703721554L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

}
