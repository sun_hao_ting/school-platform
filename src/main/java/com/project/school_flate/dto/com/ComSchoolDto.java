package com.project.school_flate.dto.com;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.com.ComSchool;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
@Data
public class ComSchoolDto extends ComSchool implements Serializable {

    private static final long serialVersionUID = 3910898379472284821L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

    /**
     * 手机经度
     */
    @Column(ignore = true)
    @Schema(description = "手机经度")
    private Double phoneLongitude;

    /**
     * 手机纬度
     */
    @Column(ignore = true)
    @Schema(description = "手机纬度")
    private Double phoneLatitude;

    /**
     * 手机距离
     */
    @Column(ignore = true)
    @Schema(description = "手机距离")
    private Double phoneDistance;

}
