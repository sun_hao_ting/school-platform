package com.project.school_flate.dto.com;

import com.mybatisflex.annotation.Column;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @Author sunht
 * @Date 2023/10/10 21:46
 * @Version v1.0.0
 * @Message
 **/
@Data
public class TaskData implements Serializable {

    private static final long serialVersionUID = -5220264972035385890L;

    @Column(ignore = true)
    @Schema(name = "procdefId",title = "流程ID")
    private String procdefId;

    @Column(ignore = true)
    @Schema(name = "taskId",title = "任务ID")
    private String taskId;

    @Column(ignore = true)
    @Schema(name = "variables",title = "流程传参")
    private Map<String,Object> variables;

}
