package com.project.school_flate.dto.com;

import com.mybatisflex.annotation.Column;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @Author sunht
 * @Date 2023/10/10 21:41
 * @Version v1.0.0
 * @Message
 **/
@Data
public class FlowableData implements Serializable {
    private static final long serialVersionUID = 7613590524794262167L;

    @Column(ignore = true)
    @Schema(name = "procdefId",title = "流程ID")
    private String procdefId;

    @Column(ignore = true)
    @Schema(name = "variables",title = "流程传参")
    private Map<String,Object> variables;

}
