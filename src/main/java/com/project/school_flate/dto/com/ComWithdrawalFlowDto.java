package com.project.school_flate.dto.com;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.com.ComWithdrawalFlow;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
public class ComWithdrawalFlowDto extends ComWithdrawalFlow implements Serializable {

    private static final long serialVersionUID = -4685272265104877732L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

    /**
     * 查询开始时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "查询开始时间")
    private Date queryBeginTime;

    /**
     * 查询结束时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "查询结束时间")
    private Date queryEndTime;

    /**
     * 配送人名称
     */
    @Column(ignore = true)
    @Schema(description = "配送人名称")
    private String deliveryName;

    /**
     * 用户名称
     */
    @Column(ignore = true)
    @Schema(description = "用户名称")
    private String userName;

    /**
     * 店铺名称
     */
    @Column(ignore = true)
    @Schema(description = "店铺名称")
    private String shopName;

}
