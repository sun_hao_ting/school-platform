package com.project.school_flate.dto.com;

import com.mybatisflex.annotation.Column;
import com.project.school_flate.entity.com.ComImage;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ComImageDto extends ComImage implements Serializable {

    private static final long serialVersionUID = -1749147416397194215L;

    @Column(ignore = true)
    @Schema(description = "条数",name = "page")
    private Integer page;

    @Column(ignore = true)
    @Schema(description = "页数",name = "limit")
    private Integer limit;

}
