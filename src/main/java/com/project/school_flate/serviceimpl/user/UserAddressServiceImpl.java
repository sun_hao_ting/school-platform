package com.project.school_flate.serviceimpl.user;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.user.UserAddressDto;
import com.project.school_flate.entity.user.UserAddress;
import com.project.school_flate.entity.user.table.UserAddressTableDef;
import com.project.school_flate.mapper.user.UserAddressMapper;
import com.project.school_flate.service.user.UserAddressService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class UserAddressServiceImpl extends ServiceImpl<UserAddressMapper, UserAddress> implements UserAddressService {

    @Autowired
    private UserAddressMapper oUserAddressMapper;

    /**
     * 获取配送地址
     * @param oUserAddressDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getUserAddress(UserAddressDto oUserAddressDto) throws Exception {
        List<UserAddress> oUserAddressList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入用户ID
        if(StringUtils.isNotBlank(oUserAddressDto.getUserId())){
            queryWrapper.where(UserAddressTableDef.USER_ADDRESS.USER_ID.eq(oUserAddressDto.getUserId()));
        }
        queryWrapper.orderBy("recently_time desc,create_time desc");
        //是否分页
        if(oUserAddressDto.getPage() != null && oUserAddressDto.getLimit() != null){
            Page<UserAddress> UserAddressPage = oUserAddressMapper.paginateWithRelations(oUserAddressDto.getPage(),oUserAddressDto.getLimit(),queryWrapper);
            oUserAddressList = UserAddressPage.getRecords();
            total = UserAddressPage.getTotalRow();
        }else{
            oUserAddressList = oUserAddressMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oUserAddressList.size();
        }
        //PoToDto
        List<UserAddressDto> oUserAddressDtoList = (List<UserAddressDto>) PoToDTO.poToDtoList(oUserAddressList,new UserAddressDto());
        return Result.ok(oUserAddressDtoList,total);
    }

    /**
     * 添加配送地址
     * @param oUserAddressDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addUserAddress(UserAddressDto oUserAddressDto) throws Exception {
        UserAddress oUserAddress = new UserAddress();
        PoToDTO.poToDto(oUserAddressDto,oUserAddress);
        if(oUserAddressMapper.insert(oUserAddress) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加配送地址失败");
        }
        return Result.ok("添加配送地址成功");
    }

    /**
     * 修改配送地址
     * @param oUserAddressDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateUserAddress(UserAddressDto oUserAddressDto) throws Exception {
        UserAddress oUserAddress = new UserAddress();
        PoToDTO.poToDto(oUserAddressDto,oUserAddress);
        if(oUserAddressMapper.update(oUserAddress) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改配送地址失败");
        }
        return Result.ok("修改配送地址成功");
    }

    /**
     * 删除配送地址
     * @param oUserAddressDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result deleteUserAddress(UserAddressDto oUserAddressDto) throws Exception {
        UserAddress oUserAddress = new UserAddress();
        PoToDTO.poToDto(oUserAddressDto,oUserAddress);
        if(oUserAddressMapper.deleteById(oUserAddress.getId()) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("删除配送地址失败");
        }
        return Result.ok("删除配送地址成功");
    }

    /**
     * 获取用户最近一次使用的配送地址
     * @param oUserAddressDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getUserAddressRecently(UserAddressDto oUserAddressDto) throws Exception {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(UserAddressTableDef.USER_ADDRESS.USER_ID.eq(oUserAddressDto.getUserId()));
        queryWrapper.orderBy("recently_time desc");
        queryWrapper.limit(1);
        UserAddress oUserAddress = oUserAddressMapper.selectOneByQuery(queryWrapper);
        return Result.ok(oUserAddress);
    }

}
