package com.project.school_flate.serviceimpl.login;

import cn.dev33.satoken.stp.StpUtil;
import com.mybatisflex.core.query.QueryWrapper;
import com.project.school_flate.dto.com.ComAdminDto;
import com.project.school_flate.dto.login.Login;
import com.project.school_flate.dto.user.UserInfoDto;
import com.project.school_flate.entity.table.user.UserInfoTable;
import com.project.school_flate.entity.user.UserInfo;
import com.project.school_flate.entity.com.ComAdmin;
import com.project.school_flate.entity.table.com.ComAdminTable;
import com.project.school_flate.mapper.user.UserInfoMapper;
import com.project.school_flate.mapper.com.ComAdminMapper;
import com.project.school_flate.service.login.LoginService;
import com.project.school_flate.util.Md5Util;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import com.project.school_flate.util.wechat.UserUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author sunht
 * @Date 2023/10/2 18:03
 * @Version v1.0.0
 * @Message
 **/
@Service(value = "loginServiceImpl")
public class LoginServiceImpl implements LoginService {
    
    @Autowired
    private UserInfoMapper userInfoMapper;
    
    @Autowired
    private ComAdminMapper comAdminMapper;
    
    /**
     * 系统用户登录接口
     * @param login
     * @return
     * @throws Exception
     */
    @Override
    public Result systemLogin(Login login) throws Exception {
        //先根据用户名查询是否有这样的人
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(ComAdminTable.COM_ADMIN.LOGIN_NAME.eq(login.getLoginName()));
        ComAdmin comAdmin = comAdminMapper.selectOneByQuery(queryWrapper);
        if(comAdmin == null){
            return Result.ok("当前用户不存在，请查询后再登录");
        }
        if(!comAdmin.getPassword().equals(Md5Util.code(login.getPassword()))){
            return Result.ok("密码错误，请重新登录");
        }
        //将密码隐藏
        comAdmin.setPassword("******");
        //用户登录
        StpUtil.login(comAdmin.getId());
        //登录缓存登录对象
        StpUtil.getSession().set("user", comAdmin);
        comAdmin.setPassword("********");
        //PoToDto
        ComAdminDto oAdminDto = new ComAdminDto();
        PoToDTO.poToDto(comAdmin,oAdminDto);
        oAdminDto.setToken(StpUtil.getTokenValue());
        return Result.ok(oAdminDto);
    }

    /**
     * 用户微信登录
     * @param login
     * @return
     * @throws Exception
     */
    @Override
    public Result wxClientLogin(Login login) throws Exception {
       String openId = UserUtil.getSessionKeyOrOpenId(login.getCode());
       if(StringUtils.isBlank(openId)){
           return Result.fail("用户微信信息不正确");
       }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(UserInfoTable.USER_INFO.WX_OPTION_ID.eq(openId));
        UserInfo userInfoOne = userInfoMapper.selectOneByQuery(queryWrapper);
        if(userInfoOne == null){
            UserInfo newUserInfo = new UserInfo();
            newUserInfo.setName("微信用户" + System.currentTimeMillis());
            newUserInfo.setWxOptionId(openId);
            userInfoMapper.insert(newUserInfo);
            userInfoOne = newUserInfo;
        }
        //用户登录
        StpUtil.login(userInfoOne.getId());
        //登录缓存登录对象
        StpUtil.getSession().set("user", userInfoOne);
        //PoToDto
        UserInfoDto oUserInfoDto = new UserInfoDto();
        PoToDTO.poToDto(userInfoOne,oUserInfoDto);
        oUserInfoDto.setToken(StpUtil.getTokenValue());
        return Result.ok(oUserInfoDto);
    }

    /**
     * 登录注销
     * @param
     * @return
     * @throws Exception
     */
    @Override
    public Result loginOut() throws Exception {
        StpUtil.logout();
        return Result.ok();
    }

}
