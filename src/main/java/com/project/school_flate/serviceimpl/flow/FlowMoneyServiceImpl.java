package com.project.school_flate.serviceimpl.flow;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.project.school_flate.dto.flow.FlowMoneyDto;
import com.project.school_flate.entity.delivery.DeliveryInfo;
import com.project.school_flate.entity.delivery.table.DeliveryInfoTableDef;
import com.project.school_flate.entity.flow.FlowMoney;
import com.project.school_flate.entity.flow.table.FlowBusinessTableDef;
import com.project.school_flate.entity.flow.table.FlowMoneyTableDef;
import com.project.school_flate.entity.shop.ShopInfo;
import com.project.school_flate.entity.shop.table.ShopInfoTableDef;
import com.project.school_flate.entity.user.UserInfo;
import com.project.school_flate.entity.user.table.UserInfoTableDef;
import com.project.school_flate.mapper.delivery.DeliveryInfoMapper;
import com.project.school_flate.mapper.flow.FlowBusinessMapper;
import com.project.school_flate.mapper.flow.FlowMoneyMapper;
import com.project.school_flate.mapper.shop.ShopInfoMapper;
import com.project.school_flate.mapper.user.UserInfoMapper;
import com.project.school_flate.service.flow.FlowMoneyService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service(value = "flowMoneyServiceImpl")
public class FlowMoneyServiceImpl implements FlowMoneyService {

    @Autowired
    private FlowMoneyMapper oFlowMoneyMapper;

    @Autowired
    private FlowBusinessMapper oFlowBusinessMapper;

    @Autowired
    private UserInfoMapper oUserInfoMapper;

    @Autowired
    private DeliveryInfoMapper oDeliveryInfoMapper;

    @Autowired
    private ShopInfoMapper oShopInfoMapper;

    /**
     * 获取平台流水
     * @param oFlowMoneyDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getFlowMoney(FlowMoneyDto oFlowMoneyDto) throws Exception {
        List<FlowMoney> oFlowMoneyList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入流水类型
        if(oFlowMoneyDto.getType() != null){
            queryWrapper.where(FlowMoneyTableDef.FLOW_MONEY.TYPE.eq(oFlowMoneyDto.getType()));
        }
        //判断是否传入用户名称
        if(StringUtils.isNotBlank(oFlowMoneyDto.getUserName())){
            QueryWrapper qwUser = new QueryWrapper();
            qwUser.where(UserInfoTableDef.USER_INFO.NAME.like(oFlowMoneyDto.getUserName()));
            List<UserInfo> oUserInfoList = oUserInfoMapper.selectListByQuery(qwUser);
            if(CollectionUtils.isEmpty(oUserInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的用户ID集合
            List<String> oUserInfoIdList = new ArrayList<>();
            oUserInfoList.forEach(item->{oUserInfoIdList.add(item.getId());});
            queryWrapper.where(FlowMoneyTableDef.FLOW_MONEY.USER_ID.in(oUserInfoIdList));
        }
        //判断是否传入配送员名称
        if(StringUtils.isNotBlank(oFlowMoneyDto.getDeliveryName())){
            QueryWrapper qwDelivery = new QueryWrapper();
            qwDelivery.where(DeliveryInfoTableDef.DELIVERY_INFO.NAME.like(oFlowMoneyDto.getDeliveryName()));
            List<DeliveryInfo> oDeliveryInfoList = oDeliveryInfoMapper.selectListByQuery(qwDelivery);
            if(CollectionUtils.isEmpty(oDeliveryInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的配送员ID集合
            List<String> oDeliveryInfoIdList = new ArrayList<>();
            oDeliveryInfoList.forEach(item->{oDeliveryInfoIdList.add(item.getId());});
            queryWrapper.where(FlowMoneyTableDef.FLOW_MONEY.DELIVERY_ID.in(oDeliveryInfoIdList));
        }
        //判断是否传入店铺名称
        if(StringUtils.isNotBlank(oFlowMoneyDto.getShopName())){
            QueryWrapper qwShop = new QueryWrapper();
            qwShop.where(ShopInfoTableDef.SHOP_INFO.NAME.like(oFlowMoneyDto.getShopName()));
            List<ShopInfo> oShopInfoList = oShopInfoMapper.selectListByQuery(qwShop);
            if(CollectionUtils.isEmpty(oShopInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的店铺ID集合
            List<String> oShopInfoIdList = new ArrayList<>();
            oShopInfoList.forEach(item->{oShopInfoIdList.add(item.getId());});
            queryWrapper.where(FlowMoneyTableDef.FLOW_MONEY.SHOP_ID.in(oShopInfoIdList));
        }
        //判断是否传入订单ID
        if(StringUtils.isNotBlank(oFlowMoneyDto.getOrderId())){
            queryWrapper.where(FlowMoneyTableDef.FLOW_MONEY.ORDER_ID.eq(oFlowMoneyDto.getOrderId()));
        }
        //判断是否传入状态
        if(oFlowMoneyDto.getState() != null){
            queryWrapper.where(FlowMoneyTableDef.FLOW_MONEY.STATE.eq(oFlowMoneyDto.getState()));
        }
        //判断是否传入校区ID
        if(StringUtils.isNotBlank(oFlowMoneyDto.getSchoolId())){
            QueryWrapper qwShop = new QueryWrapper();
            qwShop.where(ShopInfoTableDef.SHOP_INFO.SCHOOL_ID.eq(oFlowMoneyDto.getSchoolId()));
            List<ShopInfo> oShopInfoList = oShopInfoMapper.selectListByQuery(qwShop);
            if(CollectionUtils.isEmpty(oShopInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的店铺ID集合
            List<String> oShopInfoIdList = new ArrayList<>();
            oShopInfoList.forEach(item->{oShopInfoIdList.add(item.getId());});
            queryWrapper.where(FlowMoneyTableDef.FLOW_MONEY.SHOP_ID.in(oShopInfoIdList));
        }
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oFlowMoneyDto.getPage() != null && oFlowMoneyDto.getLimit() != null){
            Page<FlowMoney> FlowMoneyPage = oFlowMoneyMapper.paginateWithRelations(oFlowMoneyDto.getPage(),oFlowMoneyDto.getLimit(),queryWrapper);
            oFlowMoneyList = FlowMoneyPage.getRecords();
            total = FlowMoneyPage.getTotalRow();
        }else{
            oFlowMoneyList = oFlowMoneyMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oFlowMoneyList.size();
        }
        //PoToDto
        List<FlowMoneyDto> oFlowMoneyDtoList = (List<FlowMoneyDto>) PoToDTO.poToDtoList(oFlowMoneyList,new FlowMoneyDto());
        return Result.ok(oFlowMoneyDtoList,total);
    }

}
