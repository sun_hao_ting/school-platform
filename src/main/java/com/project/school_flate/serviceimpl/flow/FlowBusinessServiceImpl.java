package com.project.school_flate.serviceimpl.flow;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.project.school_flate.dto.flow.FlowBusinessDto;
import com.project.school_flate.dto.shop.ShopSaveUserDto;
import com.project.school_flate.entity.delivery.DeliveryInfo;
import com.project.school_flate.entity.delivery.table.DeliveryInfoTableDef;
import com.project.school_flate.entity.flow.FlowBusiness;
import com.project.school_flate.entity.flow.table.FlowBusinessTableDef;
import com.project.school_flate.entity.shop.ShopInfo;
import com.project.school_flate.entity.shop.table.ShopInfoTableDef;
import com.project.school_flate.entity.user.UserInfo;
import com.project.school_flate.entity.user.table.UserInfoTableDef;
import com.project.school_flate.mapper.delivery.DeliveryInfoMapper;
import com.project.school_flate.mapper.flow.FlowBusinessMapper;
import com.project.school_flate.mapper.shop.ShopInfoMapper;
import com.project.school_flate.mapper.user.UserInfoMapper;
import com.project.school_flate.service.flow.FlowBusinessService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service(value = "flowBusinessServiceImpl")
public class FlowBusinessServiceImpl implements FlowBusinessService {

    @Autowired
    private FlowBusinessMapper oFlowBusinessMapper;

    @Autowired
    private UserInfoMapper oUserInfoMapper;

    @Autowired
    private DeliveryInfoMapper oDeliveryInfoMapper;

    @Autowired
    private ShopInfoMapper oShopInfoMapper;

    /**
     * 获取业务流水
     * @param oFlowBusinessDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getFlowBusiness(FlowBusinessDto oFlowBusinessDto) throws Exception {
        List<FlowBusiness> oFlowBusinessList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入流水类型
        if(oFlowBusinessDto.getType() != null){
            queryWrapper.where(FlowBusinessTableDef.FLOW_BUSINESS.TYPE.eq(oFlowBusinessDto.getType()));
        }
        //判断是否传入用户名称
        if(StringUtils.isNotBlank(oFlowBusinessDto.getUserName())){
            QueryWrapper qwUser = new QueryWrapper();
            qwUser.where(UserInfoTableDef.USER_INFO.NAME.like(oFlowBusinessDto.getUserName()));
            List<UserInfo> oUserInfoList = oUserInfoMapper.selectListByQuery(qwUser);
            if(CollectionUtils.isEmpty(oUserInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的用户ID集合
            List<String> oUserInfoIdList = new ArrayList<>();
            oUserInfoList.forEach(item->{oUserInfoIdList.add(item.getId());});
            queryWrapper.where(FlowBusinessTableDef.FLOW_BUSINESS.USER_ID.in(oUserInfoIdList));
        }
        //判断是否传入配送员名称
        if(StringUtils.isNotBlank(oFlowBusinessDto.getDeliveryName())){
            QueryWrapper qwDelivery = new QueryWrapper();
            qwDelivery.where(DeliveryInfoTableDef.DELIVERY_INFO.NAME.like(oFlowBusinessDto.getDeliveryName()));
            List<DeliveryInfo> oDeliveryInfoList = oDeliveryInfoMapper.selectListByQuery(qwDelivery);
            if(CollectionUtils.isEmpty(oDeliveryInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的配送员ID集合
            List<String> oDeliveryInfoIdList = new ArrayList<>();
            oDeliveryInfoList.forEach(item->{oDeliveryInfoIdList.add(item.getId());});
            queryWrapper.where(FlowBusinessTableDef.FLOW_BUSINESS.DELIVERY_ID.in(oDeliveryInfoIdList));
        }
        //判断是否传入店铺名称
        if(StringUtils.isNotBlank(oFlowBusinessDto.getShopName())){
            QueryWrapper qwShop = new QueryWrapper();
            qwShop.where(ShopInfoTableDef.SHOP_INFO.NAME.like(oFlowBusinessDto.getShopName()));
            List<ShopInfo> oShopInfoList = oShopInfoMapper.selectListByQuery(qwShop);
            if(CollectionUtils.isEmpty(oShopInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的店铺ID集合
            List<String> oShopInfoIdList = new ArrayList<>();
            oShopInfoList.forEach(item->{oShopInfoIdList.add(item.getId());});
            queryWrapper.where(FlowBusinessTableDef.FLOW_BUSINESS.SHOP_ID.in(oShopInfoIdList));
        }
        //判断是否传入订单ID
        if(StringUtils.isNotBlank(oFlowBusinessDto.getOrderId())){
            queryWrapper.where(FlowBusinessTableDef.FLOW_BUSINESS.ORDER_ID.eq(oFlowBusinessDto.getOrderId()));
        }
        //判断是否传入状态
        if(oFlowBusinessDto.getState() != null){
            queryWrapper.where(FlowBusinessTableDef.FLOW_BUSINESS.STATE.eq(oFlowBusinessDto.getState()));
        }
        //判断是否传入校区ID
        if(StringUtils.isNotBlank(oFlowBusinessDto.getSchoolId())){
            QueryWrapper qwShop = new QueryWrapper();
            qwShop.where(ShopInfoTableDef.SHOP_INFO.SCHOOL_ID.eq(oFlowBusinessDto.getSchoolId()));
            List<ShopInfo> oShopInfoList = oShopInfoMapper.selectListByQuery(qwShop);
            if(CollectionUtils.isEmpty(oShopInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的店铺ID集合
            List<String> oShopInfoIdList = new ArrayList<>();
            oShopInfoList.forEach(item->{oShopInfoIdList.add(item.getId());});
            queryWrapper.where(FlowBusinessTableDef.FLOW_BUSINESS.SHOP_ID.in(oShopInfoIdList));
        }
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oFlowBusinessDto.getPage() != null && oFlowBusinessDto.getLimit() != null){
            Page<FlowBusiness> FlowBusinessPage = oFlowBusinessMapper.paginateWithRelations(oFlowBusinessDto.getPage(),oFlowBusinessDto.getLimit(),queryWrapper);
            oFlowBusinessList = FlowBusinessPage.getRecords();
            total = FlowBusinessPage.getTotalRow();
        }else{
            oFlowBusinessList = oFlowBusinessMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oFlowBusinessList.size();
        }
        //PoToDto
        List<FlowBusinessDto> oFlowBusinessDtoList = (List<FlowBusinessDto>) PoToDTO.poToDtoList(oFlowBusinessList,new FlowBusinessDto());
        return Result.ok(oFlowBusinessDtoList,total);
    }

    /**
     * 获取用户在店铺储蓄消费
     * @param oFlowBusinessDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getFlowMoneyConsumptionToShopSaveUser(FlowBusinessDto oFlowBusinessDto) throws Exception {
        List<FlowBusiness> oFlowBusinessList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(FlowBusinessTableDef.FLOW_BUSINESS.SHOP_ID.eq(oFlowBusinessDto.getShopId()));
        queryWrapper.where(FlowBusinessTableDef.FLOW_BUSINESS.USER_ID.eq(oFlowBusinessDto.getUserId()));
        queryWrapper.where(FlowBusinessTableDef.FLOW_BUSINESS.TYPE.eq(0));
        queryWrapper.where(FlowBusinessTableDef.FLOW_BUSINESS.INFLOW_DIRECTION.eq(0));
        queryWrapper.where(FlowBusinessTableDef.FLOW_BUSINESS.INFLOW_WAY.eq(0));
        queryWrapper.where(FlowBusinessTableDef.FLOW_BUSINESS.STATE.eq(1));
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oFlowBusinessDto.getPage() != null && oFlowBusinessDto.getLimit() != null){
            Page<FlowBusiness> FlowBusinessPage = oFlowBusinessMapper.paginateWithRelations(oFlowBusinessDto.getPage(),oFlowBusinessDto.getLimit(),queryWrapper);
            oFlowBusinessList = FlowBusinessPage.getRecords();
            total = FlowBusinessPage.getTotalRow();
        }else{
            oFlowBusinessList = oFlowBusinessMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oFlowBusinessList.size();
        }
        //PoToDto
        List<FlowBusinessDto> oFlowBusinessDtoList = (List<FlowBusinessDto>) PoToDTO.poToDtoList(oFlowBusinessList,new FlowBusinessDto());
        return Result.ok(oFlowBusinessDtoList,total);
    }

}
