package com.project.school_flate.serviceimpl.reportForms;

import cn.dev33.satoken.stp.StpUtil;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.relation.RelationManager;
import com.project.school_flate.dto.delivery.DeliveryInfoDto;
import com.project.school_flate.dto.user.UserInfoDto;
import com.project.school_flate.entity.com.ComAdmin;
import com.project.school_flate.entity.delivery.DeliveryInfo;
import com.project.school_flate.entity.flow.FlowTask;
import com.project.school_flate.entity.flow.table.FlowTaskTableDef;
import com.project.school_flate.entity.order.OrderDelivery;
import com.project.school_flate.entity.order.OrderTakeaway;
import com.project.school_flate.entity.order.table.OrderDeliveryTableDef;
import com.project.school_flate.entity.table.order.OrderDeliveryTable;
import com.project.school_flate.entity.table.order.OrderTakeawayTable;
import com.project.school_flate.entity.user.UserInfo;
import com.project.school_flate.mapper.delivery.DeliveryInfoMapper;
import com.project.school_flate.mapper.flow.FlowTaskMapper;
import com.project.school_flate.mapper.order.OrderDeliveryMapper;
import com.project.school_flate.mapper.order.OrderTakeawayMapper;
import com.project.school_flate.service.reportForms.ReportFormsDeliveryService;
import com.project.school_flate.util.DateUtils;
import com.project.school_flate.util.Result.Result;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class ReportFormsDeliveryServiceImpl implements ReportFormsDeliveryService {

    @Autowired
    private OrderTakeawayMapper oOrderTakeawayMapper;

    @Autowired
    private DeliveryInfoMapper oDeliveryInfoMapper;

    @Autowired
    private OrderDeliveryMapper oOrderDeliveryMapper;

    @Autowired
    private FlowTaskMapper oFlowTaskMapper;

    /**
     * 获取累计抢单
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getAllRobOrder(DeliveryInfoDto oDeliveryInfoDto) throws Exception {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(OrderDeliveryTable.ORDER_DELIVERY.DELIVERY_ID.eq(oDeliveryInfoDto.getId()));
        queryWrapper.where(OrderDeliveryTable.ORDER_DELIVERY.DELIVERY_STATE.eq(2));
        List<OrderDelivery> oOrderDeliveryList = oOrderDeliveryMapper.selectListByQuery(queryWrapper);
        return Result.ok(oOrderDeliveryList.size());
    }

    /**
     * 获取今日抢单
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getDayRobOrder(DeliveryInfoDto oDeliveryInfoDto) throws Exception {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(OrderTakeawayTable.ORDER_TAKEAWAY.DELIVERY_STATE.eq(3));
        //今天开始
        String createTimeBegin = DateTimeFormatter.ofPattern("yyyy-MM-dd 00:00:00") .withZone(ZoneId.systemDefault()).format(Instant.now().minus(0, ChronoUnit.DAYS));
        //今天结束
        String createTimeEnd = DateTimeFormatter.ofPattern("yyyy-MM-dd 23:59:59") .withZone(ZoneId.systemDefault()).format(Instant.now().minus(0, ChronoUnit.DAYS));
        queryWrapper.where(OrderTakeawayTable.ORDER_TAKEAWAY.CONNECT_TIME.between(DateUtils.stringTurnDate(createTimeBegin,"yyyy-MM-dd HH:mm:ss"),DateUtils.stringTurnDate(createTimeEnd,"yyyy-MM-dd HH:mm:ss")));
        List<OrderTakeaway> oOrderTakeawayList = oOrderTakeawayMapper.selectListByQuery(queryWrapper);
        if(CollectionUtils.isEmpty(oOrderTakeawayList)){
            return Result.ok(0);
        }
        //获取查到的订单ID集合
        List<String> oOrderTakeawayIdList = new ArrayList<>();
        oOrderTakeawayList.forEach(item->{oOrderTakeawayIdList.add(item.getId());});
        //获取查到的外卖订单配送
        queryWrapper = new QueryWrapper();
        queryWrapper.where(OrderDeliveryTableDef.ORDER_DELIVERY.ORDER_TAKEAWAY_ID.in(oOrderTakeawayIdList));
        queryWrapper.where(OrderDeliveryTableDef.ORDER_DELIVERY.DELIVERY_ID.eq(oDeliveryInfoDto.getId()));
        List<OrderDelivery> oOrderDeliveryList = oOrderDeliveryMapper.selectListByQuery(queryWrapper);
        return Result.ok(oOrderDeliveryList.size());
    }

    /**
     * 获取我的余额
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getDayRobOrderBalance(DeliveryInfoDto oDeliveryInfoDto) throws Exception {
        DeliveryInfo oDeliveryInfo = oDeliveryInfoMapper.selectOneById(oDeliveryInfoDto.getId());
        return Result.ok(oDeliveryInfo.getBalance());
    }

    /**
     * 获取可提现余额和累计余额
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getWithdrawalBalance(DeliveryInfoDto oDeliveryInfoDto) throws Exception {
        DeliveryInfo oDeliveryInfo = oDeliveryInfoMapper.selectOneById(oDeliveryInfoDto.getId());
        Map<String,Object> map = new HashMap<>();
        map.put("balance",oDeliveryInfo.getBalance());
        map.put("withdrawalTotal",oDeliveryInfo.getWithdrawalTotal());
        return Result.ok(map);
    }

    /**
     * 获取即将到账和累计到账
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getReceived(DeliveryInfoDto oDeliveryInfoDto) throws Exception {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(FlowTaskTableDef.FLOW_TASK.USER_ID.eq(oDeliveryInfoDto.getId()));
        List<FlowTask> oFlowTaskList = oFlowTaskMapper.selectListByQuery(queryWrapper);
        Map<String,Double> map = new HashMap<>();
        map.put("comingSoon",0.0);
        map.put("accumulated",0.0);
        for(FlowTask oFlowTask : oFlowTaskList){
            if(oFlowTask.getState() == 0){
                Double comingSoon = map.get("comingSoon");
                comingSoon += oFlowTask.getMoney();
                map.put("comingSoon",comingSoon);
            }else{
                Double accumulated = map.get("accumulated");
                accumulated += oFlowTask.getMoney();
                map.put("accumulated",accumulated);
            }
        }
        return Result.ok(map);
    }

    /**
     * 获取收入明细
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getIncome(DeliveryInfoDto oDeliveryInfoDto) throws Exception {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(FlowTaskTableDef.FLOW_TASK.DELIVERY_ID.eq(oDeliveryInfoDto.getId()));
        queryWrapper.where(FlowTaskTableDef.FLOW_TASK.STATE.eq(1));
        Map<String, Date> timeMap = new HashMap<>();
        if(oDeliveryInfoDto.getIncomeType() == 0){
            //获取本月收入
            timeMap = DateUtils.getMonthdayTime();

        }else if(oDeliveryInfoDto.getIncomeType() == 1){
            //获取今日收入
            timeMap = DateUtils.getTodayTime();
        }else{
            //获取昨日收入
            timeMap = DateUtils.getYesterdayTime();
        }
        queryWrapper.where(FlowTaskTableDef.FLOW_TASK.EXECUTE_TIME.between(timeMap.get("startTime"),timeMap.get("endTime")));
        queryWrapper.orderBy("execute_time desc");
        List<FlowTask> oFlowTaskList = oFlowTaskMapper.selectListByQuery(queryWrapper);
        return Result.ok(oFlowTaskList,oFlowTaskList.size());
    }

}

