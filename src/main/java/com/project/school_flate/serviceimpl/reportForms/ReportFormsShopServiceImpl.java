package com.project.school_flate.serviceimpl.reportForms;

import com.mybatisflex.core.query.QueryWrapper;
import com.project.school_flate.dto.shop.ShopInfoDto;
import com.project.school_flate.entity.flow.FlowTask;
import com.project.school_flate.entity.flow.table.FlowTaskTableDef;
import com.project.school_flate.entity.order.OrderTakeaway;
import com.project.school_flate.entity.shop.ShopInfo;
import com.project.school_flate.entity.table.order.OrderTakeawayTable;
import com.project.school_flate.mapper.flow.FlowTaskMapper;
import com.project.school_flate.mapper.order.OrderTakeawayMapper;
import com.project.school_flate.mapper.shop.ShopInfoMapper;
import com.project.school_flate.service.reportForms.ReportFormsShopService;
import com.project.school_flate.util.DateUtils;
import com.project.school_flate.util.Result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportFormsShopServiceImpl implements ReportFormsShopService {

    @Autowired
    private OrderTakeawayMapper oOrderTakeawayMapper;

    @Autowired
    private ShopInfoMapper oShopInfoMapper;

    @Autowired
    private FlowTaskMapper oFlowTaskMapper;

    /**
     * 获取累计单量
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getAllRobOrder(ShopInfoDto oShopInfoDto) throws Exception {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(OrderTakeawayTable.ORDER_TAKEAWAY.SHOP_ID.eq(oShopInfoDto.getId()));
        queryWrapper.where(OrderTakeawayTable.ORDER_TAKEAWAY.SHOP_STATE.eq(2));
        List<OrderTakeaway> oOrderTakeawayList = oOrderTakeawayMapper.selectListByQuery(queryWrapper);
        return Result.ok(oOrderTakeawayList.size());
    }

    /**
     * 获取今日抢单
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getDayRobOrder(ShopInfoDto oShopInfoDto) throws Exception {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(OrderTakeawayTable.ORDER_TAKEAWAY.SHOP_ID.eq(oShopInfoDto.getId()));
        queryWrapper.where(OrderTakeawayTable.ORDER_TAKEAWAY.SHOP_STATE.eq(2));
        //今天开始
        String createTimeBegin = DateTimeFormatter.ofPattern("yyyy-MM-dd 00:00:00") .withZone(ZoneId.systemDefault()).format(Instant.now().minus(0, ChronoUnit.DAYS));
        //今天结束
        String createTimeEnd = DateTimeFormatter.ofPattern("yyyy-MM-dd 23:59:59") .withZone(ZoneId.systemDefault()).format(Instant.now().minus(0, ChronoUnit.DAYS));
        queryWrapper.where(OrderTakeawayTable.ORDER_TAKEAWAY.CREATE_TIME.between(DateUtils.stringTurnDate(createTimeBegin,"yyyy-MM-dd HH:mm:ss"),DateUtils.stringTurnDate(createTimeEnd,"yyyy-MM-dd HH:mm:ss")));
        List<OrderTakeaway> oOrderTakeawayList = oOrderTakeawayMapper.selectListByQuery(queryWrapper);
        return Result.ok(oOrderTakeawayList.size());
    }

    /**
     * 获取我的余额
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getDayRobOrderBalance(ShopInfoDto oShopInfoDto) throws Exception {
        ShopInfo oShopInfo = oShopInfoMapper.selectOneById(oShopInfoDto.getId());
        return Result.ok(oShopInfo.getBalance());
    }

    /**
     * 获取可提现余额和累计提现
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getWithdrawalBalance(ShopInfoDto oShopInfoDto) throws Exception {
        ShopInfo oShopInfo = oShopInfoMapper.selectOneById(oShopInfoDto.getId());
        Map<String,Object> map = new HashMap<>();
        map.put("balance",oShopInfo.getBalance());
        map.put("withdrawalTotal",oShopInfo.getWithdrawalTotal());
        return Result.ok(map);
    }

    /**
     * 获取即将到账和累计到账
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getReceived(ShopInfoDto oShopInfoDto) throws Exception {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(FlowTaskTableDef.FLOW_TASK.SHOP_ID.eq(oShopInfoDto.getId()));
        List<FlowTask> oFlowTaskList = oFlowTaskMapper.selectListByQuery(queryWrapper);
        Map<String,Double> map = new HashMap<>();
        map.put("comingSoon",0.0);
        map.put("accumulated",0.0);
        for(FlowTask oFlowTask : oFlowTaskList){
            if(oFlowTask.getState() == 0){
                Double comingSoon = map.get("comingSoon");
                comingSoon += oFlowTask.getMoney();
                map.put("comingSoon",comingSoon);
            }else{
                Double accumulated = map.get("accumulated");
                accumulated += oFlowTask.getMoney();
                map.put("accumulated",accumulated);
            }
        }
        return Result.ok(map);
    }

    /**
     * 获取收入明细
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getIncome(ShopInfoDto oShopInfoDto) throws Exception {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(FlowTaskTableDef.FLOW_TASK.SHOP_ID.eq(oShopInfoDto.getId()));
        queryWrapper.where(FlowTaskTableDef.FLOW_TASK.STATE.eq(1));
        Map<String, Date> timeMap = new HashMap<>();
        if(oShopInfoDto.getIncomeType() == 0){
            //获取本月收入
            timeMap = DateUtils.getMonthdayTime();

        }else if(oShopInfoDto.getIncomeType() == 1){
            //获取今日收入
            timeMap = DateUtils.getTodayTime();
        }else{
            //获取昨日收入
            timeMap = DateUtils.getYesterdayTime();
        }
        queryWrapper.where(FlowTaskTableDef.FLOW_TASK.EXECUTE_TIME.between(timeMap.get("startTime"),timeMap.get("endTime")));
        queryWrapper.orderBy("execute_time desc");
        List<FlowTask> oFlowTaskList = oFlowTaskMapper.selectListByQuery(queryWrapper);
        return Result.ok(oFlowTaskList,oFlowTaskList.size());
    }

}
