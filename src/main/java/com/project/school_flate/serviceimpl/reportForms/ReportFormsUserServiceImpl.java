package com.project.school_flate.serviceimpl.reportForms;

import com.mybatisflex.core.query.QueryWrapper;
import com.project.school_flate.dto.shop.ShopInfoDto;
import com.project.school_flate.dto.user.UserInfoDto;
import com.project.school_flate.entity.flow.FlowTask;
import com.project.school_flate.entity.flow.table.FlowTaskTableDef;
import com.project.school_flate.entity.shop.ShopInfo;
import com.project.school_flate.entity.user.UserInfo;
import com.project.school_flate.mapper.flow.FlowTaskMapper;
import com.project.school_flate.mapper.user.UserInfoMapper;
import com.project.school_flate.service.reportForms.ReportFormsUserService;
import com.project.school_flate.util.DateUtils;
import com.project.school_flate.util.Result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportFormsUserServiceImpl implements ReportFormsUserService {

    @Autowired
    private UserInfoMapper oUserInfoMapper;

    @Autowired
    private FlowTaskMapper oFlowTaskMapper;

    /**
     * 获取我的余额
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getDayRobOrderBalance(UserInfoDto oUserInfoDto) throws Exception {
        UserInfo oUserInfo = oUserInfoMapper.selectOneById(oUserInfoDto.getId());
        return Result.ok(oUserInfo.getBalance());
    }

    /**
     * 获取可提现余额和累计余额
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getWithdrawalBalance(UserInfoDto oUserInfoDto) throws Exception {
        UserInfo oUserInfo = oUserInfoMapper.selectOneById(oUserInfoDto.getId());
        Map<String,Object> map = new HashMap<>();
        map.put("balance",oUserInfo.getBalance());
        map.put("withdrawalTotal",oUserInfo.getWithdrawalTotal());
        return Result.ok(map);
    }

    /**
     * 获取即将到账和累计到账
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getReceived(UserInfoDto oUserInfoDto) throws Exception {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(FlowTaskTableDef.FLOW_TASK.USER_ID.eq(oUserInfoDto.getId()));
        List<FlowTask> oFlowTaskList = oFlowTaskMapper.selectListByQuery(queryWrapper);
        Map<String,Double> map = new HashMap<>();
        map.put("comingSoon",0.0);
        map.put("accumulated",0.0);
        for(FlowTask oFlowTask : oFlowTaskList){
            if(oFlowTask.getState() == 0){
                Double comingSoon = map.get("comingSoon");
                comingSoon += oFlowTask.getMoney();
                map.put("comingSoon",comingSoon);
            }else{
                Double accumulated = map.get("accumulated");
                accumulated += oFlowTask.getMoney();
                map.put("accumulated",accumulated);
            }
        }
        return Result.ok(map);
    }

    /**
     * 获取收入明细
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getIncome(UserInfoDto oUserInfoDto) throws Exception {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(FlowTaskTableDef.FLOW_TASK.USER_ID.eq(oUserInfoDto.getId()));
        queryWrapper.where(FlowTaskTableDef.FLOW_TASK.STATE.eq(1));
        Map<String, Date> timeMap = new HashMap<>();
        if(oUserInfoDto.getIncomeType() == 0){
            //获取本月收入
            timeMap = DateUtils.getMonthdayTime();

        }else if(oUserInfoDto.getIncomeType() == 1){
            //获取今日收入
            timeMap = DateUtils.getTodayTime();
        }else{
            //获取昨日收入
            timeMap = DateUtils.getYesterdayTime();
        }
        queryWrapper.where(FlowTaskTableDef.FLOW_TASK.EXECUTE_TIME.between(timeMap.get("startTime"),timeMap.get("endTime")));
        queryWrapper.orderBy("execute_time desc");
        List<FlowTask> oFlowTaskList = oFlowTaskMapper.selectListByQuery(queryWrapper);
        return Result.ok(oFlowTaskList,oFlowTaskList.size());
    }

}
