package com.project.school_flate.serviceimpl.com;

import cn.dev33.satoken.stp.StpUtil;
import com.alibaba.fastjson2.JSONObject;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.relation.RelationManager;
import com.mybatisflex.core.update.UpdateChain;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.com.ComWithdrawalFlowDto;
import com.project.school_flate.entity.com.*;
import com.project.school_flate.entity.com.ComWithdrawalFlow;
import com.project.school_flate.entity.com.table.ComWithdrawalFlowTableDef;
import com.project.school_flate.entity.delivery.DeliveryInfo;
import com.project.school_flate.entity.delivery.table.DeliveryInfoTableDef;
import com.project.school_flate.entity.flow.FlowBusiness;
import com.project.school_flate.entity.flow.FlowMoney;
import com.project.school_flate.entity.flow.table.FlowBusinessTableDef;
import com.project.school_flate.entity.flow.table.FlowMoneyTableDef;
import com.project.school_flate.entity.order.OrderDelivery;
import com.project.school_flate.entity.order.table.OrderDeliveryTableDef;
import com.project.school_flate.entity.shop.ShopInfo;
import com.project.school_flate.entity.shop.table.ShopInfoTableDef;
import com.project.school_flate.entity.user.UserInfo;
import com.project.school_flate.entity.user.table.UserInfoTableDef;
import com.project.school_flate.mapper.ComWithdrawalFlowMapper;
import com.project.school_flate.mapper.delivery.DeliveryInfoMapper;
import com.project.school_flate.mapper.flow.FlowBusinessMapper;
import com.project.school_flate.mapper.flow.FlowMoneyMapper;
import com.project.school_flate.mapper.shop.ShopAccountMapper;
import com.project.school_flate.mapper.shop.ShopInfoMapper;
import com.project.school_flate.mapper.user.UserInfoMapper;
import com.project.school_flate.service.com.ComWithdrawalFlowService;
import com.project.school_flate.service.com.WeiXinPayService;
import com.project.school_flate.util.DateUtils;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import com.project.school_flate.util.system.ComVariableUtil;
import com.project.school_flate.util.wechat.PayDto;
import com.project.school_flate.util.wechat.WxMessagesManager;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.*;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class ComWithdrawalFlowServiceImpl extends ServiceImpl<ComWithdrawalFlowMapper, ComWithdrawalFlow> implements ComWithdrawalFlowService {

    @Autowired
    private ComWithdrawalFlowMapper oComWithdrawalFlowMapper;

    @Autowired
    private WeiXinPayService oWeiXinPayService;

    @Autowired
    private UserInfoMapper oUserInfoMapper;

    @Autowired
    private DeliveryInfoMapper oDeliveryInfoMapper;

    @Autowired
    private ShopInfoMapper oShopInfoMapper;

    @Autowired
    private ShopAccountMapper oShopAccountMapper;

    @Autowired
    private FlowBusinessMapper oFlowBusinessMapper;

    @Autowired
    private FlowMoneyMapper oFlowMoneyMapper;

    @Autowired
    private WxMessagesManager wxMessagesManager;

    /**
     * 获取微信转账流水
     * @param oComWithdrawalFlowDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getComWithdrawalFlow(ComWithdrawalFlowDto oComWithdrawalFlowDto) throws Exception {
        List<ComWithdrawalFlow> oComWithdrawalFlowList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入类型
        if(oComWithdrawalFlowDto.getType() != null){
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.TYPE.eq(oComWithdrawalFlowDto.getType()));
        }
        //判断是否传入状态
        if(oComWithdrawalFlowDto.getState() != null){
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.STATE.eq(oComWithdrawalFlowDto.getState()));
        }
        //判断是否传入审核状态
        if(oComWithdrawalFlowDto.getExamineState() != null){
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.EXAMINE_STATE.eq(oComWithdrawalFlowDto.getExamineState()));
        }
        //判断是否传入查询时间
        if(oComWithdrawalFlowDto.getQueryBeginTime() != null && oComWithdrawalFlowDto.getQueryEndTime() != null){
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.CREATE_TIME.between(oComWithdrawalFlowDto.getQueryBeginTime(),oComWithdrawalFlowDto.getQueryEndTime()));
        }
        //判断是否传入用户ID
        if(StringUtils.isNotBlank(oComWithdrawalFlowDto.getUserId())){
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.USER_ID.eq(oComWithdrawalFlowDto.getUserId()));
        }
        //判断是否传入配送员ID
        if(StringUtils.isNotBlank(oComWithdrawalFlowDto.getDeliveryId())){
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.DELIVERY_ID.eq(oComWithdrawalFlowDto.getDeliveryId()));
        }
        //判断是否传入店铺ID
        if(StringUtils.isNotBlank(oComWithdrawalFlowDto.getShopId())){
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.SHOP_ID.eq(oComWithdrawalFlowDto.getShopId()));
        }
        //判断是否传入用户名称
        if(StringUtils.isNotBlank(oComWithdrawalFlowDto.getUserName())){
            QueryWrapper qrUser = new QueryWrapper();
            qrUser.where(UserInfoTableDef.USER_INFO.NAME.like(oComWithdrawalFlowDto.getUserName()));
            List<UserInfo> oUserInfoList = oUserInfoMapper.selectListByQuery(qrUser);
            if(CollectionUtils.isEmpty(oUserInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的用户ID
            List<String> oUserInfoIdList = new ArrayList<>();
            oUserInfoList.forEach(item->{oUserInfoIdList.add(item.getId());});
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.USER_ID.in(oUserInfoIdList));
        }
        //判断是否传入配送员名称
        if(StringUtils.isNotBlank(oComWithdrawalFlowDto.getDeliveryName())){
            QueryWrapper qrDelivery = new QueryWrapper();
            qrDelivery.where(DeliveryInfoTableDef.DELIVERY_INFO.NAME.like(oComWithdrawalFlowDto.getDeliveryName()));
            List<DeliveryInfo> oDeliveryInfoList = oDeliveryInfoMapper.selectListByQuery(qrDelivery);
            if(CollectionUtils.isEmpty(oDeliveryInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的配送员ID
            List<String> oDeliveryInfoIdList = new ArrayList<>();
            oDeliveryInfoList.forEach(item->{oDeliveryInfoIdList.add(item.getId());});
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.DELIVERY_ID.in(oDeliveryInfoIdList));
        }
        //判断是否传入店铺名称
        if(StringUtils.isNotBlank(oComWithdrawalFlowDto.getShopName())){
            QueryWrapper qrShop = new QueryWrapper();
            qrShop.where(ShopInfoTableDef.SHOP_INFO.NAME.like(oComWithdrawalFlowDto.getShopName()));
            List<ShopInfo> oShopInfoList = oShopInfoMapper.selectListByQuery(qrShop);
            if(CollectionUtils.isEmpty(oShopInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的店铺ID
            List<String> oShopInfoIdList = new ArrayList<>();
            oShopInfoList.forEach(item->{oShopInfoIdList.add(item.getId());});
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.SHOP_ID.in(oShopInfoIdList));
        }
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oComWithdrawalFlowDto.getPage() != null && oComWithdrawalFlowDto.getLimit() != null){
            Page<ComWithdrawalFlow> ComWithdrawalFlowPage = oComWithdrawalFlowMapper.paginateWithRelations(oComWithdrawalFlowDto.getPage(),oComWithdrawalFlowDto.getLimit(),queryWrapper);
            oComWithdrawalFlowList = ComWithdrawalFlowPage.getRecords();
            total = ComWithdrawalFlowPage.getTotalRow();
        }else{
            oComWithdrawalFlowList = oComWithdrawalFlowMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oComWithdrawalFlowList.size();
        }
        //PoToDto
        List<ComWithdrawalFlowDto> oComWithdrawalFlowDtoList = (List<ComWithdrawalFlowDto>) PoToDTO.poToDtoList(oComWithdrawalFlowList,new ComWithdrawalFlowDto());
        return Result.ok(oComWithdrawalFlowDtoList,total);
    }

    /**
     * 添加微信转账流水
     * @param oComWithdrawalFlowDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addComWithdrawalFlow(ComWithdrawalFlowDto oComWithdrawalFlowDto) throws Exception {
        ComWithdrawalFlow oComWithdrawalFlow = new ComWithdrawalFlow();
        PoToDTO.poToDto(oComWithdrawalFlowDto,oComWithdrawalFlow);
        //创建业务流水
        FlowBusiness oFlowBusiness = new FlowBusiness();
        oFlowBusiness.setType(2);
        oFlowBusiness.setInflowDirection(1);
        oFlowBusiness.setInflowWay(0);
        oFlowBusiness.setMoney(oComWithdrawalFlow.getTotal());
        //创建金额流水
        FlowMoney oFlowMoney = new FlowMoney();
        oFlowMoney.setType(2);
        oFlowMoney.setInflowDirection(1);
        oFlowMoney.setInflowWay(0);
        oFlowMoney.setMoney(oComWithdrawalFlow.getTotal());
        if(oComWithdrawalFlow.getType() == 0){
            //获取用户信息
            UserInfo oUserInfo = oUserInfoMapper.selectOneById(oComWithdrawalFlow.getUserId());
            if(oUserInfo == null || oUserInfo.getState() == 0){
                return Result.fail("用户不存在或已被冻结");
            }
            if(oUserInfo.getBalance().compareTo(oComWithdrawalFlow.getTotal()) < 0){
                return Result.fail("用户余额不足");
            }
            //获取是否还有提现审核中
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.TYPE.eq(0));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.STATE.eq(0));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.EXAMINE_STATE.eq(2));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.USER_ID.eq(oUserInfo.getId()));
            if(CollectionUtils.isNotEmpty(oComWithdrawalFlowMapper.selectListByQuery(queryWrapper))){
                return Result.fail("还有提现未审核，请审核后再进行提现");
            }
            //获取上一次的提现时间
            queryWrapper = new QueryWrapper();
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.TYPE.eq(0));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.STATE.ne(2));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.EXAMINE_STATE.ne(0));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.USER_ID.eq(oUserInfo.getId()));
            queryWrapper.orderBy("create_time desc");
            queryWrapper.limit(1);
            ComWithdrawalFlow lastComWithdrawalFlow = oComWithdrawalFlowMapper.selectOneByQuery(queryWrapper);
            if(lastComWithdrawalFlow != null && !DateUtils.getTimeGeDay(lastComWithdrawalFlow.getCreateTime(),new Date())){
                return Result.fail("一天只能提现一次");
            }
            oFlowBusiness.setUserId(oComWithdrawalFlow.getUserId());
            oFlowMoney.setUserId(oComWithdrawalFlow.getUserId());
            //修改用户信息
            UpdateChain.of(UserInfo.class)
                    .setRaw(UserInfo::getBalance, "balance - " + oComWithdrawalFlow.getAllTotal())
                    .where(UserInfo::getId).eq(oUserInfo.getId())
                    .update();
        }else if(oComWithdrawalFlow.getType() == 1){
            //获取配送员信息
            DeliveryInfo oDeliveryInfo = oDeliveryInfoMapper.selectOneById(oComWithdrawalFlow.getDeliveryId());
            if(oDeliveryInfo == null || oDeliveryInfo.getState() == 0){
                return Result.fail("配送员不存在或已被冻结");
            }
            if(oDeliveryInfo.getBalance().compareTo(oComWithdrawalFlow.getTotal()) < 0){
                return Result.fail("配送员余额不足");
            }
            //获取是否还有提现审核中
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.TYPE.eq(1));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.STATE.eq(0));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.EXAMINE_STATE.eq(2));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.DELIVERY_ID.eq(oDeliveryInfo.getId()));
            if(CollectionUtils.isNotEmpty(oComWithdrawalFlowMapper.selectListByQuery(queryWrapper))){
                return Result.fail("还有提现未审核，请审核后再进行提现");
            }
            //获取上一次的提现时间
            queryWrapper = new QueryWrapper();
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.TYPE.eq(1));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.STATE.ne(2));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.EXAMINE_STATE.ne(0));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.DELIVERY_ID.eq(oDeliveryInfo.getId()));
            queryWrapper.orderBy("create_time desc");
            queryWrapper.limit(1);
            ComWithdrawalFlow lastComWithdrawalFlow = oComWithdrawalFlowMapper.selectOneByQuery(queryWrapper);
            if(lastComWithdrawalFlow != null && !DateUtils.getTimeGeDay(lastComWithdrawalFlow.getCreateTime(),new Date())){
                return Result.fail("一天只能提现一次");
            }
            oFlowBusiness.setDeliveryId(oComWithdrawalFlow.getDeliveryId());
            oFlowMoney.setDeliveryId(oComWithdrawalFlow.getDeliveryId());
            //修改配送员信息
            UpdateChain.of(DeliveryInfo.class)
                    .setRaw(DeliveryInfo::getBalance, "balance - " + oComWithdrawalFlow.getAllTotal())
                    .where(DeliveryInfo::getId).eq(oDeliveryInfo.getId())
                    .update();
        }else{
            //获取店铺信息
            ShopInfo oShopInfo = oShopInfoMapper.selectOneById(oComWithdrawalFlow.getShopId());
            if(oShopInfo == null || oShopInfo.getState() == 0){
                return Result.fail("店铺不存在或已被冻结");
            }
            if(oShopInfo.getBalance().compareTo(oComWithdrawalFlow.getTotal()) < 0){
                return Result.fail("店铺余额不足");
            }
            //获取是否还有提现审核中
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.TYPE.eq(2));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.STATE.eq(0));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.EXAMINE_STATE.eq(2));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.SHOP_ID.eq(oShopInfo.getId()));
            if(CollectionUtils.isNotEmpty(oComWithdrawalFlowMapper.selectListByQuery(queryWrapper))){
                return Result.fail("还有提现未审核，请审核后再进行提现");
            }
            //获取上一次的提现时间
            queryWrapper = new QueryWrapper();
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.TYPE.eq(2));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.STATE.ne(2));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.EXAMINE_STATE.ne(0));
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.SHOP_ID.eq(oShopInfo.getId()));
            queryWrapper.orderBy("create_time desc");
            queryWrapper.limit(1);
            ComWithdrawalFlow lastComWithdrawalFlow = oComWithdrawalFlowMapper.selectOneByQuery(queryWrapper);
            if(lastComWithdrawalFlow != null && !DateUtils.getTimeGeDay(lastComWithdrawalFlow.getCreateTime(),new Date())){
                return Result.fail("一天只能提现一次");
            }
            oFlowBusiness.setShopId(oComWithdrawalFlow.getShopId());
            oFlowMoney.setShopId(oComWithdrawalFlow.getShopId());
            //修改店铺信息
            UpdateChain.of(ShopInfo.class)
                    .setRaw(ShopInfo::getBalance, "balance - " + oComWithdrawalFlow.getAllTotal())
                    .where(ShopInfo::getId).eq(oShopInfo.getId())
                    .update();
        }
        //添加微信转账流水
        if(oComWithdrawalFlowMapper.insert(oComWithdrawalFlow) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加微信转账流水失败");
        }
        //创建业务流水
        oFlowBusiness.setOrderId(oComWithdrawalFlow.getId());
        if(oFlowBusinessMapper.insert(oFlowBusiness) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加微信转账流水失败");
        }
        //创建金额流水
        oFlowMoney.setOrderId(oComWithdrawalFlow.getId());
        if(oFlowMoneyMapper.insert(oFlowMoney) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加微信转账流水失败");
        }
        return Result.ok();
    }

    /**
     * 平台确认转账
     * @param oComWithdrawalFlowDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result comOkWithdrawal(ComWithdrawalFlowDto oComWithdrawalFlowDto) throws Exception {
        //重新获取微信流水
        ComWithdrawalFlow oComWithdrawalFlow = oComWithdrawalFlowMapper.selectOneWithRelationsById(oComWithdrawalFlowDto.getId());
        if(oComWithdrawalFlow.getState() != 0 || oComWithdrawalFlow.getExamineState() != 2){
            return Result.fail("提现申请已审核");
        }
        oComWithdrawalFlow.setReason(oComWithdrawalFlowDto.getReason());
        oComWithdrawalFlow.setAuditTime(new Date());
        //获取登陆管理员
        ComAdmin oComAdmin = new ComAdmin();
        if(StpUtil.isLogin()){
            oComAdmin = (ComAdmin) StpUtil.getSession().get("comAdmin");
            if(oComAdmin == null || StringUtils.isBlank(oComAdmin.getId())){
                return Result.fail("请先重新登录");
            }
        }else{
            return Result.fail("请先重新登录");
        }
        oComWithdrawalFlow.setAdminId(oComAdmin.getId());
        PayDto oPayDto = new PayDto();
        oPayDto.setTotalAmount(oComWithdrawalFlow.getTotal());
        if(oComWithdrawalFlow.getType() == 0){
            //获取用户信息
            UserInfo oUserInfo = oUserInfoMapper.selectOneById(oComWithdrawalFlow.getUserId());
            if(oUserInfo == null || oUserInfo.getState() == 0){
                return Result.fail("用户不存在或已被冻结");
            }
            //修改用户信息
            UpdateChain.of(UserInfo.class)
                    .setRaw(UserInfo::getWithdrawalNum, "withdrawal_num + " + 1)
                    .where(UserInfo::getId).eq(oUserInfo.getId())
                    .update();
            oPayDto.setOutBatchNo(oComWithdrawalFlow.getId());
            oPayDto.setBatchName(oUserInfo.getName() + ":" + DateUtils.dateTurnString(new Date(),"yyyyMMdd") + "转账记录");
            oPayDto.setOutDetailNo(oUserInfo.getId() + (oUserInfo.getWithdrawalNum() + 1));
            oPayDto.setOpenid(oUserInfo.getWxOptionId());
            oPayDto.setNotifyUrl("https://www.fanshanming.top/api/comWithdrawalFlow/comWithdrawalFlowUserCallback");
            //用户转账（回调业务逻辑）
            comWithdrawalFlowUserCallback(oUserInfo.getId(),oComWithdrawalFlow.getId(),oComWithdrawalFlow.getTotal(),oUserInfo.getWxOptionId(),oComWithdrawalFlow.getAllTotal());
        }else if(oComWithdrawalFlow.getType() == 1){
            //获取配送员信息
            DeliveryInfo oDeliveryInfo = oDeliveryInfoMapper.selectOneById(oComWithdrawalFlow.getUserId());
            if(oDeliveryInfo == null || oDeliveryInfo.getState() == 0){
                return Result.fail("配送员不存在或已被冻结");
            }
            //修改配送员信息
            UpdateChain.of(DeliveryInfo.class)
                    .setRaw(DeliveryInfo::getWithdrawalNum, "withdrawal_num + " + 1)
                    .where(DeliveryInfo::getId).eq(oDeliveryInfo.getId())
                    .update();
            oPayDto.setOutBatchNo(oComWithdrawalFlow.getId());
            oPayDto.setBatchName(oDeliveryInfo.getName() + ":" + DateUtils.dateTurnString(new Date(),"yyyyMMdd") + "转账记录");
            oPayDto.setOutDetailNo(oDeliveryInfo.getId() + (oDeliveryInfo.getWithdrawalNum() + 1));
            oPayDto.setOpenid(oDeliveryInfo.getWxOptionId());
            oPayDto.setNotifyUrl("https://www.fanshanming.top/api/comWithdrawalFlow/comWithdrawalFlowDeliveryCallback");
            //配送员转账（回调业务逻辑）
            comWithdrawalFlowDeliveryCallback(oDeliveryInfo.getId(),oComWithdrawalFlow.getId(),oComWithdrawalFlow.getTotal(),oDeliveryInfo.getWxOptionId(),oComWithdrawalFlow.getAllTotal());
        }else{
            //获取店铺信息
            RelationManager.setMaxDepth(1);
            RelationManager.addQueryRelations("shopAccount");
            ShopInfo oShopInfo = oShopInfoMapper.selectOneWithRelationsById(oComWithdrawalFlow.getUserId());
            if(oShopInfo == null || oShopInfo.getState() == 0){
                return Result.fail("店铺不存在或已被冻结");
            }
            //修改店铺信息
            UpdateChain.of(ShopInfo.class)
                    .setRaw(ShopInfo::getWithdrawalNum, "withdrawal_num + " + 1)
                    .where(ShopInfo::getId).eq(oShopInfo.getId())
                    .update();
            oPayDto.setOutBatchNo(oComWithdrawalFlow.getId());
            oPayDto.setBatchName(oShopInfo.getName() + ":" + DateUtils.dateTurnString(new Date(),"yyyyMMdd") + "转账记录");
            oPayDto.setOutDetailNo(oShopInfo.getId() + (oShopInfo.getWithdrawalNum() + 1));
            oPayDto.setOpenid(oShopInfo.getShopAccount().getWxOptionId());
            oPayDto.setNotifyUrl("https://www.fanshanming.top/api/comWithdrawalFlow/comWithdrawalFlowShopCallback");
            //配送员转账（回调业务逻辑）
            comWithdrawalFlowUserCallback(oShopInfo.getId(),oComWithdrawalFlow.getId(),oComWithdrawalFlow.getTotal(),oShopInfo.getShopAccount().getWxOptionId(),oComWithdrawalFlow.getAllTotal());
        }
        //转账
//        Result result = oWeiXinPayService.transferPay(oPayDto);
        //处理微信流水
        oComWithdrawalFlow.setExamineState(1);
        if(oComWithdrawalFlowMapper.update(oComWithdrawalFlow) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("平台确认转账失败");
        }
        return Result.ok();
    }

    /**
     * 平台驳回转账
     * @param oComWithdrawalFlowDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result comRejectWithdrawal(ComWithdrawalFlowDto oComWithdrawalFlowDto) throws Exception {
        //重新获取微信流水
        ComWithdrawalFlow oComWithdrawalFlow = oComWithdrawalFlowMapper.selectOneWithRelationsById(oComWithdrawalFlowDto.getId());
        if(oComWithdrawalFlow.getState() != 0 || oComWithdrawalFlow.getExamineState() != 2){
            return Result.fail("提现申请已审核");
        }
        oComWithdrawalFlow.setReason(oComWithdrawalFlowDto.getReason());
        oComWithdrawalFlow.setAuditTime(new Date());
        //获取登陆管理员
        ComAdmin oComAdmin = new ComAdmin();
        if(StpUtil.isLogin()){
            oComAdmin = (ComAdmin) StpUtil.getSession().get("comAdmin");
            if(oComAdmin == null || StringUtils.isBlank(oComAdmin.getId())){
                return Result.fail("请先重新登录");
            }
        }else{
            return Result.fail("请先重新登录");
        }
        oComWithdrawalFlow.setAdminId(oComAdmin.getId());
        //处理微信流水
        oComWithdrawalFlow.setExamineState(0);
        if(oComWithdrawalFlowMapper.update(oComWithdrawalFlow) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("平台确认转账失败");
        }
        //推送消息
        Map<String,String> pushMap = new HashMap<>();
        pushMap.put("type","用户提现");
        pushMap.put("oOrderTakeawayId",oComWithdrawalFlow.getId());
        pushMap.put("state","提现失败");
        pushMap.put("content","您的提现已驳回，请重新尝试");
        if(oComWithdrawalFlow.getType() == 0){
            //修改用户信息
            UpdateChain.of(UserInfo.class)
                    .setRaw(UserInfo::getBalance, "balance + " + oComWithdrawalFlow.getAllTotal())
                    .where(UserInfo::getId).eq(oComWithdrawalFlow.getUserId())
                    .update();
            wxMessagesManager.pushWxMessages(oComWithdrawalFlow.getUserInfo().getWxOptionId(),pushMap);
        }else if(oComWithdrawalFlow.getType() == 1){
            //修改配送员信息
            UpdateChain.of(DeliveryInfo.class)
                    .setRaw(DeliveryInfo::getBalance, "balance + " + oComWithdrawalFlow.getAllTotal())
                    .where(DeliveryInfo::getId).eq(oComWithdrawalFlow.getDeliveryId())
                    .update();
            wxMessagesManager.pushWxMessages(oComWithdrawalFlow.getDeliveryInfo().getWxOptionId(),pushMap);
        }else{
            //修改店铺信息
            UpdateChain.of(ShopInfo.class)
                    .setRaw(ShopInfo::getBalance, "balance + " + oComWithdrawalFlow.getAllTotal())
                    .where(ShopInfo::getId).eq(oComWithdrawalFlow.getShopId())
                    .update();
            wxMessagesManager.pushWxMessages(oComWithdrawalFlow.getShopInfo().getShopAccount().getWxOptionId(),pushMap);
        }
        return Result.ok();
    }

    /**
     * 用户转账（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, String> comWithdrawalFlowUserCallback(JSONObject jsonObject) throws Exception {
        try{
            System.out.println("-------------------------------------------");
            System.out.println("退款通知" + jsonObject);
            System.out.println("-------------------------------------------");
            String outTradeNo = oWeiXinPayService.notifyOrder(jsonObject,"退款成功").get("out_trade_no").toString();
            //判断是否已经回调
            ComWithdrawalFlow oComWithdrawalFlow = oComWithdrawalFlowMapper.selectOneById(outTradeNo);
            if(oComWithdrawalFlow.getState() == 0){

            }
        }catch (Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }finally {
            Map<String, String> res = new HashMap<>();
            res.put("code", "SUCCESS");
            return res;
        }

    }

    /**
     * 用户转账（回调业务逻辑）
     * @param userId
     * @return
     * @throws Exception
     */
    public void comWithdrawalFlowUserCallback(String userId, String oComWithdrawalFlowId, Double total, String wxOptionId, Double allTotal) throws Exception {
        //修改业务流水
        UpdateChain.of(FlowBusiness.class)
                .set(FlowBusiness::getState, 1)
                .where(FlowBusiness::getOrderId).eq(oComWithdrawalFlowId)
                .update();
        //修改金额流水
        UpdateChain.of(FlowMoney.class)
                .set(FlowMoney::getState, 1)
                .where(FlowMoney::getOrderId).eq(oComWithdrawalFlowId)
                .update();
        //处理微信流水
        UpdateChain.of(ComWithdrawalFlow.class)
                .set(ComWithdrawalFlow::getState, 1)
                .where(ComWithdrawalFlow::getId).eq(oComWithdrawalFlowId)
                .update();
        //修改用户信息
        UpdateChain.of(UserInfo.class)
                .setRaw(UserInfo::getWithdrawalTotal, "withdrawal_total + " + allTotal)
                .where(UserInfo::getId).eq(userId)
                .update();
        //推送消息
        Map<String,String> pushMap = new HashMap<>();
        pushMap.put("type","用户提现");
        pushMap.put("oOrderTakeawayId",oComWithdrawalFlowId);
        pushMap.put("state","提现成功");
        pushMap.put("content","您的提现已成功");
        wxMessagesManager.pushWxMessages(wxOptionId,pushMap);
    }

    /**
     * 配送员转账（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, String> comWithdrawalFlowDeliveryCallback(JSONObject jsonObject) throws Exception {
        try{
            System.out.println("-------------------------------------------");
            System.out.println("退款通知" + jsonObject);
            System.out.println("-------------------------------------------");
            String outTradeNo = oWeiXinPayService.notifyOrder(jsonObject,"退款成功").get("out_trade_no").toString();
            //判断是否已经回调
            ComWithdrawalFlow oComWithdrawalFlow = oComWithdrawalFlowMapper.selectOneById(outTradeNo);
            if(oComWithdrawalFlow.getState() == 0){

            }
        }catch (Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }finally {
            Map<String, String> res = new HashMap<>();
            res.put("code", "SUCCESS");
            return res;
        }

    }

    /**
     * 配送员转账（回调业务逻辑）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    public void comWithdrawalFlowDeliveryCallback(String deliveryId, String oComWithdrawalFlowId, Double total, String wxOptionId, Double allTotal) throws Exception {
        //修改业务流水
        UpdateChain.of(FlowBusiness.class)
                .set(FlowBusiness::getState, 1)
                .where(FlowBusiness::getOrderId).eq(oComWithdrawalFlowId)
                .update();
        //修改金额流水
        UpdateChain.of(FlowMoney.class)
                .set(FlowMoney::getState, 1)
                .where(FlowMoney::getOrderId).eq(oComWithdrawalFlowId)
                .update();
        //处理微信流水
        UpdateChain.of(ComWithdrawalFlow.class)
                .set(ComWithdrawalFlow::getState, 1)
                .where(ComWithdrawalFlow::getId).eq(oComWithdrawalFlowId)
                .update();
        //修改配送员信息
        UpdateChain.of(DeliveryInfo.class)
                .setRaw(DeliveryInfo::getWithdrawalTotal, "withdrawal_total + " + allTotal)
                .where(DeliveryInfo::getId).eq(deliveryId)
                .update();
        //推送消息
        Map<String,String> pushMap = new HashMap<>();
        pushMap.put("type","用户提现");
        pushMap.put("oOrderTakeawayId",oComWithdrawalFlowId);
        pushMap.put("state","提现成功");
        pushMap.put("content","您的提现已成功");
        wxMessagesManager.pushWxMessages(wxOptionId,pushMap);
    }

    /**
     * 店铺转账（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, String> comWithdrawalFlowShopCallback(JSONObject jsonObject) throws Exception {
        try{
            System.out.println("-------------------------------------------");
            System.out.println("退款通知" + jsonObject);
            System.out.println("-------------------------------------------");
            String outTradeNo = oWeiXinPayService.notifyOrder(jsonObject,"退款成功").get("out_trade_no").toString();
            //判断是否已经回调
            ComWithdrawalFlow oComWithdrawalFlow = oComWithdrawalFlowMapper.selectOneById(outTradeNo);
            if(oComWithdrawalFlow.getState() == 0){

            }
        }catch (Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }finally {
            Map<String, String> res = new HashMap<>();
            res.put("code", "SUCCESS");
            return res;
        }

    }

    /**
     * 店铺转账（回调业务逻辑）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    public void comWithdrawalFlowShopCallback(String shopId, String oComWithdrawalFlowId, Double total, String wxOptionId, Double allTotal) throws Exception {
        //修改业务流水
        UpdateChain.of(FlowBusiness.class)
                .set(FlowBusiness::getState, 1)
                .where(FlowBusiness::getOrderId).eq(oComWithdrawalFlowId)
                .update();
        //修改金额流水
        UpdateChain.of(FlowMoney.class)
                .set(FlowMoney::getState, 1)
                .where(FlowMoney::getOrderId).eq(oComWithdrawalFlowId)
                .update();
        //处理微信流水
        UpdateChain.of(ComWithdrawalFlow.class)
                .set(ComWithdrawalFlow::getState, 1)
                .where(ComWithdrawalFlow::getId).eq(oComWithdrawalFlowId)
                .update();
        //修改配送员信息
        UpdateChain.of(ShopInfo.class)
                .setRaw(ShopInfo::getWithdrawalTotal, "withdrawal_total + " + allTotal)
                .where(ShopInfo::getId).eq(shopId)
                .update();
        //推送消息
        Map<String,String> pushMap = new HashMap<>();
        pushMap.put("type","用户提现");
        pushMap.put("oOrderTakeawayId",oComWithdrawalFlowId);
        pushMap.put("state","提现成功");
        pushMap.put("content","您的提现已成功");
        wxMessagesManager.pushWxMessages(wxOptionId,pushMap);
    }

    /**
     * 获取微信转账规则
     * @param oComWithdrawalFlowDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getComWithdrawalFlowRule(ComWithdrawalFlowDto oComWithdrawalFlowDto) throws Exception {
        Map<String,Object> map = new HashMap<>();
        map.put("minWithdrawal",ComVariableUtil.getSystemValue("min_withdrawal"));
        map.put("withdrawalFees",ComVariableUtil.getSystemValue("withdrawal_fees"));
        return Result.ok(map);
    }

}
