package com.project.school_flate.serviceimpl.com;

import cn.dev33.satoken.stp.StpUtil;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.project.school_flate.dto.com.ComAdminDto;
import com.project.school_flate.entity.com.ComAdmin;
import com.project.school_flate.entity.table.com.ComAdminTable;
import com.project.school_flate.mapper.com.ComAdminMapper;
import com.project.school_flate.service.com.ComAdminService;
import com.project.school_flate.util.Md5Util;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author sunht
 * @Date 2023/10/7 9:47
 * @Version v1.0.0
 * @Message
 **/
@Service(value = "AdminService")
public class ComAdminServiceImpl implements ComAdminService {

    @Autowired
    private ComAdminMapper oComAdminMapper;

    /**
     * 获取管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getComAdmin(ComAdminDto oComAdminDto) throws Exception {
        List<ComAdmin> oComAdminList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(ComAdminTable.COM_ADMIN.STATE.eq(1));
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oComAdminDto.getPage() != null && oComAdminDto.getLimit() != null){
            Page<ComAdmin> ComAdminPage = oComAdminMapper.paginateWithRelations(oComAdminDto.getPage(),oComAdminDto.getLimit(),queryWrapper);
            oComAdminList = ComAdminPage.getRecords();
            total = ComAdminPage.getTotalRow();
        }else{
            oComAdminList = oComAdminMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oComAdminList.size();
        }
        //PoToDto
        List<ComAdminDto> oComAdminDtoList = (List<ComAdminDto>) PoToDTO.poToDtoList(oComAdminList,new ComAdminDto());
        return Result.ok(oComAdminDtoList,total);
    }

    /**
     * 添加管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addComAdmin(ComAdminDto oComAdminDto) throws Exception {
        ComAdmin oComAdmin = new ComAdmin();
        PoToDTO.poToDto(oComAdminDto,oComAdmin);
        if(oComAdminMapper.insert(oComAdmin) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加管理员失败");
        }
        return Result.ok("添加管理员成功");
    }

    /**
     * 修改管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateComAdmin(ComAdminDto oComAdminDto) throws Exception {
        ComAdmin oComAdmin = new ComAdmin();
        PoToDTO.poToDto(oComAdminDto,oComAdmin);
        if(oComAdminMapper.update(oComAdmin) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改管理员失败");
        }
        return Result.ok("修改管理员成功");
    }

    /**
     * 冻结管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result freezeComAdmin(ComAdminDto oComAdminDto) throws Exception {
        ComAdmin oComAdmin = new ComAdmin();
        PoToDTO.poToDto(oComAdminDto,oComAdmin);
        oComAdmin.setState(0);
        if(oComAdminMapper.update(oComAdmin) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("冻结管理员失败");
        }
        return Result.ok("冻结管理员成功");
    }

    /**
     * 获取管理员（后台）
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getComAdminBack(ComAdminDto oComAdminDto) throws Exception {
        List<ComAdmin> oComAdminList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oComAdminDto.getPage() != null && oComAdminDto.getLimit() != null){
            Page<ComAdmin> ComAdminPage = oComAdminMapper.paginateWithRelations(oComAdminDto.getPage(),oComAdminDto.getLimit(),queryWrapper);
            oComAdminList = ComAdminPage.getRecords();
            total = ComAdminPage.getTotalRow();
        }else{
            oComAdminList = oComAdminMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oComAdminList.size();
        }
        //PoToDto
        List<ComAdminDto> oComAdminDtoList = (List<ComAdminDto>) PoToDTO.poToDtoList(oComAdminList,new ComAdminDto());
        return Result.ok(oComAdminDtoList,total);
    }

    /**
     * 解冻管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    @Override
    public Result thawComAdmin(ComAdminDto oComAdminDto) throws Exception {
        ComAdmin oComAdmin = new ComAdmin();
        PoToDTO.poToDto(oComAdminDto,oComAdmin);
        oComAdmin.setState(1);
        if(oComAdminMapper.update(oComAdmin) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("解冻管理员失败");
        }
        return Result.ok("解冻管理员成功");
    }

    /**
     * 登录管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    @Override
    public Result loginComAdmin(ComAdminDto oComAdminDto) throws Exception {
        ComAdmin oComAdmin = new ComAdmin();
        PoToDTO.poToDto(oComAdminDto,oComAdmin);
        //判断登录名是否存在
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(ComAdminTable.COM_ADMIN.LOGIN_NAME.eq(oComAdmin.getLoginName()));
        ComAdmin oldComAdmin = oComAdminMapper.selectOneByQuery(queryWrapper);
        if(oldComAdmin == null){
            return Result.fail("登录名不存在");
        }
        //判断密码是否正确
        if(!oldComAdmin.getPassword().equals(Md5Util.code(oComAdmin.getPassword()))){
            return Result.fail("密码错误");
        }
        //PoToDto
        ComAdminDto comAdminDto = new ComAdminDto();
        PoToDTO.poToDto(oldComAdmin,comAdminDto);
        comAdminDto.setPassword("******");
        //用户登录
        StpUtil.login(comAdminDto.getId());
        //登录缓存登录对象
        StpUtil.getSession().set("comAdmin", comAdminDto);
        comAdminDto.setToken(StpUtil.getTokenValue());
        return Result.ok(comAdminDto);
    }

}
