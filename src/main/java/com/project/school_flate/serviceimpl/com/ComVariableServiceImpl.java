package com.project.school_flate.serviceimpl.com;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.project.school_flate.entity.com.ComVariable;
import com.project.school_flate.entity.com.table.ComVariableTableDef;
import com.project.school_flate.entity.table.com.ComVariableTable;
import com.project.school_flate.mapper.com.ComVariableMapper;
import com.project.school_flate.service.com.ComVariableService;
import com.project.school_flate.util.AesEncryptUtils;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

@Service(value = "VariableServiceImpl")
public class ComVariableServiceImpl implements ComVariableService {
    @Autowired
    private ComVariableMapper oComVariableMapper;

    /**
     * 添加系统参数
     * @param
     * @return
     * @throws Exception
     */
    @Transactional
    @Override
    public Result addVariable(ComVariable variable) throws Exception {
        if(StringUtils.isBlank(variable.getSystemTitle()) || StringUtils.isBlank(variable.getSystemValues()) || StringUtils.isBlank(variable.getType())){
            return Result.fail("请填写完整");
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(ComVariableTableDef.COM_VARIABLE.SYSTEM_TITLE.eq(variable.getSystemTitle()));
        if(oComVariableMapper.selectOneByQuery(queryWrapper) != null){
            return Result.fail("系统参数已存在");
        }
        if(oComVariableMapper.insertWithPk(variable) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加系统参数失败");
        }
        return Result.ok();
    }

    /**
     * 获取系统参数
     * @param variable
     * @return
     * @throws Exception
     */
    @Override
    public Result getVariable(ComVariable variable) throws Exception {
        List<ComVariable> oComVariableList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入TITLE
        if(StringUtils.isNotBlank(variable.getSystemTitle())){
            queryWrapper.where(ComVariableTable.COM_VARIABLE.SYSTEM_TITLE.eq(variable.getSystemTitle()));
        }
        //判断是否传入TYPE
        if(StringUtils.isNotBlank(variable.getType())){
            queryWrapper.where(ComVariableTable.COM_VARIABLE.TYPE.like(variable.getType()));
        }
        queryWrapper.orderBy("type desc");
        //是否分页
        if(variable.getPage() != null && variable.getLimit() != null){
            Page<ComVariable> ComVariablePage = oComVariableMapper.paginateWithRelations(variable.getPage(),variable.getLimit(),queryWrapper);
            oComVariableList = ComVariablePage.getRecords();
            total = ComVariablePage.getTotalRow();
        }else{
            oComVariableList = oComVariableMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oComVariableList.size();
        }
        return Result.ok(oComVariableList,total);
    }

    /**
     * 修改系统参数
     * @param
     * @return
     * @throws Exception
     */
    @Transactional
    @Override
    public Result updateVariable(ComVariable variable) throws Exception {
        if(oComVariableMapper.update(variable) == 0) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改系统参数失败");
        }
        return Result.ok();
    }

    /**
     * 删除系统参数
     * @param
     * @return
     * @throws Exception
     */
    @Override
    public Result deleteVariable(ComVariable variable) throws Exception {
        if(oComVariableMapper.delete(variable) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("删除系统参数失败");
        }
        return Result.ok();
    }

}
