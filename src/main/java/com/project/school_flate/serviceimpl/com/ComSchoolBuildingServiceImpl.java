package com.project.school_flate.serviceimpl.com;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.com.ComSchoolBuildingDto;
import com.project.school_flate.entity.com.ComSchool;
import com.project.school_flate.entity.com.ComSchoolBuilding;
import com.project.school_flate.entity.com.ComSchoolBuilding;
import com.project.school_flate.entity.order.table.OrderTakeawayTableDef;
import com.project.school_flate.entity.table.com.ComAdminTable;
import com.project.school_flate.entity.table.com.ComSchoolBuildingTable;
import com.project.school_flate.entity.table.com.ComSchoolTable;
import com.project.school_flate.mapper.com.ComSchoolBuildingMapper;
import com.project.school_flate.mapper.com.ComSchoolMapper;
import com.project.school_flate.mapper.order.OrderTakeawayMapper;
import com.project.school_flate.service.com.ComSchoolBuildingService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class ComSchoolBuildingServiceImpl extends ServiceImpl<ComSchoolBuildingMapper, ComSchoolBuilding> implements ComSchoolBuildingService {

    @Autowired
    private ComSchoolBuildingMapper oComSchoolBuildingMapper;

    @Autowired
    private ComSchoolMapper oComSchoolMapper;

    @Autowired
    private OrderTakeawayMapper oOrderTakeawayMapper;

    /**
     * 获取校园楼栋
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getComSchoolBuilding(ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception {
        List<ComSchoolBuilding> oComSchoolBuildingList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入校园ID
        if(StringUtils.isNotBlank(oComSchoolBuildingDto.getSchoolId())){
            queryWrapper.where(ComSchoolBuildingTable.COM_SCHOOL_BUILDING.SCHOOL_ID.eq(oComSchoolBuildingDto.getSchoolId()));
        }
        queryWrapper.where(ComSchoolBuildingTable.COM_SCHOOL_BUILDING.STATE.eq(1));
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oComSchoolBuildingDto.getPage() != null && oComSchoolBuildingDto.getLimit() != null){
            Page<ComSchoolBuilding> ComSchoolBuildingPage = oComSchoolBuildingMapper.paginateWithRelations(oComSchoolBuildingDto.getPage(),oComSchoolBuildingDto.getLimit(),queryWrapper);
            oComSchoolBuildingList = ComSchoolBuildingPage.getRecords();
            total = ComSchoolBuildingPage.getTotalRow();
        }else{
            oComSchoolBuildingList = oComSchoolBuildingMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oComSchoolBuildingList.size();
        }
        //PoToDto
        List<ComSchoolBuildingDto> oComSchoolBuildingDtoList = (List<ComSchoolBuildingDto>) PoToDTO.poToDtoList(oComSchoolBuildingList,new ComSchoolBuildingDto());
        return Result.ok(oComSchoolBuildingDtoList,total);
    }

    /**
     * 添加校园楼栋
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addComSchoolBuilding(ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception {
        ComSchoolBuilding oComSchoolBuilding = new ComSchoolBuilding();
        PoToDTO.poToDto(oComSchoolBuildingDto,oComSchoolBuilding);
        //获取校园信息
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(ComSchoolTable.COM_SCHOOL.ID.eq(oComSchoolBuilding.getSchoolId()));
        queryWrapper.where(ComSchoolTable.COM_SCHOOL.STATE.eq(1));
        ComSchool oComSchool = oComSchoolMapper.selectOneByQuery(queryWrapper);
        if(oComSchool == null){
            return Result.fail("校园不存在");
        }
        if(oComSchoolBuildingMapper.insert(oComSchoolBuilding) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加校园楼栋失败");
        }
        return Result.ok("添加校园楼栋成功");
    }

    /**
     * 修改校园楼栋
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateComSchoolBuilding(ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception {
        ComSchoolBuilding oComSchoolBuilding = new ComSchoolBuilding();
        PoToDTO.poToDto(oComSchoolBuildingDto,oComSchoolBuilding);
        if(oComSchoolBuildingMapper.update(oComSchoolBuilding) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改校园楼栋失败");
        }
        return Result.ok("修改校园楼栋成功");
    }

    /**
     * 冻结校园楼栋
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result freezeComSchoolBuilding(ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception {
        ComSchoolBuilding oComSchoolBuilding = new ComSchoolBuilding();
        PoToDTO.poToDto(oComSchoolBuildingDto,oComSchoolBuilding);
        //判断是否还有订单未完成
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(OrderTakeawayTableDef.ORDER_TAKEAWAY.SCHOOL_BUILDING_ID.eq(oComSchoolBuilding.getId()));
        queryWrapper.where(OrderTakeawayTableDef.ORDER_TAKEAWAY.PAY_STATE.notIn(2,4,6));
        if(CollectionUtils.isNotEmpty(oOrderTakeawayMapper.selectListByQuery(queryWrapper))){
            return Result.fail("还有订单未完成");
        }
        oComSchoolBuilding.setState(0);
        if(oComSchoolBuildingMapper.update(oComSchoolBuilding) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("冻结校园楼栋失败");
        }
        return Result.ok("冻结校园楼栋成功");
    }

    /**
     * 获取校园楼栋（后台）
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getComSchoolBuildingBack(ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception {
        List<ComSchoolBuilding> oComSchoolBuildingList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入校园ID
        if(StringUtils.isNotBlank(oComSchoolBuildingDto.getSchoolId())){
            queryWrapper.where(ComSchoolBuildingTable.COM_SCHOOL_BUILDING.SCHOOL_ID.eq(oComSchoolBuildingDto.getSchoolId()));
        }
        //判断是否传入校园楼栋名称
        if(StringUtils.isNotBlank(oComSchoolBuildingDto.getName())){
            queryWrapper.where(ComSchoolBuildingTable.COM_SCHOOL_BUILDING.NAME.eq(oComSchoolBuildingDto.getName()));
        }
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oComSchoolBuildingDto.getPage() != null && oComSchoolBuildingDto.getLimit() != null){
            Page<ComSchoolBuilding> ComSchoolBuildingPage = oComSchoolBuildingMapper.paginateWithRelations(oComSchoolBuildingDto.getPage(),oComSchoolBuildingDto.getLimit(),queryWrapper);
            oComSchoolBuildingList = ComSchoolBuildingPage.getRecords();
            total = ComSchoolBuildingPage.getTotalRow();
        }else{
            oComSchoolBuildingList = oComSchoolBuildingMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oComSchoolBuildingList.size();
        }
        //PoToDto
        List<ComSchoolBuildingDto> oComSchoolBuildingDtoList = (List<ComSchoolBuildingDto>) PoToDTO.poToDtoList(oComSchoolBuildingList,new ComSchoolBuildingDto());
        return Result.ok(oComSchoolBuildingDtoList,total);
    }

    /**
     * 解冻校园楼栋
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    @Transactional
    @Override
    public Result thawComSchoolBuilding(ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception {
        ComSchoolBuilding oComSchoolBuilding = new ComSchoolBuilding();
        PoToDTO.poToDto(oComSchoolBuildingDto,oComSchoolBuilding);
        oComSchoolBuilding.setState(1);
        if(oComSchoolBuildingMapper.update(oComSchoolBuilding) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("冻结校园楼栋失败");
        }
        return Result.ok("冻结校园楼栋成功");
    }

}
