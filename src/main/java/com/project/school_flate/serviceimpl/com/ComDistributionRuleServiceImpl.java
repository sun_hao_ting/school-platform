package com.project.school_flate.serviceimpl.com;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.com.ComDistributionRuleDto;
import com.project.school_flate.entity.com.ComDistributionRule;
import com.project.school_flate.entity.com.table.ComDistributionRuleTableDef;
import com.project.school_flate.mapper.com.ComDistributionRuleMapper;
import com.project.school_flate.service.com.ComDistributionRuleService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class ComDistributionRuleServiceImpl extends ServiceImpl<ComDistributionRuleMapper, ComDistributionRule> implements ComDistributionRuleService {

    @Autowired
    private ComDistributionRuleMapper oComDistributionRuleMapper;

    /**
     * 获取分销商等级规则
     * @param oComDistributionRuleDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getComDistributionRule(ComDistributionRuleDto oComDistributionRuleDto) throws Exception {
        List<ComDistributionRule> oComDistributionRuleList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入等级
        if(oComDistributionRuleDto.getGrade() != null){
            queryWrapper.where(ComDistributionRuleTableDef.COM_DISTRIBUTION_RULE.GRADE.eq(oComDistributionRuleDto.getGrade()));
        }
        //判断是否传入类型
        if(oComDistributionRuleDto.getType() != null){
            queryWrapper.where(ComDistributionRuleTableDef.COM_DISTRIBUTION_RULE.TYPE.eq(oComDistributionRuleDto.getType()));
        }
        queryWrapper.orderBy("id desc");
        //是否分页
        if(oComDistributionRuleDto.getPage() != null && oComDistributionRuleDto.getLimit() != null){
            Page<ComDistributionRule> ComDistributionRulePage = oComDistributionRuleMapper.paginateWithRelations(oComDistributionRuleDto.getPage(),oComDistributionRuleDto.getLimit(),queryWrapper);
            oComDistributionRuleList = ComDistributionRulePage.getRecords();
            total = ComDistributionRulePage.getTotalRow();
        }else{
            oComDistributionRuleList = oComDistributionRuleMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oComDistributionRuleList.size();
        }
        //PoToDto
        List<ComDistributionRuleDto> oComDistributionRuleDtoList = (List<ComDistributionRuleDto>) PoToDTO.poToDtoList(oComDistributionRuleList,new ComDistributionRuleDto());
        return Result.ok(oComDistributionRuleDtoList,total);
    }

    /**
     * 添加分销商等级规则
     * @param oComDistributionRuleDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addComDistributionRule(ComDistributionRuleDto oComDistributionRuleDto) throws Exception {
        ComDistributionRule oComDistributionRule = new ComDistributionRule();
        PoToDTO.poToDto(oComDistributionRuleDto,oComDistributionRule);
        //获取所有等级
        List<ComDistributionRule> oldComDistributionRuleList = oComDistributionRuleMapper.selectAll();
        oldComDistributionRuleList.sort((t1,t2) -> t1.getGrade().compareTo(t2.getGrade()));
        if(CollectionUtils.isEmpty(oldComDistributionRuleList)){
            if(oComDistributionRule.getGrade() != 1){
                return Result.fail("分销商等级从1级开始");
            }
        }else{
            //判断普通还是专业
            if(oComDistributionRule.getType() == 0){
                for(ComDistributionRule oldComDistributionRule : oldComDistributionRuleList){
                    if(oldComDistributionRule.getType() == 0){
                        return Result.fail("普通分销商的等级规则已存在");
                    }
                }
            }else{
                //获取当前的最后一级
                ComDistributionRule lastComDistributionRule = oldComDistributionRuleList.get(oldComDistributionRuleList.size()-1);
                if(oComDistributionRule.getGrade().compareTo(lastComDistributionRule.getGrade()) <= 0){
                    return Result.fail("专业分销商的等级规则已存在");
                }else{
                    if(oComDistributionRule.getGrade().compareTo(lastComDistributionRule.getGrade() + 1) != 0){
                        return Result.fail("专业分销商的等级需要一级一级添加");
                    }else if(oComDistributionRule.getCommission().compareTo(lastComDistributionRule.getCommission()) <= 0){
                        return Result.fail("添加的提成比例要比上一级高");
                    }
                    else if(oComDistributionRule.getIncome().compareTo(lastComDistributionRule.getIncome()) <= 0){
                        return Result.fail("添加的升级收益条件要比上一级高");
                    }
                }
            }
        }
        if(oComDistributionRuleMapper.insert(oComDistributionRule) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加分销商等级规则失败");
        }
        return Result.ok("添加分销商等级规则成功");
    }

    /**
     * 修改分销商等级规则
     * @param oComDistributionRuleDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateComDistributionRule(ComDistributionRuleDto oComDistributionRuleDto) throws Exception {
        ComDistributionRule oComDistributionRule = new ComDistributionRule();
        PoToDTO.poToDto(oComDistributionRuleDto,oComDistributionRule);
        //判断是不是专业
        if(oComDistributionRule.getType() == 1){
            //获取修改等级的上下级
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.where(ComDistributionRuleTableDef.COM_DISTRIBUTION_RULE.TYPE.eq(1));
            queryWrapper.where(ComDistributionRuleTableDef.COM_DISTRIBUTION_RULE.GRADE.eq(oComDistributionRule.getGrade() + 1)).or(ComDistributionRuleTableDef.COM_DISTRIBUTION_RULE.GRADE.eq(oComDistributionRule.getGrade() - 1));
            queryWrapper.orderBy("grade desc");
            List<ComDistributionRule> oldComDistributionRuleList = oComDistributionRuleMapper.selectListByQuery(queryWrapper);
            for(ComDistributionRule oldComDistributionRule : oldComDistributionRuleList){
                if(oComDistributionRule.getGrade().compareTo(oldComDistributionRule.getGrade()) > 0){
                    if(oComDistributionRule.getCommission().compareTo(oldComDistributionRule.getCommission()) <= 0){
                        return Result.fail("添加的提成比例要比上一级高");
                    }
                    else if(oComDistributionRule.getIncome().compareTo(oldComDistributionRule.getIncome()) <= 0){
                        return Result.fail("添加的升级收益条件要比上一级高");
                    }
                }else{
                    if(oComDistributionRule.getCommission().compareTo(oldComDistributionRule.getCommission()) >= 0){
                        return Result.fail("添加的提成比例要比下一级低");
                    }
                    else if(oComDistributionRule.getIncome().compareTo(oldComDistributionRule.getIncome()) >= 0){
                        return Result.fail("添加的升级收益条件要比下一级低");
                    }
                }
            }
        }
        if(oComDistributionRuleMapper.update(oComDistributionRule) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改分销商等级规则失败");
        }
        return Result.ok("修改分销商等级规则成功");
    }

    /**
     * 删除分销商等级规则
     * @param oComDistributionRuleDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result deleteComDistributionRule(ComDistributionRuleDto oComDistributionRuleDto) throws Exception {
        ComDistributionRule oComDistributionRule = new ComDistributionRule();
        PoToDTO.poToDto(oComDistributionRuleDto,oComDistributionRule);
        //判断是否是专业
        if(oComDistributionRule.getType() == 1){
            //获取最后一个规则
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.where(ComDistributionRuleTableDef.COM_DISTRIBUTION_RULE.TYPE.eq(1));
            queryWrapper.orderBy("grade desc");
            queryWrapper.limit(1);
            ComDistributionRule lastComDistributionRule = oComDistributionRuleMapper.selectOneByQuery(queryWrapper);
            if(oComDistributionRule.getGrade().compareTo(lastComDistributionRule.getGrade()) != 0){
                return Result.fail("删除分销商等级需要从最顶级删除");
            }
        }

        if(oComDistributionRuleMapper.deleteById(oComDistributionRule.getId()) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("删除分销商等级规则失败");
        }
        return Result.ok("删除分销商等级规则成功");
    }

}
