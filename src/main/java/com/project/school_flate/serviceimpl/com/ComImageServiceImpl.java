package com.project.school_flate.serviceimpl.com;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.com.ComImageDto;
import com.project.school_flate.entity.com.ComImage;
import com.project.school_flate.mapper.com.ComImageMapper;
import com.project.school_flate.service.com.ComImageService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class ComImageServiceImpl extends ServiceImpl<ComImageMapper, ComImage> implements ComImageService {

    @Autowired
    private ComImageMapper oComImageMapper;

    /**
     * 获取系统图
     * @param oComImageDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getComImage(ComImageDto oComImageDto) throws Exception {
        List<ComImage> oComImageList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderBy("id desc");
        //是否分页
        if(oComImageDto.getPage() != null && oComImageDto.getLimit() != null){
            Page<ComImage> ComImagePage = oComImageMapper.paginateWithRelations(oComImageDto.getPage(),oComImageDto.getLimit(),queryWrapper);
            oComImageList = ComImagePage.getRecords();
            total = ComImagePage.getTotalRow();
        }else{
            oComImageList = oComImageMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oComImageList.size();
        }
        //PoToDto
        List<ComImageDto> oComImageDtoList = (List<ComImageDto>) PoToDTO.poToDtoList(oComImageList,new ComImageDto());
        return Result.ok(oComImageDtoList,total);
    }

    /**
     * 添加系统图
     * @param oComImageDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addComImage(ComImageDto oComImageDto) throws Exception {
        ComImage oComImage = new ComImage();
        PoToDTO.poToDto(oComImageDto,oComImage);
        if(oComImageMapper.insert(oComImage) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加系统图失败");
        }
        return Result.ok("添加系统图成功");
    }

    /**
     * 修改系统图
     * @param oComImageDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateComImage(ComImageDto oComImageDto) throws Exception {
        ComImage oComImage = new ComImage();
        PoToDTO.poToDto(oComImageDto,oComImage);
        if(oComImageMapper.update(oComImage) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改系统图失败");
        }
        return Result.ok("修改系统图成功");
    }

    /**
     * 删除系统图
     * @param oComImageDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result deleteComImage(ComImageDto oComImageDto) throws Exception {
        ComImage oComImage = new ComImage();
        PoToDTO.poToDto(oComImageDto,oComImage);
        if(oComImageMapper.deleteById(oComImage.getId()) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("删除系统图失败");
        }
        return Result.ok("删除系统图成功");
    }
    
}
