package com.project.school_flate.serviceimpl.com;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.project.school_flate.dto.com.ComRoleDto;
import com.project.school_flate.entity.com.ComRole;
import com.project.school_flate.entity.table.com.ComAdminTable;
import com.project.school_flate.entity.table.com.ComRoleTable;
import com.project.school_flate.mapper.com.ComRoleMapper;
import com.project.school_flate.service.com.ComRoleService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

@Service
public class ComRoleServiceImpl implements ComRoleService {

    @Autowired
    private ComRoleMapper oComRoleMapper;

    /**
     * 获取角色
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getComRole(ComRoleDto oComRoleDto) throws Exception {
        List<ComRole> oComRoleList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(ComRoleTable.COM_ROLE.STATE.eq(1));
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oComRoleDto.getPage() != null && oComRoleDto.getLimit() != null){
            Page<ComRole> ComRolePage = oComRoleMapper.paginateWithRelations(oComRoleDto.getPage(),oComRoleDto.getLimit(),queryWrapper);
            oComRoleList = ComRolePage.getRecords();
            total = ComRolePage.getTotalRow();
        }else{
            oComRoleList = oComRoleMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oComRoleList.size();
        }
        //PoToDto
        List<ComRoleDto> oComRoleDtoList = (List<ComRoleDto>) PoToDTO.poToDtoList(oComRoleList,new ComRoleDto());
        return Result.ok(oComRoleDtoList,total);
    }

    /**
     * 添加角色
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addComRole(ComRoleDto oComRoleDto) throws Exception {
        ComRole oComRole = new ComRole();
        PoToDTO.poToDto(oComRoleDto,oComRole);
        if(oComRoleMapper.insert(oComRole) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加角色失败");
        }
        return Result.ok("添加角色成功");
    }

    /**
     * 修改角色
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateComRole(ComRoleDto oComRoleDto) throws Exception {
        ComRole oComRole = new ComRole();
        PoToDTO.poToDto(oComRoleDto,oComRole);
        if(oComRoleMapper.update(oComRole) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改角色失败");
        }
        return Result.ok("修改角色成功");
    }

    /**
     * 冻结角色
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result freezeComRole(ComRoleDto oComRoleDto) throws Exception {
        ComRole oComRole = new ComRole();
        PoToDTO.poToDto(oComRoleDto,oComRole);
        oComRole.setState(0);
        if(oComRoleMapper.update(oComRole) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("冻结角色失败");
        }
        return Result.ok("冻结角色成功");
    }

    /**
     * 获取角色（后台）
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getComRoleBack(ComRoleDto oComRoleDto) throws Exception {
        List<ComRole> oComRoleList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oComRoleDto.getPage() != null && oComRoleDto.getLimit() != null){
            Page<ComRole> ComRolePage = oComRoleMapper.paginateWithRelations(oComRoleDto.getPage(),oComRoleDto.getLimit(),queryWrapper);
            oComRoleList = ComRolePage.getRecords();
            total = ComRolePage.getTotalRow();
        }else{
            oComRoleList = oComRoleMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oComRoleList.size();
        }
        //PoToDto
        List<ComRoleDto> oComRoleDtoList = (List<ComRoleDto>) PoToDTO.poToDtoList(oComRoleList,new ComRoleDto());
        return Result.ok(oComRoleDtoList,total);
    }

    /**
     * 解冻角色
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    @Transactional
    @Override
    public Result thawComRole(ComRoleDto oComRoleDto) throws Exception {
        ComRole oComRole = new ComRole();
        PoToDTO.poToDto(oComRoleDto,oComRole);
        oComRole.setState(1);
        if(oComRoleMapper.update(oComRole) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("解冻角色失败");
        }
        return Result.ok("解冻角色成功");
    }

}
