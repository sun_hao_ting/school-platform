package com.project.school_flate.serviceimpl.com;

import cn.dev33.satoken.stp.StpUtil;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.com.ComFeedbackDto;
import com.project.school_flate.entity.com.ComAdmin;
import com.project.school_flate.entity.com.ComFeedback;
import com.project.school_flate.entity.com.table.ComFeedbackTableDef;
import com.project.school_flate.entity.com.table.ComWithdrawalFlowTableDef;
import com.project.school_flate.entity.user.UserInfo;
import com.project.school_flate.entity.user.table.UserInfoTableDef;
import com.project.school_flate.mapper.com.ComFeedbackMapper;
import com.project.school_flate.mapper.user.UserInfoMapper;
import com.project.school_flate.service.com.ComFeedbackService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class ComFeedbackServiceImpl extends ServiceImpl<ComFeedbackMapper, ComFeedback> implements ComFeedbackService {

    @Autowired
    private ComFeedbackMapper oComFeedbackMapper;

    @Autowired
    private UserInfoMapper oUserInfoMapper;

    /**
     * 获取反馈
     * @param oComFeedbackDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getComFeedback(ComFeedbackDto oComFeedbackDto) throws Exception {
        List<ComFeedback> oComFeedbackList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入用户ID
        if(StringUtils.isNotBlank(oComFeedbackDto.getUserId())){
            queryWrapper.where(ComFeedbackTableDef.COM_FEEDBACK.USER_ID.eq(oComFeedbackDto.getUserId()));
        }
        //判断是否传入用户名
        if(StringUtils.isNotBlank(oComFeedbackDto.getUserName())){
            QueryWrapper qrUser = new QueryWrapper();
            qrUser.where(UserInfoTableDef.USER_INFO.NAME.like(oComFeedbackDto.getUserName()));
            List<UserInfo> oUserInfoList = oUserInfoMapper.selectListByQuery(qrUser);
            if(CollectionUtils.isEmpty(oUserInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的用户ID
            List<String> oUserInfoIdList = new ArrayList<>();
            oUserInfoList.forEach(item->{oUserInfoIdList.add(item.getId());});
            queryWrapper.where(ComWithdrawalFlowTableDef.COM_WITHDRAWAL_FLOW.USER_ID.in(oUserInfoIdList));
        }
        //判断是否传入状态
        if(oComFeedbackDto.getState() != null){
            queryWrapper.where(ComFeedbackTableDef.COM_FEEDBACK.STATE.eq(oComFeedbackDto.getState()));
        }
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oComFeedbackDto.getPage() != null && oComFeedbackDto.getLimit() != null){
            Page<ComFeedback> ComFeedbackPage = oComFeedbackMapper.paginateWithRelations(oComFeedbackDto.getPage(),oComFeedbackDto.getLimit(),queryWrapper);
            oComFeedbackList = ComFeedbackPage.getRecords();
            total = ComFeedbackPage.getTotalRow();
        }else{
            oComFeedbackList = oComFeedbackMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oComFeedbackList.size();
        }
        //PoToDto
        List<ComFeedbackDto> oComFeedbackDtoList = (List<ComFeedbackDto>) PoToDTO.poToDtoList(oComFeedbackList,new ComFeedbackDto());
        return Result.ok(oComFeedbackDtoList,total);
    }

    /**
     * 添加反馈
     * @param oComFeedbackDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addComFeedback(ComFeedbackDto oComFeedbackDto) throws Exception {
        ComFeedback oComFeedback = new ComFeedback();
        PoToDTO.poToDto(oComFeedbackDto,oComFeedback);
        if(oComFeedbackMapper.insert(oComFeedback) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加反馈失败");
        }
        return Result.ok("添加反馈成功");
    }

    /**
     * 修改反馈
     * @param oComFeedbackDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateComFeedback(ComFeedbackDto oComFeedbackDto) throws Exception {
        ComFeedback oComFeedback = new ComFeedback();
        PoToDTO.poToDto(oComFeedbackDto,oComFeedback);
        //获取登陆管理员
        ComAdmin oComAdmin = new ComAdmin();
        if(StpUtil.isLogin()){
            oComAdmin = (ComAdmin) StpUtil.getSession().get("comAdmin");
            if(oComAdmin == null || StringUtils.isBlank(oComAdmin.getId())){
                return Result.fail("请先重新登录");
            }
        }else{
            return Result.fail("请先重新登录");
        }
        oComFeedback.setAdminId(oComAdmin.getId());
        oComFeedback.setState(1);
        if(oComFeedbackMapper.update(oComFeedback) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改反馈失败");
        }
        return Result.ok("修改反馈成功");
    }

    /**
     * 删除反馈
     * @param oComFeedbackDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result deleteComFeedback(ComFeedbackDto oComFeedbackDto) throws Exception {
        ComFeedback oComFeedback = new ComFeedback();
        PoToDTO.poToDto(oComFeedbackDto,oComFeedback);
        if(oComFeedbackMapper.deleteById(oComFeedback.getId()) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("删除反馈失败");
        }
        return Result.ok("删除反馈成功");
    }

}
