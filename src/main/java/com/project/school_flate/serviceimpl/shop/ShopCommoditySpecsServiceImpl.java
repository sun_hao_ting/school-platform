package com.project.school_flate.serviceimpl.shop;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.shop.ShopCommoditySpecsDto;
import com.project.school_flate.entity.shop.ShopCommoditySpecs;
import com.project.school_flate.entity.shop.ShopCommoditySpecs;
import com.project.school_flate.entity.table.shop.ShopCommoditySpecsTable;
import com.project.school_flate.mapper.shop.ShopCommoditySpecsMapper;
import com.project.school_flate.service.shop.ShopCommoditySpecsService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class ShopCommoditySpecsServiceImpl extends ServiceImpl<ShopCommoditySpecsMapper, ShopCommoditySpecs> implements ShopCommoditySpecsService {

    @Autowired
    private ShopCommoditySpecsMapper oShopCommoditySpecsMapper;

    /**
     * 获取商品规格
     * @param oShopCommoditySpecsDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getShopCommoditySpecs(ShopCommoditySpecsDto oShopCommoditySpecsDto) throws Exception {
        List<ShopCommoditySpecs> oShopCommoditySpecsList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入商品规格类型ID
        if(StringUtils.isNotBlank(oShopCommoditySpecsDto.getCommoditySpecsTypeId())){
            ShopCommoditySpecsTable.SHOP_COMMODITY_SPECS.COMMODITY_SPECS_TYPE_ID.eq(oShopCommoditySpecsDto.getCommoditySpecsTypeId());
        }
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oShopCommoditySpecsDto.getPage() != null && oShopCommoditySpecsDto.getLimit() != null){
            Page<ShopCommoditySpecs> ShopCommoditySpecsPage = oShopCommoditySpecsMapper.paginateWithRelations(oShopCommoditySpecsDto.getPage(),oShopCommoditySpecsDto.getLimit(),queryWrapper);
            oShopCommoditySpecsList = ShopCommoditySpecsPage.getRecords();
            total = ShopCommoditySpecsPage.getTotalRow();
        }else{
            oShopCommoditySpecsList = oShopCommoditySpecsMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oShopCommoditySpecsList.size();
        }
        //PoToDto
        List<ShopCommoditySpecsDto> oShopCommoditySpecsDtoList = (List<ShopCommoditySpecsDto>) PoToDTO.poToDtoList(oShopCommoditySpecsList,new ShopCommoditySpecsDto());
        return Result.ok(oShopCommoditySpecsDtoList,total);
    }

    /**
     * 添加商品规格
     * @param oShopCommoditySpecsDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addShopCommoditySpecs(ShopCommoditySpecsDto oShopCommoditySpecsDto) throws Exception {
        ShopCommoditySpecs oShopCommoditySpecs = new ShopCommoditySpecs();
        PoToDTO.poToDto(oShopCommoditySpecsDto,oShopCommoditySpecs);
        if(oShopCommoditySpecsMapper.insert(oShopCommoditySpecs) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加商品规格失败");
        }
        return Result.ok("添加商品规格成功");
    }

    /**
     * 修改商品规格
     * @param oShopCommoditySpecsDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateShopCommoditySpecs(ShopCommoditySpecsDto oShopCommoditySpecsDto) throws Exception {
        ShopCommoditySpecs oShopCommoditySpecs = new ShopCommoditySpecs();
        PoToDTO.poToDto(oShopCommoditySpecsDto,oShopCommoditySpecs);
        if(oShopCommoditySpecsMapper.update(oShopCommoditySpecs) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改商品规格失败");
        }
        return Result.ok("修改商品规格成功");
    }

    /**
     * 删除商品规格
     * @param oShopCommoditySpecsDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result deleteShopCommoditySpecs(ShopCommoditySpecsDto oShopCommoditySpecsDto) throws Exception {
        ShopCommoditySpecs oShopCommoditySpecs = new ShopCommoditySpecs();
        PoToDTO.poToDto(oShopCommoditySpecsDto,oShopCommoditySpecs);
        if(oShopCommoditySpecsMapper.deleteById(oShopCommoditySpecs.getId()) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("删除商品规格失败");
        }
        return Result.ok("删除商品规格成功");
    }

    /**
     * 获取商品规格（用户）
     * @param oShopCommoditySpecsDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getShopCommoditySpecsUser(ShopCommoditySpecsDto oShopCommoditySpecsDto) throws Exception {
        List<ShopCommoditySpecs> oShopCommoditySpecsList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入商品规格类型ID
        if(StringUtils.isNotBlank(oShopCommoditySpecsDto.getCommoditySpecsTypeId())){
            ShopCommoditySpecsTable.SHOP_COMMODITY_SPECS.COMMODITY_SPECS_TYPE_ID.eq(oShopCommoditySpecsDto.getCommoditySpecsTypeId());
        }
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oShopCommoditySpecsDto.getPage() != null && oShopCommoditySpecsDto.getLimit() != null){
            Page<ShopCommoditySpecs> ShopCommoditySpecsPage = oShopCommoditySpecsMapper.paginateWithRelations(oShopCommoditySpecsDto.getPage(),oShopCommoditySpecsDto.getLimit(),queryWrapper);
            oShopCommoditySpecsList = ShopCommoditySpecsPage.getRecords();
            total = ShopCommoditySpecsPage.getTotalRow();
        }else{
            oShopCommoditySpecsList = oShopCommoditySpecsMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oShopCommoditySpecsList.size();
        }
        //PoToDto
        List<ShopCommoditySpecsDto> oShopCommoditySpecsDtoList = (List<ShopCommoditySpecsDto>) PoToDTO.poToDtoList(oShopCommoditySpecsList,new ShopCommoditySpecsDto());
        return Result.ok(oShopCommoditySpecsDtoList,total);
    }

}
