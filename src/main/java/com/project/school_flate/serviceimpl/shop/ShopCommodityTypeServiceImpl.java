package com.project.school_flate.serviceimpl.shop;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.relation.RelationManager;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.shop.ShopCommodityTypeDto;
import com.project.school_flate.entity.shop.ShopCommodityType;
import com.project.school_flate.entity.shop.ShopCommodityType;
import com.project.school_flate.entity.table.shop.ShopCommodityTable;
import com.project.school_flate.entity.table.shop.ShopCommodityTypeTable;
import com.project.school_flate.mapper.shop.ShopCommodityTypeMapper;
import com.project.school_flate.service.shop.ShopCommodityTypeService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class ShopCommodityTypeServiceImpl extends ServiceImpl<ShopCommodityTypeMapper, ShopCommodityType> implements ShopCommodityTypeService {

    @Autowired
    private ShopCommodityTypeMapper oShopCommodityTypeMapper;

    /**
     * 获取商品分类
     * @param oShopCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getShopCommodityType(ShopCommodityTypeDto oShopCommodityTypeDto) throws Exception {
        List<ShopCommodityType> oShopCommodityTypeList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入店铺ID
        if(StringUtils.isNotBlank(oShopCommodityTypeDto.getShopId())){
            queryWrapper.where(ShopCommodityTypeTable.SHOP_COMMODITY_TYPE.SHOP_ID.eq(oShopCommodityTypeDto.getShopId()));
        }
        queryWrapper.orderBy("no asc");
        //是否分页
        if(oShopCommodityTypeDto.getPage() != null && oShopCommodityTypeDto.getLimit() != null){
            Page<ShopCommodityType> ShopCommodityTypePage = oShopCommodityTypeMapper.paginateWithRelations(oShopCommodityTypeDto.getPage(),oShopCommodityTypeDto.getLimit(),queryWrapper);
            oShopCommodityTypeList = ShopCommodityTypePage.getRecords();
            total = ShopCommodityTypePage.getTotalRow();
        }else{
            oShopCommodityTypeList = oShopCommodityTypeMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oShopCommodityTypeList.size();
        }
        //PoToDto
        List<ShopCommodityTypeDto> oShopCommodityTypeDtoList = (List<ShopCommodityTypeDto>) PoToDTO.poToDtoList(oShopCommodityTypeList,new ShopCommodityTypeDto());
        return Result.ok(oShopCommodityTypeDtoList,total);
    }

    /**
     * 添加商品分类
     * @param oShopCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addShopCommodityType(ShopCommodityTypeDto oShopCommodityTypeDto) throws Exception {
        ShopCommodityType oShopCommodityType = new ShopCommodityType();
        PoToDTO.poToDto(oShopCommodityTypeDto,oShopCommodityType);
        if(oShopCommodityTypeMapper.insert(oShopCommodityType) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加商品分类失败");
        }
        return Result.ok("添加商品分类成功");
    }

    /**
     * 修改商品分类
     * @param oShopCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateShopCommodityType(ShopCommodityTypeDto oShopCommodityTypeDto) throws Exception {
        ShopCommodityType oShopCommodityType = new ShopCommodityType();
        PoToDTO.poToDto(oShopCommodityTypeDto,oShopCommodityType);
        if(oShopCommodityTypeMapper.update(oShopCommodityType) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改商品分类失败");
        }
        return Result.ok("修改商品分类成功");
    }

    /**
     * 删除商品分类
     * @param oShopCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result deleteShopCommodityType(ShopCommodityTypeDto oShopCommodityTypeDto) throws Exception {
        ShopCommodityType oShopCommodityType = new ShopCommodityType();
        PoToDTO.poToDto(oShopCommodityTypeDto,oShopCommodityType);
        if(oShopCommodityTypeMapper.deleteById(oShopCommodityType.getId()) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("删除商品分类失败");
        }
        return Result.ok("删除商品分类成功");
    }

    /**
     * 获取商品分类（客户）
     * @param oShopCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getShopCommodityTypeUser(ShopCommodityTypeDto oShopCommodityTypeDto) throws Exception {
        List<ShopCommodityType> oShopCommodityTypeList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入店铺ID
        queryWrapper.where(ShopCommodityTypeTable.SHOP_COMMODITY_TYPE.SHOP_ID.eq(oShopCommodityTypeDto.getShopId()));
        queryWrapper.orderBy("no asc");
        //是否分页
        RelationManager.setMaxDepth(4);
        if(oShopCommodityTypeDto.getPage() != null && oShopCommodityTypeDto.getLimit() != null){
            Page<ShopCommodityType> ShopCommodityTypePage = oShopCommodityTypeMapper.paginateWithRelations(oShopCommodityTypeDto.getPage(),oShopCommodityTypeDto.getLimit(),queryWrapper);
            oShopCommodityTypeList = ShopCommodityTypePage.getRecords();
            total = ShopCommodityTypePage.getTotalRow();
        }else{
            oShopCommodityTypeList = oShopCommodityTypeMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oShopCommodityTypeList.size();
        }
        //PoToDto
        List<ShopCommodityTypeDto> oShopCommodityTypeDtoList = (List<ShopCommodityTypeDto>) PoToDTO.poToDtoList(oShopCommodityTypeList,new ShopCommodityTypeDto());
        return Result.ok(oShopCommodityTypeDtoList,total);
    }

}
