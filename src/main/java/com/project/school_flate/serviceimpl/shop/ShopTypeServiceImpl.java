package com.project.school_flate.serviceimpl.shop;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.shop.ShopTypeDto;
import com.project.school_flate.entity.shop.ShopType;
import com.project.school_flate.entity.table.shop.ShopTypeTable;
import com.project.school_flate.mapper.shop.ShopTypeMapper;
import com.project.school_flate.service.shop.ShopTypeService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class ShopTypeServiceImpl extends ServiceImpl<ShopTypeMapper, ShopType> implements ShopTypeService {

    @Autowired
    private ShopTypeMapper oShopTypeMapper;

    /**
     * 获取店铺类型
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getShopType(ShopTypeDto oShopTypeDto) throws Exception {
        List<ShopType> oShopTypeList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(ShopTypeTable.SHOP_TYPE.STATE.eq(1));
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oShopTypeDto.getPage() != null && oShopTypeDto.getLimit() != null){
            Page<ShopType> ShopTypePage = oShopTypeMapper.paginateWithRelations(oShopTypeDto.getPage(),oShopTypeDto.getLimit(),queryWrapper);
            oShopTypeList = ShopTypePage.getRecords();
            total = ShopTypePage.getTotalRow();
        }else{
            oShopTypeList = oShopTypeMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oShopTypeList.size();
        }
        //PoToDto
        List<ShopTypeDto> oShopTypeDtoList = (List<ShopTypeDto>) PoToDTO.poToDtoList(oShopTypeList,new ShopTypeDto());
        return Result.ok(oShopTypeDtoList,total);
    }

    /**
     * 添加店铺类型
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addShopType(ShopTypeDto oShopTypeDto) throws Exception {
        ShopType oShopType = new ShopType();
        PoToDTO.poToDto(oShopTypeDto,oShopType);
        if(oShopTypeMapper.insert(oShopType) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加店铺类型失败");
        }
        return Result.ok("添加店铺类型成功");
    }

    /**
     * 修改店铺类型
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateShopType(ShopTypeDto oShopTypeDto) throws Exception {
        ShopType oShopType = new ShopType();
        PoToDTO.poToDto(oShopTypeDto,oShopType);
        if(oShopTypeMapper.update(oShopType) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改店铺类型失败");
        }
        return Result.ok("修改店铺类型成功");
    }

    /**
     * 冻结店铺类型
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result freezeShopType(ShopTypeDto oShopTypeDto) throws Exception {
        ShopType oShopType = new ShopType();
        PoToDTO.poToDto(oShopTypeDto,oShopType);
        oShopType.setState(0);
        if(oShopTypeMapper.update(oShopType) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("冻结店铺类型失败");
        }
        return Result.ok("冻结店铺类型成功");
    }

    /**
     * 获取店铺类型（后台）
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getShopTypeBack(ShopTypeDto oShopTypeDto) throws Exception {
        List<ShopType> oShopTypeList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入名称
        if(StringUtils.isNotBlank(oShopTypeDto.getName())){
            queryWrapper.where(ShopTypeTable.SHOP_TYPE.NAME.like(oShopTypeDto.getName()));
        }
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oShopTypeDto.getPage() != null && oShopTypeDto.getLimit() != null){
            Page<ShopType> ShopTypePage = oShopTypeMapper.paginateWithRelations(oShopTypeDto.getPage(),oShopTypeDto.getLimit(),queryWrapper);
            oShopTypeList = ShopTypePage.getRecords();
            total = ShopTypePage.getTotalRow();
        }else{
            oShopTypeList = oShopTypeMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oShopTypeList.size();
        }
        //PoToDto
        List<ShopTypeDto> oShopTypeDtoList = (List<ShopTypeDto>) PoToDTO.poToDtoList(oShopTypeList,new ShopTypeDto());
        return Result.ok(oShopTypeDtoList,total);
    }

    /**
     * 解冻店铺类型
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    @Override
    public Result thawShopType(ShopTypeDto oShopTypeDto) throws Exception {
        ShopType oShopType = new ShopType();
        PoToDTO.poToDto(oShopTypeDto,oShopType);
        oShopType.setState(1);
        if(oShopTypeMapper.update(oShopType) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("解冻店铺类型失败");
        }
        return Result.ok("解冻店铺类型成功");
    }

    /**
     * 获取店铺类型（用户）
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getShopTypeUser(ShopTypeDto oShopTypeDto) throws Exception {
        List<ShopType> oShopTypeList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(ShopTypeTable.SHOP_TYPE.STATE.eq(1));
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oShopTypeDto.getPage() != null && oShopTypeDto.getLimit() != null){
            Page<ShopType> ShopTypePage = oShopTypeMapper.paginateWithRelations(oShopTypeDto.getPage(),oShopTypeDto.getLimit(),queryWrapper);
            oShopTypeList = ShopTypePage.getRecords();
            total = ShopTypePage.getTotalRow();
        }else{
            oShopTypeList = oShopTypeMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oShopTypeList.size();
        }
        //PoToDto
        List<ShopTypeDto> oShopTypeDtoList = (List<ShopTypeDto>) PoToDTO.poToDtoList(oShopTypeList,new ShopTypeDto());
        return Result.ok(oShopTypeDtoList,total);
    }

}
