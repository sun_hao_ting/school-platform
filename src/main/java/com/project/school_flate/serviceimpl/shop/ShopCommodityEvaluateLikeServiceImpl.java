package com.project.school_flate.serviceimpl.shop;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.shop.ShopCommodityEvaluateLikeDto;
import com.project.school_flate.entity.shop.ShopCommodityEvaluateLike;
import com.project.school_flate.entity.shop.ShopCommodityEvaluateLike;
import com.project.school_flate.mapper.shop.ShopCommodityEvaluateLikeMapper;
import com.project.school_flate.service.shop.ShopCommodityEvaluateLikeService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class ShopCommodityEvaluateLikeServiceImpl extends ServiceImpl<ShopCommodityEvaluateLikeMapper, ShopCommodityEvaluateLike> implements ShopCommodityEvaluateLikeService {

    @Autowired
    private ShopCommodityEvaluateLikeMapper oShopCommodityEvaluateLikeMapper;

    /**
     * 获取商品评论点赞
     * @param oShopCommodityEvaluateLikeDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getShopCommodityEvaluateLike(ShopCommodityEvaluateLikeDto oShopCommodityEvaluateLikeDto) throws Exception {
        List<ShopCommodityEvaluateLike> oShopCommodityEvaluateLikeList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oShopCommodityEvaluateLikeDto.getPage() != null && oShopCommodityEvaluateLikeDto.getLimit() != null){
            Page<ShopCommodityEvaluateLike> ShopCommodityEvaluateLikePage = oShopCommodityEvaluateLikeMapper.paginateWithRelations(oShopCommodityEvaluateLikeDto.getPage(),oShopCommodityEvaluateLikeDto.getLimit(),queryWrapper);
            oShopCommodityEvaluateLikeList = ShopCommodityEvaluateLikePage.getRecords();
            total = ShopCommodityEvaluateLikePage.getTotalRow();
        }else{
            oShopCommodityEvaluateLikeList = oShopCommodityEvaluateLikeMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oShopCommodityEvaluateLikeList.size();
        }
        //PoToDto
        List<ShopCommodityEvaluateLikeDto> oShopCommodityEvaluateLikeDtoList = (List<ShopCommodityEvaluateLikeDto>) PoToDTO.poToDtoList(oShopCommodityEvaluateLikeList,new ShopCommodityEvaluateLikeDto());
        return Result.ok(oShopCommodityEvaluateLikeDtoList,total);
    }

    /**
     * 添加商品评论点赞
     * @param oShopCommodityEvaluateLikeDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addShopCommodityEvaluateLike(ShopCommodityEvaluateLikeDto oShopCommodityEvaluateLikeDto) throws Exception {
        ShopCommodityEvaluateLike oShopCommodityEvaluateLike = new ShopCommodityEvaluateLike();
        PoToDTO.poToDto(oShopCommodityEvaluateLikeDto,oShopCommodityEvaluateLike);
        if(oShopCommodityEvaluateLikeMapper.insert(oShopCommodityEvaluateLike) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加商品评论点赞失败");
        }
        return Result.ok("添加商品评论点赞成功");
    }

    /**
     * 修改商品评论点赞
     * @param oShopCommodityEvaluateLikeDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateShopCommodityEvaluateLike(ShopCommodityEvaluateLikeDto oShopCommodityEvaluateLikeDto) throws Exception {
        ShopCommodityEvaluateLike oShopCommodityEvaluateLike = new ShopCommodityEvaluateLike();
        PoToDTO.poToDto(oShopCommodityEvaluateLikeDto,oShopCommodityEvaluateLike);
        if(oShopCommodityEvaluateLikeMapper.update(oShopCommodityEvaluateLike) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改商品评论点赞失败");
        }
        return Result.ok("修改商品评论点赞成功");
    }

    /**
     * 删除商品评论点赞
     * @param oShopCommodityEvaluateLikeDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result deleteShopCommodityEvaluateLike(ShopCommodityEvaluateLikeDto oShopCommodityEvaluateLikeDto) throws Exception {
        ShopCommodityEvaluateLike oShopCommodityEvaluateLike = new ShopCommodityEvaluateLike();
        PoToDTO.poToDto(oShopCommodityEvaluateLikeDto,oShopCommodityEvaluateLike);
        if(oShopCommodityEvaluateLikeMapper.deleteById(oShopCommodityEvaluateLike.getId()) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("删除商品评论点赞失败");
        }
        return Result.ok("删除商品评论点赞成功");
    }

    /**
     * 获取商品评论点赞（用户）
     * @param oShopCommodityEvaluateLikeDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getShopCommodityEvaluateLikeUser(ShopCommodityEvaluateLikeDto oShopCommodityEvaluateLikeDto) throws Exception {
        List<ShopCommodityEvaluateLike> oShopCommodityEvaluateLikeList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oShopCommodityEvaluateLikeDto.getPage() != null && oShopCommodityEvaluateLikeDto.getLimit() != null){
            Page<ShopCommodityEvaluateLike> ShopCommodityEvaluateLikePage = oShopCommodityEvaluateLikeMapper.paginateWithRelations(oShopCommodityEvaluateLikeDto.getPage(),oShopCommodityEvaluateLikeDto.getLimit(),queryWrapper);
            oShopCommodityEvaluateLikeList = ShopCommodityEvaluateLikePage.getRecords();
            total = ShopCommodityEvaluateLikePage.getTotalRow();
        }else{
            oShopCommodityEvaluateLikeList = oShopCommodityEvaluateLikeMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oShopCommodityEvaluateLikeList.size();
        }
        //PoToDto
        List<ShopCommodityEvaluateLikeDto> oShopCommodityEvaluateLikeDtoList = (List<ShopCommodityEvaluateLikeDto>) PoToDTO.poToDtoList(oShopCommodityEvaluateLikeList,new ShopCommodityEvaluateLikeDto());
        return Result.ok(oShopCommodityEvaluateLikeDtoList,total);
    }

}
