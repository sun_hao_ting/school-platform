package com.project.school_flate.serviceimpl.shop;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.shop.ShopCollectDto;
import com.project.school_flate.entity.shop.ShopCollect;
import com.project.school_flate.entity.shop.table.ShopCollectTableDef;
import com.project.school_flate.mapper.shop.ShopCollectMapper;
import com.project.school_flate.service.shop.ShopCollectService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class ShopCollectServiceImpl extends ServiceImpl<ShopCollectMapper, ShopCollect> implements ShopCollectService {

    @Autowired
    private ShopCollectMapper oShopCollectMapper;

    /**
     * 获取店铺收藏
     * @param oShopCollectDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getShopCollect(ShopCollectDto oShopCollectDto) throws Exception {
        List<ShopCollect> oShopCollectList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入用户ID
        if(StringUtils.isNotBlank(oShopCollectDto.getUserId())){
            queryWrapper.where(ShopCollectTableDef.SHOP_COLLECT.USER_ID.eq(oShopCollectDto.getUserId()));
        }
        //判断是否传入店铺ID
        if(StringUtils.isNotBlank(oShopCollectDto.getShopId())){
            queryWrapper.where(ShopCollectTableDef.SHOP_COLLECT.SHOP_ID.eq(oShopCollectDto.getShopId()));
        }
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oShopCollectDto.getPage() != null && oShopCollectDto.getLimit() != null){
            Page<ShopCollect> ShopCollectPage = oShopCollectMapper.paginateWithRelations(oShopCollectDto.getPage(),oShopCollectDto.getLimit(),queryWrapper);
            oShopCollectList = ShopCollectPage.getRecords();
            total = ShopCollectPage.getTotalRow();
        }else{
            oShopCollectList = oShopCollectMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oShopCollectList.size();
        }
        //PoToDto
        List<ShopCollectDto> oShopCollectDtoList = (List<ShopCollectDto>) PoToDTO.poToDtoList(oShopCollectList,new ShopCollectDto());
        return Result.ok(oShopCollectDtoList,total);
    }

    /**
     * 添加店铺收藏
     * @param oShopCollectDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addShopCollect(ShopCollectDto oShopCollectDto) throws Exception {
        ShopCollect oShopCollect = new ShopCollect();
        PoToDTO.poToDto(oShopCollectDto,oShopCollect);
        if(oShopCollectMapper.insert(oShopCollect) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加店铺收藏失败");
        }
        return Result.ok("添加店铺收藏成功");
    }

    /**
     * 修改店铺收藏
     * @param oShopCollectDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateShopCollect(ShopCollectDto oShopCollectDto) throws Exception {
        ShopCollect oShopCollect = new ShopCollect();
        PoToDTO.poToDto(oShopCollectDto,oShopCollect);
        if(oShopCollectMapper.update(oShopCollect) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改店铺收藏失败");
        }
        return Result.ok("修改店铺收藏成功");
    }

    /**
     * 删除店铺收藏
     * @param oShopCollectDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result deleteShopCollect(ShopCollectDto oShopCollectDto) throws Exception {
        ShopCollect oShopCollect = new ShopCollect();
        PoToDTO.poToDto(oShopCollectDto,oShopCollect);
        if(StringUtils.isNotBlank(oShopCollect.getId())){
            if(oShopCollectMapper.deleteById(oShopCollect.getId()) == 0){
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return Result.fail("删除店铺收藏失败");
            }
        }else{
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.where(ShopCollectTableDef.SHOP_COLLECT.USER_ID.eq(oShopCollect.getUserId()));
            queryWrapper.where(ShopCollectTableDef.SHOP_COLLECT.SHOP_ID.eq(oShopCollect.getShopId()));
            if(oShopCollectMapper.deleteByQuery(queryWrapper) == 0){
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return Result.fail("删除店铺收藏失败");
            }
        }
        return Result.ok("删除店铺收藏成功");
    }

}
