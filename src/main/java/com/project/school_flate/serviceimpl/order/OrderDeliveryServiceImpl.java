package com.project.school_flate.serviceimpl.order;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.order.OrderDeliveryDto;
import com.project.school_flate.entity.order.OrderDelivery;
import com.project.school_flate.entity.order.OrderDelivery;
import com.project.school_flate.entity.order.table.OrderDeliveryTableDef;
import com.project.school_flate.mapper.order.OrderDeliveryMapper;
import com.project.school_flate.service.order.OrderDeliveryService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class OrderDeliveryServiceImpl extends ServiceImpl<OrderDeliveryMapper, OrderDelivery> implements OrderDeliveryService {

    @Autowired
    private OrderDeliveryMapper oOrderDeliveryMapper;

    /**
     * 获取外卖订单配送
     * @param oOrderDeliveryDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getOrderDelivery(OrderDeliveryDto oOrderDeliveryDto) throws Exception {
        List<OrderDelivery> oOrderDeliveryList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入订单ID
        if(StringUtils.isNotBlank(oOrderDeliveryDto.getOrderTakeawayId())){
            queryWrapper.where(OrderDeliveryTableDef.ORDER_DELIVERY.ORDER_TAKEAWAY_ID.eq(oOrderDeliveryDto.getOrderTakeawayId()));
        }
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oOrderDeliveryDto.getPage() != null && oOrderDeliveryDto.getLimit() != null){
            Page<OrderDelivery> OrderDeliveryPage = oOrderDeliveryMapper.paginateWithRelations(oOrderDeliveryDto.getPage(),oOrderDeliveryDto.getLimit(),queryWrapper);
            oOrderDeliveryList = OrderDeliveryPage.getRecords();
            total = OrderDeliveryPage.getTotalRow();
        }else{
            oOrderDeliveryList = oOrderDeliveryMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oOrderDeliveryList.size();
        }
        //PoToDto
        List<OrderDeliveryDto> oOrderDeliveryDtoList = (List<OrderDeliveryDto>) PoToDTO.poToDtoList(oOrderDeliveryList,new OrderDeliveryDto());
        return Result.ok(oOrderDeliveryDtoList,total);
    }

    /**
     * 添加外卖订单配送
     * @param oOrderDeliveryDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addOrderDelivery(OrderDeliveryDto oOrderDeliveryDto) throws Exception {
        OrderDelivery oOrderDelivery = new OrderDelivery();
        PoToDTO.poToDto(oOrderDeliveryDto,oOrderDelivery);
        if(oOrderDeliveryMapper.insert(oOrderDelivery) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加外卖订单配送失败");
        }
        return Result.ok("添加外卖订单配送成功");
    }

    /**
     * 修改外卖订单配送
     * @param oOrderDeliveryDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateOrderDelivery(OrderDeliveryDto oOrderDeliveryDto) throws Exception {
        OrderDelivery oOrderDelivery = new OrderDelivery();
        PoToDTO.poToDto(oOrderDeliveryDto,oOrderDelivery);
        if(oOrderDeliveryMapper.update(oOrderDelivery) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改外卖订单配送失败");
        }
        return Result.ok("修改外卖订单配送成功");
    }

    /**
     * 删除外卖订单配送
     * @param oOrderDeliveryDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result deleteOrderDelivery(OrderDeliveryDto oOrderDeliveryDto) throws Exception {
        OrderDelivery oOrderDelivery = new OrderDelivery();
        PoToDTO.poToDto(oOrderDeliveryDto,oOrderDelivery);
        if(oOrderDeliveryMapper.deleteById(oOrderDelivery.getId()) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("删除外卖订单配送失败");
        }
        return Result.ok("删除外卖订单配送成功");
    }

}
