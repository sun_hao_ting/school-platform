package com.project.school_flate.serviceimpl.order;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.order.OrderIntegralCommodityDto;
import com.project.school_flate.entity.order.OrderIntegralCommodity;
import com.project.school_flate.mapper.order.OrderIntegralCommodityMapper;
import com.project.school_flate.service.order.OrderIntegralCommodityService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class OrderIntegralCommodityServiceImpl extends ServiceImpl<OrderIntegralCommodityMapper, OrderIntegralCommodity> implements OrderIntegralCommodityService {

    @Autowired
    private OrderIntegralCommodityMapper oOrderIntegralCommodityMapper;

    /**
     * 获取积分订单商品
     * @param oOrderIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getOrderIntegralCommodity(OrderIntegralCommodityDto oOrderIntegralCommodityDto) throws Exception {
        List<OrderIntegralCommodity> oOrderIntegralCommodityList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oOrderIntegralCommodityDto.getPage() != null && oOrderIntegralCommodityDto.getLimit() != null){
            Page<OrderIntegralCommodity> OrderIntegralCommodityPage = oOrderIntegralCommodityMapper.paginateWithRelations(oOrderIntegralCommodityDto.getPage(),oOrderIntegralCommodityDto.getLimit(),queryWrapper);
            oOrderIntegralCommodityList = OrderIntegralCommodityPage.getRecords();
            total = OrderIntegralCommodityPage.getTotalRow();
        }else{
            oOrderIntegralCommodityList = oOrderIntegralCommodityMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oOrderIntegralCommodityList.size();
        }
        //PoToDto
        List<OrderIntegralCommodityDto> oOrderIntegralCommodityDtoList = (List<OrderIntegralCommodityDto>) PoToDTO.poToDtoList(oOrderIntegralCommodityList,new OrderIntegralCommodityDto());
        return Result.ok(oOrderIntegralCommodityDtoList,total);
    }

    /**
     * 添加积分订单商品
     * @param oOrderIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addOrderIntegralCommodity(OrderIntegralCommodityDto oOrderIntegralCommodityDto) throws Exception {
        OrderIntegralCommodity oOrderIntegralCommodity = new OrderIntegralCommodity();
        PoToDTO.poToDto(oOrderIntegralCommodityDto,oOrderIntegralCommodity);
        if(oOrderIntegralCommodityMapper.insert(oOrderIntegralCommodity) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加积分订单商品失败");
        }
        return Result.ok("添加积分订单商品成功");
    }

    /**
     * 修改积分订单商品
     * @param oOrderIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateOrderIntegralCommodity(OrderIntegralCommodityDto oOrderIntegralCommodityDto) throws Exception {
        OrderIntegralCommodity oOrderIntegralCommodity = new OrderIntegralCommodity();
        PoToDTO.poToDto(oOrderIntegralCommodityDto,oOrderIntegralCommodity);
        if(oOrderIntegralCommodityMapper.update(oOrderIntegralCommodity) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改积分订单商品失败");
        }
        return Result.ok("修改积分订单商品成功");
    }

    /**
     * 删除积分订单商品
     * @param oOrderIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result deleteOrderIntegralCommodity(OrderIntegralCommodityDto oOrderIntegralCommodityDto) throws Exception {
        OrderIntegralCommodity oOrderIntegralCommodity = new OrderIntegralCommodity();
        PoToDTO.poToDto(oOrderIntegralCommodityDto,oOrderIntegralCommodity);
        if(oOrderIntegralCommodityMapper.deleteById(oOrderIntegralCommodity.getId()) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("删除积分订单商品失败");
        }
        return Result.ok("删除积分订单商品成功");
    }
    
}
