package com.project.school_flate.serviceimpl.integral;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.integral.IntegralExchangeSaveRecordDto;
import com.project.school_flate.entity.delivery.DeliveryInfo;
import com.project.school_flate.entity.delivery.table.DeliveryInfoTableDef;
import com.project.school_flate.entity.flow.table.FlowMoneyTableDef;
import com.project.school_flate.entity.integral.IntegralExchangeSaveRecord;
import com.project.school_flate.entity.integral.table.IntegralExchangeSaveRecordTableDef;
import com.project.school_flate.entity.shop.ShopInfo;
import com.project.school_flate.entity.shop.ShopSaveUser;
import com.project.school_flate.entity.shop.table.ShopInfoTableDef;
import com.project.school_flate.entity.user.UserInfo;
import com.project.school_flate.entity.user.table.UserInfoTableDef;
import com.project.school_flate.mapper.delivery.DeliveryInfoMapper;
import com.project.school_flate.mapper.integral.IntegralExchangeSaveRecordMapper;
import com.project.school_flate.mapper.shop.ShopInfoMapper;
import com.project.school_flate.mapper.shop.ShopSaveUserMapper;
import com.project.school_flate.mapper.user.UserInfoMapper;
import com.project.school_flate.service.integral.IntegralExchangeSaveRecordService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class IntegralExchangeSaveRecordServiceImpl extends ServiceImpl<IntegralExchangeSaveRecordMapper, IntegralExchangeSaveRecord> implements IntegralExchangeSaveRecordService {

    @Autowired
    private IntegralExchangeSaveRecordMapper oIntegralExchangeSaveRecordMapper;

    @Autowired
    private UserInfoMapper oUserInfoMapper;

    @Autowired
    private ShopSaveUserMapper oShopSaveUserMapper;

    @Autowired
    private ShopInfoMapper oShopInfoMapper;

    /**
     * 获取积分兑换储蓄记录
     * @param oIntegralExchangeSaveRecordDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getIntegralExchangeSaveRecord(IntegralExchangeSaveRecordDto oIntegralExchangeSaveRecordDto) throws Exception {
        List<IntegralExchangeSaveRecord> oIntegralExchangeSaveRecordList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入用户名称
        if(StringUtils.isNotBlank(oIntegralExchangeSaveRecordDto.getUserName())){
            QueryWrapper qwUser = new QueryWrapper();
            qwUser.where(UserInfoTableDef.USER_INFO.NAME.like(oIntegralExchangeSaveRecordDto.getUserName()));
            List<UserInfo> oUserInfoList = oUserInfoMapper.selectListByQuery(qwUser);
            if(CollectionUtils.isEmpty(oUserInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的用户ID集合
            List<String> oUserInfoIdList = new ArrayList<>();
            oUserInfoList.forEach(item->{oUserInfoIdList.add(item.getId());});
            queryWrapper.where(IntegralExchangeSaveRecordTableDef.INTEGRAL_EXCHANGE_SAVE_RECORD.USER_ID.in(oUserInfoIdList));
        }
        //判断是否传入店铺名称
        if(StringUtils.isNotBlank(oIntegralExchangeSaveRecordDto.getShopName())){
            QueryWrapper qwShop = new QueryWrapper();
            qwShop.where(ShopInfoTableDef.SHOP_INFO.NAME.like(oIntegralExchangeSaveRecordDto.getShopName()));
            List<ShopInfo> oShopInfoList = oShopInfoMapper.selectListByQuery(qwShop);
            if(CollectionUtils.isEmpty(oShopInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的店铺ID集合
            List<String> oShopInfoIdList = new ArrayList<>();
            oShopInfoList.forEach(item->{oShopInfoIdList.add(item.getId());});
            queryWrapper.where(IntegralExchangeSaveRecordTableDef.INTEGRAL_EXCHANGE_SAVE_RECORD.SHOP_ID.in(oShopInfoIdList));
        }
        //判断是否传入校区ID
        if(StringUtils.isNotBlank(oIntegralExchangeSaveRecordDto.getSchoolId())){
            QueryWrapper qwShop = new QueryWrapper();
            qwShop.where(ShopInfoTableDef.SHOP_INFO.SCHOOL_ID.eq(oIntegralExchangeSaveRecordDto.getSchoolId()));
            List<ShopInfo> oShopInfoList = oShopInfoMapper.selectListByQuery(qwShop);
            if(CollectionUtils.isEmpty(oShopInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的店铺ID集合
            List<String> oShopInfoIdList = new ArrayList<>();
            oShopInfoList.forEach(item->{oShopInfoIdList.add(item.getId());});
            queryWrapper.where(IntegralExchangeSaveRecordTableDef.INTEGRAL_EXCHANGE_SAVE_RECORD.SHOP_ID.in(oShopInfoIdList));
        }
        queryWrapper.orderBy("create_time asc");
        //是否分页
        if(oIntegralExchangeSaveRecordDto.getPage() != null && oIntegralExchangeSaveRecordDto.getLimit() != null){
            Page<IntegralExchangeSaveRecord> IntegralExchangeSaveRecordPage = oIntegralExchangeSaveRecordMapper.paginateWithRelations(oIntegralExchangeSaveRecordDto.getPage(),oIntegralExchangeSaveRecordDto.getLimit(),queryWrapper);
            oIntegralExchangeSaveRecordList = IntegralExchangeSaveRecordPage.getRecords();
            total = IntegralExchangeSaveRecordPage.getTotalRow();
        }else{
            oIntegralExchangeSaveRecordList = oIntegralExchangeSaveRecordMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oIntegralExchangeSaveRecordList.size();
        }
        //PoToDto
        List<IntegralExchangeSaveRecordDto> oIntegralExchangeSaveRecordDtoList = (List<IntegralExchangeSaveRecordDto>) PoToDTO.poToDtoList(oIntegralExchangeSaveRecordList,new IntegralExchangeSaveRecordDto());
        return Result.ok(oIntegralExchangeSaveRecordDtoList,total);
    }

    /**
     * 添加积分兑换储蓄记录
     * @param oIntegralExchangeSaveRecordDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addIntegralExchangeSaveRecord(IntegralExchangeSaveRecordDto oIntegralExchangeSaveRecordDto) throws Exception {
        IntegralExchangeSaveRecord oIntegralExchangeSaveRecord = new IntegralExchangeSaveRecord();
        PoToDTO.poToDto(oIntegralExchangeSaveRecordDto,oIntegralExchangeSaveRecord);
        //获取用户信息
        UserInfo oUserInfo = oUserInfoMapper.selectOneById(oIntegralExchangeSaveRecord.getUserId());
        if(oUserInfo.getIntegral().compareTo(oIntegralExchangeSaveRecord.getIntegral()) < 0){
            return Result.fail("客户积分余额不足");
        }
        if(oIntegralExchangeSaveRecordMapper.insert(oIntegralExchangeSaveRecord) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加积分兑换储蓄记录失败");
        }
        //添加店铺用户储蓄
        ShopSaveUser oShopSaveUser = new ShopSaveUser();
        oShopSaveUser.setShopId(oIntegralExchangeSaveRecord.getShopId());
        oShopSaveUser.setUserId(oIntegralExchangeSaveRecord.getUserId());
        oShopSaveUser.setUserBalance(oIntegralExchangeSaveRecord.getPrice());
        oShopSaveUser.setRechargePrice(oIntegralExchangeSaveRecord.getPrice());
        oShopSaveUser.setGiftsPrice(0.0);
        oShopSaveUser.setDiscountRatio(1.0);
        if(oShopSaveUserMapper.insert(oShopSaveUser) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加积分兑换储蓄记录失败");
        }
        //修改用户积分
        oUserInfo.setIntegral(oUserInfo.getIntegral() - oIntegralExchangeSaveRecord.getIntegral());
        if(oUserInfoMapper.update(oUserInfo) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加积分兑换储蓄记录失败");
        }
        return Result.ok("添加积分兑换储蓄记录成功");
    }

    /**
     * 修改积分兑换储蓄记录
     * @param oIntegralExchangeSaveRecordDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateIntegralExchangeSaveRecord(IntegralExchangeSaveRecordDto oIntegralExchangeSaveRecordDto) throws Exception {
        IntegralExchangeSaveRecord oIntegralExchangeSaveRecord = new IntegralExchangeSaveRecord();
        PoToDTO.poToDto(oIntegralExchangeSaveRecordDto,oIntegralExchangeSaveRecord);
        if(oIntegralExchangeSaveRecordMapper.update(oIntegralExchangeSaveRecord) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改积分兑换储蓄记录失败");
        }
        return Result.ok("修改积分兑换储蓄记录成功");
    }

    /**
     * 删除积分兑换储蓄记录
     * @param oIntegralExchangeSaveRecordDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result deleteIntegralExchangeSaveRecord(IntegralExchangeSaveRecordDto oIntegralExchangeSaveRecordDto) throws Exception {
        IntegralExchangeSaveRecord oIntegralExchangeSaveRecord = new IntegralExchangeSaveRecord();
        PoToDTO.poToDto(oIntegralExchangeSaveRecordDto,oIntegralExchangeSaveRecord);
        if(oIntegralExchangeSaveRecordMapper.deleteById(oIntegralExchangeSaveRecord.getId()) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("删除积分兑换储蓄记录失败");
        }
        return Result.ok("删除积分兑换储蓄记录成功");
    }

}
