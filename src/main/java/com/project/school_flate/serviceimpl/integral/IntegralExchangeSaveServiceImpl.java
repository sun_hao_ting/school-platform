package com.project.school_flate.serviceimpl.integral;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.integral.IntegralExchangeSaveDto;
import com.project.school_flate.entity.integral.IntegralExchangeSave;
import com.project.school_flate.entity.integral.table.IntegralExchangeSaveRecordTableDef;
import com.project.school_flate.entity.integral.table.IntegralExchangeSaveTableDef;
import com.project.school_flate.entity.shop.ShopInfo;
import com.project.school_flate.entity.shop.table.ShopCommodityTableDef;
import com.project.school_flate.entity.shop.table.ShopInfoTableDef;
import com.project.school_flate.mapper.integral.IntegralExchangeSaveMapper;
import com.project.school_flate.mapper.shop.ShopInfoMapper;
import com.project.school_flate.service.integral.IntegralExchangeSaveService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class IntegralExchangeSaveServiceImpl extends ServiceImpl<IntegralExchangeSaveMapper, IntegralExchangeSave> implements IntegralExchangeSaveService {

    @Autowired
    private IntegralExchangeSaveMapper oIntegralExchangeSaveMapper;

    @Autowired
    private ShopInfoMapper oShopInfoMapper;

    /**
     * 获取积分兑换
     * @param oIntegralExchangeSaveDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getIntegralExchangeSave(IntegralExchangeSaveDto oIntegralExchangeSaveDto) throws Exception {
        List<IntegralExchangeSave> oIntegralExchangeSaveList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入店铺名称
        if(StringUtils.isNotBlank(oIntegralExchangeSaveDto.getShopName())){
            QueryWrapper qwShop = new QueryWrapper();
            qwShop.where(ShopInfoTableDef.SHOP_INFO.NAME.like(oIntegralExchangeSaveDto.getShopName()));
            List<ShopInfo> oShopInfoList = oShopInfoMapper.selectListByQuery(qwShop);
            if(CollectionUtils.isEmpty(oShopInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的店铺ID集合
            List<String> oShopInfoIdList = new ArrayList<>();
            oShopInfoList.forEach(item->{oShopInfoIdList.add(item.getId());});
            queryWrapper.where(IntegralExchangeSaveTableDef.INTEGRAL_EXCHANGE_SAVE.SHOP_ID.in(oShopInfoIdList));
        }
        //判断是否传入校区ID
        if(StringUtils.isNotBlank(oIntegralExchangeSaveDto.getSchoolId())){
            QueryWrapper qwShop = new QueryWrapper();
            qwShop.where(ShopInfoTableDef.SHOP_INFO.SCHOOL_ID.eq(oIntegralExchangeSaveDto.getSchoolId()));
            List<ShopInfo> oShopInfoList = oShopInfoMapper.selectListByQuery(qwShop);
            if(CollectionUtils.isEmpty(oShopInfoList)){
                return Result.ok(new ArrayList(),total);
            }
            //获取查到的店铺ID集合
            List<String> oShopInfoIdList = new ArrayList<>();
            oShopInfoList.forEach(item->{oShopInfoIdList.add(item.getId());});
            queryWrapper.where(IntegralExchangeSaveTableDef.INTEGRAL_EXCHANGE_SAVE.SHOP_ID.in(oShopInfoIdList));
        }
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oIntegralExchangeSaveDto.getPage() != null && oIntegralExchangeSaveDto.getLimit() != null){
            Page<IntegralExchangeSave> IntegralExchangeSavePage = oIntegralExchangeSaveMapper.paginateWithRelations(oIntegralExchangeSaveDto.getPage(),oIntegralExchangeSaveDto.getLimit(),queryWrapper);
            oIntegralExchangeSaveList = IntegralExchangeSavePage.getRecords();
            total = IntegralExchangeSavePage.getTotalRow();
        }else{
            oIntegralExchangeSaveList = oIntegralExchangeSaveMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oIntegralExchangeSaveList.size();
        }
        //PoToDto
        List<IntegralExchangeSaveDto> oIntegralExchangeSaveDtoList = (List<IntegralExchangeSaveDto>) PoToDTO.poToDtoList(oIntegralExchangeSaveList,new IntegralExchangeSaveDto());
        return Result.ok(oIntegralExchangeSaveDtoList,total);
    }

    /**
     * 添加积分兑换
     * @param oIntegralExchangeSaveDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addIntegralExchangeSave(IntegralExchangeSaveDto oIntegralExchangeSaveDto) throws Exception {
        IntegralExchangeSave oIntegralExchangeSave = new IntegralExchangeSave();
        PoToDTO.poToDto(oIntegralExchangeSaveDto,oIntegralExchangeSave);
        if(oIntegralExchangeSaveMapper.insert(oIntegralExchangeSave) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加积分兑换失败");
        }
        return Result.ok("添加积分兑换成功");
    }

    /**
     * 修改积分兑换
     * @param oIntegralExchangeSaveDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateIntegralExchangeSave(IntegralExchangeSaveDto oIntegralExchangeSaveDto) throws Exception {
        IntegralExchangeSave oIntegralExchangeSave = new IntegralExchangeSave();
        PoToDTO.poToDto(oIntegralExchangeSaveDto,oIntegralExchangeSave);
        if(oIntegralExchangeSaveMapper.update(oIntegralExchangeSave) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改积分兑换失败");
        }
        return Result.ok("修改积分兑换成功");
    }

    /**
     * 删除积分兑换
     * @param oIntegralExchangeSaveDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result deleteIntegralExchangeSave(IntegralExchangeSaveDto oIntegralExchangeSaveDto) throws Exception {
        IntegralExchangeSave oIntegralExchangeSave = new IntegralExchangeSave();
        PoToDTO.poToDto(oIntegralExchangeSaveDto,oIntegralExchangeSave);
        if(oIntegralExchangeSaveMapper.deleteById(oIntegralExchangeSave.getId()) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("删除积分兑换失败");
        }
        return Result.ok("删除积分兑换成功");
    }

    /**
     * 获取积分兑换（用户）
     * @param oIntegralExchangeSaveDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getIntegralExchangeSaveUser(IntegralExchangeSaveDto oIntegralExchangeSaveDto) throws Exception {
        List<IntegralExchangeSave> oIntegralExchangeSaveList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入校园ID
        if(StringUtils.isNotBlank(oIntegralExchangeSaveDto.getSchoolId())){
            //获取校园下的店铺
            QueryWrapper qwShopInfo = new QueryWrapper();
            qwShopInfo.where(ShopInfoTableDef.SHOP_INFO.SCHOOL_ID.eq(oIntegralExchangeSaveDto.getSchoolId()));
            List<ShopInfo> oShopInfoList = oShopInfoMapper.selectListByQuery(qwShopInfo);
            if(CollectionUtils.isEmpty(oShopInfoList)){
                return Result.ok(new ArrayList(),0);
            }
            //获取查到的店铺ID集合
            List<String> oShopInfoIdList = new ArrayList<>();
            oShopInfoList.forEach(item->{oShopInfoIdList.add(item.getId());});
            queryWrapper.where(IntegralExchangeSaveTableDef.INTEGRAL_EXCHANGE_SAVE.SHOP_ID.in(oShopInfoIdList));
        }
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oIntegralExchangeSaveDto.getPage() != null && oIntegralExchangeSaveDto.getLimit() != null){
            Page<IntegralExchangeSave> IntegralExchangePageSave = oIntegralExchangeSaveMapper.paginateWithRelations(oIntegralExchangeSaveDto.getPage(),oIntegralExchangeSaveDto.getLimit(),queryWrapper);
            oIntegralExchangeSaveList = IntegralExchangePageSave.getRecords();
            total = IntegralExchangePageSave.getTotalRow();
        }else{
            oIntegralExchangeSaveList = oIntegralExchangeSaveMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oIntegralExchangeSaveList.size();
        }
        //PoToDto
        List<IntegralExchangeSaveDto> oIntegralExchangeSaveDtoList = (List<IntegralExchangeSaveDto>) PoToDTO.poToDtoList(oIntegralExchangeSaveList,new IntegralExchangeSaveDto());
        return Result.ok(oIntegralExchangeSaveDtoList,total);
    }

}
