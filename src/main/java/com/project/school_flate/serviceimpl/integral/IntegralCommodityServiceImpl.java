package com.project.school_flate.serviceimpl.integral;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.integral.IntegralCommodityDto;
import com.project.school_flate.entity.integral.IntegralCommodity;
import com.project.school_flate.entity.table.integral.IntegralCommodityTable;
import com.project.school_flate.mapper.integral.IntegralCommodityMapper;
import com.project.school_flate.service.integral.IntegralCommodityService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class IntegralCommodityServiceImpl extends ServiceImpl<IntegralCommodityMapper, IntegralCommodity> implements IntegralCommodityService {

    @Autowired
    private IntegralCommodityMapper oIntegralCommodityMapper;

    /**
     * 获取积分商品
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getIntegralCommodity(IntegralCommodityDto oIntegralCommodityDto) throws Exception {
        List<IntegralCommodity> oIntegralCommodityList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        if(StringUtils.isNotBlank(oIntegralCommodityDto.getId())){
            queryWrapper.where(IntegralCommodityTable.INTEGRAL_COMMODITY.ID.eq(oIntegralCommodityDto.getId()));
        }
        //判断是否传入商品名称
        if(StringUtils.isNotBlank(oIntegralCommodityDto.getName())){
            queryWrapper.where(IntegralCommodityTable.INTEGRAL_COMMODITY.NAME.like(oIntegralCommodityDto.getName()));
        }
        //判断是否传入商品类型ID
        if(StringUtils.isNotBlank(oIntegralCommodityDto.getCommodityTypeId())){
            queryWrapper.where(IntegralCommodityTable.INTEGRAL_COMMODITY.COMMODITY_TYPE_ID.like(oIntegralCommodityDto.getCommodityTypeId()));
        }
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oIntegralCommodityDto.getPage() != null && oIntegralCommodityDto.getLimit() != null){
            Page<IntegralCommodity> IntegralCommodityPage = oIntegralCommodityMapper.paginateWithRelations(oIntegralCommodityDto.getPage(),oIntegralCommodityDto.getLimit(),queryWrapper);
            oIntegralCommodityList = IntegralCommodityPage.getRecords();
            total = IntegralCommodityPage.getTotalRow();
        }else{
            oIntegralCommodityList = oIntegralCommodityMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oIntegralCommodityList.size();
        }
        //PoToDto
        List<IntegralCommodityDto> oIntegralCommodityDtoList = (List<IntegralCommodityDto>) PoToDTO.poToDtoList(oIntegralCommodityList,new IntegralCommodityDto());
        return Result.ok(oIntegralCommodityDtoList,total);
    }

    /**
     * 添加积分商品
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addIntegralCommodity(IntegralCommodityDto oIntegralCommodityDto) throws Exception {
        IntegralCommodity oIntegralCommodity = new IntegralCommodity();
        PoToDTO.poToDto(oIntegralCommodityDto,oIntegralCommodity);
        if(oIntegralCommodityMapper.insert(oIntegralCommodity) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加积分商品失败");
        }
        return Result.ok("添加积分商品成功");
    }

    /**
     * 修改积分商品
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateIntegralCommodity(IntegralCommodityDto oIntegralCommodityDto) throws Exception {
        IntegralCommodity oIntegralCommodity = new IntegralCommodity();
        PoToDTO.poToDto(oIntegralCommodityDto,oIntegralCommodity);
        if(oIntegralCommodityMapper.update(oIntegralCommodity) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改积分商品失败");
        }
        return Result.ok("修改积分商品成功");
    }

    /**
     * 冻结积分商品
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result freezeIntegralCommodity(IntegralCommodityDto oIntegralCommodityDto) throws Exception {
        IntegralCommodity oIntegralCommodity = new IntegralCommodity();
        PoToDTO.poToDto(oIntegralCommodityDto,oIntegralCommodity);
        oIntegralCommodity.setState(0);
        if(oIntegralCommodityMapper.update(oIntegralCommodity) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("冻结积分商品失败");
        }
        return Result.ok("冻结积分商品成功");
    }

    /**
     * 获取积分商品（后台）
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getIntegralCommodityBack(IntegralCommodityDto oIntegralCommodityDto) throws Exception {
        List<IntegralCommodity> oIntegralCommodityList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oIntegralCommodityDto.getPage() != null && oIntegralCommodityDto.getLimit() != null){
            Page<IntegralCommodity> IntegralCommodityPage = oIntegralCommodityMapper.paginateWithRelations(oIntegralCommodityDto.getPage(),oIntegralCommodityDto.getLimit(),queryWrapper);
            oIntegralCommodityList = IntegralCommodityPage.getRecords();
            total = IntegralCommodityPage.getTotalRow();
        }else{
            oIntegralCommodityList = oIntegralCommodityMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oIntegralCommodityList.size();
        }
        //PoToDto
        List<IntegralCommodityDto> oIntegralCommodityDtoList = (List<IntegralCommodityDto>) PoToDTO.poToDtoList(oIntegralCommodityList,new IntegralCommodityDto());
        return Result.ok(oIntegralCommodityDtoList,total);
    }

    /**
     * 解冻积分商品
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Transactional
    @Override
    public Result thawIntegralCommodity(IntegralCommodityDto oIntegralCommodityDto) throws Exception {
        IntegralCommodity oIntegralCommodity = new IntegralCommodity();
        PoToDTO.poToDto(oIntegralCommodityDto,oIntegralCommodity);
        oIntegralCommodity.setState(1);
        if(oIntegralCommodityMapper.update(oIntegralCommodity) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("解冻积分商品失败");
        }
        return Result.ok("解冻积分商品成功");
    }

    /**
     * 获取积分商品（用户）
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getIntegralCommodityUser(IntegralCommodityDto oIntegralCommodityDto) throws Exception {
        List<IntegralCommodity> oIntegralCommodityList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        if(StringUtils.isNotBlank(oIntegralCommodityDto.getId())){
            queryWrapper.where(IntegralCommodityTable.INTEGRAL_COMMODITY.ID.eq(oIntegralCommodityDto.getId()));
        }
        //判断是否传入商品名称
        if(StringUtils.isNotBlank(oIntegralCommodityDto.getName())){
            queryWrapper.where(IntegralCommodityTable.INTEGRAL_COMMODITY.NAME.like(oIntegralCommodityDto.getName()));
        }
        //判断是否传入商品类型ID
        if(StringUtils.isNotBlank(oIntegralCommodityDto.getCommodityTypeId())){
            queryWrapper.where(IntegralCommodityTable.INTEGRAL_COMMODITY.COMMODITY_TYPE_ID.like(oIntegralCommodityDto.getCommodityTypeId()));
        }
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oIntegralCommodityDto.getPage() != null && oIntegralCommodityDto.getLimit() != null){
            Page<IntegralCommodity> IntegralCommodityPage = oIntegralCommodityMapper.paginateWithRelations(oIntegralCommodityDto.getPage(),oIntegralCommodityDto.getLimit(),queryWrapper);
            oIntegralCommodityList = IntegralCommodityPage.getRecords();
            total = IntegralCommodityPage.getTotalRow();
        }else{
            oIntegralCommodityList = oIntegralCommodityMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oIntegralCommodityList.size();
        }
        //PoToDto
        List<IntegralCommodityDto> oIntegralCommodityDtoList = (List<IntegralCommodityDto>) PoToDTO.poToDtoList(oIntegralCommodityList,new IntegralCommodityDto());
        return Result.ok(oIntegralCommodityDtoList,total);
    }

}
