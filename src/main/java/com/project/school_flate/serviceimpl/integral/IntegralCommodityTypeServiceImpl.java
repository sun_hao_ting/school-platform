package com.project.school_flate.serviceimpl.integral;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.integral.IntegralCommodityTypeDto;
import com.project.school_flate.entity.integral.IntegralCommodityType;
import com.project.school_flate.entity.integral.table.IntegralCommodityTypeTableDef;
import com.project.school_flate.mapper.integral.IntegralCommodityTypeMapper;
import com.project.school_flate.service.integral.IntegralCommodityTypeService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class IntegralCommodityTypeServiceImpl extends ServiceImpl<IntegralCommodityTypeMapper, IntegralCommodityType> implements IntegralCommodityTypeService {

    @Autowired
    private IntegralCommodityTypeMapper oIntegralCommodityTypeMapper;

    /**
     * 获取积分商品分类
     * @param oIntegralCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getIntegralCommodityType(IntegralCommodityTypeDto oIntegralCommodityTypeDto) throws Exception {
        List<IntegralCommodityType> oIntegralCommodityTypeList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入名称
        if(StringUtils.isNotBlank(oIntegralCommodityTypeDto.getName())){
            queryWrapper.where(IntegralCommodityTypeTableDef.INTEGRAL_COMMODITY_TYPE.NAME.like(oIntegralCommodityTypeDto.getName()));
        }
        queryWrapper.orderBy("no asc");
        //是否分页
        if(oIntegralCommodityTypeDto.getPage() != null && oIntegralCommodityTypeDto.getLimit() != null){
            Page<IntegralCommodityType> IntegralCommodityTypePage = oIntegralCommodityTypeMapper.paginateWithRelations(oIntegralCommodityTypeDto.getPage(),oIntegralCommodityTypeDto.getLimit(),queryWrapper);
            oIntegralCommodityTypeList = IntegralCommodityTypePage.getRecords();
            total = IntegralCommodityTypePage.getTotalRow();
        }else{
            oIntegralCommodityTypeList = oIntegralCommodityTypeMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oIntegralCommodityTypeList.size();
        }
        //PoToDto
        List<IntegralCommodityTypeDto> oIntegralCommodityTypeDtoList = (List<IntegralCommodityTypeDto>) PoToDTO.poToDtoList(oIntegralCommodityTypeList,new IntegralCommodityTypeDto());
        return Result.ok(oIntegralCommodityTypeDtoList,total);
    }

    /**
     * 添加积分商品分类
     * @param oIntegralCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addIntegralCommodityType(IntegralCommodityTypeDto oIntegralCommodityTypeDto) throws Exception {
        IntegralCommodityType oIntegralCommodityType = new IntegralCommodityType();
        PoToDTO.poToDto(oIntegralCommodityTypeDto,oIntegralCommodityType);
        if(oIntegralCommodityTypeMapper.insert(oIntegralCommodityType) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加积分商品分类失败");
        }
        return Result.ok("添加积分商品分类成功");
    }

    /**
     * 修改积分商品分类
     * @param oIntegralCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateIntegralCommodityType(IntegralCommodityTypeDto oIntegralCommodityTypeDto) throws Exception {
        IntegralCommodityType oIntegralCommodityType = new IntegralCommodityType();
        PoToDTO.poToDto(oIntegralCommodityTypeDto,oIntegralCommodityType);
        if(oIntegralCommodityTypeMapper.update(oIntegralCommodityType) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改积分商品分类失败");
        }
        return Result.ok("修改积分商品分类成功");
    }

    /**
     * 删除积分商品分类
     * @param oIntegralCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result deleteIntegralCommodityType(IntegralCommodityTypeDto oIntegralCommodityTypeDto) throws Exception {
        IntegralCommodityType oIntegralCommodityType = new IntegralCommodityType();
        PoToDTO.poToDto(oIntegralCommodityTypeDto,oIntegralCommodityType);
        if(oIntegralCommodityTypeMapper.deleteById(oIntegralCommodityType.getId()) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("删除积分商品分类失败");
        }
        return Result.ok("删除积分商品分类成功");
    }

    /**
     * 获取积分商品分类（用户）
     * @param oIntegralCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getIntegralCommodityTypeUser(IntegralCommodityTypeDto oIntegralCommodityTypeDto) throws Exception {
        List<IntegralCommodityType> oIntegralCommodityTypeList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderBy("no asc");
        //是否分页
        if(oIntegralCommodityTypeDto.getPage() != null && oIntegralCommodityTypeDto.getLimit() != null){
            Page<IntegralCommodityType> IntegralCommodityTypePage = oIntegralCommodityTypeMapper.paginateWithRelations(oIntegralCommodityTypeDto.getPage(),oIntegralCommodityTypeDto.getLimit(),queryWrapper);
            oIntegralCommodityTypeList = IntegralCommodityTypePage.getRecords();
            total = IntegralCommodityTypePage.getTotalRow();
        }else{
            oIntegralCommodityTypeList = oIntegralCommodityTypeMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oIntegralCommodityTypeList.size();
        }
        //PoToDto
        List<IntegralCommodityTypeDto> oIntegralCommodityTypeDtoList = (List<IntegralCommodityTypeDto>) PoToDTO.poToDtoList(oIntegralCommodityTypeList,new IntegralCommodityTypeDto());
        return Result.ok(oIntegralCommodityTypeDtoList,total);
    }

}
