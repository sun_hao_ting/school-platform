package com.project.school_flate.serviceimpl.delivery;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.delivery.DeliveryEvaluateDto;
import com.project.school_flate.entity.delivery.DeliveryEvaluate;
import com.project.school_flate.entity.table.delivery.DeliveryEvaluateTable;
import com.project.school_flate.mapper.delivery.DeliveryEvaluateMapper;
import com.project.school_flate.service.delivery.DeliveryEvaluateService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class DeliveryEvaluateServiceImpl extends ServiceImpl<DeliveryEvaluateMapper, DeliveryEvaluate> implements DeliveryEvaluateService {

    @Autowired
    private DeliveryEvaluateMapper oDeliveryEvaluateMapper;

    /**
     * 获取配送员评价
     * @param oDeliveryEvaluateDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getDeliveryEvaluate(DeliveryEvaluateDto oDeliveryEvaluateDto) throws Exception {
        List<DeliveryEvaluate> oDeliveryEvaluateList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入配送员ID
        if(StringUtils.isNotBlank(oDeliveryEvaluateDto.getDeliveryId())){
            queryWrapper.where(DeliveryEvaluateTable.DELIVERY_EVALUATE.DELIVERY_ID.eq(oDeliveryEvaluateDto.getDeliveryId()));
        }
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oDeliveryEvaluateDto.getPage() != null && oDeliveryEvaluateDto.getLimit() != null){
            Page<DeliveryEvaluate> DeliveryEvaluatePage = oDeliveryEvaluateMapper.paginateWithRelations(oDeliveryEvaluateDto.getPage(),oDeliveryEvaluateDto.getLimit(),queryWrapper);
            oDeliveryEvaluateList = DeliveryEvaluatePage.getRecords();
            total = DeliveryEvaluatePage.getTotalRow();
        }else{
            oDeliveryEvaluateList = oDeliveryEvaluateMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oDeliveryEvaluateList.size();
        }
        //PoToDto
        List<DeliveryEvaluateDto> oDeliveryEvaluateDtoList = (List<DeliveryEvaluateDto>) PoToDTO.poToDtoList(oDeliveryEvaluateList,new DeliveryEvaluateDto());
        return Result.ok(oDeliveryEvaluateDtoList,total);
    }

    /**
     * 添加配送员评价
     * @param oDeliveryEvaluateDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addDeliveryEvaluate(DeliveryEvaluateDto oDeliveryEvaluateDto) throws Exception {
        DeliveryEvaluate oDeliveryEvaluate = new DeliveryEvaluate();
        PoToDTO.poToDto(oDeliveryEvaluateDto,oDeliveryEvaluate);
        if(oDeliveryEvaluateMapper.insert(oDeliveryEvaluate) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加配送员评价失败");
        }
        return Result.ok("添加配送员评价成功");
    }

    /**
     * 修改配送员评价
     * @param oDeliveryEvaluateDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateDeliveryEvaluate(DeliveryEvaluateDto oDeliveryEvaluateDto) throws Exception {
        DeliveryEvaluate oDeliveryEvaluate = new DeliveryEvaluate();
        PoToDTO.poToDto(oDeliveryEvaluateDto,oDeliveryEvaluate);
        if(oDeliveryEvaluateMapper.update(oDeliveryEvaluate) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改配送员评价失败");
        }
        return Result.ok("修改配送员评价成功");
    }

    /**
     * 删除配送员评价
     * @param oDeliveryEvaluateDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result deleteDeliveryEvaluate(DeliveryEvaluateDto oDeliveryEvaluateDto) throws Exception {
        DeliveryEvaluate oDeliveryEvaluate = new DeliveryEvaluate();
        PoToDTO.poToDto(oDeliveryEvaluateDto,oDeliveryEvaluate);
        if(oDeliveryEvaluateMapper.deleteById(oDeliveryEvaluate.getId()) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("删除配送员评价失败");
        }
        return Result.ok("删除配送员评价成功");
    }
    
}
