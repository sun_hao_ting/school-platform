package com.project.school_flate.serviceimpl.delivery;

import cn.dev33.satoken.stp.StpUtil;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.update.UpdateChain;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.project.school_flate.dto.delivery.DeliveryExamineDto;
import com.project.school_flate.entity.com.ComAdmin;
import com.project.school_flate.entity.delivery.DeliveryExamine;
import com.project.school_flate.entity.delivery.DeliveryInfo;
import com.project.school_flate.entity.shop.ShopInfo;
import com.project.school_flate.entity.table.delivery.DeliveryExamineTable;
import com.project.school_flate.mapper.delivery.DeliveryExamineMapper;
import com.project.school_flate.mapper.delivery.DeliveryInfoMapper;
import com.project.school_flate.service.delivery.DeliveryExamineService;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *  服务层实现。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Service
public class DeliveryExamineServiceImpl extends ServiceImpl<DeliveryExamineMapper, DeliveryExamine> implements DeliveryExamineService {

    @Autowired
    private DeliveryExamineMapper oDeliveryExamineMapper;

    @Autowired
    private DeliveryInfoMapper oDeliveryInfoMapper;

    /**
     * 获取配送员审核
     * @param oDeliveryExamineDto
     * @return
     * @throws Exception
     */
    @Override
    public Result getDeliveryExamine(DeliveryExamineDto oDeliveryExamineDto) throws Exception {
        List<DeliveryExamine> oDeliveryExamineList = new ArrayList<>();
        long total = 0;
        QueryWrapper queryWrapper = new QueryWrapper();
        //判断是否传入配送员ID
        if(StringUtils.isNotBlank(oDeliveryExamineDto.getDeliveryId())){
            queryWrapper.where(DeliveryExamineTable.DELIVERY_EXAMINE.DELIVERY_ID.eq(oDeliveryExamineDto.getDeliveryId()));
        }
        //判断是否传入配送员姓名
        if(StringUtils.isNotBlank(oDeliveryExamineDto.getFullName())){
            queryWrapper.where(DeliveryExamineTable.DELIVERY_EXAMINE.FULL_NAME.like(oDeliveryExamineDto.getFullName()));
        }
        //判断是否传入配送员手机号
        if(StringUtils.isNotBlank(oDeliveryExamineDto.getPhone())){
            queryWrapper.where(DeliveryExamineTable.DELIVERY_EXAMINE.PHONE.like(oDeliveryExamineDto.getPhone()));
        }
        //判断是否传入配送员身份证号
        if(StringUtils.isNotBlank(oDeliveryExamineDto.getIdCard())){
            queryWrapper.where(DeliveryExamineTable.DELIVERY_EXAMINE.ID_CARD.like(oDeliveryExamineDto.getIdCard()));
        }
        //判断是否传入配送员校区ID
        if(StringUtils.isNotBlank(oDeliveryExamineDto.getSchoolId())){
            queryWrapper.where(DeliveryExamineTable.DELIVERY_EXAMINE.SCHOOL_ID.eq(oDeliveryExamineDto.getSchoolId()));
        }
        queryWrapper.orderBy("create_time desc");
        //是否分页
        if(oDeliveryExamineDto.getPage() != null && oDeliveryExamineDto.getLimit() != null){
            Page<DeliveryExamine> DeliveryExaminePage = oDeliveryExamineMapper.paginateWithRelations(oDeliveryExamineDto.getPage(),oDeliveryExamineDto.getLimit(),queryWrapper);
            oDeliveryExamineList = DeliveryExaminePage.getRecords();
            total = DeliveryExaminePage.getTotalRow();
        }else{
            oDeliveryExamineList = oDeliveryExamineMapper.selectListWithRelationsByQuery(queryWrapper);
            total = oDeliveryExamineList.size();
        }
        //PoToDto
        List<DeliveryExamineDto> oDeliveryExamineDtoList = (List<DeliveryExamineDto>) PoToDTO.poToDtoList(oDeliveryExamineList,new DeliveryExamineDto());
        return Result.ok(oDeliveryExamineDtoList,total);
    }

    /**
     * 添加配送员审核
     * @param oDeliveryExamineDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result addDeliveryExamine(DeliveryExamineDto oDeliveryExamineDto) throws Exception {
        DeliveryExamine oDeliveryExamine = new DeliveryExamine();
        PoToDTO.poToDto(oDeliveryExamineDto,oDeliveryExamine);
        if(oDeliveryExamineMapper.insert(oDeliveryExamine) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("添加配送员审核失败");
        }
        //修改配送员审核状态
        UpdateChain.of(DeliveryInfo.class)
                .set(DeliveryInfo::getExamineState, 2)
                .where(DeliveryInfo::getId).eq(oDeliveryExamine.getDeliveryId())
                .update();
        return Result.ok("添加配送员审核成功");
    }

    /**
     * 修改配送员审核
     * @param oDeliveryExamineDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result updateDeliveryExamine(DeliveryExamineDto oDeliveryExamineDto) throws Exception {
        DeliveryExamine oDeliveryExamine = new DeliveryExamine();
        PoToDTO.poToDto(oDeliveryExamineDto,oDeliveryExamine);
        oDeliveryExamine.setAuditTime(new Date());
        //获取登陆管理员
        ComAdmin oComAdmin = new ComAdmin();
        if(StpUtil.isLogin()){
            oComAdmin = (ComAdmin) StpUtil.getSession().get("comAdmin");
            if(oComAdmin == null || StringUtils.isBlank(oComAdmin.getId())){
                return Result.fail("请先重新登录");
            }
        }else{
            return Result.fail("请先重新登录");
        }
        oDeliveryExamine.setAdminId(oComAdmin.getId());
        if(oDeliveryExamineMapper.update(oDeliveryExamine) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("修改配送员审核失败");
        }
//        //修改配送员审核状态
//        DeliveryInfo oDeliveryInfo = new DeliveryInfo();
//        oDeliveryInfo.setId(oDeliveryExamine.getDeliveryId());
//        oDeliveryInfo.setExamineState(oDeliveryExamine.getIsPass());
//        //判断是否通过
//        if(oDeliveryExamine.getIsPass() == 1){
//            //修改配送员信息
//            oDeliveryInfo.setFullName(oDeliveryExamine.getFullName());
//            oDeliveryInfo.setPhone(oDeliveryExamine.getPhone());
//            oDeliveryInfo.setSex(oDeliveryExamine.getSex());
//            oDeliveryInfo.setIdFront(oDeliveryExamine.getIdFront());
//            oDeliveryInfo.setIdCard(oDeliveryExamine.getIdCard());
//            oDeliveryInfo.setIdOpposite(oDeliveryExamine.getIdOpposite());
//            oDeliveryInfo.setIdStudent(oDeliveryExamine.getIdStudent());
//            oDeliveryInfo.setSchoolId(oDeliveryExamine.getSchoolId());
//            oDeliveryInfo.setWorkType(oDeliveryExamine.getWorkType());
//        }
//        if(oDeliveryInfoMapper.update(oDeliveryInfo) == 0){
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return Result.fail("修改店铺审核失败");
//        }
        return Result.ok("修改配送员审核成功");
    }

    /**
     * 删除配送员审核
     * @param oDeliveryExamineDto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Result deleteDeliveryExamine(DeliveryExamineDto oDeliveryExamineDto) throws Exception {
        DeliveryExamine oDeliveryExamine = new DeliveryExamine();
        PoToDTO.poToDto(oDeliveryExamineDto,oDeliveryExamine);
        if(oDeliveryExamineMapper.deleteById(oDeliveryExamine.getId()) == 0){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.fail("删除配送员审核失败");
        }
        return Result.ok("删除配送员审核成功");
    }

}
