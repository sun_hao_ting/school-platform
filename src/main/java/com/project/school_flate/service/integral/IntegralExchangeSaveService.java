package com.project.school_flate.service.integral;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.integral.IntegralExchangeSaveDto;
import com.project.school_flate.entity.integral.IntegralExchangeSave;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface IntegralExchangeSaveService extends IService<IntegralExchangeSave> {

    /**
     * 获取积分兑换
     * @param oIntegralExchangeSaveDto
     * @return
     * @throws Exception
     */
    Result getIntegralExchangeSave(IntegralExchangeSaveDto oIntegralExchangeSaveDto) throws Exception;

    /**
     * 添加积分兑换
     * @param oIntegralExchangeSaveDto
     * @return
     * @throws Exception
     */
    Result addIntegralExchangeSave(IntegralExchangeSaveDto oIntegralExchangeSaveDto) throws Exception;

    /**
     * 修改积分兑换
     * @param oIntegralExchangeSaveDto
     * @return
     * @throws Exception
     */
    Result updateIntegralExchangeSave(IntegralExchangeSaveDto oIntegralExchangeSaveDto) throws Exception;

    /**
     * 删除积分兑换
     * @param oIntegralExchangeSaveDto
     * @return
     * @throws Exception
     */
    Result deleteIntegralExchangeSave(IntegralExchangeSaveDto oIntegralExchangeSaveDto) throws Exception;

    /**
     * 获取积分兑换（用户）
     * @param oIntegralExchangeSaveDto
     * @return
     * @throws Exception
     */
    Result getIntegralExchangeSaveUser(IntegralExchangeSaveDto oIntegralExchangeSaveDto) throws Exception;

}
