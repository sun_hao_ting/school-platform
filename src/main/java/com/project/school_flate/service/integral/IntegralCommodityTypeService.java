package com.project.school_flate.service.integral;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.integral.IntegralCommodityTypeDto;
import com.project.school_flate.entity.integral.IntegralCommodityType;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface IntegralCommodityTypeService extends IService<IntegralCommodityType> {

    /**
     * 获取积分商品分类
     * @param oIntegralCommodityTypeDto
     * @return
     * @throws Exception
     */
    Result getIntegralCommodityType(IntegralCommodityTypeDto oIntegralCommodityTypeDto) throws Exception;

    /**
     * 添加积分商品分类
     * @param oIntegralCommodityTypeDto
     * @return
     * @throws Exception
     */
    Result addIntegralCommodityType(IntegralCommodityTypeDto oIntegralCommodityTypeDto) throws Exception;

    /**
     * 修改积分商品分类
     * @param oIntegralCommodityTypeDto
     * @return
     * @throws Exception
     */
    Result updateIntegralCommodityType(IntegralCommodityTypeDto oIntegralCommodityTypeDto) throws Exception;

    /**
     * 删除积分商品分类
     * @param oIntegralCommodityTypeDto
     * @return
     * @throws Exception
     */
    Result deleteIntegralCommodityType(IntegralCommodityTypeDto oIntegralCommodityTypeDto) throws Exception;

    /**
     * 获取积分商品分类（用户）
     * @param oIntegralCommodityTypeDto
     * @return
     * @throws Exception
     */
    Result getIntegralCommodityTypeUser(IntegralCommodityTypeDto oIntegralCommodityTypeDto) throws Exception;

}
