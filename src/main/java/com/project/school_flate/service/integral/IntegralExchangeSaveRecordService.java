package com.project.school_flate.service.integral;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.integral.IntegralExchangeSaveRecordDto;
import com.project.school_flate.entity.integral.IntegralExchangeSaveRecord;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface IntegralExchangeSaveRecordService extends IService<IntegralExchangeSaveRecord> {

    /**
     * 获取积分兑换储蓄记录
     * @param oIntegralExchangeSaveRecordDto
     * @return
     * @throws Exception
     */
    Result getIntegralExchangeSaveRecord(IntegralExchangeSaveRecordDto oIntegralExchangeSaveRecordDto) throws Exception;

    /**
     * 添加积分兑换储蓄记录
     * @param oIntegralExchangeSaveRecordDto
     * @return
     * @throws Exception
     */
    Result addIntegralExchangeSaveRecord(IntegralExchangeSaveRecordDto oIntegralExchangeSaveRecordDto) throws Exception;

    /**
     * 修改积分兑换储蓄记录
     * @param oIntegralExchangeSaveRecordDto
     * @return
     * @throws Exception
     */
    Result updateIntegralExchangeSaveRecord(IntegralExchangeSaveRecordDto oIntegralExchangeSaveRecordDto) throws Exception;

    /**
     * 删除积分兑换储蓄记录
     * @param oIntegralExchangeSaveRecordDto
     * @return
     * @throws Exception
     */
    Result deleteIntegralExchangeSaveRecord(IntegralExchangeSaveRecordDto oIntegralExchangeSaveRecordDto) throws Exception;


}
