package com.project.school_flate.service.integral;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.integral.IntegralCommodityDto;
import com.project.school_flate.entity.integral.IntegralCommodity;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface IntegralCommodityService extends IService<IntegralCommodity> {

    /**
     * 获取积分商品
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    Result getIntegralCommodity(IntegralCommodityDto oIntegralCommodityDto) throws Exception;

    /**
     * 添加积分商品
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    Result addIntegralCommodity(IntegralCommodityDto oIntegralCommodityDto) throws Exception;

    /**
     * 修改积分商品
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    Result updateIntegralCommodity(IntegralCommodityDto oIntegralCommodityDto) throws Exception;

    /**
     * 冻结积分商品
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    Result freezeIntegralCommodity(IntegralCommodityDto oIntegralCommodityDto) throws Exception;

    /**
     * 获取积分商品（后台）
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    Result getIntegralCommodityBack(IntegralCommodityDto oIntegralCommodityDto) throws Exception;

    /**
     * 解冻积分商品
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    Result thawIntegralCommodity(IntegralCommodityDto oIntegralCommodityDto) throws Exception;

    /**
     * 获取积分商品（用户）
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    Result getIntegralCommodityUser(IntegralCommodityDto oIntegralCommodityDto) throws Exception;

}
