package com.project.school_flate.service.flow;

public interface FlowTaskService {

     /**
      * 执行任务，把金额发向各个角色
      * 定时任务，间隔:每天2时执行一次
      *
      */
     void executeFlowTask() throws Exception;
}
