package com.project.school_flate.service.flow;

import com.project.school_flate.dto.flow.FlowBusinessDto;
import com.project.school_flate.dto.shop.ShopSaveUserDto;
import com.project.school_flate.entity.flow.FlowBusiness;
import com.project.school_flate.util.Result.Result;

public interface FlowBusinessService {

    /**
     * 获取业务流水
     * @param oFlowBusinessDto
     * @return
     * @throws Exception
     */
    Result getFlowBusiness(FlowBusinessDto oFlowBusinessDto) throws Exception;

    /**
     * 获取用户在店铺储蓄消费
     * @param oFlowBusinessDto
     * @return
     * @throws Exception
     */
    Result getFlowMoneyConsumptionToShopSaveUser(FlowBusinessDto oFlowBusinessDto) throws Exception;

}
