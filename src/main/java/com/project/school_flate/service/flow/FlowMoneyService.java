package com.project.school_flate.service.flow;

import com.project.school_flate.dto.flow.FlowMoneyDto;
import com.project.school_flate.dto.shop.ShopSaveUserDto;
import com.project.school_flate.entity.flow.FlowMoney;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.RequestBody;

public interface FlowMoneyService {

    /**
     * 获取平台流水
     * @param oFlowMoneyDto
     * @return
     * @throws Exception
     */
    Result getFlowMoney(FlowMoneyDto oFlowMoneyDto) throws Exception;

}
