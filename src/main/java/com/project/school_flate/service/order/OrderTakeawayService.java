package com.project.school_flate.service.order;

import com.alibaba.fastjson2.JSONObject;
import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.order.OrderTakeawayDto;
import com.project.school_flate.entity.order.OrderTakeaway;
import com.project.school_flate.util.Result.Result;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface OrderTakeawayService extends IService<OrderTakeaway> {

    /**
     * 获取外卖订单
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result getOrderTakeaway(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

    /**
     * 添加外卖订单
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result addOrderTakeaway(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

    /**
     * 修改外卖订单
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result updateOrderTakeaway(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

    /**
     * 删除外卖订单
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result deleteOrderTakeaway(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

    /**
     * 店铺出餐
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result diningOrderTakeawayToShop(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

    /**
     * 店铺自动出餐
     * @param oOrderTakeawayId
     * @return
     * @throws Exception
     */
    void automaticDiningOrderTakeawayToShop(String oOrderTakeawayId) throws Exception;

    /**
     * 获取外卖订单大厅（配送员）
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result getOrderTakeawayHallToDelivery(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

    /**
     * 配送员抢单
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result robOrderTakeawayToDelivery(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

    /**
     * 配送员自动抢单
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result automaticRobOrderTakeawayToDelivery(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

    /**
     * 配送员取餐
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result takeOrderTakeawayToDelivery(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

    /**
     * 生成中转订单二维码
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result transferOrderTakeawayCode(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

    /**
     * 配送员中转订单
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result transferOrderTakeawayToDelivery(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

    /**
     * 配送员确认完成
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result okOrderTakeawayToDelivery(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

    /**
     * 获取外卖订单（配送员）
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result getOrderTakeawayToDelivery(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

    /**
     * 获取外卖订单（商家）
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result getOrderTakeawayToShop(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

    /**
     * 微信支付外卖订单成功（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    Map<String, String> wxPayOrderTakeawayCallback(JSONObject jsonObject) throws Exception;

    /**
     * 储蓄支付外卖订单成功（回调）
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result savePayOrderTakeawayCallback(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

    /**
     * 客户确认完成
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result okOrderTakeawayToUser(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

    /**
     * 客户自动确认完成
     * @param oOrderTakeawayId
     * @return
     * @throws Exception
     */
    void automaticOkOrderTakeawayToUser(String oOrderTakeawayId) throws Exception;

    /**
     * 自动删除外卖订单
     * @param oOrderTakeawayId
     * @return
     * @throws Exception
     */
    void automaticDeleteOrderTakeaway(String oOrderTakeawayId) throws Exception;

    /**
     * 获取外卖订单（用户）
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    Result getOrderTakeawayToUser(OrderTakeawayDto oOrderTakeawayDto) throws Exception;

}
