package com.project.school_flate.service.order;

import com.alibaba.fastjson2.JSONObject;
import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.order.OrderTakeawayCancellationDto;
import com.project.school_flate.dto.order.OrderTakeawayDto;
import com.project.school_flate.entity.order.OrderTakeawayCancellation;
import com.project.school_flate.util.Result.Result;

import java.util.Map;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface OrderTakeawayCancellationService extends IService<OrderTakeawayCancellation> {

    /**
     * 获取订单取消
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    Result getOrderTakeawayCancellation(OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception;

    /**
     * 添加订单取消
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    Result addOrderTakeawayCancellation(OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception;

    /**
     * 修改订单取消
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    Result updateOrderTakeawayCancellation(OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception;

    /**
     * 删除订单取消
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    Result deleteOrderTakeawayCancellation(OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception;

    /**
     * 店铺取消订单
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    Result shopOrderTakeawayCancellation(OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception;

    /**
     * 取消微信支付订单（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    Map<String, String> cancellationOrderTakeawayCallback(JSONObject jsonObject) throws Exception;

    /**
     * 用户取消订单
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    Result userApplyOrderTakeawayCancellation(OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception;

    /**
     * 平台确认取消订单
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    Result comOkOrderTakeawayCancellation(OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception;

    /**
     * 平台驳回取消订单
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    Result comRejectOrderTakeawayCancellation(OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception;

    /**
     * 获取订单取消理由模板
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    Result getOrderTakeawayCancellationTemplate(OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception;

}
