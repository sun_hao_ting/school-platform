package com.project.school_flate.service.order;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.order.OrderTakeawayCommodityDto;
import com.project.school_flate.dto.order.OrderTakeawayDto;
import com.project.school_flate.entity.order.OrderTakeawayCommodity;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface OrderTakeawayCommodityService extends IService<OrderTakeawayCommodity> {

    /**
     * 获取外卖订单商品
     * @param oOrderTakeawayCommodityDto
     * @return
     * @throws Exception
     */
    Result getOrderTakeawayCommodity(OrderTakeawayCommodityDto oOrderTakeawayCommodityDto) throws Exception;

    /**
     * 添加外卖订单商品
     * @param oOrderTakeawayCommodityDto
     * @return
     * @throws Exception
     */
    Result addOrderTakeawayCommodity(OrderTakeawayCommodityDto oOrderTakeawayCommodityDto) throws Exception;

    /**
     * 修改外卖订单商品
     * @param oOrderTakeawayCommodityDto
     * @return
     * @throws Exception
     */
    Result updateOrderTakeawayCommodity(OrderTakeawayCommodityDto oOrderTakeawayCommodityDto) throws Exception;

    /**
     * 删除外卖订单商品
     * @param oOrderTakeawayCommodityDto
     * @return
     * @throws Exception
     */
    Result deleteOrderTakeawayCommodity(OrderTakeawayCommodityDto oOrderTakeawayCommodityDto) throws Exception;

    /**
     * 核算购物车外卖订单价格（客户）
     * @param oOrderTakeawayCommodityDto
     * @return
     * @throws Exception
     */
    Result countCarOrderIntegral(OrderTakeawayCommodityDto oOrderTakeawayCommodityDto) throws Exception;

}
