package com.project.school_flate.service.order;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.order.OrderIntegralCommodityDto;
import com.project.school_flate.entity.order.OrderIntegralCommodity;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface OrderIntegralCommodityService extends IService<OrderIntegralCommodity> {

    /**
     * 获取积分订单商品
     * @param oOrderIntegralCommodityDto
     * @return
     * @throws Exception
     */
    Result getOrderIntegralCommodity(OrderIntegralCommodityDto oOrderIntegralCommodityDto) throws Exception;

    /**
     * 添加积分订单商品
     * @param oOrderIntegralCommodityDto
     * @return
     * @throws Exception
     */
    Result addOrderIntegralCommodity(OrderIntegralCommodityDto oOrderIntegralCommodityDto) throws Exception;

    /**
     * 修改积分订单商品
     * @param oOrderIntegralCommodityDto
     * @return
     * @throws Exception
     */
    Result updateOrderIntegralCommodity(OrderIntegralCommodityDto oOrderIntegralCommodityDto) throws Exception;

    /**
     * 删除积分订单商品
     * @param oOrderIntegralCommodityDto
     * @return
     * @throws Exception
     */
    Result deleteOrderIntegralCommodity(OrderIntegralCommodityDto oOrderIntegralCommodityDto) throws Exception;

}
