package com.project.school_flate.service.order;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.order.OrderDeliveryDto;
import com.project.school_flate.entity.order.OrderDelivery;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface OrderDeliveryService extends IService<OrderDelivery> {

    /**
     * 获取外卖订单配送
     * @param oOrderDeliveryDto
     * @return
     * @throws Exception
     */
    Result getOrderDelivery(OrderDeliveryDto oOrderDeliveryDto) throws Exception;

    /**
     * 添加外卖订单配送
     * @param oOrderDeliveryDto
     * @return
     * @throws Exception
     */
    Result addOrderDelivery(OrderDeliveryDto oOrderDeliveryDto) throws Exception;

    /**
     * 修改外卖订单配送
     * @param oOrderDeliveryDto
     * @return
     * @throws Exception
     */
    Result updateOrderDelivery(OrderDeliveryDto oOrderDeliveryDto) throws Exception;

    /**
     * 删除外卖订单配送
     * @param oOrderDeliveryDto
     * @return
     * @throws Exception
     */
    Result deleteOrderDelivery(OrderDeliveryDto oOrderDeliveryDto) throws Exception;

}
