package com.project.school_flate.service.login;


import com.project.school_flate.dto.login.Login;
import com.project.school_flate.util.Result.Result;

/**
 * @Author sunht
 * @Date 2023/10/2 18:03
 * @Version v1.0.0
 * @Message
 **/
public interface LoginService {

    /**
     * 系统用户登录接口
     * @param login
     * @return
     * @throws Exception
     */
    Result systemLogin(Login login) throws Exception;
    /**
     * 客户微信登录
     * @param login
     * @return
     * @throws Exception
     */
    Result wxClientLogin(Login login) throws Exception;

    /**
     * 登录注销
     * @param
     * @return
     * @throws Exception
     */
    Result loginOut() throws Exception;

}
