package com.project.school_flate.service.user;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.user.UserAddressDto;
import com.project.school_flate.entity.user.UserAddress;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface UserAddressService extends IService<UserAddress> {

    /**
     * 获取配送地址
     * @param oUserAddressDto
     * @return
     * @throws Exception
     */
    Result getUserAddress(UserAddressDto oUserAddressDto) throws Exception;

    /**
     * 添加配送地址
     * @param oUserAddressDto
     * @return
     * @throws Exception
     */
    Result addUserAddress(UserAddressDto oUserAddressDto) throws Exception;

    /**
     * 修改配送地址
     * @param oUserAddressDto
     * @return
     * @throws Exception
     */
    Result updateUserAddress(UserAddressDto oUserAddressDto) throws Exception;

    /**
     * 删除配送地址
     * @param oUserAddressDto
     * @return
     * @throws Exception
     */
    Result deleteUserAddress(UserAddressDto oUserAddressDto) throws Exception;

    /**
     * 获取用户最近一次使用的配送地址
     * @param oUserAddressDto
     * @return
     * @throws Exception
     */
    Result getUserAddressRecently(UserAddressDto oUserAddressDto) throws Exception;

}
