package com.project.school_flate.service.user;

import com.alibaba.fastjson2.JSONObject;
import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.user.UserInfoDto;
import com.project.school_flate.entity.user.UserInfo;
import com.project.school_flate.util.Result.Result;

import java.util.Map;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface UserInfoService extends IService<UserInfo> {

    /**
     * 获取用户
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result getUserInfo(UserInfoDto oUserInfoDto) throws Exception;

    /**
     * 添加用户
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result addUserInfo(UserInfoDto oUserInfoDto) throws Exception;

    /**
     * 修改用户
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result updateUserInfo(UserInfoDto oUserInfoDto) throws Exception;

    /**
     * 冻结用户
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result freezeUserInfo(UserInfoDto oUserInfoDto) throws Exception;

    /**
     * 获取用户（后台）
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result getUserInfoBack(UserInfoDto oUserInfoDto) throws Exception;

    /**
     * 解冻用户
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result thawUserInfo(UserInfoDto oUserInfoDto) throws Exception;

    /**
     * 用户填写推广
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result promotionUserInfo(UserInfoDto oUserInfoDto) throws Exception;

    /**
     * 用户公众号登录
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result loginUserInfoOfficial(UserInfoDto oUserInfoDto) throws Exception;

    /**
     * 用户绑定手机号
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result bindingUserInfoPhone(UserInfoDto oUserInfoDto) throws Exception;

    /**
     * 获取用户推广的用户
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result getPromotionUserInfo(UserInfoDto oUserInfoDto) throws Exception;

    /**
     * 获取用户推广的储蓄用户
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result getSavePromotionUserInfo(UserInfoDto oUserInfoDto) throws Exception;

    /**
     * 获取成为专业分销商金额价格
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result getInfoSpecialityDistributionMoney(UserInfoDto oUserInfoDto) throws Exception;

    /**
     * 用户成为专业分销商
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result becomeUserInfoSpecialityDistribution(UserInfoDto oUserInfoDto) throws Exception;

    /**
     * 用户成为专业分销商（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    Map<String, String> becomeUserInfoSpecialityDistributionCallback(JSONObject jsonObject) throws Exception;

    /**
     * 获取用户分销商等级规则
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result getUserInfoDistributionGrade(UserInfoDto oUserInfoDto) throws Exception;

}
