package com.project.school_flate.service.shop;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.shop.ShopCommodityEvaluateLikeDto;
import com.project.school_flate.dto.shop.ShopCommodityDto;
import com.project.school_flate.entity.shop.ShopCommodityEvaluateLike;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ShopCommodityEvaluateLikeService extends IService<ShopCommodityEvaluateLike> {

    /**
     * 获取商品评论点赞
     * @param oShopCommodityEvaluateLikeDto
     * @return
     * @throws Exception
     */
    Result getShopCommodityEvaluateLike(ShopCommodityEvaluateLikeDto oShopCommodityEvaluateLikeDto) throws Exception;

    /**
     * 添加商品评论点赞
     * @param oShopCommodityEvaluateLikeDto
     * @return
     * @throws Exception
     */
    Result addShopCommodityEvaluateLike(ShopCommodityEvaluateLikeDto oShopCommodityEvaluateLikeDto) throws Exception;

    /**
     * 修改商品评论点赞
     * @param oShopCommodityEvaluateLikeDto
     * @return
     * @throws Exception
     */
    Result updateShopCommodityEvaluateLike(ShopCommodityEvaluateLikeDto oShopCommodityEvaluateLikeDto) throws Exception;

    /**
     * 删除商品评论点赞
     * @param oShopCommodityEvaluateLikeDto
     * @return
     * @throws Exception
     */
    Result deleteShopCommodityEvaluateLike(ShopCommodityEvaluateLikeDto oShopCommodityEvaluateLikeDto) throws Exception;

    /**
     * 获取商品评论点赞（用户）
     * @param oShopCommodityEvaluateLikeDto
     * @return
     * @throws Exception
     */
    Result getShopCommodityEvaluateLikeUser(ShopCommodityEvaluateLikeDto oShopCommodityEvaluateLikeDto) throws Exception;

}
