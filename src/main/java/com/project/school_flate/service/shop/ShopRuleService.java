package com.project.school_flate.service.shop;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.shop.ShopRuleDto;
import com.project.school_flate.entity.shop.ShopRule;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ShopRuleService extends IService<ShopRule> {

    /**
     * 获取店铺规则
     * @param oShopRuleDto
     * @return
     * @throws Exception
     */
    Result getShopRule(ShopRuleDto oShopRuleDto) throws Exception;

    /**
     * 添加店铺规则
     * @param oShopRuleDto
     * @return
     * @throws Exception
     */
    Result addShopRule(ShopRuleDto oShopRuleDto) throws Exception;

    /**
     * 修改店铺规则
     * @param oShopRuleDto
     * @return
     * @throws Exception
     */
    Result updateShopRule(ShopRuleDto oShopRuleDto) throws Exception;

    /**
     * 删除店铺规则
     * @param oShopRuleDto
     * @return
     * @throws Exception
     */
    Result deleteShopRule(ShopRuleDto oShopRuleDto) throws Exception;

    /**
     * 批量添加店铺规则
     * @param oShopRuleDto
     * @return
     * @throws Exception
     */
    Result batchAddShopRule(ShopRuleDto oShopRuleDto) throws Exception;

    /**
     * 批量添加储蓄规则
     * @param oShopRuleDto
     * @return
     * @throws Exception
     */
    Result batchAddShopRuleToSave(ShopRuleDto oShopRuleDto) throws Exception;

}
