package com.project.school_flate.service.shop;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.shop.ShopCommoditySpecsDto;
import com.project.school_flate.entity.shop.ShopCommoditySpecs;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ShopCommoditySpecsService extends IService<ShopCommoditySpecs> {

    /**
     * 获取商品规格
     * @param oShopCommoditySpecsDto
     * @return
     * @throws Exception
     */
    Result getShopCommoditySpecs(ShopCommoditySpecsDto oShopCommoditySpecsDto) throws Exception;

    /**
     * 添加商品规格
     * @param oShopCommoditySpecsDto
     * @return
     * @throws Exception
     */
    Result addShopCommoditySpecs(ShopCommoditySpecsDto oShopCommoditySpecsDto) throws Exception;

    /**
     * 修改商品规格
     * @param oShopCommoditySpecsDto
     * @return
     * @throws Exception
     */
    Result updateShopCommoditySpecs(ShopCommoditySpecsDto oShopCommoditySpecsDto) throws Exception;

    /**
     * 删除商品规格
     * @param oShopCommoditySpecsDto
     * @return
     * @throws Exception
     */
    Result deleteShopCommoditySpecs(ShopCommoditySpecsDto oShopCommoditySpecsDto) throws Exception;

    /**
     * 获取商品规格（用户）
     * @param oShopCommoditySpecsDto
     * @return
     * @throws Exception
     */
    Result getShopCommoditySpecsUser(ShopCommoditySpecsDto oShopCommoditySpecsDto) throws Exception;

}
