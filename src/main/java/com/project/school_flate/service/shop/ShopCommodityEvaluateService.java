package com.project.school_flate.service.shop;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.shop.ShopCommodityEvaluateDto;
import com.project.school_flate.entity.shop.ShopCommodityEvaluate;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ShopCommodityEvaluateService extends IService<ShopCommodityEvaluate> {

    /**
     * 获取商品评价
     * @param oShopCommodityEvaluateDto
     * @return
     * @throws Exception
     */
    Result getShopCommodityEvaluate(ShopCommodityEvaluateDto oShopCommodityEvaluateDto) throws Exception;

    /**
     * 添加商品评价
     * @param oShopCommodityEvaluateDto
     * @return
     * @throws Exception
     */
    Result addShopCommodityEvaluate(ShopCommodityEvaluateDto oShopCommodityEvaluateDto) throws Exception;

    /**
     * 修改商品评价
     * @param oShopCommodityEvaluateDto
     * @return
     * @throws Exception
     */
    Result updateShopCommodityEvaluate(ShopCommodityEvaluateDto oShopCommodityEvaluateDto) throws Exception;

    /**
     * 删除商品评价
     * @param oShopCommodityEvaluateDto
     * @return
     * @throws Exception
     */
    Result deleteShopCommodityEvaluate(ShopCommodityEvaluateDto oShopCommodityEvaluateDto) throws Exception;

    /**
     * 获取商品评价（客户）
     * @param oShopCommodityEvaluateDto
     * @return
     * @throws Exception
     */
    Result getShopCommodityEvaluateUser(ShopCommodityEvaluateDto oShopCommodityEvaluateDto) throws Exception;

    /**
     * 用户自动商品评价
     * @param oOrderTakeawayId
     * @return
     * @throws Exception
     */
    void automaticShopCommodityEvaluate(String oOrderTakeawayId) throws Exception;

}
