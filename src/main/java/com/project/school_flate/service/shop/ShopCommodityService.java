package com.project.school_flate.service.shop;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.shop.ShopCommodityDto;
import com.project.school_flate.entity.shop.ShopCommodity;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ShopCommodityService extends IService<ShopCommodity> {

    /**
     * 获取商品
     * @param oShopCommodityDto
     * @return
     * @throws Exception
     */
    Result getShopCommodity(ShopCommodityDto oShopCommodityDto) throws Exception;

    /**
     * 添加商品
     * @param oShopCommodityDto
     * @return
     * @throws Exception
     */
    Result addShopCommodity(ShopCommodityDto oShopCommodityDto) throws Exception;

    /**
     * 修改商品
     * @param oShopCommodityDto
     * @return
     * @throws Exception
     */
    Result updateShopCommodity(ShopCommodityDto oShopCommodityDto) throws Exception;

    /**
     * 删除商品
     * @param oShopCommodityDto
     * @return
     * @throws Exception
     */
    Result deleteShopCommodity(ShopCommodityDto oShopCommodityDto) throws Exception;

    /**
     * 获取商品（客户）
     * @param oShopCommodityDto
     * @return
     * @throws Exception
     */
    Result getShopCommodityUser(ShopCommodityDto oShopCommodityDto) throws Exception;

    /**
     * 获取推荐商品（客户）
     * @param oShopCommodityDto
     * @return
     * @throws Exception
     */
    Result getRecommendShopCommodityUser(ShopCommodityDto oShopCommodityDto) throws Exception;

}
