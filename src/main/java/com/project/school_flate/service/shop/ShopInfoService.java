package com.project.school_flate.service.shop;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.shop.ShopInfoDto;
import com.project.school_flate.entity.shop.ShopInfo;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ShopInfoService extends IService<ShopInfo> {

    /**
     * 获取店铺
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result getShopInfo(ShopInfoDto oShopInfoDto) throws Exception;

    /**
     * 添加店铺
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result addShopInfo(ShopInfoDto oShopInfoDto) throws Exception;

    /**
     * 修改店铺
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result updateShopInfo(ShopInfoDto oShopInfoDto) throws Exception;

    /**
     * 冻结店铺
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result freezeShopInfo(ShopInfoDto oShopInfoDto) throws Exception;

    /**
     * 获取店铺（后台）
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result getShopInfoBack(ShopInfoDto oShopInfoDto) throws Exception;

    /**
     * 解冻店铺
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result thawShopInfo(ShopInfoDto oShopInfoDto) throws Exception;

    /**
     * 获取店铺（客户）
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result getShopInfoUser(ShopInfoDto oShopInfoDto) throws Exception;

    /**
     * 店铺申请储蓄
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result applyShopInfoSave(ShopInfoDto oShopInfoDto) throws Exception;

    /**
     * 审核申请储蓄
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result examineShopInfoSave(ShopInfoDto oShopInfoDto) throws Exception;

    /**
     * 获取用户是否是店铺新人
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result getShopInfoUserNew(ShopInfoDto oShopInfoDto) throws Exception;

    /**
     * 获取店铺配送费和打包费
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result getShopInfoDeliveryPacking(ShopInfoDto oShopInfoDto) throws Exception;

    /**
     * 获取推荐店铺（客户）
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result getRecommendShopInfoUser(ShopInfoDto oShopInfoDto) throws Exception;

}
