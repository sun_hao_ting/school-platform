package com.project.school_flate.service.shop;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.shop.ShopExamineDto;
import com.project.school_flate.entity.shop.ShopExamine;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ShopExamineService extends IService<ShopExamine> {

    /**
     * 获取店铺审核
     * @param oShopExamineDto
     * @return
     * @throws Exception
     */
    Result getShopExamine(ShopExamineDto oShopExamineDto) throws Exception;

    /**
     * 添加店铺审核
     * @param oShopExamineDto
     * @return
     * @throws Exception
     */
    Result addShopExamine(ShopExamineDto oShopExamineDto) throws Exception;

    /**
     * 修改店铺审核
     * @param oShopExamineDto
     * @return
     * @throws Exception
     */
    Result updateShopExamine(ShopExamineDto oShopExamineDto) throws Exception;

    /**
     * 删除店铺审核
     * @param oShopExamineDto
     * @return
     * @throws Exception
     */
    Result deleteShopExamine(ShopExamineDto oShopExamineDto) throws Exception;
    
}
