package com.project.school_flate.service.shop;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.shop.ShopCommoditySpecsTypeDto;
import com.project.school_flate.entity.shop.ShopCommoditySpecsType;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ShopCommoditySpecsTypeService extends IService<ShopCommoditySpecsType> {

    /**
     * 获取商品规格类型
     * @param oShopCommoditySpecsTypeDto
     * @return
     * @throws Exception
     */
    Result getShopCommoditySpecsType(ShopCommoditySpecsTypeDto oShopCommoditySpecsTypeDto) throws Exception;

    /**
     * 添加商品规格类型
     * @param oShopCommoditySpecsTypeDto
     * @return
     * @throws Exception
     */
    Result addShopCommoditySpecsType(ShopCommoditySpecsTypeDto oShopCommoditySpecsTypeDto) throws Exception;

    /**
     * 修改商品规格类型
     * @param oShopCommoditySpecsTypeDto
     * @return
     * @throws Exception
     */
    Result updateShopCommoditySpecsType(ShopCommoditySpecsTypeDto oShopCommoditySpecsTypeDto) throws Exception;

    /**
     * 删除商品规格类型
     * @param oShopCommoditySpecsTypeDto
     * @return
     * @throws Exception
     */
    Result deleteShopCommoditySpecsType(ShopCommoditySpecsTypeDto oShopCommoditySpecsTypeDto) throws Exception;

    /**
     * 获取商品规格类型（用户）
     * @param oShopCommoditySpecsTypeDto
     * @return
     * @throws Exception
     */
    Result getShopCommoditySpecsTypeUser(ShopCommoditySpecsTypeDto oShopCommoditySpecsTypeDto) throws Exception;

    /**
     * 批量添加商品规格类型
     * @param oShopCommoditySpecsTypeDto
     * @return
     * @throws Exception
     */
    Result batchAddShopCommoditySpecsType(ShopCommoditySpecsTypeDto oShopCommoditySpecsTypeDto) throws Exception;

}
