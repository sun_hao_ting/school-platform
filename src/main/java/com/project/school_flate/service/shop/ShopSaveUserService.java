package com.project.school_flate.service.shop;

import com.alibaba.fastjson2.JSONObject;
import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.shop.ShopSaveUserDto;
import com.project.school_flate.entity.shop.ShopSaveUser;
import com.project.school_flate.util.Result.Result;

import java.util.Map;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ShopSaveUserService extends IService<ShopSaveUser> {

    /**
     * 获取店铺用户储蓄
     * @param oShopSaveUserDto
     * @return
     * @throws Exception
     */
    Result getShopSaveUser(ShopSaveUserDto oShopSaveUserDto) throws Exception;

    /**
     * 添加店铺用户储蓄
     * @param oShopSaveUserDto
     * @return
     * @throws Exception
     */
    Result addShopSaveUser(ShopSaveUserDto oShopSaveUserDto) throws Exception;

    /**
     * 修改店铺用户储蓄
     * @param oShopSaveUserDto
     * @return
     * @throws Exception
     */
    Result updateShopSaveUser(ShopSaveUserDto oShopSaveUserDto) throws Exception;

    /**
     * 删除店铺用户储蓄
     * @param oShopSaveUserDto
     * @return
     * @throws Exception
     */
    Result deleteShopSaveUser(ShopSaveUserDto oShopSaveUserDto) throws Exception;

    /**
     * 客户充值店铺储蓄
     * @param oShopSaveUserDto
     * @return
     * @throws Exception
     */
    Result rechargeShopSaveUser(ShopSaveUserDto oShopSaveUserDto) throws Exception;

    /**
     * 客户充值店铺储蓄（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    Map<String, String> rechargeShopSaveUserCallback(JSONObject jsonObject) throws Exception;

    /**
     * 获取用户储蓄余额
     * @param oShopSaveUserDto
     * @return
     * @throws Exception
     */
    Result getShopSaveUserBalance(ShopSaveUserDto oShopSaveUserDto) throws Exception;

    /**
     * 获取用户储蓄店铺
     * @param oShopSaveUserDto
     * @return
     * @throws Exception
     */
    Result getShopSaveUserShop(ShopSaveUserDto oShopSaveUserDto) throws Exception;

    /**
     * 获取店铺用户储蓄总额余额
     * @param oShopSaveUserDto
     * @return
     * @throws Exception
     */
    Result getShopSaveUserTotal(ShopSaveUserDto oShopSaveUserDto) throws Exception;

}
