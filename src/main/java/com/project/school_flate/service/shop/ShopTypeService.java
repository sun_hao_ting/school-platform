package com.project.school_flate.service.shop;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.shop.ShopTypeDto;
import com.project.school_flate.entity.shop.ShopType;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ShopTypeService extends IService<ShopType> {

    /**
     * 获取店铺类型
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    Result getShopType(ShopTypeDto oShopTypeDto) throws Exception;

    /**
     * 添加店铺类型
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    Result addShopType(ShopTypeDto oShopTypeDto) throws Exception;

    /**
     * 修改店铺类型
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    Result updateShopType(ShopTypeDto oShopTypeDto) throws Exception;

    /**
     * 冻结店铺类型
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    Result freezeShopType(ShopTypeDto oShopTypeDto) throws Exception;

    /**
     * 获取店铺类型（后台）
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    Result getShopTypeBack(ShopTypeDto oShopTypeDto) throws Exception;

    /**
     * 解冻店铺类型
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    Result thawShopType(ShopTypeDto oShopTypeDto) throws Exception;

    /**
     * 获取店铺类型（用户）
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    Result getShopTypeUser(ShopTypeDto oShopTypeDto) throws Exception;

}
