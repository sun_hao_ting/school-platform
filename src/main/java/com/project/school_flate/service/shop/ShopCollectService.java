package com.project.school_flate.service.shop;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.shop.ShopCollectDto;
import com.project.school_flate.entity.shop.ShopCollect;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ShopCollectService extends IService<ShopCollect> {

    /**
     * 获取店铺收藏
     * @param oShopCollectDto
     * @return
     * @throws Exception
     */
    Result getShopCollect(ShopCollectDto oShopCollectDto) throws Exception;

    /**
     * 添加店铺收藏
     * @param oShopCollectDto
     * @return
     * @throws Exception
     */
    Result addShopCollect(ShopCollectDto oShopCollectDto) throws Exception;

    /**
     * 修改店铺收藏
     * @param oShopCollectDto
     * @return
     * @throws Exception
     */
    Result updateShopCollect(ShopCollectDto oShopCollectDto) throws Exception;

    /**
     * 删除店铺收藏
     * @param oShopCollectDto
     * @return
     * @throws Exception
     */
    Result deleteShopCollect(ShopCollectDto oShopCollectDto) throws Exception;
    
}
