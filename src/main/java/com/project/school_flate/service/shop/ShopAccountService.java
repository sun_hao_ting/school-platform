package com.project.school_flate.service.shop;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.shop.ShopAccountDto;
import com.project.school_flate.dto.shop.ShopInfoDto;
import com.project.school_flate.entity.shop.ShopAccount;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ShopAccountService extends IService<ShopAccount> {

    /**
     * 获取店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    Result getShopAccount(ShopAccountDto oShopAccountDto) throws Exception;

    /**
     * 添加店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    Result addShopAccount(ShopAccountDto oShopAccountDto) throws Exception;

    /**
     * 修改店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    Result updateShopAccount(ShopAccountDto oShopAccountDto) throws Exception;

    /**
     * 删除店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    Result deleteShopAccount(ShopAccountDto oShopAccountDto) throws Exception;

    /**
     * 冻结店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    Result freezeShopAccount(ShopAccountDto oShopAccountDto) throws Exception;

    /**
     * 获取店铺账号（后台）
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    Result getShopAccountBack(ShopAccountDto oShopAccountDto) throws Exception;

    /**
     * 解冻店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    Result thawShopAccount(ShopAccountDto oShopAccountDto) throws Exception;

    /**
     * 注册店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    Result registerShopAccount(ShopAccountDto oShopAccountDto) throws Exception;

    /**
     * 登录店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    Result loginShopAccount(ShopAccountDto oShopAccountDto) throws Exception;

    /**
     * 退出店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    Result exitShopAccount(ShopAccountDto oShopAccountDto) throws Exception;

    /**
     * 店铺账号修改密码
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    Result updateShopAccountPassword(ShopAccountDto oShopAccountDto) throws Exception;

    /**
     * 公众号登录店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    Result loginShopAccountOfficial(ShopAccountDto oShopAccountDto) throws Exception;

}
