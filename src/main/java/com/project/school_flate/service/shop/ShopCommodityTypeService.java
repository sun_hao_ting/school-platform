package com.project.school_flate.service.shop;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.shop.ShopCommodityTypeDto;
import com.project.school_flate.entity.shop.ShopCommodityType;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ShopCommodityTypeService extends IService<ShopCommodityType> {

    /**
     * 获取商品分类
     * @param oShopCommodityTypeDto
     * @return
     * @throws Exception
     */
    Result getShopCommodityType(ShopCommodityTypeDto oShopCommodityTypeDto) throws Exception;

    /**
     * 添加商品分类
     * @param oShopCommodityTypeDto
     * @return
     * @throws Exception
     */
    Result addShopCommodityType(ShopCommodityTypeDto oShopCommodityTypeDto) throws Exception;

    /**
     * 修改商品分类
     * @param oShopCommodityTypeDto
     * @return
     * @throws Exception
     */
    Result updateShopCommodityType(ShopCommodityTypeDto oShopCommodityTypeDto) throws Exception;

    /**
     * 删除商品分类
     * @param oShopCommodityTypeDto
     * @return
     * @throws Exception
     */
    Result deleteShopCommodityType(ShopCommodityTypeDto oShopCommodityTypeDto) throws Exception;

    /**
     * 获取商品分类（客户）
     * @param oShopCommodityTypeDto
     * @return
     * @throws Exception
     */
    Result getShopCommodityTypeUser(ShopCommodityTypeDto oShopCommodityTypeDto) throws Exception;

}
