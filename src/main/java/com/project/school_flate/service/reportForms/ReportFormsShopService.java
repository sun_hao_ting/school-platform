package com.project.school_flate.service.reportForms;

import com.project.school_flate.dto.shop.ShopInfoDto;
import com.project.school_flate.util.Result.Result;

public interface ReportFormsShopService {

    /**
     * 获取累计单量
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result getAllRobOrder(ShopInfoDto oShopInfoDto) throws Exception;

    /**
     * 获取今日抢单
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result getDayRobOrder(ShopInfoDto oShopInfoDto) throws Exception;

    /**
     * 获取我的余额
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result getDayRobOrderBalance(ShopInfoDto oShopInfoDto) throws Exception;

    /**
     * 获取可提现余额和累计提现
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result getWithdrawalBalance(ShopInfoDto oShopInfoDto) throws Exception;

    /**
     * 获取即将到账和累计到账
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result getReceived(ShopInfoDto oShopInfoDto) throws Exception;

    /**
     * 获取收入明细
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    Result getIncome(ShopInfoDto oShopInfoDto) throws Exception;

}
