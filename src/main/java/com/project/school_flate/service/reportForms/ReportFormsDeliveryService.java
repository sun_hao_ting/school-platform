package com.project.school_flate.service.reportForms;

import com.project.school_flate.dto.delivery.DeliveryInfoDto;
import com.project.school_flate.util.Result.Result;

public interface ReportFormsDeliveryService {

    /**
     * 获取累计抢单
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result getAllRobOrder(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

    /**
     * 获取今日抢单
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result getDayRobOrder(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

    /**
     * 获取我的余额
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result getDayRobOrderBalance(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

    /**
     * 获取可提现余额和累计余额
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result getWithdrawalBalance(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

    /**
     * 获取即将到账和累计到账
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result getReceived(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

    /**
     * 获取收入明细
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result getIncome(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

}
