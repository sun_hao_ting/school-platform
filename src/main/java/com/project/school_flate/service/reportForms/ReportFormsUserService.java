package com.project.school_flate.service.reportForms;

import com.project.school_flate.dto.shop.ShopInfoDto;
import com.project.school_flate.dto.user.UserInfoDto;
import com.project.school_flate.util.Result.Result;

public interface ReportFormsUserService {

    /**
     * 获取我的余额
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result getDayRobOrderBalance(UserInfoDto oUserInfoDto) throws Exception;

    /**
     * 获取可提现余额和累计余额
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result getWithdrawalBalance(UserInfoDto oUserInfoDto) throws Exception;

    /**
     * 获取即将到账和累计到账
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result getReceived(UserInfoDto oUserInfoDto) throws Exception;

    /**
     * 获取收入明细
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    Result getIncome(UserInfoDto oUserInfoDto) throws Exception;

}
