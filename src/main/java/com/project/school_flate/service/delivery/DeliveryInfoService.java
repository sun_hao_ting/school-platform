package com.project.school_flate.service.delivery;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.delivery.DeliveryInfoDto;
import com.project.school_flate.entity.delivery.DeliveryInfo;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface DeliveryInfoService extends IService<DeliveryInfo> {

    /**
     * 获取配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result getDeliveryInfo(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

    /**
     * 添加配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result addDeliveryInfo(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

    /**
     * 修改配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result updateDeliveryInfo(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

    /**
     * 冻结配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result freezeDeliveryInfo(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

    /**
     * 获取配送员（后台）
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result getDeliveryInfoBack(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

    /**
     * 解冻配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result thawDeliveryInfo(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

    /**
     * 注册配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result registerDeliveryInfo(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

    /**
     * 登录配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result loginDeliveryInfo(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

    /**
     * 退出配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result exitDeliveryInfo(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

    /**
     * 配送员修改密码
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result updateDeliveryInfoPassword(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

    /**
     * 公众号登录配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    Result loginDeliveryInfoOfficial(DeliveryInfoDto oDeliveryInfoDto) throws Exception;

}
