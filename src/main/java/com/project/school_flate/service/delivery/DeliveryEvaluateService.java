package com.project.school_flate.service.delivery;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.delivery.DeliveryEvaluateDto;
import com.project.school_flate.entity.delivery.DeliveryEvaluate;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface DeliveryEvaluateService extends IService<DeliveryEvaluate> {

    /**
     * 获取配送员评价
     * @param oDeliveryEvaluateDto
     * @return
     * @throws Exception
     */
    Result getDeliveryEvaluate(DeliveryEvaluateDto oDeliveryEvaluateDto) throws Exception;

    /**
     * 添加配送员评价
     * @param oDeliveryEvaluateDto
     * @return
     * @throws Exception
     */
    Result addDeliveryEvaluate(DeliveryEvaluateDto oDeliveryEvaluateDto) throws Exception;

    /**
     * 修改配送员评价
     * @param oDeliveryEvaluateDto
     * @return
     * @throws Exception
     */
    Result updateDeliveryEvaluate(DeliveryEvaluateDto oDeliveryEvaluateDto) throws Exception;

    /**
     * 删除配送员评价
     * @param oDeliveryEvaluateDto
     * @return
     * @throws Exception
     */
    Result deleteDeliveryEvaluate(DeliveryEvaluateDto oDeliveryEvaluateDto) throws Exception;
    
}
