package com.project.school_flate.service.delivery;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.delivery.DeliveryExamineDto;
import com.project.school_flate.dto.delivery.DeliveryInfoDto;
import com.project.school_flate.entity.delivery.DeliveryExamine;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface DeliveryExamineService extends IService<DeliveryExamine> {

    /**
     * 获取配送员审核
     * @param oDeliveryExamineDto
     * @return
     * @throws Exception
     */
    Result getDeliveryExamine(DeliveryExamineDto oDeliveryExamineDto) throws Exception;

    /**
     * 添加配送员审核
     * @param oDeliveryExamineDto
     * @return
     * @throws Exception
     */
    Result addDeliveryExamine(DeliveryExamineDto oDeliveryExamineDto) throws Exception;

    /**
     * 修改配送员审核
     * @param oDeliveryExamineDto
     * @return
     * @throws Exception
     */
    Result updateDeliveryExamine(DeliveryExamineDto oDeliveryExamineDto) throws Exception;

    /**
     * 删除配送员审核
     * @param oDeliveryExamineDto
     * @return
     * @throws Exception
     */
    Result deleteDeliveryExamine(DeliveryExamineDto oDeliveryExamineDto) throws Exception;

}
