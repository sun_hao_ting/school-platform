package com.project.school_flate.service.com;

import com.project.school_flate.dto.com.ComRoleDto;
import com.project.school_flate.util.Result.Result;

public interface ComRoleService {

    /**
     * 获取角色
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    Result getComRole(ComRoleDto oComRoleDto) throws Exception;

    /**
     * 添加角色
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    Result addComRole(ComRoleDto oComRoleDto) throws Exception;

    /**
     * 修改角色
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    Result updateComRole(ComRoleDto oComRoleDto) throws Exception;

    /**
     * 冻结角色
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    Result freezeComRole(ComRoleDto oComRoleDto) throws Exception;

    /**
     * 获取角色（后台）
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    Result getComRoleBack(ComRoleDto oComRoleDto) throws Exception;

    /**
     * 解冻角色
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    Result thawComRole(ComRoleDto oComRoleDto) throws Exception;

}
