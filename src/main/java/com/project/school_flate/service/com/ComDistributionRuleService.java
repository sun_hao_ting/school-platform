package com.project.school_flate.service.com;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.com.ComDistributionRuleDto;
import com.project.school_flate.entity.com.ComDistributionRule;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ComDistributionRuleService extends IService<ComDistributionRule> {

    /**
     * 获取分销商等级规则
     * @param oComDistributionRuleDto
     * @return
     * @throws Exception
     */
    Result getComDistributionRule(ComDistributionRuleDto oComDistributionRuleDto) throws Exception;

    /**
     * 添加分销商等级规则
     * @param oComDistributionRuleDto
     * @return
     * @throws Exception
     */
    Result addComDistributionRule(ComDistributionRuleDto oComDistributionRuleDto) throws Exception;

    /**
     * 修改分销商等级规则
     * @param oComDistributionRuleDto
     * @return
     * @throws Exception
     */
    Result updateComDistributionRule(ComDistributionRuleDto oComDistributionRuleDto) throws Exception;

    /**
     * 删除分销商等级规则
     * @param oComDistributionRuleDto
     * @return
     * @throws Exception
     */
    Result deleteComDistributionRule(ComDistributionRuleDto oComDistributionRuleDto) throws Exception;

}
