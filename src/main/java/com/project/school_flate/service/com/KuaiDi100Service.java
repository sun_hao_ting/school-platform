package com.project.school_flate.service.com;


import com.project.school_flate.util.Result.Result;
import com.project.school_flate.util.kuidi100.ExpressShipment;
import com.project.school_flate.util.kuidi100.SelectKuaiDi;

public interface KuaiDi100Service {
    /**
     * 商家寄件下单
     * @throws Exception
     */
    Result testBorderOfficial(ExpressShipment expressShipment) throws Exception;
    /**
     * 实时快递查询
     * @throws Exception
     */
    Result testQueryTrack(SelectKuaiDi selectKuaiDi) throws Exception;
}
