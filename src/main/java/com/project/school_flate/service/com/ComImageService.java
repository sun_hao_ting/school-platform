package com.project.school_flate.service.com;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.com.ComImageDto;
import com.project.school_flate.entity.com.ComImage;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ComImageService extends IService<ComImage> {

    /**
     * 获取系统图
     * @param oComImageDto
     * @return
     * @throws Exception
     */
    Result getComImage(ComImageDto oComImageDto) throws Exception;

    /**
     * 添加系统图
     * @param oComImageDto
     * @return
     * @throws Exception
     */
    Result addComImage(ComImageDto oComImageDto) throws Exception;

    /**
     * 修改系统图
     * @param oComImageDto
     * @return
     * @throws Exception
     */
    Result updateComImage(ComImageDto oComImageDto) throws Exception;

    /**
     * 删除系统图
     * @param oComImageDto
     * @return
     * @throws Exception
     */
    Result deleteComImage(ComImageDto oComImageDto) throws Exception;
    
}
