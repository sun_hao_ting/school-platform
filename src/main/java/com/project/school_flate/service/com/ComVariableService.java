package com.project.school_flate.service.com;


import com.project.school_flate.entity.com.ComVariable;
import com.project.school_flate.util.Result.Result;

public interface ComVariableService {

    /**
     * 添加系统参数
     * @param
     * @return
     * @throws Exception
     */
    Result addVariable(ComVariable variable) throws Exception;

    /**
     * 获取系统参数
     * @param variable
     * @return
     * @throws Exception
     */
    Result getVariable(ComVariable variable) throws Exception;

    /**
     * 修改系统参数
     * @param
     * @return
     * @throws Exception
     */
    Result updateVariable(ComVariable variable) throws Exception;

    /**
     * 删除系统参数
     * @param
     * @return
     * @throws Exception
     */
    Result deleteVariable(ComVariable variable) throws Exception;

}
