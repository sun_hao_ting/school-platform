package com.project.school_flate.service.com;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.com.ComFeedbackDto;
import com.project.school_flate.entity.com.ComFeedback;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ComFeedbackService extends IService<ComFeedback> {

    /**
     * 获取反馈
     * @param oComFeedbackDto
     * @return
     * @throws Exception
     */
    Result getComFeedback(ComFeedbackDto oComFeedbackDto) throws Exception;

    /**
     * 添加反馈
     * @param oComFeedbackDto
     * @return
     * @throws Exception
     */
    Result addComFeedback(ComFeedbackDto oComFeedbackDto) throws Exception;

    /**
     * 修改反馈
     * @param oComFeedbackDto
     * @return
     * @throws Exception
     */
    Result updateComFeedback(ComFeedbackDto oComFeedbackDto) throws Exception;

    /**
     * 删除反馈
     * @param oComFeedbackDto
     * @return
     * @throws Exception
     */
    Result deleteComFeedback(ComFeedbackDto oComFeedbackDto) throws Exception;

}
