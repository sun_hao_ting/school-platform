package com.project.school_flate.service.com;

import com.alibaba.fastjson2.JSONObject;
import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.com.ComWithdrawalFlowDto;
import com.project.school_flate.entity.com.ComWithdrawalFlow;
import com.project.school_flate.util.Result.Result;

import java.util.Map;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ComWithdrawalFlowService extends IService<ComWithdrawalFlow> {

    /**
     * 获取微信转账流水
     * @param oComWithdrawalFlowDto
     * @return
     * @throws Exception
     */
    Result getComWithdrawalFlow(ComWithdrawalFlowDto oComWithdrawalFlowDto) throws Exception;

    /**
     * 添加微信转账流水
     * @param oComWithdrawalFlowDto
     * @return
     * @throws Exception
     */
    Result addComWithdrawalFlow(ComWithdrawalFlowDto oComWithdrawalFlowDto) throws Exception;

    /**
     * 平台确认转账
     * @param oComWithdrawalFlowDto
     * @return
     * @throws Exception
     */
    Result comOkWithdrawal(ComWithdrawalFlowDto oComWithdrawalFlowDto) throws Exception;

    /**
     * 平台驳回转账
     * @param oComWithdrawalFlowDto
     * @return
     * @throws Exception
     */
    Result comRejectWithdrawal(ComWithdrawalFlowDto oComWithdrawalFlowDto) throws Exception;

    /**
     * 用户转账（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    Map<String, String> comWithdrawalFlowUserCallback(JSONObject jsonObject) throws Exception;

    /**
     * 配送员转账（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    Map<String, String> comWithdrawalFlowDeliveryCallback(JSONObject jsonObject) throws Exception;

    /**
     * 店铺转账（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    Map<String, String> comWithdrawalFlowShopCallback(JSONObject jsonObject) throws Exception;

    /**
     * 获取微信转账规则
     * @param oComWithdrawalFlowDto
     * @return
     * @throws Exception
     */
    Result getComWithdrawalFlowRule(ComWithdrawalFlowDto oComWithdrawalFlowDto) throws Exception;

}
