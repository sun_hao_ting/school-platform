package com.project.school_flate.service.com;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.com.ComSchoolDto;
import com.project.school_flate.entity.com.ComSchool;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ComSchoolService extends IService<ComSchool> {

    /**
     * 获取校园
     * @param oComSchoolDto
     * @return
     * @throws Exception
     */
    Result getComSchool(ComSchoolDto oComSchoolDto) throws Exception;

    /**
     * 添加校园
     * @param oComSchoolDto
     * @return
     * @throws Exception
     */
    Result addComSchool(ComSchoolDto oComSchoolDto) throws Exception;

    /**
     * 修改校园
     * @param oComSchoolDto
     * @return
     * @throws Exception
     */
    Result updateComSchool(ComSchoolDto oComSchoolDto) throws Exception;

    /**
     * 冻结校园
     * @param oComSchoolDto
     * @return
     * @throws Exception
     */
    Result freezeComSchool(ComSchoolDto oComSchoolDto) throws Exception;

    /**
     * 获取校园（后台）
     * @param oComSchoolDto
     * @return
     * @throws Exception
     */
    Result getComSchoolBack(ComSchoolDto oComSchoolDto) throws Exception;

    /**
     * 解冻校园
     * @param oComSchoolDto
     * @return
     * @throws Exception
     */
    Result thawComSchool(ComSchoolDto oComSchoolDto) throws Exception;

    /**
     * 获取用户定位校园
     * @param oComSchoolDto
     * @return
     * @throws Exception
     */
    Result getUserLocationComSchool(ComSchoolDto oComSchoolDto) throws Exception;

}
