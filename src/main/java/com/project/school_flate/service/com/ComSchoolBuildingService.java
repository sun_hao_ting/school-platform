package com.project.school_flate.service.com;

import com.mybatisflex.core.service.IService;
import com.project.school_flate.dto.com.ComSchoolBuildingDto;
import com.project.school_flate.entity.com.ComSchoolBuilding;
import com.project.school_flate.util.Result.Result;

/**
 *  服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ComSchoolBuildingService extends IService<ComSchoolBuilding> {

    /**
     * 获取校园楼栋
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    Result getComSchoolBuilding(ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception;

    /**
     * 添加校园楼栋
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    Result addComSchoolBuilding(ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception;

    /**
     * 修改校园楼栋
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    Result updateComSchoolBuilding(ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception;

    /**
     * 冻结校园楼栋
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    Result freezeComSchoolBuilding(ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception;

    /**
     * 获取校园楼栋（后台）
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    Result getComSchoolBuildingBack(ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception;

    /**
     * 解冻校园楼栋
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    Result thawComSchoolBuilding(ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception;

}
