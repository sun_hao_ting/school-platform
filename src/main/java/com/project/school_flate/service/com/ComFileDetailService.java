package com.project.school_flate.service.com;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mybatisflex.core.service.IService;
import com.project.school_flate.entity.com.ComFileDetail;
import com.project.school_flate.util.Result.Result;
import org.dromara.x.file.storage.core.FileInfo;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;

/**
 * 文件记录表 服务层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
public interface ComFileDetailService extends IService<ComFileDetail> {

    /**
     * 生成 URL，有效期为1小时
     * @param oComFileDetail
     * @return
     * @throws Exception
     */
    public Result generateUrl(ComFileDetail oComFileDetail) throws Exception;

    /**
     * 下载文件到路径
     * @param fileInfo
     * @return
     * @throws Exception
     */
    public void downToPath(FileInfo fileInfo, String filePath) throws Exception;

    /**
     * 下载为字节数组
     * @param fileInfo
     * @return
     * @throws Exception
     */
    public byte[] downToByte(FileInfo fileInfo) throws Exception;

    /**
     * 下载文件到到 OutputStream 中
     * @param fileInfo
     * @return
     * @throws Exception
     */
    public void downToOS(FileInfo fileInfo, OutputStream out) throws Exception;

    /**
     * 上传公共附件
     * @param request
     * @return
     * @throws Exception
     */
    Result uploadPublicFile(HttpServletRequest request) throws Exception;

    /**
     * 下载附件
     * @param oComFileDetail,response
     * @return
     * @throws Exception
     */
    Result downFile(ComFileDetail oComFileDetail, HttpServletResponse response) throws Exception;

    /**
     * 上传私密附件
     * @param request
     * @return
     * @throws Exception
     */
    Result uploadPrivateFile(HttpServletRequest request) throws Exception;

}
