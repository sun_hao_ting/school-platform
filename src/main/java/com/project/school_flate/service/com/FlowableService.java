package com.project.school_flate.service.com;



import com.project.school_flate.dto.com.FlowableData;
import com.project.school_flate.dto.com.TaskData;
import com.project.school_flate.util.Result.Result;

import java.io.InputStream;

/**
 * @Author sunht
 * @Date 2023/10/10 21:35
 * @Version v1.0.0
 * @Message
 **/
public interface FlowableService {
    /**
     * 查看部署流程
     * @param
     * @return
     * @throws Exception
     */
    Result getProcesses() throws Exception;

    /**
     * 删除流程
     * @param
     * @return
     * @throws Exception
     */
    Result deleteProcesses(FlowableData flowableData) throws Exception;

    /**
     * 部署流程文件
     * @param name
     * @param inputStream
     * @return
     * @throws Exception
     */
    Result deployment(String name, InputStream inputStream) throws Exception;

    /**
     * 流程启动
     * @param flowableData
     * @return
     * @throws Exception
     */
    Result startFlowable(FlowableData flowableData) throws Exception;

    /**
     * 处理任务
     * @param taskData
     * @return
     * @throws Exception
     */
    Result taskFlowable(TaskData taskData) throws Exception;

    /**
     * 查询自己负责的任务
     * @param taskData
     * @return
     * @throws Exception
     */
    Result findPersonTaskList(TaskData taskData) throws Exception;
}
