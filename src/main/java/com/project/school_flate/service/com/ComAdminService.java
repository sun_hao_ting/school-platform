package com.project.school_flate.service.com;


import com.project.school_flate.dto.com.ComAdminDto;
import com.project.school_flate.dto.com.ComAdminDto;
import com.project.school_flate.util.Result.Result;

/**
 * @Author sunht
 * @Date 2023/10/7 9:47
 * @Version v1.0.0
 * @Message
 **/
public interface ComAdminService {

    /**
     * 获取管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    Result getComAdmin(ComAdminDto oComAdminDto) throws Exception;

    /**
     * 添加管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    Result addComAdmin(ComAdminDto oComAdminDto) throws Exception;

    /**
     * 修改管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    Result updateComAdmin(ComAdminDto oComAdminDto) throws Exception;

    /**
     * 冻结管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    Result freezeComAdmin(ComAdminDto oComAdminDto) throws Exception;

    /**
     * 获取管理员（后台）
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    Result getComAdminBack(ComAdminDto oComAdminDto) throws Exception;

    /**
     * 解冻管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    Result thawComAdmin(ComAdminDto oComAdminDto) throws Exception;

    /**
     * 登录管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    Result loginComAdmin(ComAdminDto oComAdminDto) throws Exception;

}
