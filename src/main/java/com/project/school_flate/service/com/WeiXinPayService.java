package com.project.school_flate.service.com;

import com.alibaba.fastjson2.JSONObject;
import com.project.school_flate.util.Result.Result;
import com.project.school_flate.util.wechat.PayDto;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

public interface WeiXinPayService {
    /**
     * 支付接口
     *
     * @param payDto
     * @return
     */
    Result entPay(PayDto payDto) throws Exception;

    /**
     * 支付接口_app
     *
     * @param payDto
     * @return
     */
    Result entPayApp(PayDto payDto) throws Exception;

    /**
     * 支付接口_H5
     *
     * @param payDto
     * @return
     */
    Result entPayH5(PayDto payDto) throws Exception;

    /**
     * 回调支付_js_api
     *
     * @return
     */
    Result encryption(String prepayId) throws Exception;

    /**
     * 回调支付_js_app
     *
     * @return
     */
    Result encryptionApp(String prepayId) throws Exception;

    /**
     * 回调通知解密
     * @param jsonObject
     * @return
     */
    JSONObject notifyOrder(JSONObject jsonObject, String summary);

    /**
     * 微信提现
     *
     * @param payDto
     * @return
     */
    Result weixinTransferBat(PayDto payDto) throws Exception;

    /**
     * 退款接口_jsApi
     *
     * @param payDto
     * @return
     */
    Result refundsPay(PayDto payDto) throws Exception;

    /**
     * 扫码JS-SDK
     *
     * @param payDto
     * @return
     */
    Result scanCode(String thisUrl) throws Exception;

    /**
     * 转账_jsApi
     *
     * @param payDto
     * @return
     */
    Result transferPay(PayDto payDto) throws Exception;

}
