package com.project.school_flate.controller;

import com.alibaba.fastjson2.JSONObject;
import com.project.school_flate.config.activemq.DelayedActive;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.ScheduledMessage;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jms.JmsProperties;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.Message;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.jms.*;
import java.io.Serializable;
import java.util.HashMap;
@Slf4j
@RestController
@RequestMapping("/kafka")
public class HelloController {

//    @Autowired(required = false)
//    private KafkaTemplate<String,String> kafkaTemplate;
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

//    @GetMapping("/send1")
//    public String send1() throws Exception{
//        ActiveMQQueue queue = new ActiveMQQueue("aabb");
//        new DelayedActive().mqSend(queue,"测试环境数据发送");
//        return "send successfully!";
//    }
//
//    @GetMapping("/sendOut")
//    public void sendOut() throws Exception{
//        long delay = 3 * 1000;//设置3s的延迟
//        System.out.println("发送消息时间:" + System.currentTimeMillis());
//        ActiveMQQueue queue = new ActiveMQQueue("aabb");
//        new DelayedActive().delaySend(queue,"测试环境数据发送",delay);
//    }
//
//    @GetMapping("/sendTopic")
//    public void sendTopic() throws Exception{
//        long delay = 3 * 1000;//设置3s的延迟
//        System.out.println("发送消息时间:" + System.currentTimeMillis());
//        ActiveMQTopic topic = new ActiveMQTopic("aabb");
//        new DelayedActive().delaySend(topic,"广播测试环境数据发送",delay);
//    }

}
