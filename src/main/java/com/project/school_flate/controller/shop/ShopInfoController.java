package com.project.school_flate.controller.shop;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.shop.ShopInfoDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.shop.ShopInfo;
import com.project.school_flate.service.shop.ShopInfoService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "店铺")
@RequestMapping("/shopInfo")
public class ShopInfoController {

    @Autowired
    private ShopInfoService oShopInfoService;

    /**
     * 获取店铺
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取店铺")
    @PostMapping(value = "getShopInfo", produces = "application/json;charset=utf-8")
    public Result getShopInfo(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oShopInfoService.getShopInfo(oShopInfoDto);
    }

    /**
     * 添加店铺
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加店铺")
    @PostMapping(value = "addShopInfo", produces = "application/json;charset=utf-8")
    public Result addShopInfo(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oShopInfoService.addShopInfo(oShopInfoDto);
    }

    /**
     * 修改店铺
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改店铺")
    @PostMapping(value = "updateShopInfo", produces = "application/json;charset=utf-8")
    public Result updateShopInfo(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oShopInfoService.updateShopInfo(oShopInfoDto);
    }

    /**
     * 冻结店铺
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "冻结店铺")
    @PostMapping(value = "freezeShopInfo", produces = "application/json;charset=utf-8")
    public Result freezeShopInfo(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oShopInfoService.freezeShopInfo(oShopInfoDto);
    }

    /**
     * 获取店铺（后台）
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取店铺（后台）")
    @PostMapping(value = "getShopInfoBack", produces = "application/json;charset=utf-8")
    public Result getShopInfoBack(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oShopInfoService.getShopInfoBack(oShopInfoDto);
    }

    /**
     * 解冻店铺
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "解冻店铺")
    @PostMapping(value = "thawShopInfo", produces = "application/json;charset=utf-8")
    public Result thawShopInfo(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oShopInfoService.thawShopInfo(oShopInfoDto);
    }

    /**
     * 获取店铺（客户）
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取店铺（客户）")
    @PostMapping(value = "getShopInfoUser", produces = "application/json;charset=utf-8")
    public Result getShopInfoUser(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oShopInfoService.getShopInfoUser(oShopInfoDto);
    }

    /**
     * 店铺申请储蓄
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "店铺申请储蓄")
    @PostMapping(value = "applyShopInfoSave", produces = "application/json;charset=utf-8")
    public Result applyShopInfoSave(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oShopInfoService.applyShopInfoSave(oShopInfoDto);
    }

    /**
     * 审核申请储蓄
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "审核申请储蓄")
    @PostMapping(value = "examineShopInfoSave", produces = "application/json;charset=utf-8")
    public Result examineShopInfoSave(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oShopInfoService.examineShopInfoSave(oShopInfoDto);
    }

    /**
     * 获取用户是否是店铺新人
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取用户是否是店铺新人")
    @PostMapping(value = "getShopInfoUserNew", produces = "application/json;charset=utf-8")
    public Result getShopInfoUserNew(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oShopInfoService.getShopInfoUserNew(oShopInfoDto);
    }

    /**
     * 获取店铺配送费和打包费
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取店铺配送费和打包费")
    @PostMapping(value = "getShopInfoDeliveryPacking", produces = "application/json;charset=utf-8")
    public Result getShopInfoDeliveryPacking(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oShopInfoService.getShopInfoDeliveryPacking(oShopInfoDto);
    }

    /**
     * 获取推荐店铺（客户）
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取推荐店铺（客户）")
    @PostMapping(value = "getRecommendShopInfoUser", produces = "application/json;charset=utf-8")
    public Result getRecommendShopInfoUser(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oShopInfoService.getRecommendShopInfoUser(oShopInfoDto);
    }

}
