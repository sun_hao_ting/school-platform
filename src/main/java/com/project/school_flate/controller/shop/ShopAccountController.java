package com.project.school_flate.controller.shop;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.dto.shop.ShopAccountDto;
import com.project.school_flate.dto.shop.ShopInfoDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.service.shop.ShopAccountService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "店铺账号")
@RequestMapping("/shopAccount")
public class ShopAccountController {

    @Autowired
    private ShopAccountService oShopAccountService;

    /**
     * 获取店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取店铺账号")
    @PostMapping(value = "getShopAccount", produces = "application/json;charset=utf-8")
    public Result getShopAccount(@RequestBody ShopAccountDto oShopAccountDto) throws Exception{
        return oShopAccountService.getShopAccount(oShopAccountDto);
    }

    /**
     * 添加店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加店铺账号")
    @PostMapping(value = "addShopAccount", produces = "application/json;charset=utf-8")
    public Result addShopAccount(@RequestBody ShopAccountDto oShopAccountDto) throws Exception{
        return oShopAccountService.addShopAccount(oShopAccountDto);
    }

    /**
     * 修改店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改店铺账号")
    @PostMapping(value = "updateShopAccount", produces = "application/json;charset=utf-8")
    public Result updateShopAccount(@RequestBody ShopAccountDto oShopAccountDto) throws Exception{
        return oShopAccountService.updateShopAccount(oShopAccountDto);
    }

    /**
     * 冻结店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "冻结店铺账号")
    @PostMapping(value = "freezeShopAccount", produces = "application/json;charset=utf-8")
    public Result freezeShopAccount(@RequestBody ShopAccountDto oShopAccountDto) throws Exception{
        return oShopAccountService.freezeShopAccount(oShopAccountDto);
    }

    /**
     * 获取店铺账号（后台）
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取店铺账号（后台）")
    @PostMapping(value = "getShopAccountBack", produces = "application/json;charset=utf-8")
    public Result getShopAccountBack(@RequestBody ShopAccountDto oShopAccountDto) throws Exception{
        return oShopAccountService.getShopAccountBack(oShopAccountDto);
    }

    /**
     * 解冻店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "解冻店铺账号")
    @PostMapping(value = "thawShopAccount", produces = "application/json;charset=utf-8")
    public Result thawShopAccount(@RequestBody ShopAccountDto oShopAccountDto) throws Exception{
        return oShopAccountService.thawShopAccount(oShopAccountDto);
    }

    /**
     * 注册店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "注册店铺")
    @PostMapping(value = "registerShopAccount", produces = "application/json;charset=utf-8")
    public Result registerShopAccount(@RequestBody ShopAccountDto oShopAccountDto) throws Exception{
        return oShopAccountService.registerShopAccount(oShopAccountDto);
    }

    /**
     * 登录店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "登录店铺")
    @PostMapping(value = "loginShopAccount", produces = "application/json;charset=utf-8")
    public Result loginShopAccount(@RequestBody ShopAccountDto oShopAccountDto) throws Exception{
        return oShopAccountService.loginShopAccount(oShopAccountDto);
    }

    /**
     * 退出店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "退出店铺")
    @PostMapping(value = "exitShopAccount", produces = "application/json;charset=utf-8")
    public Result exitShopAccount(@RequestBody ShopAccountDto oShopAccountDto) throws Exception{
        return oShopAccountService.exitShopAccount(oShopAccountDto);
    }

    /**
     * 店铺账号修改密码
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "店铺修改密码")
    @PostMapping(value = "updateShopAccountPassword", produces = "application/json;charset=utf-8")
    public Result updateShopAccountPassword(@RequestBody ShopAccountDto oShopAccountDto) throws Exception{
        return oShopAccountService.updateShopAccountPassword(oShopAccountDto);
    }

    /**
     * 公众号登录店铺账号
     * @param oShopAccountDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "公众号登录店铺账号")
    @PostMapping(value = "loginShopAccountOfficial", produces = "application/json;charset=utf-8")
    public Result loginShopAccountOfficial(@RequestBody ShopAccountDto oShopAccountDto) throws Exception{
        return oShopAccountService.loginShopAccountOfficial(oShopAccountDto);
    }

}
