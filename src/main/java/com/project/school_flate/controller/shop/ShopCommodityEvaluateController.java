package com.project.school_flate.controller.shop;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.shop.ShopCommodityEvaluateDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.shop.ShopCommodityEvaluate;
import com.project.school_flate.service.shop.ShopCommodityEvaluateService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "商品评价")
@RequestMapping("/shopCommodityEvaluate")
public class ShopCommodityEvaluateController {

    @Autowired
    private ShopCommodityEvaluateService oShopCommodityEvaluateService;

    /**
     * 获取商品评价
     * @param oShopCommodityEvaluateDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取商品评价")
    @PostMapping(value = "getShopCommodityEvaluate", produces = "application/json;charset=utf-8")
    public Result getShopCommodityEvaluate(@RequestBody ShopCommodityEvaluateDto oShopCommodityEvaluateDto) throws Exception{
        return oShopCommodityEvaluateService.getShopCommodityEvaluate(oShopCommodityEvaluateDto);
    }

    /**
     * 添加商品评价
     * @param oShopCommodityEvaluateDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加商品评价")
    @PostMapping(value = "addShopCommodityEvaluate", produces = "application/json;charset=utf-8")
    public Result addShopCommodityEvaluate(@RequestBody ShopCommodityEvaluateDto oShopCommodityEvaluateDto) throws Exception{
        return oShopCommodityEvaluateService.addShopCommodityEvaluate(oShopCommodityEvaluateDto);
    }

    /**
     * 修改商品评价
     * @param oShopCommodityEvaluateDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改商品评价")
    @PostMapping(value = "updateShopCommodityEvaluate", produces = "application/json;charset=utf-8")
    public Result updateShopCommodityEvaluate(@RequestBody ShopCommodityEvaluateDto oShopCommodityEvaluateDto) throws Exception{
        return oShopCommodityEvaluateService.updateShopCommodityEvaluate(oShopCommodityEvaluateDto);
    }

    /**
     * 删除商品评价
     * @param oShopCommodityEvaluateDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除商品评价")
    @PostMapping(value = "deleteShopCommodityEvaluate", produces = "application/json;charset=utf-8")
    public Result deleteShopCommodityEvaluate(@RequestBody ShopCommodityEvaluateDto oShopCommodityEvaluateDto) throws Exception{
        return oShopCommodityEvaluateService.deleteShopCommodityEvaluate(oShopCommodityEvaluateDto);
    }

    /**
     * 获取商品评价（客户）
     * @param oShopCommodityEvaluateDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取商品评价（客户）")
    @PostMapping(value = "getShopCommodityEvaluateUser", produces = "application/json;charset=utf-8")
    public Result getShopCommodityEvaluateUser(@RequestBody ShopCommodityEvaluateDto oShopCommodityEvaluateDto) throws Exception{
        return oShopCommodityEvaluateService.getShopCommodityEvaluateUser(oShopCommodityEvaluateDto);
    }

    /**
     * 用户自动商品评价
     * @param oOrderTakeawayId
     * @return
     * @throws Exception
     */
    @JmsListener(destination = "automaticShopCommodityEvaluate", containerFactory = "queueListener")
    public void automaticShopCommodityEvaluate(String oOrderTakeawayId) throws Exception{
        oShopCommodityEvaluateService.automaticShopCommodityEvaluate(oOrderTakeawayId);
    }

}
