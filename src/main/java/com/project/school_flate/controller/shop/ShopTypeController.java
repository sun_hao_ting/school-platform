package com.project.school_flate.controller.shop;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.shop.ShopTypeDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.shop.ShopType;
import com.project.school_flate.service.shop.ShopTypeService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "店铺类型")
@RequestMapping("/shopType")
public class ShopTypeController {

    @Autowired
    private ShopTypeService oShopTypeService;

    /**
     * 获取店铺类型
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取店铺类型")
    @PostMapping(value = "getShopType", produces = "application/json;charset=utf-8")
    public Result getShopType(@RequestBody ShopTypeDto oShopTypeDto) throws Exception{
        return oShopTypeService.getShopType(oShopTypeDto);
    }

    /**
     * 添加店铺类型
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加店铺类型")
    @PostMapping(value = "addShopType", produces = "application/json;charset=utf-8")
    public Result addShopType(@RequestBody ShopTypeDto oShopTypeDto) throws Exception{
        return oShopTypeService.addShopType(oShopTypeDto);
    }

    /**
     * 修改店铺类型
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改店铺类型")
    @PostMapping(value = "updateShopType", produces = "application/json;charset=utf-8")
    public Result updateShopType(@RequestBody ShopTypeDto oShopTypeDto) throws Exception{
        return oShopTypeService.updateShopType(oShopTypeDto);
    }

    /**
     * 冻结店铺类型
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "冻结店铺类型")
    @PostMapping(value = "freezeShopType", produces = "application/json;charset=utf-8")
    public Result freezeShopType(@RequestBody ShopTypeDto oShopTypeDto) throws Exception{
        return oShopTypeService.freezeShopType(oShopTypeDto);
    }

    /**
     * 获取店铺类型（后台）
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取店铺类型（后台）")
    @PostMapping(value = "getShopTypeBack", produces = "application/json;charset=utf-8")
    public Result getShopTypeBack(@RequestBody ShopTypeDto oShopTypeDto) throws Exception{
        return oShopTypeService.getShopTypeBack(oShopTypeDto);
    }

    /**
     * 解冻店铺类型
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "解冻店铺类型")
    @PostMapping(value = "thawShopType", produces = "application/json;charset=utf-8")
    public Result thawShopType(@RequestBody ShopTypeDto oShopTypeDto) throws Exception{
        return oShopTypeService.thawShopType(oShopTypeDto);
    }

    /**
     * 获取店铺类型（用户）
     * @param oShopTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取店铺类型（用户）")
    @PostMapping(value = "getShopTypeUser", produces = "application/json;charset=utf-8")
    public Result getShopTypeUser(@RequestBody ShopTypeDto oShopTypeDto) throws Exception{
        return oShopTypeService.getShopTypeUser(oShopTypeDto);
    }

}
