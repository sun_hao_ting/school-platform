package com.project.school_flate.controller.shop;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.shop.ShopExamineDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.shop.ShopExamine;
import com.project.school_flate.service.shop.ShopExamineService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "店铺审核")
@RequestMapping("/shopExamine")
public class ShopExamineController {

    @Autowired
    private ShopExamineService oShopExamineService;

    /**
     * 获取店铺审核
     * @param oShopExamineDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取店铺审核")
    @PostMapping(value = "getShopExamine", produces = "application/json;charset=utf-8")
    public Result getShopExamine(@RequestBody ShopExamineDto oShopExamineDto) throws Exception{
        return oShopExamineService.getShopExamine(oShopExamineDto);
    }

    /**
     * 添加店铺审核
     * @param oShopExamineDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加店铺审核")
    @PostMapping(value = "addShopExamine", produces = "application/json;charset=utf-8")
    public Result addShopExamine(@RequestBody ShopExamineDto oShopExamineDto) throws Exception{
        return oShopExamineService.addShopExamine(oShopExamineDto);
    }

    /**
     * 修改店铺审核
     * @param oShopExamineDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改店铺审核")
    @PostMapping(value = "updateShopExamine", produces = "application/json;charset=utf-8")
    public Result updateShopExamine(@RequestBody ShopExamineDto oShopExamineDto) throws Exception{
        return oShopExamineService.updateShopExamine(oShopExamineDto);
    }

    /**
     * 删除店铺审核
     * @param oShopExamineDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除店铺审核")
    @PostMapping(value = "deleteShopExamine", produces = "application/json;charset=utf-8")
    public Result deleteShopExamine(@RequestBody ShopExamineDto oShopExamineDto) throws Exception{
        return oShopExamineService.deleteShopExamine(oShopExamineDto);
    }

}
