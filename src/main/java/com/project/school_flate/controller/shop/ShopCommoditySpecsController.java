package com.project.school_flate.controller.shop;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.shop.ShopCommoditySpecsDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.shop.ShopCommoditySpecs;
import com.project.school_flate.service.shop.ShopCommoditySpecsService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "商品规格")
@RequestMapping("/shopCommoditySpecs")
public class ShopCommoditySpecsController {

    @Autowired
    private ShopCommoditySpecsService oShopCommoditySpecsService;

    /**
     * 获取商品规格
     * @param oShopCommoditySpecsDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取商品规格")
    @PostMapping(value = "getShopCommoditySpecs", produces = "application/json;charset=utf-8")
    public Result getShopCommoditySpecs(@RequestBody ShopCommoditySpecsDto oShopCommoditySpecsDto) throws Exception{
        return oShopCommoditySpecsService.getShopCommoditySpecs(oShopCommoditySpecsDto);
    }

    /**
     * 添加商品规格
     * @param oShopCommoditySpecsDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加商品规格")
    @PostMapping(value = "addShopCommoditySpecs", produces = "application/json;charset=utf-8")
    public Result addShopCommoditySpecs(@RequestBody ShopCommoditySpecsDto oShopCommoditySpecsDto) throws Exception{
        return oShopCommoditySpecsService.addShopCommoditySpecs(oShopCommoditySpecsDto);
    }

    /**
     * 修改商品规格
     * @param oShopCommoditySpecsDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改商品规格")
    @PostMapping(value = "updateShopCommoditySpecs", produces = "application/json;charset=utf-8")
    public Result updateShopCommoditySpecs(@RequestBody ShopCommoditySpecsDto oShopCommoditySpecsDto) throws Exception{
        return oShopCommoditySpecsService.updateShopCommoditySpecs(oShopCommoditySpecsDto);
    }

    /**
     * 删除商品规格
     * @param oShopCommoditySpecsDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除商品规格")
    @PostMapping(value = "deleteShopCommoditySpecs", produces = "application/json;charset=utf-8")
    public Result deleteShopCommoditySpecs(@RequestBody ShopCommoditySpecsDto oShopCommoditySpecsDto) throws Exception{
        return oShopCommoditySpecsService.deleteShopCommoditySpecs(oShopCommoditySpecsDto);
    }

    /**
     * 获取商品规格（用户）
     * @param oShopCommoditySpecsDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取商品规格（用户）")
    @PostMapping(value = "getShopCommoditySpecsUser", produces = "application/json;charset=utf-8")
    public Result getShopCommoditySpecsUser(@RequestBody ShopCommoditySpecsDto oShopCommoditySpecsDto) throws Exception{
        return oShopCommoditySpecsService.getShopCommoditySpecsUser(oShopCommoditySpecsDto);
    }

}
