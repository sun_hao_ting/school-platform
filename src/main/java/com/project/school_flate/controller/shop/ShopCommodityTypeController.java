package com.project.school_flate.controller.shop;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.shop.ShopCommodityTypeDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.shop.ShopCommodityType;
import com.project.school_flate.service.shop.ShopCommodityTypeService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "商品分类")
@RequestMapping("/shopCommodityType")
public class ShopCommodityTypeController {

    @Autowired
    private ShopCommodityTypeService oShopCommodityTypeService;

    /**
     * 获取商品分类
     * @param oShopCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取商品分类")
    @PostMapping(value = "getShopCommodityType", produces = "application/json;charset=utf-8")
    public Result getShopCommodityType(@RequestBody ShopCommodityTypeDto oShopCommodityTypeDto) throws Exception{
        return oShopCommodityTypeService.getShopCommodityType(oShopCommodityTypeDto);
    }

    /**
     * 添加商品分类
     * @param oShopCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加商品分类")
    @PostMapping(value = "addShopCommodityType", produces = "application/json;charset=utf-8")
    public Result addShopCommodityType(@RequestBody ShopCommodityTypeDto oShopCommodityTypeDto) throws Exception{
        return oShopCommodityTypeService.addShopCommodityType(oShopCommodityTypeDto);
    }

    /**
     * 修改商品分类
     * @param oShopCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改商品分类")
    @PostMapping(value = "updateShopCommodityType", produces = "application/json;charset=utf-8")
    public Result updateShopCommodityType(@RequestBody ShopCommodityTypeDto oShopCommodityTypeDto) throws Exception{
        return oShopCommodityTypeService.updateShopCommodityType(oShopCommodityTypeDto);
    }

    /**
     * 删除商品分类
     * @param oShopCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除商品分类")
    @PostMapping(value = "deleteShopCommodityType", produces = "application/json;charset=utf-8")
    public Result deleteShopCommodityType(@RequestBody ShopCommodityTypeDto oShopCommodityTypeDto) throws Exception{
        return oShopCommodityTypeService.deleteShopCommodityType(oShopCommodityTypeDto);
    }

    /**
     * 获取商品分类（客户）
     * @param oShopCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取商品分类（客户）")
    @PostMapping(value = "getShopCommodityTypeUser", produces = "application/json;charset=utf-8")
    public Result getShopCommodityTypeUser(@RequestBody ShopCommodityTypeDto oShopCommodityTypeDto) throws Exception{
        return oShopCommodityTypeService.getShopCommodityTypeUser(oShopCommodityTypeDto);
    }

}
