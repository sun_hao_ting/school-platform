package com.project.school_flate.controller.shop;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.dto.shop.ShopCommoditySpecsTypeDto;
import com.project.school_flate.util.Result.Result;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.service.shop.ShopCommoditySpecsTypeService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "商品规格类型")
@RequestMapping("/shopCommoditySpecsType")
public class ShopCommoditySpecsTypeController {

    @Autowired
    private ShopCommoditySpecsTypeService oShopCommoditySpecsTypeService;

    /**
     * 获取商品规格类型
     * @param oShopCommoditySpecsTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取商品规格类型")
    @PostMapping(value = "getShopCommoditySpecsType", produces = "application/json;charset=utf-8")
    public Result getShopCommoditySpecsType(@RequestBody ShopCommoditySpecsTypeDto oShopCommoditySpecsTypeDto) throws Exception{
        return oShopCommoditySpecsTypeService.getShopCommoditySpecsType(oShopCommoditySpecsTypeDto);
    }

    /**
     * 添加商品规格类型
     * @param oShopCommoditySpecsTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加商品规格类型")
    @PostMapping(value = "addShopCommoditySpecsType", produces = "application/json;charset=utf-8")
    public Result addShopCommoditySpecsType(@RequestBody ShopCommoditySpecsTypeDto oShopCommoditySpecsTypeDto) throws Exception{
        return oShopCommoditySpecsTypeService.addShopCommoditySpecsType(oShopCommoditySpecsTypeDto);
    }

    /**
     * 修改商品规格类型
     * @param oShopCommoditySpecsTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改商品规格类型")
    @PostMapping(value = "updateShopCommoditySpecsType", produces = "application/json;charset=utf-8")
    public Result updateShopCommoditySpecsType(@RequestBody ShopCommoditySpecsTypeDto oShopCommoditySpecsTypeDto) throws Exception{
        return oShopCommoditySpecsTypeService.updateShopCommoditySpecsType(oShopCommoditySpecsTypeDto);
    }

    /**
     * 删除商品规格类型
     * @param oShopCommoditySpecsTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除商品规格类型")
    @PostMapping(value = "deleteShopCommoditySpecsType", produces = "application/json;charset=utf-8")
    public Result deleteShopCommoditySpecsType(@RequestBody ShopCommoditySpecsTypeDto oShopCommoditySpecsTypeDto) throws Exception{
        return oShopCommoditySpecsTypeService.deleteShopCommoditySpecsType(oShopCommoditySpecsTypeDto);
    }

    /**
     * 获取商品规格类型（用户）
     * @param oShopCommoditySpecsTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取商品规格类型（用户）")
    @PostMapping(value = "getShopCommoditySpecsTypeUser", produces = "application/json;charset=utf-8")
    public Result getShopCommoditySpecsTypeUser(@RequestBody ShopCommoditySpecsTypeDto oShopCommoditySpecsTypeDto) throws Exception{
        return oShopCommoditySpecsTypeService.getShopCommoditySpecsTypeUser(oShopCommoditySpecsTypeDto);
    }

    /**
     * 批量添加商品规格类型
     * @param oShopCommoditySpecsTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "批量添加商品规格类型")
    @PostMapping(value = "batchAddShopCommoditySpecsType", produces = "application/json;charset=utf-8")
    public Result batchAddShopCommoditySpecsType(@RequestBody ShopCommoditySpecsTypeDto oShopCommoditySpecsTypeDto) throws Exception{
        return oShopCommoditySpecsTypeService.batchAddShopCommoditySpecsType(oShopCommoditySpecsTypeDto);
    }

}
