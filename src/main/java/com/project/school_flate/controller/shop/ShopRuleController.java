package com.project.school_flate.controller.shop;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.dto.shop.ShopRuleDto;
import com.project.school_flate.util.Result.Result;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.service.shop.ShopRuleService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "店铺规则")
@RequestMapping("/shopRule")
public class ShopRuleController {

    @Autowired
    private ShopRuleService oShopRuleService;

    /**
     * 获取店铺规则
     * @param oShopRuleDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取店铺规则")
    @PostMapping(value = "getShopRule", produces = "application/json;charset=utf-8")
    public Result getShopRule(@RequestBody ShopRuleDto oShopRuleDto) throws Exception{
        return oShopRuleService.getShopRule(oShopRuleDto);
    }

    /**
     * 添加店铺规则
     * @param oShopRuleDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加店铺规则")
    @PostMapping(value = "addShopRule", produces = "application/json;charset=utf-8")
    public Result addShopRule(@RequestBody ShopRuleDto oShopRuleDto) throws Exception{
        return oShopRuleService.addShopRule(oShopRuleDto);
    }

    /**
     * 修改店铺规则
     * @param oShopRuleDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改店铺规则")
    @PostMapping(value = "updateShopRule", produces = "application/json;charset=utf-8")
    public Result updateShopRule(@RequestBody ShopRuleDto oShopRuleDto) throws Exception{
        return oShopRuleService.updateShopRule(oShopRuleDto);
    }

    /**
     * 删除店铺规则
     * @param oShopRuleDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除店铺规则")
    @PostMapping(value = "deleteShopRule", produces = "application/json;charset=utf-8")
    public Result deleteShopRule(@RequestBody ShopRuleDto oShopRuleDto) throws Exception{
        return oShopRuleService.deleteShopRule(oShopRuleDto);
    }

    /**
     * 批量添加店铺规则
     * @param oShopRuleDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "批量添加店铺规则")
    @PostMapping(value = "batchAddShopRule", produces = "application/json;charset=utf-8")
    public Result batchAddShopRule(@RequestBody ShopRuleDto oShopRuleDto) throws Exception{
        return oShopRuleService.batchAddShopRule(oShopRuleDto);
    }

    /**
     * 批量添加储蓄规则
     * @param oShopRuleDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "批量添加储蓄规则")
    @PostMapping(value = "batchAddShopRuleToSave", produces = "application/json;charset=utf-8")
    public Result batchAddShopRuleToSave(@RequestBody ShopRuleDto oShopRuleDto) throws Exception{
        return oShopRuleService.batchAddShopRuleToSave(oShopRuleDto);
    }

}
