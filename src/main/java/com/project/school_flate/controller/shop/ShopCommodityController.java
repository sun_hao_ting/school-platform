package com.project.school_flate.controller.shop;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.shop.ShopCommodityDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.shop.ShopCommodity;
import com.project.school_flate.service.shop.ShopCommodityService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "商品")
@RequestMapping("/shopCommodity")
public class ShopCommodityController {

    @Autowired
    private ShopCommodityService oShopCommodityService;

    /**
     * 获取商品
     * @param oShopCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取商品")
    @PostMapping(value = "getShopCommodity", produces = "application/json;charset=utf-8")
    public Result getShopCommodity(@RequestBody ShopCommodityDto oShopCommodityDto) throws Exception{
        return oShopCommodityService.getShopCommodity(oShopCommodityDto);
    }

    /**
     * 添加商品
     * @param oShopCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加商品")
    @PostMapping(value = "addShopCommodity", produces = "application/json;charset=utf-8")
    public Result addShopCommodity(@RequestBody ShopCommodityDto oShopCommodityDto) throws Exception{
        return oShopCommodityService.addShopCommodity(oShopCommodityDto);
    }

    /**
     * 修改商品
     * @param oShopCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改商品")
    @PostMapping(value = "updateShopCommodity", produces = "application/json;charset=utf-8")
    public Result updateShopCommodity(@RequestBody ShopCommodityDto oShopCommodityDto) throws Exception{
        return oShopCommodityService.updateShopCommodity(oShopCommodityDto);
    }

    /**
     * 删除商品
     * @param oShopCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除商品")
    @PostMapping(value = "deleteShopCommodity", produces = "application/json;charset=utf-8")
    public Result deleteShopCommodity(@RequestBody ShopCommodityDto oShopCommodityDto) throws Exception{
        return oShopCommodityService.deleteShopCommodity(oShopCommodityDto);
    }

    /**
     * 获取商品（客户）
     * @param oShopCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取商品（客户）")
    @PostMapping(value = "getShopCommodityUser", produces = "application/json;charset=utf-8")
    public Result getShopCommodityUser(@RequestBody ShopCommodityDto oShopCommodityDto) throws Exception{
        return oShopCommodityService.getShopCommodityUser(oShopCommodityDto);
    }

    /**
     * 获取推荐商品（客户）
     * @param oShopCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取推荐商品（客户）")
    @PostMapping(value = "getRecommendShopCommodityUser", produces = "application/json;charset=utf-8")
    public Result getRecommendShopCommodityUser(@RequestBody ShopCommodityDto oShopCommodityDto) throws Exception{
        return oShopCommodityService.getRecommendShopCommodityUser(oShopCommodityDto);
    }

}
