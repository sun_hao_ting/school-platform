package com.project.school_flate.controller.shop;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.shop.ShopCollectDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.shop.ShopCollect;
import com.project.school_flate.service.shop.ShopCollectService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "店铺收藏")
@RequestMapping("/shopCollect")
public class ShopCollectController {

    @Autowired
    private ShopCollectService oShopCollectService;

    /**
     * 获取店铺收藏
     * @param oShopCollectDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取店铺收藏")
    @PostMapping(value = "getShopCollect", produces = "application/json;charset=utf-8")
    public Result getShopCollect(@RequestBody ShopCollectDto oShopCollectDto) throws Exception{
        return oShopCollectService.getShopCollect(oShopCollectDto);
    }

    /**
     * 添加店铺收藏
     * @param oShopCollectDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加店铺收藏")
    @PostMapping(value = "addShopCollect", produces = "application/json;charset=utf-8")
    public Result addShopCollect(@RequestBody ShopCollectDto oShopCollectDto) throws Exception{
        return oShopCollectService.addShopCollect(oShopCollectDto);
    }

    /**
     * 修改店铺收藏
     * @param oShopCollectDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改店铺收藏")
    @PostMapping(value = "updateShopCollect", produces = "application/json;charset=utf-8")
    public Result updateShopCollect(@RequestBody ShopCollectDto oShopCollectDto) throws Exception{
        return oShopCollectService.updateShopCollect(oShopCollectDto);
    }

    /**
     * 删除店铺收藏
     * @param oShopCollectDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除店铺收藏")
    @PostMapping(value = "deleteShopCollect", produces = "application/json;charset=utf-8")
    public Result deleteShopCollect(@RequestBody ShopCollectDto oShopCollectDto) throws Exception{
        return oShopCollectService.deleteShopCollect(oShopCollectDto);
    }

}
