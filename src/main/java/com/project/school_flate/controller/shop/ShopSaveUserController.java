package com.project.school_flate.controller.shop;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.alibaba.fastjson2.JSONObject;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.shop.ShopInfoDto;
import com.project.school_flate.dto.shop.ShopSaveUserDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.shop.ShopSaveUser;
import com.project.school_flate.service.shop.ShopSaveUserService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@RestController
@Tag(name = "店铺用户储蓄")
@RequestMapping("/shopSaveUser")
public class ShopSaveUserController {

    @Autowired
    private ShopSaveUserService oShopSaveUserService;

    /**
     * 获取店铺用户储蓄
     * @param oShopSaveUserDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取店铺用户储蓄")
    @PostMapping(value = "getShopSaveUser", produces = "application/json;charset=utf-8")
    public Result getShopSaveUser(@RequestBody ShopSaveUserDto oShopSaveUserDto) throws Exception{
        return oShopSaveUserService.getShopSaveUser(oShopSaveUserDto);
    }

    /**
     * 添加店铺用户储蓄
     * @param oShopSaveUserDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "添加店铺用户储蓄")
    @PostMapping(value = "addShopSaveUser", produces = "application/json;charset=utf-8")
    public Result addShopSaveUser(@RequestBody ShopSaveUserDto oShopSaveUserDto) throws Exception{
        return oShopSaveUserService.addShopSaveUser(oShopSaveUserDto);
    }

    /**
     * 修改店铺用户储蓄
     * @param oShopSaveUserDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "修改店铺用户储蓄")
    @PostMapping(value = "updateShopSaveUser", produces = "application/json;charset=utf-8")
    public Result updateShopSaveUser(@RequestBody ShopSaveUserDto oShopSaveUserDto) throws Exception{
        return oShopSaveUserService.updateShopSaveUser(oShopSaveUserDto);
    }

    /**
     * 删除店铺用户储蓄
     * @param oShopSaveUserDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "删除店铺用户储蓄")
    @PostMapping(value = "deleteShopSaveUser", produces = "application/json;charset=utf-8")
    public Result deleteShopSaveUser(@RequestBody ShopSaveUserDto oShopSaveUserDto) throws Exception{
        return oShopSaveUserService.deleteShopSaveUser(oShopSaveUserDto);
    }

    /**
     * 客户充值店铺储蓄
     * @param oShopSaveUserDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "客户充值店铺储蓄")
    @PostMapping(value = "rechargeShopSaveUser", produces = "application/json;charset=utf-8")
    public Result rechargeShopSaveUser(@RequestBody ShopSaveUserDto oShopSaveUserDto) throws Exception{
        return oShopSaveUserService.rechargeShopSaveUser(oShopSaveUserDto);
    }

    /**
     * 客户充值店铺储蓄（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    @Operation(summary = "客户充值店铺储蓄（回调）")
    @PostMapping(value = "rechargeShopSaveUserCallback", produces = "application/json;charset=utf-8")
    public Map<String, String> rechargeShopSaveUserCallback(@RequestBody JSONObject jsonObject) throws Exception{
        return oShopSaveUserService.rechargeShopSaveUserCallback(jsonObject);
    }

    /**
     * 获取用户储蓄余额
     * @param oShopSaveUserDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取用户储蓄余额")
    @PostMapping(value = "getShopSaveUserBalance", produces = "application/json;charset=utf-8")
    public Result getShopSaveUserBalance(@RequestBody ShopSaveUserDto oShopSaveUserDto) throws Exception{
        return oShopSaveUserService.getShopSaveUserBalance(oShopSaveUserDto);
    }

    /**
     * 获取用户储蓄店铺
     * @param oShopSaveUserDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取用户储蓄店铺")
    @PostMapping(value = "getShopSaveUserShop", produces = "application/json;charset=utf-8")
    public Result getShopSaveUserShop(@RequestBody ShopSaveUserDto oShopSaveUserDto) throws Exception{
        return oShopSaveUserService.getShopSaveUserShop(oShopSaveUserDto);
    }

    /**
     * 获取店铺用户储蓄总额余额
     * @param oShopSaveUserDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取店铺用户储蓄总额余额")
    @PostMapping(value = "getShopSaveUserTotal", produces = "application/json;charset=utf-8")
    public Result getShopSaveUserTotal(@RequestBody ShopSaveUserDto oShopSaveUserDto) throws Exception{
        return oShopSaveUserService.getShopSaveUserTotal(oShopSaveUserDto);
    }

}
