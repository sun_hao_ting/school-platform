package com.project.school_flate.controller.shop;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.shop.ShopCommodityEvaluateLikeDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.shop.ShopCommodityEvaluateLike;
import com.project.school_flate.service.shop.ShopCommodityEvaluateLikeService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "商品评论点赞")
@RequestMapping("/shopCommodityEvaluateLike")
public class ShopCommodityEvaluateLikeController {

    @Autowired
    private ShopCommodityEvaluateLikeService oShopCommodityEvaluateLikeService;

    /**
     * 获取商品评论点赞
     * @param oShopCommodityEvaluateLikeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取商品评论点赞")
    @PostMapping(value = "getShopCommodityEvaluateLike", produces = "application/json;charset=utf-8")
    public Result getShopCommodityEvaluateLike(@RequestBody ShopCommodityEvaluateLikeDto oShopCommodityEvaluateLikeDto) throws Exception{
        return oShopCommodityEvaluateLikeService.getShopCommodityEvaluateLike(oShopCommodityEvaluateLikeDto);
    }

    /**
     * 添加商品评论点赞
     * @param oShopCommodityEvaluateLikeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加商品评论点赞")
    @PostMapping(value = "addShopCommodityEvaluateLike", produces = "application/json;charset=utf-8")
    public Result addShopCommodityEvaluateLike(@RequestBody ShopCommodityEvaluateLikeDto oShopCommodityEvaluateLikeDto) throws Exception{
        return oShopCommodityEvaluateLikeService.addShopCommodityEvaluateLike(oShopCommodityEvaluateLikeDto);
    }

    /**
     * 修改商品评论点赞
     * @param oShopCommodityEvaluateLikeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改商品评论点赞")
    @PostMapping(value = "updateShopCommodityEvaluateLike", produces = "application/json;charset=utf-8")
    public Result updateShopCommodityEvaluateLike(@RequestBody ShopCommodityEvaluateLikeDto oShopCommodityEvaluateLikeDto) throws Exception{
        return oShopCommodityEvaluateLikeService.updateShopCommodityEvaluateLike(oShopCommodityEvaluateLikeDto);
    }

    /**
     * 删除商品评论点赞
     * @param oShopCommodityEvaluateLikeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除商品评论点赞")
    @PostMapping(value = "deleteShopCommodityEvaluateLike", produces = "application/json;charset=utf-8")
    public Result deleteShopCommodityEvaluateLike(@RequestBody ShopCommodityEvaluateLikeDto oShopCommodityEvaluateLikeDto) throws Exception{
        return oShopCommodityEvaluateLikeService.deleteShopCommodityEvaluateLike(oShopCommodityEvaluateLikeDto);
    }

    /**
     * 获取商品评论点赞（用户）
     * @param oShopCommodityEvaluateLikeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取商品评论点赞（用户）")
    @PostMapping(value = "getShopCommodityEvaluateLikeUser", produces = "application/json;charset=utf-8")
    public Result getShopCommodityEvaluateLikeUser(@RequestBody ShopCommodityEvaluateLikeDto oShopCommodityEvaluateLikeDto) throws Exception{
        return oShopCommodityEvaluateLikeService.getShopCommodityEvaluateLikeUser(oShopCommodityEvaluateLikeDto);
    }

}
