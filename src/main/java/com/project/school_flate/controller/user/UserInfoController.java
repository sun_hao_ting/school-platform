package com.project.school_flate.controller.user;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.alibaba.fastjson2.JSONObject;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.user.UserInfoDto;
import com.project.school_flate.entity.user.UserInfo;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.service.user.UserInfoService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@RestController
@Tag(name = "用户")
@RequestMapping("/userInfo")
public class UserInfoController {

    @Autowired
    private UserInfoService oUserInfoService;

    /**
     * 获取用户
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取用户")
    @PostMapping(value = "getUserInfo", produces = "application/json;charset=utf-8")
    public Result getUserInfo(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oUserInfoService.getUserInfo(oUserInfoDto);
    }

    /**
     * 添加用户
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "添加用户")
    @PostMapping(value = "addUserInfo", produces = "application/json;charset=utf-8")
    public Result addUserInfo(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oUserInfoService.addUserInfo(oUserInfoDto);
    }

    /**
     * 修改用户
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "修改用户")
    @PostMapping(value = "updateUserInfo", produces = "application/json;charset=utf-8")
    public Result updateUserInfo(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oUserInfoService.updateUserInfo(oUserInfoDto);
    }

    /**
     * 冻结用户
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "冻结用户")
    @PostMapping(value = "freezeUserInfo", produces = "application/json;charset=utf-8")
    public Result freezeUserInfo(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oUserInfoService.freezeUserInfo(oUserInfoDto);
    }

    /**
     * 获取用户（后台）
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取用户（后台）")
    @PostMapping(value = "getUserInfoBack", produces = "application/json;charset=utf-8")
    public Result getUserInfoBack(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oUserInfoService.getUserInfoBack(oUserInfoDto);
    }

    /**
     * 解冻用户
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "解冻用户")
    @PostMapping(value = "thawUserInfo", produces = "application/json;charset=utf-8")
    public Result thawUserInfo(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oUserInfoService.thawUserInfo(oUserInfoDto);
    }

    /**
     * 用户填写推广
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "用户填写推广")
    @PostMapping(value = "promotionUserInfo", produces = "application/json;charset=utf-8")
    public Result promotionUserInfo(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oUserInfoService.promotionUserInfo(oUserInfoDto);
    }

    /**
     * 用户公众号登录
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "用户公众号登录")
    @PostMapping(value = "loginUserInfoOfficial", produces = "application/json;charset=utf-8")
    public Result loginUserInfoOfficial(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oUserInfoService.loginUserInfoOfficial(oUserInfoDto);
    }

    /**
     * 用户绑定手机号
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "用户绑定手机号")
    @PostMapping(value = "bindingUserInfoPhone", produces = "application/json;charset=utf-8")
    public Result bindingUserInfoPhone(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oUserInfoService.bindingUserInfoPhone(oUserInfoDto);
    }

    /**
     * 获取用户推广的用户
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取用户推广的用户")
    @PostMapping(value = "getPromotionUserInfo", produces = "application/json;charset=utf-8")
    public Result getPromotionUserInfo(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oUserInfoService.getPromotionUserInfo(oUserInfoDto);
    }

    /**
     * 获取用户推广的储蓄用户
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取用户推广的储蓄用户")
    @PostMapping(value = "getSavePromotionUserInfo", produces = "application/json;charset=utf-8")
    public Result getSavePromotionUserInfo(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oUserInfoService.getSavePromotionUserInfo(oUserInfoDto);
    }

    /**
     * 获取成为专业分销商金额价格
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取成为专业分销商金额价格")
    @PostMapping(value = "getInfoSpecialityDistributionMoney", produces = "application/json;charset=utf-8")
    public Result getInfoSpecialityDistributionMoney(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oUserInfoService.getInfoSpecialityDistributionMoney(oUserInfoDto);
    }

    /**
     * 用户成为专业分销商
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "用户成为普通分销商")
    @PostMapping(value = "becomeUserInfoSpecialityDistribution", produces = "application/json;charset=utf-8")
    public Result becomeUserInfoSpecialityDistribution(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oUserInfoService.becomeUserInfoSpecialityDistribution(oUserInfoDto);
    }

    /**
     * 用户成为专业分销商（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    @Operation(summary = "用户成为普通分销商（回调）")
    @PostMapping(value = "becomeUserInfoSpecialityDistributionCallback", produces = "application/json;charset=utf-8")
    public Map<String, String> becomeUserInfoSpecialityDistributionCallback(@RequestBody JSONObject jsonObject) throws Exception{
        return oUserInfoService.becomeUserInfoSpecialityDistributionCallback(jsonObject);
    }

    /**
     * 获取用户分销商等级规则
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取用户分销商等级规则")
    @PostMapping(value = "getUserInfoDistributionGrade", produces = "application/json;charset=utf-8")
    public Result getUserInfoDistributionGrade(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oUserInfoService.getUserInfoDistributionGrade(oUserInfoDto);
    }

}
