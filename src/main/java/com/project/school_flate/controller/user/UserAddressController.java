package com.project.school_flate.controller.user;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.dto.user.UserAddressDto;
import com.project.school_flate.util.Result.Result;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.service.user.UserAddressService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "配送地址")
@RequestMapping("/userAddress")
public class UserAddressController {

    @Autowired
    private UserAddressService oUserAddressService;

    /**
     * 获取配送地址
     * @param oUserAddressDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取配送地址")
    @PostMapping(value = "getUserAddress", produces = "application/json;charset=utf-8")
    public Result getUserAddress(@RequestBody UserAddressDto oUserAddressDto) throws Exception{
        return oUserAddressService.getUserAddress(oUserAddressDto);
    }

    /**
     * 添加配送地址
     * @param oUserAddressDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加配送地址")
    @PostMapping(value = "addUserAddress", produces = "application/json;charset=utf-8")
    public Result addUserAddress(@RequestBody UserAddressDto oUserAddressDto) throws Exception{
        return oUserAddressService.addUserAddress(oUserAddressDto);
    }

    /**
     * 修改配送地址
     * @param oUserAddressDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改配送地址")
    @PostMapping(value = "updateUserAddress", produces = "application/json;charset=utf-8")
    public Result updateUserAddress(@RequestBody UserAddressDto oUserAddressDto) throws Exception{
        return oUserAddressService.updateUserAddress(oUserAddressDto);
    }

    /**
     * 删除配送地址
     * @param oUserAddressDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除配送地址")
    @PostMapping(value = "deleteUserAddress", produces = "application/json;charset=utf-8")
    public Result deleteUserAddress(@RequestBody UserAddressDto oUserAddressDto) throws Exception{
        return oUserAddressService.deleteUserAddress(oUserAddressDto);
    }

    /**
     * 获取用户最近一次使用的配送地址
     * @param oUserAddressDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取用户最近一次使用的配送地址")
    @PostMapping(value = "getUserAddressRecently", produces = "application/json;charset=utf-8")
    public Result getUserAddressRecently(@RequestBody UserAddressDto oUserAddressDto) throws Exception{
        return oUserAddressService.getUserAddressRecently(oUserAddressDto);
    }

}
