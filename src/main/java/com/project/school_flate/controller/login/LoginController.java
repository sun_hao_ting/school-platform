package com.project.school_flate.controller.login;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.dto.login.Login;
import com.project.school_flate.service.login.LoginService;
import com.project.school_flate.util.Result.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author sunht
 * @Date 2023/10/2 17:49
 * @Version v1.0.0
 * @Message
 **/
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@RequestMapping("/login")
@Tag(name = "登录管理器", description = "登录管理器")
public class LoginController {
    @Autowired
    private LoginService loginService;

    /**
     * 系统用户登录接口
     * @param login
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @PostMapping(value = "/systemLogin", produces = "application/json;charset=utf-8")
    @Operation(summary = "系统用户登录接口")
    public Result systemLogin(@RequestBody Login login) throws Exception{
        return loginService.systemLogin(login);
    }

    /**
     * 客户微信登录
     * @param login
     * @return
     * @throws Exception
     */
    @PostMapping("/wxClientLogin")
    @Operation(summary = "客户微信登录")
    public Result wxClientLogin(@RequestBody Login login) throws Exception{
        return loginService.wxClientLogin(login);
    }

    /**
     * 登录注销
     * @param
     * @return
     * @throws Exception
     */
    @GetMapping("/loginOut")
    @Operation(summary = "登录注销")
    public Result loginOut() throws Exception{
        return loginService.loginOut();
    }

}
