package com.project.school_flate.controller;

import com.alibaba.fastjson2.JSONObject;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.TextMessage;
import java.util.HashMap;
import java.util.List;

@Component
public class HelloListener {

//    @JmsListener(destination = "${spring.activemq.queue-name}", containerFactory = "queueListener")
//    @JmsListener(destination = "aabb", containerFactory = "queueListener")
//    public void readActiveQueue(TextMessage textMessage) throws JMSException {
//        System.out.println("接受消息时间:" + System.currentTimeMillis());
//        System.out.println("queue接受到：" + textMessage.getText());
//        //关闭自动ack
//        textMessage.acknowledge();
//    }
    @JmsListener(destination = "aabb", containerFactory = "topicListener")
    public void readActiveTopic(TextMessage textMessage) throws JMSException {
        System.out.println("接受消息时间:" + System.currentTimeMillis());
        System.out.println("queue接受到：" + textMessage.getText());
        //关闭自动ack
        textMessage.acknowledge();
    }


}
