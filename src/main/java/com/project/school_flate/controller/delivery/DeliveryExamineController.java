package com.project.school_flate.controller.delivery;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.delivery.DeliveryExamineDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.delivery.DeliveryExamine;
import com.project.school_flate.service.delivery.DeliveryExamineService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "配送员审核")
@RequestMapping("/deliveryExamine")
public class DeliveryExamineController {

    @Autowired
    private DeliveryExamineService oDeliveryExamineService;

    /**
     * 获取配送员审核
     * @param oDeliveryExamineDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取配送员审核")
    @PostMapping(value = "getDeliveryExamine", produces = "application/json;charset=utf-8")
    public Result getDeliveryExamine(@RequestBody DeliveryExamineDto oDeliveryExamineDto) throws Exception{
        return oDeliveryExamineService.getDeliveryExamine(oDeliveryExamineDto);
    }

    /**
     * 添加配送员审核
     * @param oDeliveryExamineDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加配送员审核")
    @PostMapping(value = "addDeliveryExamine", produces = "application/json;charset=utf-8")
    public Result addDeliveryExamine(@RequestBody DeliveryExamineDto oDeliveryExamineDto) throws Exception{
        return oDeliveryExamineService.addDeliveryExamine(oDeliveryExamineDto);
    }

    /**
     * 修改配送员审核
     * @param oDeliveryExamineDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改配送员审核")
    @PostMapping(value = "updateDeliveryExamine", produces = "application/json;charset=utf-8")
    public Result updateDeliveryExamine(@RequestBody DeliveryExamineDto oDeliveryExamineDto) throws Exception{
        return oDeliveryExamineService.updateDeliveryExamine(oDeliveryExamineDto);
    }

    /**
     * 删除配送员审核
     * @param oDeliveryExamineDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除配送员审核")
    @PostMapping(value = "deleteDeliveryExamine", produces = "application/json;charset=utf-8")
    public Result deleteDeliveryExamine(@RequestBody DeliveryExamineDto oDeliveryExamineDto) throws Exception{
        return oDeliveryExamineService.deleteDeliveryExamine(oDeliveryExamineDto);
    }

}
