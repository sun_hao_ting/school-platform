package com.project.school_flate.controller.delivery;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.delivery.DeliveryInfoDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.delivery.DeliveryInfo;
import com.project.school_flate.service.delivery.DeliveryInfoService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "配送员")
@RequestMapping("/deliveryInfo")
public class DeliveryInfoController {

    @Autowired
    private DeliveryInfoService oDeliveryInfoService;

    /**
     * 获取配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取配送员")
    @PostMapping(value = "getDeliveryInfo", produces = "application/json;charset=utf-8")
    public Result getDeliveryInfo(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oDeliveryInfoService.getDeliveryInfo(oDeliveryInfoDto);
    }

    /**
     * 添加配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加配送员")
    @PostMapping(value = "addDeliveryInfo", produces = "application/json;charset=utf-8")
    public Result addDeliveryInfo(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oDeliveryInfoService.addDeliveryInfo(oDeliveryInfoDto);
    }

    /**
     * 修改配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改配送员")
    @PostMapping(value = "updateDeliveryInfo", produces = "application/json;charset=utf-8")
    public Result updateDeliveryInfo(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oDeliveryInfoService.updateDeliveryInfo(oDeliveryInfoDto);
    }

    /**
     * 冻结配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "冻结配送员")
    @PostMapping(value = "freezeDeliveryInfo", produces = "application/json;charset=utf-8")
    public Result freezeDeliveryInfo(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oDeliveryInfoService.freezeDeliveryInfo(oDeliveryInfoDto);
    }

    /**
     * 获取配送员（后台）
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取配送员（后台）")
    @PostMapping(value = "getDeliveryInfoBack", produces = "application/json;charset=utf-8")
    public Result getDeliveryInfoBack(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oDeliveryInfoService.getDeliveryInfoBack(oDeliveryInfoDto);
    }

    /**
     * 解冻配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "解冻配送员")
    @PostMapping(value = "thawDeliveryInfo", produces = "application/json;charset=utf-8")
    public Result thawDeliveryInfo(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oDeliveryInfoService.thawDeliveryInfo(oDeliveryInfoDto);
    }

    /**
     * 注册配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "注册配送员")
    @PostMapping(value = "registerDeliveryInfo", produces = "application/json;charset=utf-8")
    public Result registerDeliveryInfo(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oDeliveryInfoService.registerDeliveryInfo(oDeliveryInfoDto);
    }

    /**
     * 登录配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "登录配送员")
    @PostMapping(value = "loginDeliveryInfo", produces = "application/json;charset=utf-8")
    public Result loginDeliveryInfo(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oDeliveryInfoService.loginDeliveryInfo(oDeliveryInfoDto);
    }

    /**
     * 退出配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "退出配送员")
    @PostMapping(value = "exitDeliveryInfo", produces = "application/json;charset=utf-8")
    public Result exitDeliveryInfo(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oDeliveryInfoService.exitDeliveryInfo(oDeliveryInfoDto);
    }

    /**
     * 配送员修改密码
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "配送员修改密码")
    @PostMapping(value = "updateDeliveryInfoPassword", produces = "application/json;charset=utf-8")
    public Result updateDeliveryInfoPassword(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oDeliveryInfoService.updateDeliveryInfoPassword(oDeliveryInfoDto);
    }

    /**
     * 公众号登录配送员
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "公众号登录配送员")
    @PostMapping(value = "loginDeliveryInfoOfficial", produces = "application/json;charset=utf-8")
    public Result loginDeliveryInfoOfficial(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oDeliveryInfoService.loginDeliveryInfoOfficial(oDeliveryInfoDto);
    }

}
