package com.project.school_flate.controller.delivery;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.delivery.DeliveryEvaluateDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.delivery.DeliveryEvaluate;
import com.project.school_flate.service.delivery.DeliveryEvaluateService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "配送员评价")
@RequestMapping("/deliveryEvaluate")
public class DeliveryEvaluateController {

    @Autowired
    private DeliveryEvaluateService oDeliveryEvaluateService;

    /**
     * 获取配送员评价
     * @param oDeliveryEvaluateDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取配送员评价")
    @PostMapping(value = "getDeliveryEvaluate", produces = "application/json;charset=utf-8")
    public Result getDeliveryEvaluate(@RequestBody DeliveryEvaluateDto oDeliveryEvaluateDto) throws Exception{
        return oDeliveryEvaluateService.getDeliveryEvaluate(oDeliveryEvaluateDto);
    }

    /**
     * 添加配送员评价
     * @param oDeliveryEvaluateDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加配送员评价")
    @PostMapping(value = "addDeliveryEvaluate", produces = "application/json;charset=utf-8")
    public Result addDeliveryEvaluate(@RequestBody DeliveryEvaluateDto oDeliveryEvaluateDto) throws Exception{
        return oDeliveryEvaluateService.addDeliveryEvaluate(oDeliveryEvaluateDto);
    }

    /**
     * 修改配送员评价
     * @param oDeliveryEvaluateDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改配送员评价")
    @PostMapping(value = "updateDeliveryEvaluate", produces = "application/json;charset=utf-8")
    public Result updateDeliveryEvaluate(@RequestBody DeliveryEvaluateDto oDeliveryEvaluateDto) throws Exception{
        return oDeliveryEvaluateService.updateDeliveryEvaluate(oDeliveryEvaluateDto);
    }

    /**
     * 删除配送员评价
     * @param oDeliveryEvaluateDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除配送员评价")
    @PostMapping(value = "deleteDeliveryEvaluate", produces = "application/json;charset=utf-8")
    public Result deleteDeliveryEvaluate(@RequestBody DeliveryEvaluateDto oDeliveryEvaluateDto) throws Exception{
        return oDeliveryEvaluateService.deleteDeliveryEvaluate(oDeliveryEvaluateDto);
    }

}
