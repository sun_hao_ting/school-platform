package com.project.school_flate.controller.integral;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.project.school_flate.dto.integral.IntegralExchangeSaveRecordDto;
import com.project.school_flate.dto.integral.IntegralExchangeSaveRecordDto;
import com.project.school_flate.entity.integral.IntegralExchangeSaveRecord;
import com.project.school_flate.util.PoToDTO;
import com.project.school_flate.util.Result.Result;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.integral.IntegralExchangeSaveRecord;
import com.project.school_flate.service.integral.IntegralExchangeSaveRecordService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "积分兑换储蓄记录")
@RequestMapping("/integralExchangeSaveRecord")
public class IntegralExchangeSaveRecordController {

    @Autowired
    private IntegralExchangeSaveRecordService oIntegralExchangeSaveRecordService;

    /**
     * 获取积分兑换储蓄记录
     * @param oIntegralExchangeSaveRecordDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取积分兑换储蓄记录")
    @PostMapping(value = "getIntegralExchangeSaveRecord", produces = "application/json;charset=utf-8")
    public Result getIntegralExchangeSaveRecord(@RequestBody IntegralExchangeSaveRecordDto oIntegralExchangeSaveRecordDto) throws Exception{
        return oIntegralExchangeSaveRecordService.getIntegralExchangeSaveRecord(oIntegralExchangeSaveRecordDto);
    }

    /**
     * 添加积分兑换储蓄记录
     * @param oIntegralExchangeSaveRecordDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加积分兑换储蓄记录")
    @PostMapping(value = "addIntegralExchangeSaveRecord", produces = "application/json;charset=utf-8")
    public Result addIntegralExchangeSaveRecord(@RequestBody IntegralExchangeSaveRecordDto oIntegralExchangeSaveRecordDto) throws Exception{
        return oIntegralExchangeSaveRecordService.addIntegralExchangeSaveRecord(oIntegralExchangeSaveRecordDto);
    }

    /**
     * 修改积分兑换储蓄记录
     * @param oIntegralExchangeSaveRecordDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改积分兑换储蓄记录")
    @PostMapping(value = "updateIntegralExchangeSaveRecord", produces = "application/json;charset=utf-8")
    public Result updateIntegralExchangeSaveRecord(@RequestBody IntegralExchangeSaveRecordDto oIntegralExchangeSaveRecordDto) throws Exception{
        return oIntegralExchangeSaveRecordService.updateIntegralExchangeSaveRecord(oIntegralExchangeSaveRecordDto);
    }

    /**
     * 删除积分兑换储蓄记录
     * @param oIntegralExchangeSaveRecordDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除积分兑换储蓄记录")
    @PostMapping(value = "deleteIntegralExchangeSaveRecord", produces = "application/json;charset=utf-8")
    public Result deleteIntegralExchangeSaveRecord(@RequestBody IntegralExchangeSaveRecordDto oIntegralExchangeSaveRecordDto) throws Exception{
        return oIntegralExchangeSaveRecordService.deleteIntegralExchangeSaveRecord(oIntegralExchangeSaveRecordDto);
    }

}
