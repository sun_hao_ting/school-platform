package com.project.school_flate.controller.integral;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.integral.IntegralCommodityDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.integral.IntegralCommodity;
import com.project.school_flate.service.integral.IntegralCommodityService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "积分商品")
@RequestMapping("/integralCommodity")
public class IntegralCommodityController {

    @Autowired
    private IntegralCommodityService oIntegralCommodityService;

    /**
     * 获取积分商品
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取积分商品")
    @PostMapping(value = "getIntegralCommodity", produces = "application/json;charset=utf-8")
    public Result getIntegralCommodity(@RequestBody IntegralCommodityDto oIntegralCommodityDto) throws Exception{
        return oIntegralCommodityService.getIntegralCommodity(oIntegralCommodityDto);
    }

    /**
     * 添加积分商品
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加积分商品")
    @PostMapping(value = "addIntegralCommodity", produces = "application/json;charset=utf-8")
    public Result addIntegralCommodity(@RequestBody IntegralCommodityDto oIntegralCommodityDto) throws Exception{
        return oIntegralCommodityService.addIntegralCommodity(oIntegralCommodityDto);
    }

    /**
     * 修改积分商品
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改积分商品")
    @PostMapping(value = "updateIntegralCommodity", produces = "application/json;charset=utf-8")
    public Result updateIntegralCommodity(@RequestBody IntegralCommodityDto oIntegralCommodityDto) throws Exception{
        return oIntegralCommodityService.updateIntegralCommodity(oIntegralCommodityDto);
    }

    /**
     * 冻结积分商品
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "冻结积分商品")
    @PostMapping(value = "freezeIntegralCommodity", produces = "application/json;charset=utf-8")
    public Result freezeIntegralCommodity(@RequestBody IntegralCommodityDto oIntegralCommodityDto) throws Exception{
        return oIntegralCommodityService.freezeIntegralCommodity(oIntegralCommodityDto);
    }

    /**
     * 获取积分商品（后台）
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取积分商品（后台）")
    @PostMapping(value = "getIntegralCommodityBack", produces = "application/json;charset=utf-8")
    public Result getIntegralCommodityBack(@RequestBody IntegralCommodityDto oIntegralCommodityDto) throws Exception{
        return oIntegralCommodityService.getIntegralCommodityBack(oIntegralCommodityDto);
    }

    /**
     * 解冻积分商品
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "解冻积分商品")
    @PostMapping(value = "thawIntegralCommodity", produces = "application/json;charset=utf-8")
    public Result thawIntegralCommodity(@RequestBody IntegralCommodityDto oIntegralCommodityDto) throws Exception{
        return oIntegralCommodityService.thawIntegralCommodity(oIntegralCommodityDto);
    }

    /**
     * 获取积分商品（用户）
     * @param oIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取积分商品（用户）")
    @PostMapping(value = "getIntegralCommodityUser", produces = "application/json;charset=utf-8")
    public Result getIntegralCommodityUser(@RequestBody IntegralCommodityDto oIntegralCommodityDto) throws Exception{
        return oIntegralCommodityService.getIntegralCommodityUser(oIntegralCommodityDto);
    }

}
