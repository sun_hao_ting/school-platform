package com.project.school_flate.controller.integral;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.dto.integral.IntegralExchangeSaveDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.service.integral.IntegralExchangeSaveService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "积分兑换储蓄")
@RequestMapping("/integralExchange")
public class IntegralExchangeSaveController {

    @Autowired
    private IntegralExchangeSaveService oIntegralExchangeSaveService;

    /**
     * 获取积分兑换
     * @param oIntegralExchangeSaveDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取积分兑换")
    @PostMapping(value = "getIntegralExchangeSave", produces = "application/json;charset=utf-8")
    public Result getIntegralExchangeSave(@RequestBody IntegralExchangeSaveDto oIntegralExchangeSaveDto) throws Exception{
        return oIntegralExchangeSaveService.getIntegralExchangeSave(oIntegralExchangeSaveDto);
    }

    /**
     * 添加积分兑换
     * @param oIntegralExchangeSaveDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加积分兑换")
    @PostMapping(value = "addIntegralExchangeSave", produces = "application/json;charset=utf-8")
    public Result addIntegralExchangeSave(@RequestBody IntegralExchangeSaveDto oIntegralExchangeSaveDto) throws Exception{
        return oIntegralExchangeSaveService.addIntegralExchangeSave(oIntegralExchangeSaveDto);
    }

    /**
     * 修改积分兑换
     * @param oIntegralExchangeSaveDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改积分兑换")
    @PostMapping(value = "updateIntegralExchangeSave", produces = "application/json;charset=utf-8")
    public Result updateIntegralExchangeSave(@RequestBody IntegralExchangeSaveDto oIntegralExchangeSaveDto) throws Exception{
        return oIntegralExchangeSaveService.updateIntegralExchangeSave(oIntegralExchangeSaveDto);
    }

    /**
     * 删除积分兑换
     * @param oIntegralExchangeSaveDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除积分兑换")
    @PostMapping(value = "deleteIntegralExchangeSave", produces = "application/json;charset=utf-8")
    public Result deleteIntegralExchangeSave(@RequestBody IntegralExchangeSaveDto oIntegralExchangeSaveDto) throws Exception{
        return oIntegralExchangeSaveService.deleteIntegralExchangeSave(oIntegralExchangeSaveDto);
    }

    /**
     * 获取积分兑换（用户）
     * @param oIntegralExchangeSaveDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取积分兑换（用户）")
    @PostMapping(value = "getIntegralExchangeSaveUser", produces = "application/json;charset=utf-8")
    public Result getIntegralExchangeSaveUser(@RequestBody IntegralExchangeSaveDto oIntegralExchangeSaveDto) throws Exception{
        return oIntegralExchangeSaveService.getIntegralExchangeSaveUser(oIntegralExchangeSaveDto);
    }

}
