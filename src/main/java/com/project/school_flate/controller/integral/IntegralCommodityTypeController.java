package com.project.school_flate.controller.integral;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.integral.IntegralCommodityTypeDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.integral.IntegralCommodityType;
import com.project.school_flate.service.integral.IntegralCommodityTypeService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "积分商品分类")
@RequestMapping("/integralCommodityType")
public class IntegralCommodityTypeController {

    @Autowired
    private IntegralCommodityTypeService oIntegralCommodityTypeService;

    /**
     * 获取积分商品分类
     * @param oIntegralCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取积分商品分类")
    @PostMapping(value = "getIntegralCommodityType", produces = "application/json;charset=utf-8")
    public Result getIntegralCommodityType(@RequestBody IntegralCommodityTypeDto oIntegralCommodityTypeDto) throws Exception{
        return oIntegralCommodityTypeService.getIntegralCommodityType(oIntegralCommodityTypeDto);
    }

    /**
     * 添加积分商品分类
     * @param oIntegralCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加积分商品分类")
    @PostMapping(value = "addIntegralCommodityType", produces = "application/json;charset=utf-8")
    public Result addIntegralCommodityType(@RequestBody IntegralCommodityTypeDto oIntegralCommodityTypeDto) throws Exception{
        return oIntegralCommodityTypeService.addIntegralCommodityType(oIntegralCommodityTypeDto);
    }

    /**
     * 修改积分商品分类
     * @param oIntegralCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改积分商品分类")
    @PostMapping(value = "updateIntegralCommodityType", produces = "application/json;charset=utf-8")
    public Result updateIntegralCommodityType(@RequestBody IntegralCommodityTypeDto oIntegralCommodityTypeDto) throws Exception{
        return oIntegralCommodityTypeService.updateIntegralCommodityType(oIntegralCommodityTypeDto);
    }

    /**
     * 删除积分商品分类
     * @param oIntegralCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除积分商品分类")
    @PostMapping(value = "deleteIntegralCommodityType", produces = "application/json;charset=utf-8")
    public Result deleteIntegralCommodityType(@RequestBody IntegralCommodityTypeDto oIntegralCommodityTypeDto) throws Exception{
        return oIntegralCommodityTypeService.deleteIntegralCommodityType(oIntegralCommodityTypeDto);
    }

    /**
     * 获取积分商品分类（用户）
     * @param oIntegralCommodityTypeDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取积分商品分类（用户）")
    @PostMapping(value = "getIntegralCommodityTypeUser", produces = "application/json;charset=utf-8")
    public Result getIntegralCommodityTypeUser(@RequestBody IntegralCommodityTypeDto oIntegralCommodityTypeDto) throws Exception{
        return oIntegralCommodityTypeService.getIntegralCommodityTypeUser(oIntegralCommodityTypeDto);
    }

}
