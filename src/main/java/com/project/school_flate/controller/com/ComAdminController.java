package com.project.school_flate.controller.com;

import cn.dev33.satoken.annotation.SaIgnore;
import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.dto.com.ComAdminDto;
import com.project.school_flate.dto.com.ComAdminDto;
import com.project.school_flate.service.com.ComAdminService;
import com.project.school_flate.util.Result.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author sunht
 * @Date 2023/10/7 9:46
 * @Version v1.0.0
 * @Message
 **/
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@RequestMapping("/comAdmin")
@Tag(name = "管理员")
public class ComAdminController {

    @Autowired
    private ComAdminService oComAdminService;

    /**
     * 获取管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取管理员")
    @PostMapping(value = "getComAdmin", produces = "application/json;charset=utf-8")
    public Result getComAdmin(@RequestBody ComAdminDto oComAdminDto) throws Exception{
        return oComAdminService.getComAdmin(oComAdminDto);
    }

    /**
     * 添加管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加管理员")
    @PostMapping(value = "addComAdmin", produces = "application/json;charset=utf-8")
    public Result addComAdmin(@RequestBody ComAdminDto oComAdminDto) throws Exception{
        return oComAdminService.addComAdmin(oComAdminDto);
    }

    /**
     * 修改管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改管理员")
    @PostMapping(value = "updateComAdmin", produces = "application/json;charset=utf-8")
    public Result updateComAdmin(@RequestBody ComAdminDto oComAdminDto) throws Exception{
        return oComAdminService.updateComAdmin(oComAdminDto);
    }

    /**
     * 冻结管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "冻结管理员")
    @PostMapping(value = "freezeComAdmin", produces = "application/json;charset=utf-8")
    public Result freezeComAdmin(@RequestBody ComAdminDto oComAdminDto) throws Exception{
        return oComAdminService.freezeComAdmin(oComAdminDto);
    }

    /**
     * 获取管理员（后台）
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取管理员（后台）")
    @PostMapping(value = "getComAdminBack", produces = "application/json;charset=utf-8")
    public Result getComAdminBack(@RequestBody ComAdminDto oComAdminDto) throws Exception{
        return oComAdminService.getComAdminBack(oComAdminDto);
    }

    /**
     * 解冻管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "解冻管理员")
    @PostMapping(value = "thawComAdmin", produces = "application/json;charset=utf-8")
    public Result thawComAdmin(@RequestBody ComAdminDto oComAdminDto) throws Exception{
        return oComAdminService.thawComAdmin(oComAdminDto);
    }

    /**
     * 登录管理员
     * @param oComAdminDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "登录管理员")
    @PostMapping(value = "loginComAdmin", produces = "application/json;charset=utf-8")
    public Result loginComAdmin(@RequestBody ComAdminDto oComAdminDto) throws Exception{
        return oComAdminService.loginComAdmin(oComAdminDto);
    }

}
