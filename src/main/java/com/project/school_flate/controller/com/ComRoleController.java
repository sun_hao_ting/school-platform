package com.project.school_flate.controller.com;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.dto.com.ComRoleDto;
import com.project.school_flate.service.com.ComRoleService;
import com.project.school_flate.util.Result.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "角色")
@RequestMapping("/comRole")
public class ComRoleController {

    @Autowired
    private ComRoleService oComRoleService;

    /**
     * 获取角色
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取角色")
    @PostMapping(value = "getComRole", produces = "application/json;charset=utf-8")
    public Result getComRole(@RequestBody ComRoleDto oComRoleDto) throws Exception{
        return oComRoleService.getComRole(oComRoleDto);
    }

    /**
     * 添加角色
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加角色")
    @PostMapping(value = "addComRole", produces = "application/json;charset=utf-8")
    public Result addComRole(@RequestBody ComRoleDto oComRoleDto) throws Exception{
        return oComRoleService.addComRole(oComRoleDto);
    }

    /**
     * 修改角色
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改角色")
    @PostMapping(value = "updateComRole", produces = "application/json;charset=utf-8")
    public Result updateComRole(@RequestBody ComRoleDto oComRoleDto) throws Exception{
        return oComRoleService.updateComRole(oComRoleDto);
    }

    /**
     * 冻结角色
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "冻结角色")
    @PostMapping(value = "freezeComRole", produces = "application/json;charset=utf-8")
    public Result freezeComRole(@RequestBody ComRoleDto oComRoleDto) throws Exception{
        return oComRoleService.freezeComRole(oComRoleDto);
    }

    /**
     * 获取角色（后台）
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取角色（后台）")
    @PostMapping(value = "getComRoleBack", produces = "application/json;charset=utf-8")
    public Result getComRoleBack(@RequestBody ComRoleDto oComRoleDto) throws Exception{
        return oComRoleService.getComRoleBack(oComRoleDto);
    }

    /**
     * 解冻角色
     * @param oComRoleDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "解冻角色")
    @PostMapping(value = "thawComRole", produces = "application/json;charset=utf-8")
    public Result thawComRole(@RequestBody ComRoleDto oComRoleDto) throws Exception{
        return oComRoleService.thawComRole(oComRoleDto);
    }

}
