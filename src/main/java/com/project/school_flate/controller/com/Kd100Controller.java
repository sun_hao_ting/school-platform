package com.project.school_flate.controller.com;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.alibaba.fastjson2.JSONObject;
import com.project.school_flate.service.com.KuaiDi100Service;
import com.project.school_flate.util.Result.Result;
import com.project.school_flate.util.kuidi100.ExpressShipment;
import com.project.school_flate.util.kuidi100.SelectKuaiDi;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@RequestMapping("/Kd100")
@Tag(name = "快递100", description = "快递100管理器")
public class Kd100Controller {

    @Autowired
    private KuaiDi100Service kuaiDi100Service;
    /**
     * 快递寄件
     * @param expressShipment 订单ID
     * @return
     * @throws Exception
     */
    @SaCheckLogin
    @Operation(summary = "快递100寄件")
    @RequestMapping(value = "/expressShipment", method = RequestMethod.POST)
    public Result expressShipment(@RequestBody ExpressShipment expressShipment) throws Exception {
        return kuaiDi100Service.testBorderOfficial(expressShipment);
    }
    /**
     * 查询快递
     * @param selectKuaiDi 订单ID
     * @throws Exception
     */
    @SaCheckLogin
    @Operation(summary = "查询快递")
    @RequestMapping(value = "/getKuaiDi", method = RequestMethod.POST)
    public Result getKuaiDi(@RequestBody SelectKuaiDi selectKuaiDi) throws Exception {
        return kuaiDi100Service.testQueryTrack(selectKuaiDi);
    }

    /**
     * 快递100发货回调
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/rollbackKuaiDi100")
    @ResponseBody
    public Object  rollbackKuaiDi100(HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("result",false);
        map.put("returnCode","500");
        map.put("message","保存失败");
        try {
            String param = request.getParameter("param");
            String deliveryRecordId = request.getParameter("deliveryRecordId");
            JSONObject jsonObject = JSONObject.parseObject(param);
            System.out.println("param>>" + jsonObject.get("data"));
            System.out.println("deliveryRecordId>>" + deliveryRecordId);

            // 处理快递结果
            map.put("result",true);
            map.put("returnCode","200");
            map.put("message","保存成功");
            //这里必须返回，否则认为失败，过30分钟又会重复推送。
            return map;
        } catch (Exception e) {
            map.put("message","保存失败:" + e.getMessage());
            //保存失败，服务端等30分钟会重复推送。
            return map;
        }
    }
}
