package com.project.school_flate.controller.com;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.service.com.WeXinService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

//@DecryptBody(value = DecryptBodyMethod.AES)
//@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "公众号基本配置发送的Token验证")
@Slf4j
public class WeXinController {
    @Autowired
    private WeXinService weXinService;

    public static final String Token="852c980504ca9f3d772f686ea3a2b4c8"; //微信公众平台中设置的token

//    @RequestMapping("/")
//    public void index(HttpServletResponse response, HttpServletRequest request) throws Exception{
//        String method=request.getMethod();
//        //签名验证是get请求
//        if ("GET".equals(method)){
//            // 微信加密签名
//            String signature = request.getParameter("signature");
//            // 随机字符串
//            String echostr = request.getParameter("echostr");
//            // 时间戳
//            String timestamp = request.getParameter("timestamp");
//            // 随机数
//            String nonce = request.getParameter("nonce");
//            String[] str = { Token, timestamp, nonce };
//            // 字典排序
//            Arrays.sort(str);
//            String bigStr = str[0] + str[1] + str[2];
//            // SHA1加密
//            String digest = sha1(bigStr);
//            // 确认请求来至微信
//            if (digest.equals(signature)) {
//                response.getWriter().print(echostr);
//            }
//        }
//    }
//
//    /**
//     * @description: sha1
//     * @author: lvyq
//     * @date: 2022/7/8 15:18
//     * @version 1.0
//     */
//
//    public static String sha1(String data) throws NoSuchAlgorithmException {
//        MessageDigest md = MessageDigest.getInstance("SHA1");
//        //把字符串转为字节数组
//        byte[] b = data.getBytes();
//        //使用指定的字节来更新我们的摘要
//        md.update(b);
//        //获取密文  （完成摘要计算）
//        byte[] b2 = md.digest();
//        //获取计算的长度
//        int len = b2.length;
//        //16进制字符串
//        String str = "0123456789abcdef";
//        //把字符串转为字符串数组
//        char[] ch = str.toCharArray();
//        //创建一个40位长度的字节数组
//        char[] chs = new char[len*2];
//        //循环20次
//        for(int i=0,k=0;i<len;i++) {
//            //获取摘要计算后的字节数组中的每个字节
//            byte b3 = b2[i];
//            // >>>:无符号右移
//            // &:按位与
//            //0xf:0-15的数字
//            chs[k++] = ch[b3 >>> 4 & 0xf];
//            chs[k++] = ch[b3 & 0xf];
//        }
//        //字符数组转为字符串
//        return new String(chs);
//    }

//    @RequestMapping("/error")
//    public void index(HttpServletResponse response, HttpServletRequest request) throws Exception{
//        System.out.println("------------------------------------");
//        System.out.println("报错");
//        System.out.println(request);
//        System.out.println("------------------------------------");
//
//    }

}
