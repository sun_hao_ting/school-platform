package com.project.school_flate.controller.com;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.com.ComSchoolDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.com.ComSchool;
import com.project.school_flate.service.com.ComSchoolService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "校园")
@RequestMapping("/comSchool")
public class ComSchoolController {

    @Autowired
    private ComSchoolService oComSchoolService;

    /**
     * 获取校园
     * @param oComSchoolDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取校园")
    @PostMapping(value = "getComSchool", produces = "application/json;charset=utf-8")
    public Result getComSchool(@RequestBody ComSchoolDto oComSchoolDto) throws Exception{
        return oComSchoolService.getComSchool(oComSchoolDto);
    }

    /**
     * 添加校园
     * @param oComSchoolDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加校园")
    @PostMapping(value = "addComSchool", produces = "application/json;charset=utf-8")
    public Result addComSchool(@RequestBody ComSchoolDto oComSchoolDto) throws Exception{
        return oComSchoolService.addComSchool(oComSchoolDto);
    }

    /**
     * 修改校园
     * @param oComSchoolDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改校园")
    @PostMapping(value = "updateComSchool", produces = "application/json;charset=utf-8")
    public Result updateComSchool(@RequestBody ComSchoolDto oComSchoolDto) throws Exception{
        return oComSchoolService.updateComSchool(oComSchoolDto);
    }

    /**
     * 冻结校园
     * @param oComSchoolDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "冻结校园")
    @PostMapping(value = "freezeComSchool", produces = "application/json;charset=utf-8")
    public Result freezeComSchool(@RequestBody ComSchoolDto oComSchoolDto) throws Exception{
        return oComSchoolService.freezeComSchool(oComSchoolDto);
    }

    /**
     * 获取校园（后台）
     * @param oComSchoolDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取校园（后台）")
    @PostMapping(value = "getComSchoolBack", produces = "application/json;charset=utf-8")
    public Result getComSchoolBack(@RequestBody ComSchoolDto oComSchoolDto) throws Exception{
        return oComSchoolService.getComSchoolBack(oComSchoolDto);
    }

    /**
     * 解冻校园
     * @param oComSchoolDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "解冻校园")
    @PostMapping(value = "thawComSchool", produces = "application/json;charset=utf-8")
    public Result thawComSchool(@RequestBody ComSchoolDto oComSchoolDto) throws Exception{
        return oComSchoolService.thawComSchool(oComSchoolDto);
    }

    /**
     * 获取用户定位校园
     * @param oComSchoolDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取用户定位校园")
    @PostMapping(value = "getUserLocationComSchool", produces = "application/json;charset=utf-8")
    public Result getUserLocationComSchool(@RequestBody ComSchoolDto oComSchoolDto) throws Exception{
        return oComSchoolService.getUserLocationComSchool(oComSchoolDto);
    }

}
