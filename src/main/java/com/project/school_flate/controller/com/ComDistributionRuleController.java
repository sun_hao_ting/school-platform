package com.project.school_flate.controller.com;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.com.ComDistributionRuleDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.com.ComDistributionRule;
import com.project.school_flate.service.com.ComDistributionRuleService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "分销商等级规则")
@RequestMapping("/comDistributionRule")
public class ComDistributionRuleController {

    @Autowired
    private ComDistributionRuleService oComDistributionRuleService;

    /**
     * 获取分销商等级规则
     * @param oComDistributionRuleDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取分销商等级规则")
    @PostMapping(value = "getComDistributionRule", produces = "application/json;charset=utf-8")
    public Result getComDistributionRule(@RequestBody ComDistributionRuleDto oComDistributionRuleDto) throws Exception{
        return oComDistributionRuleService.getComDistributionRule(oComDistributionRuleDto);
    }

    /**
     * 添加分销商等级规则
     * @param oComDistributionRuleDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加分销商等级规则")
    @PostMapping(value = "addComDistributionRule", produces = "application/json;charset=utf-8")
    public Result addComDistributionRule(@RequestBody ComDistributionRuleDto oComDistributionRuleDto) throws Exception{
        return oComDistributionRuleService.addComDistributionRule(oComDistributionRuleDto);
    }

    /**
     * 修改分销商等级规则
     * @param oComDistributionRuleDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改分销商等级规则")
    @PostMapping(value = "updateComDistributionRule", produces = "application/json;charset=utf-8")
    public Result updateComDistributionRule(@RequestBody ComDistributionRuleDto oComDistributionRuleDto) throws Exception{
        return oComDistributionRuleService.updateComDistributionRule(oComDistributionRuleDto);
    }

    /**
     * 删除分销商等级规则
     * @param oComDistributionRuleDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除分销商等级规则")
    @PostMapping(value = "deleteComDistributionRule", produces = "application/json;charset=utf-8")
    public Result deleteComDistributionRule(@RequestBody ComDistributionRuleDto oComDistributionRuleDto) throws Exception{
        return oComDistributionRuleService.deleteComDistributionRule(oComDistributionRuleDto);
    }

}
