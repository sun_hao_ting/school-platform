package com.project.school_flate.controller.com;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.com.ComFeedbackDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.com.ComFeedback;
import com.project.school_flate.service.com.ComFeedbackService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "反馈")
@RequestMapping("/comFeedback")
public class ComFeedbackController {

    @Autowired
    private ComFeedbackService oComFeedbackService;

    /**
     * 获取反馈
     * @param oComFeedbackDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取反馈")
    @PostMapping(value = "getComFeedback", produces = "application/json;charset=utf-8")
    public Result getComFeedback(@RequestBody ComFeedbackDto oComFeedbackDto) throws Exception{
        return oComFeedbackService.getComFeedback(oComFeedbackDto);
    }

    /**
     * 添加反馈
     * @param oComFeedbackDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加反馈")
    @PostMapping(value = "addComFeedback", produces = "application/json;charset=utf-8")
    public Result addComFeedback(@RequestBody ComFeedbackDto oComFeedbackDto) throws Exception{
        return oComFeedbackService.addComFeedback(oComFeedbackDto);
    }

    /**
     * 修改反馈
     * @param oComFeedbackDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改反馈")
    @PostMapping(value = "updateComFeedback", produces = "application/json;charset=utf-8")
    public Result updateComFeedback(@RequestBody ComFeedbackDto oComFeedbackDto) throws Exception{
        return oComFeedbackService.updateComFeedback(oComFeedbackDto);
    }

    /**
     * 删除反馈
     * @param oComFeedbackDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除反馈")
    @PostMapping(value = "deleteComFeedback", produces = "application/json;charset=utf-8")
    public Result deleteComFeedback(@RequestBody ComFeedbackDto oComFeedbackDto) throws Exception{
        return oComFeedbackService.deleteComFeedback(oComFeedbackDto);
    }

}
