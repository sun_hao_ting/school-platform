package com.project.school_flate.controller.com;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.com.ComSchoolBuildingDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.com.ComSchoolBuilding;
import com.project.school_flate.service.com.ComSchoolBuildingService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "校园楼栋")
@RequestMapping("/comSchoolBuilding")
public class ComSchoolBuildingController {

    @Autowired
    private ComSchoolBuildingService oComSchoolBuildingService;

    /**
     * 获取校园楼栋
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取校园楼栋")
    @PostMapping(value = "getComSchoolBuilding", produces = "application/json;charset=utf-8")
    public Result getComSchoolBuilding(@RequestBody ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception{
        return oComSchoolBuildingService.getComSchoolBuilding(oComSchoolBuildingDto);
    }

    /**
     * 添加校园楼栋
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加校园楼栋")
    @PostMapping(value = "addComSchoolBuilding", produces = "application/json;charset=utf-8")
    public Result addComSchoolBuilding(@RequestBody ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception{
        return oComSchoolBuildingService.addComSchoolBuilding(oComSchoolBuildingDto);
    }

    /**
     * 修改校园楼栋
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改校园楼栋")
    @PostMapping(value = "updateComSchoolBuilding", produces = "application/json;charset=utf-8")
    public Result updateComSchoolBuilding(@RequestBody ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception{
        return oComSchoolBuildingService.updateComSchoolBuilding(oComSchoolBuildingDto);
    }

    /**
     * 冻结校园楼栋
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "冻结校园楼栋")
    @PostMapping(value = "freezeComSchoolBuilding", produces = "application/json;charset=utf-8")
    public Result freezeComSchoolBuilding(@RequestBody ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception{
        return oComSchoolBuildingService.freezeComSchoolBuilding(oComSchoolBuildingDto);
    }

    /**
     * 获取校园楼栋（后台）
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取校园楼栋")
    @PostMapping(value = "getComSchoolBuildingBack", produces = "application/json;charset=utf-8")
    public Result getComSchoolBuildingBack(@RequestBody ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception{
        return oComSchoolBuildingService.getComSchoolBuildingBack(oComSchoolBuildingDto);
    }

    /**
     * 解冻校园楼栋
     * @param oComSchoolBuildingDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "解冻校园楼栋")
    @PostMapping(value = "thawComSchoolBuilding", produces = "application/json;charset=utf-8")
    public Result thawComSchoolBuilding(@RequestBody ComSchoolBuildingDto oComSchoolBuildingDto) throws Exception{
        return oComSchoolBuildingService.thawComSchoolBuilding(oComSchoolBuildingDto);
    }

}
