package com.project.school_flate.controller.com;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.entity.com.ComVariable;
import com.project.school_flate.service.com.ComVariableService;
import com.project.school_flate.util.Result.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author sunht
 * @Date 2023/10/2 19:40
 * @Version v1.0.0
 * @Message
 **/
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@RequestMapping("/comVariable")
@Tag(name = "系统变量管理器", description = "系统变量管理器")
public class ComVariableController {
    @Autowired
    private ComVariableService comVariableService;
    /**
     * 添加系统参数
     * @param
     * @return
     * @throws Exception
     */
    @PostMapping("/addVariable")
    @Operation(summary = "添加系统参数")
    public Result addVariable(@RequestBody ComVariable variable) throws Exception{
        return comVariableService.addVariable(variable);
    }

    /**
     * 获取系统参数
     * @param variable
     * @return
     * @throws Exception
     */
    @PostMapping("/getVariable")
    @Operation(summary = "获取系统参数")
    public Result getVariable(@RequestBody ComVariable variable) throws Exception{
        return comVariableService.getVariable(variable);
    }

    /**
     * 修改系统参数
     * @param
     * @return
     * @throws Exception
     */
    @PostMapping("/updateVariable")
    @Operation(summary = "修改系统参数")
    public Result updateVariable(@RequestBody ComVariable variable) throws Exception{
        return comVariableService.updateVariable(variable);
    }

    /**
     * 删除系统参数
     * @param
     * @return
     * @throws Exception
     */
    @PostMapping("/deleteVariable")
    @Operation(summary = "删除系统参数")
    public Result deleteVariable(@RequestBody ComVariable variable) throws Exception{
        return comVariableService.deleteVariable(variable);
    }

}
