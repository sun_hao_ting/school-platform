package com.project.school_flate.controller.com;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.dto.com.ComFeedbackDto;
import com.project.school_flate.entity.com.ComFileDetail;
import com.project.school_flate.service.com.ComFileDetailService;
import com.project.school_flate.util.Result.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.dromara.x.file.storage.core.FileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@Tag(name = "文件记录表")
@RequestMapping("/comFileDetail")
public class ComFileDetailController {

    @Autowired
    private ComFileDetailService oComFileDetailService;

    /**
     * 生成 URL，有效期为1小时
     * @param oComFileDetail
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "生成 URL，有效期为1小时")
    @PostMapping(value = "generateUrl", produces = "application/json;charset=utf-8")
    public Result generateUrl(@RequestBody ComFileDetail oComFileDetail) throws Exception{
        return oComFileDetailService.generateUrl(oComFileDetail);
    }

    /**
     * 上传公共附件
     * @param request
     * @return
     * @throws Exception
     */
    @Operation(summary = "上传公共附件")
    @PostMapping(value = "uploadPublicFile", produces = "application/json;charset=utf-8")
    public Result uploadPublicFile(HttpServletRequest request) throws Exception{
        return oComFileDetailService.uploadPublicFile(request);
    }

    /**
     * 下载附件
     * @param oComFileDetail,response
     * @return
     * @throws Exception
     */
    @Operation(summary = "下载附件")
    @PostMapping(value = "downFile", produces = "application/json;charset=utf-8")
    public Result downFile(@RequestBody ComFileDetail oComFileDetail, HttpServletResponse response) throws Exception{
        return oComFileDetailService.downFile(oComFileDetail,response);
    }

    /**
     * 上传私密附件
     * @param request
     * @return
     * @throws Exception
     */
    @Operation(summary = "上传私密附件")
    @PostMapping(value = "uploadPrivateFile", produces = "application/json;charset=utf-8")
    public Result uploadPrivateFile(HttpServletRequest request) throws Exception{
        return oComFileDetailService.uploadPrivateFile(request);
    }

}
