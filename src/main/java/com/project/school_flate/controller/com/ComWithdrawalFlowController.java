package com.project.school_flate.controller.com;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.alibaba.fastjson2.JSONObject;
import com.project.school_flate.dto.com.ComWithdrawalFlowDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.service.com.ComWithdrawalFlowService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;

import java.util.Map;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@RestController
@Tag(name = "微信转账流水")
@RequestMapping("/comWithdrawalFlow")
public class ComWithdrawalFlowController {

    @Autowired
    private ComWithdrawalFlowService oComWithdrawalFlowService;

    /**
     * 获取微信转账流水
     * @param oComWithdrawalFlowDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取微信转账流水")
    @PostMapping(value = "getComWithdrawalFlow", produces = "application/json;charset=utf-8")
    public Result getComWithdrawalFlow(@RequestBody ComWithdrawalFlowDto oComWithdrawalFlowDto) throws Exception{
        return oComWithdrawalFlowService.getComWithdrawalFlow(oComWithdrawalFlowDto);
    }

    /**
     * 添加微信转账流水
     * @param oComWithdrawalFlowDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "添加微信转账流水")
    @PostMapping(value = "addComWithdrawalFlow", produces = "application/json;charset=utf-8")
    public Result addComWithdrawalFlow(@RequestBody ComWithdrawalFlowDto oComWithdrawalFlowDto) throws Exception{
        return oComWithdrawalFlowService.addComWithdrawalFlow(oComWithdrawalFlowDto);
    }

    /**
     * 平台确认转账
     * @param oComWithdrawalFlowDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "平台确认转账")
    @PostMapping(value = "comOkWithdrawal", produces = "application/json;charset=utf-8")
    public Result comOkWithdrawal(@RequestBody ComWithdrawalFlowDto oComWithdrawalFlowDto) throws Exception{
        return oComWithdrawalFlowService.comOkWithdrawal(oComWithdrawalFlowDto);
    }

    /**
     * 平台驳回转账
     * @param oComWithdrawalFlowDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "平台驳回转账")
    @PostMapping(value = "comRejectWithdrawal", produces = "application/json;charset=utf-8")
    public Result comRejectWithdrawal(@RequestBody ComWithdrawalFlowDto oComWithdrawalFlowDto) throws Exception{
        return oComWithdrawalFlowService.comRejectWithdrawal(oComWithdrawalFlowDto);
    }

    /**
     * 用户转账（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    @Operation(summary = "用户转账（回调）")
    @PostMapping(value = "comWithdrawalFlowUserCallback", produces = "application/json;charset=utf-8")
    public Map<String, String> comWithdrawalFlowUserCallback(@RequestBody JSONObject jsonObject) throws Exception{
        return oComWithdrawalFlowService.comWithdrawalFlowUserCallback(jsonObject);
    }

    /**
     * 配送员转账（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    @Operation(summary = "配送员转账（回调）")
    @PostMapping(value = "comWithdrawalFlowDeliveryCallback", produces = "application/json;charset=utf-8")
    public Map<String, String> comWithdrawalFlowDeliveryCallback(@RequestBody JSONObject jsonObject) throws Exception{
        return oComWithdrawalFlowService.comWithdrawalFlowDeliveryCallback(jsonObject);
    }

    /**
     * 店铺转账（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    @Operation(summary = "店铺转账（回调）")
    @PostMapping(value = "comWithdrawalFlowShopCallback", produces = "application/json;charset=utf-8")
    public Map<String, String> comWithdrawalFlowShopCallback(@RequestBody JSONObject jsonObject) throws Exception{
        return oComWithdrawalFlowService.comWithdrawalFlowShopCallback(jsonObject);
    }

    /**
     * 获取微信转账规则
     * @param oComWithdrawalFlowDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取微信转账规则")
    @PostMapping(value = "getComWithdrawalFlowRule", produces = "application/json;charset=utf-8")
    public Result getComWithdrawalFlowRule(@RequestBody ComWithdrawalFlowDto oComWithdrawalFlowDto) throws Exception{
        return oComWithdrawalFlowService.getComWithdrawalFlowRule(oComWithdrawalFlowDto);
    }

}
