package com.project.school_flate.controller.com;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.alibaba.fastjson2.JSONObject;
import com.project.school_flate.service.com.WeiXinPayService;
import com.project.school_flate.util.Result.Result;
import com.project.school_flate.util.wechat.PayDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@Tag(name = "微信支付接口")
@Slf4j
public class WeiXinPayController {

    @Autowired
    private WeiXinPayService weiXinPayService;

    /**
     * 支付接口_jsApi
     *
     * @param payDto
     * @return
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @SaCheckLogin
    @Operation(summary = "支付接口_jsApi")
    @PostMapping(value = "/entPay", produces = "application/json;charset=utf-8")
    public Result entPay(@RequestBody PayDto payDto) throws Exception {
        return weiXinPayService.entPay(payDto);
    }

    /**
     * 支付接口_app
     *
     * @param payDto
     * @return
     */
    @SaCheckLogin
    @Operation(summary = "支付接口_app")
    @PostMapping(value = "/entPayApp")
    public Result entPayApp(@RequestBody PayDto payDto) throws Exception {
        return weiXinPayService.entPayApp(payDto);
    }

    /**
     * 支付接口_H5
     *
     * @param payDto
     * @return
     */
    @SaCheckLogin
    @Operation(summary = "支付接口_H5")
    @PostMapping(value = "/entPayH5")
    public Result entPayH5(@RequestBody PayDto payDto) throws Exception {
        return weiXinPayService.entPayH5(payDto);
    }

    /**
     * 回调支付_js_api
     *
     * @param payDto
     * @return
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @SaCheckLogin
    @Operation(summary = "回调支付_js_api")
    @PostMapping(value = "/encryption", produces = "application/json;charset=utf-8")
    public Result encryption(@RequestBody PayDto payDto) throws Exception {
        return weiXinPayService.encryption(payDto.getPrepayId());
    }

    /**
     * 回调支付_js_app
     *
     * @param payDto
     * @return
     */
    @SaCheckLogin
    @Operation(summary = "回调支付_js_app")
    @PostMapping(value = "/encryptionApp")
    public Result encryptionApp(@RequestBody PayDto payDto) throws Exception {
        return weiXinPayService.encryptionApp(payDto.getPrepayId());
    }

    /**
     * 微信提现
     *
     * @param payDto
     * @return
     */
    @SaCheckLogin
    @Operation(summary = "微信提现_js_app")
    @PostMapping(value = "/weixinTransferBat")
    public Result weixinTransferBat(@RequestBody PayDto payDto) throws Exception {
        return weiXinPayService.weixinTransferBat(payDto);
    }

//    /**
//     * 回调通知
//     * @param jsonObject
//     * @return
//     */
//    @SaCheckLogin
//    @Operation(summary = "支付回调通知")
//    @PostMapping(value = "/notifyOrder")
//    public Map<String, String> notifyOrder(@RequestBody JSONObject jsonObject) {
//        return weiXinPayService.notifyOrder(jsonObject);
//    }

    /**
     * 退款接口_jsApi
     *
     * @param payDto
     * @return
     */
    @SaCheckLogin
    @Operation(summary = "支付接口_jsApi")
    @PostMapping(value = "/refundsPay")
    public Result refundsPay(@RequestBody PayDto payDto) throws Exception {
        return weiXinPayService.refundsPay(payDto);
    }

    /**
     * 扫码JS-SDK
     *
     * @param payDto
     * @return
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @SaCheckLogin
    @Operation(summary = "扫码JS-SDK")
    @PostMapping(value = "/scanCode", produces = "application/json;charset=utf-8")
    public Result scanCode(@RequestBody PayDto payDto) throws Exception {
        return weiXinPayService.scanCode(payDto.getThisUrl());
    }

    /**
     * 转账_jsApi
     *
     * @param payDto
     * @return
     */
    @SaCheckLogin
    @Operation(summary = "转账_jsApi")
    @PostMapping(value = "/transferPay")
    public Result transferPay(@RequestBody PayDto payDto) throws Exception {
        return weiXinPayService.transferPay(payDto);
    }

}
