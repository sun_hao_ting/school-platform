package com.project.school_flate.controller.com;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.com.ComImageDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.com.ComImage;
import com.project.school_flate.service.com.ComImageService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "系统图")
@RequestMapping("/comImage")
public class ComImageController {

    @Autowired
    private ComImageService oComImageService;

    /**
     * 获取系统图
     * @param oComImageDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取系统图")
    @PostMapping(value = "getComImage", produces = "application/json;charset=utf-8")
    public Result getComImage(@RequestBody ComImageDto oComImageDto) throws Exception{
        return oComImageService.getComImage(oComImageDto);
    }

    /**
     * 添加系统图
     * @param oComImageDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加系统图")
    @PostMapping(value = "addComImage", produces = "application/json;charset=utf-8")
    public Result addComImage(@RequestBody ComImageDto oComImageDto) throws Exception{
        return oComImageService.addComImage(oComImageDto);
    }

    /**
     * 修改系统图
     * @param oComImageDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改系统图")
    @PostMapping(value = "updateComImage", produces = "application/json;charset=utf-8")
    public Result updateComImage(@RequestBody ComImageDto oComImageDto) throws Exception{
        return oComImageService.updateComImage(oComImageDto);
    }

    /**
     * 删除系统图
     * @param oComImageDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除系统图")
    @PostMapping(value = "deleteComImage", produces = "application/json;charset=utf-8")
    public Result deleteComImage(@RequestBody ComImageDto oComImageDto) throws Exception{
        return oComImageService.deleteComImage(oComImageDto);
    }

}
