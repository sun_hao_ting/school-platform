package com.project.school_flate.controller.reportForms;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.dto.delivery.DeliveryInfoDto;
import com.project.school_flate.dto.shop.ShopInfoDto;
import com.project.school_flate.dto.user.UserInfoDto;
import com.project.school_flate.service.reportForms.ReportFormsShopService;
import com.project.school_flate.util.Result.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "店铺报表")
@RequestMapping("/reportFormsShop")
public class ReportFormsShopController {

    @Autowired
    private ReportFormsShopService oReportFormsShopService;

    /**
     * 获取累计单量
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取累计单量")
    @PostMapping(value = "getAllRobOrder", produces = "application/json;charset=utf-8")
    public Result getAllRobOrder(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oReportFormsShopService.getAllRobOrder(oShopInfoDto);
    }

    /**
     * 获取今日抢单
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取今日抢单")
    @PostMapping(value = "getDayRobOrder", produces = "application/json;charset=utf-8")
    public Result getDayRobOrder(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oReportFormsShopService.getDayRobOrder(oShopInfoDto);
    }

    /**
     * 获取我的余额
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取我的余额")
    @PostMapping(value = "getDayRobOrderBalance", produces = "application/json;charset=utf-8")
    public Result getDayRobOrderBalance(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oReportFormsShopService.getDayRobOrderBalance(oShopInfoDto);
    }

    /**
     * 获取可提现余额和累计提现
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取可提现余额和累计提现")
    @PostMapping(value = "getWithdrawalBalance", produces = "application/json;charset=utf-8")
    public Result getWithdrawalBalance(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oReportFormsShopService.getWithdrawalBalance(oShopInfoDto);
    }

    /**
     * 获取即将到账和累计到账
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取即将到账和累计到账")
    @PostMapping(value = "getReceived", produces = "application/json;charset=utf-8")
    public Result getReceived(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oReportFormsShopService.getReceived(oShopInfoDto);
    }

    /**
     * 获取收入明细
     * @param oShopInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取即将到账和累计到账")
    @PostMapping(value = "getIncome", produces = "application/json;charset=utf-8")
    public Result getIncome(@RequestBody ShopInfoDto oShopInfoDto) throws Exception{
        return oReportFormsShopService.getIncome(oShopInfoDto);
    }

}
