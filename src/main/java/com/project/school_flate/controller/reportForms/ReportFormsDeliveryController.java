package com.project.school_flate.controller.reportForms;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.dto.delivery.DeliveryInfoDto;
import com.project.school_flate.dto.user.UserInfoDto;
import com.project.school_flate.service.reportForms.ReportFormsDeliveryService;
import com.project.school_flate.util.Result.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "配送员报表")
@RequestMapping("/reportFormsDelivery")
public class ReportFormsDeliveryController {

    @Autowired
    private ReportFormsDeliveryService oReportFormsDeliveryService;

    /**
     * 获取累计抢单
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取累计抢单")
    @PostMapping(value = "getAllRobOrder", produces = "application/json;charset=utf-8")
    public Result getAllRobOrder(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oReportFormsDeliveryService.getAllRobOrder(oDeliveryInfoDto);
    }

    /**
     * 获取今日抢单
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取今日抢单")
    @PostMapping(value = "getDayRobOrder", produces = "application/json;charset=utf-8")
    public Result getDayRobOrder(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oReportFormsDeliveryService.getDayRobOrder(oDeliveryInfoDto);
    }

    /**
     * 获取我的余额
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @SaCheckLogin
    @Operation(summary = "获取我的余额")
    @PostMapping(value = "getDayRobOrderBalance", produces = "application/json;charset=utf-8")
    public Result getDayRobOrderBalance(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oReportFormsDeliveryService.getDayRobOrderBalance(oDeliveryInfoDto);
    }

    /**
     * 获取可提现余额和累计余额
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取可提现余额和累计余额")
    @PostMapping(value = "getWithdrawalBalance", produces = "application/json;charset=utf-8")
    public Result getWithdrawalBalance(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oReportFormsDeliveryService.getWithdrawalBalance(oDeliveryInfoDto);
    }

    /**
     * 获取即将到账和累计到账
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取即将到账和累计到账")
    @PostMapping(value = "getReceived", produces = "application/json;charset=utf-8")
    public Result getReceived(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oReportFormsDeliveryService.getReceived(oDeliveryInfoDto);
    }

    /**
     * 获取收入明细
     * @param oDeliveryInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取即将到账和累计到账")
    @PostMapping(value = "getIncome", produces = "application/json;charset=utf-8")
    public Result getIncome(@RequestBody DeliveryInfoDto oDeliveryInfoDto) throws Exception{
        return oReportFormsDeliveryService.getIncome(oDeliveryInfoDto);
    }

}
