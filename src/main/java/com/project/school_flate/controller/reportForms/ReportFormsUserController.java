package com.project.school_flate.controller.reportForms;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.dto.delivery.DeliveryInfoDto;
import com.project.school_flate.dto.shop.ShopInfoDto;
import com.project.school_flate.dto.user.UserInfoDto;
import com.project.school_flate.service.reportForms.ReportFormsUserService;
import com.project.school_flate.util.Result.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "用户报表")
@RequestMapping("/reportFormsUser")
public class ReportFormsUserController {

    @Autowired
    private ReportFormsUserService oReportFormsUserService;

    /**
     * 获取我的余额
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @SaCheckLogin
    @Operation(summary = "获取我的余额")
    @PostMapping(value = "getDayRobOrderBalance", produces = "application/json;charset=utf-8")
    public Result getDayRobOrderBalance(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oReportFormsUserService.getDayRobOrderBalance(oUserInfoDto);
    }

    /**
     * 获取可提现余额和累计余额
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取可提现余额和累计余额")
    @PostMapping(value = "getWithdrawalBalance", produces = "application/json;charset=utf-8")
    public Result getWithdrawalBalance(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oReportFormsUserService.getWithdrawalBalance(oUserInfoDto);
    }

    /**
     * 获取即将到账和累计到账
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取即将到账和累计到账")
    @PostMapping(value = "getReceived", produces = "application/json;charset=utf-8")
    public Result getReceived(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oReportFormsUserService.getReceived(oUserInfoDto);
    }

    /**
     * 获取收入明细
     * @param oUserInfoDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取即将到账和累计到账")
    @PostMapping(value = "getIncome", produces = "application/json;charset=utf-8")
    public Result getIncome(@RequestBody UserInfoDto oUserInfoDto) throws Exception{
        return oReportFormsUserService.getIncome(oUserInfoDto);
    }

}
