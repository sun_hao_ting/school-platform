package com.project.school_flate.controller.order;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.alibaba.fastjson2.JSONObject;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.order.OrderTakeawayCancellationDto;
import com.project.school_flate.dto.order.OrderTakeawayDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.order.OrderTakeawayCancellation;
import com.project.school_flate.service.order.OrderTakeawayCancellationService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@RestController
@Tag(name = "订单取消")
@RequestMapping("/orderTakeawayCancellation")
public class OrderTakeawayCancellationController {

    @Autowired
    private OrderTakeawayCancellationService oOrderTakeawayCancellationService;

    /**
     * 获取订单取消
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取订单取消")
    @PostMapping(value = "getOrderTakeawayCancellation", produces = "application/json;charset=utf-8")
    public Result getOrderTakeawayCancellation(@RequestBody OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception{
        return oOrderTakeawayCancellationService.getOrderTakeawayCancellation(oOrderTakeawayCancellationDto);
    }

    /**
     * 添加订单取消
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "添加订单取消")
    @PostMapping(value = "addOrderTakeawayCancellation", produces = "application/json;charset=utf-8")
    public Result addOrderTakeawayCancellation(@RequestBody OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception{
        return oOrderTakeawayCancellationService.addOrderTakeawayCancellation(oOrderTakeawayCancellationDto);
    }

    /**
     * 修改订单取消
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "修改订单取消")
    @PostMapping(value = "updateOrderTakeawayCancellation", produces = "application/json;charset=utf-8")
    public Result updateOrderTakeawayCancellation(@RequestBody OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception{
        return oOrderTakeawayCancellationService.updateOrderTakeawayCancellation(oOrderTakeawayCancellationDto);
    }

    /**
     * 删除订单取消
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "删除订单取消")
    @PostMapping(value = "deleteOrderTakeawayCancellation", produces = "application/json;charset=utf-8")
    public Result deleteOrderTakeawayCancellation(@RequestBody OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception{
        return oOrderTakeawayCancellationService.deleteOrderTakeawayCancellation(oOrderTakeawayCancellationDto);
    }

    /**
     * 店铺取消订单
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "店铺取消订单")
    @PostMapping(value = "shopOrderTakeawayCancellation", produces = "application/json;charset=utf-8")
    public Result shopOrderTakeawayCancellation(@RequestBody OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception{
        return oOrderTakeawayCancellationService.shopOrderTakeawayCancellation(oOrderTakeawayCancellationDto);
    }

    /**
     * 取消微信支付订单（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    @Operation(summary = "取消微信支付订单（回调）")
    @PostMapping(value = "cancellationOrderTakeawayCallback", produces = "application/json;charset=utf-8")
    public Map<String, String> cancellationOrderTakeawayCallback(@RequestBody JSONObject jsonObject) throws Exception{
        return oOrderTakeawayCancellationService.cancellationOrderTakeawayCallback(jsonObject);
    }

    /**
     * 用户申请取消订单
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "用户申请取消订单")
    @PostMapping(value = "userApplyOrderTakeawayCancellation", produces = "application/json;charset=utf-8")
    public Result userApplyOrderTakeawayCancellation(@RequestBody OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception{
        return oOrderTakeawayCancellationService.userApplyOrderTakeawayCancellation(oOrderTakeawayCancellationDto);
    }

    /**
     * 平台确认取消订单
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "平台确认取消订单")
    @PostMapping(value = "comOkOrderTakeawayCancellation", produces = "application/json;charset=utf-8")
    public Result comOkOrderTakeawayCancellation(@RequestBody OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception{
        return oOrderTakeawayCancellationService.comOkOrderTakeawayCancellation(oOrderTakeawayCancellationDto);
    }

    /**
     * 平台驳回取消订单
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "平台驳回取消订单")
    @PostMapping(value = "comRejectOrderTakeawayCancellation", produces = "application/json;charset=utf-8")
    public Result comRejectOrderTakeawayCancellation(@RequestBody OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception{
        return oOrderTakeawayCancellationService.comRejectOrderTakeawayCancellation(oOrderTakeawayCancellationDto);
    }

    /**
     * 获取订单取消理由模板
     * @param oOrderTakeawayCancellationDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取订单取消理由模板")
    @PostMapping(value = "getOrderTakeawayCancellationTemplate", produces = "application/json;charset=utf-8")
    public Result getOrderTakeawayCancellationTemplate(@RequestBody OrderTakeawayCancellationDto oOrderTakeawayCancellationDto) throws Exception{
        return oOrderTakeawayCancellationService.getOrderTakeawayCancellationTemplate(oOrderTakeawayCancellationDto);
    }

}
