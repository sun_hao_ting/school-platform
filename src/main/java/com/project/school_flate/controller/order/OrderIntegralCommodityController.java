package com.project.school_flate.controller.order;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.order.OrderIntegralCommodityDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.order.OrderIntegralCommodity;
import com.project.school_flate.service.order.OrderIntegralCommodityService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "积分订单商品")
@RequestMapping("/orderIntegralCommodity")
public class OrderIntegralCommodityController {

    @Autowired
    private OrderIntegralCommodityService oOrderIntegralCommodityService;

    /**
     * 获取积分订单商品
     * @param oOrderIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取积分订单商品")
    @PostMapping(value = "getOrderIntegralCommodity", produces = "application/json;charset=utf-8")
    public Result getOrderIntegralCommodity(@RequestBody OrderIntegralCommodityDto oOrderIntegralCommodityDto) throws Exception{
        return oOrderIntegralCommodityService.getOrderIntegralCommodity(oOrderIntegralCommodityDto);
    }

    /**
     * 添加积分订单商品
     * @param oOrderIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加积分订单商品")
    @PostMapping(value = "addOrderIntegralCommodity", produces = "application/json;charset=utf-8")
    public Result addOrderIntegralCommodity(@RequestBody OrderIntegralCommodityDto oOrderIntegralCommodityDto) throws Exception{
        return oOrderIntegralCommodityService.addOrderIntegralCommodity(oOrderIntegralCommodityDto);
    }

    /**
     * 修改积分订单商品
     * @param oOrderIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改积分订单商品")
    @PostMapping(value = "updateOrderIntegralCommodity", produces = "application/json;charset=utf-8")
    public Result updateOrderIntegralCommodity(@RequestBody OrderIntegralCommodityDto oOrderIntegralCommodityDto) throws Exception{
        return oOrderIntegralCommodityService.updateOrderIntegralCommodity(oOrderIntegralCommodityDto);
    }

    /**
     * 删除积分订单商品
     * @param oOrderIntegralCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除积分订单商品")
    @PostMapping(value = "deleteOrderIntegralCommodity", produces = "application/json;charset=utf-8")
    public Result deleteOrderIntegralCommodity(@RequestBody OrderIntegralCommodityDto oOrderIntegralCommodityDto) throws Exception{
        return oOrderIntegralCommodityService.deleteOrderIntegralCommodity(oOrderIntegralCommodityDto);
    }

}
