package com.project.school_flate.controller.order;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.order.OrderTakeawayCommodityDto;
import com.project.school_flate.dto.order.OrderTakeawayDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.order.OrderTakeawayCommodity;
import com.project.school_flate.service.order.OrderTakeawayCommodityService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "外卖订单商品")
@RequestMapping("/orderTakeawayCommodity")
public class OrderTakeawayCommodityController {

    @Autowired
    private OrderTakeawayCommodityService oOrderTakeawayCommodityService;

    /**
     * 获取外卖订单商品
     * @param oOrderTakeawayCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取外卖订单商品")
    @PostMapping(value = "getOrderTakeawayCommodity", produces = "application/json;charset=utf-8")
    public Result getOrderTakeawayCommodity(@RequestBody OrderTakeawayCommodityDto oOrderTakeawayCommodityDto) throws Exception{
        return oOrderTakeawayCommodityService.getOrderTakeawayCommodity(oOrderTakeawayCommodityDto);
    }

    /**
     * 添加外卖订单商品
     * @param oOrderTakeawayCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加外卖订单商品")
    @PostMapping(value = "addOrderTakeawayCommodity", produces = "application/json;charset=utf-8")
    public Result addOrderTakeawayCommodity(@RequestBody OrderTakeawayCommodityDto oOrderTakeawayCommodityDto) throws Exception{
        return oOrderTakeawayCommodityService.addOrderTakeawayCommodity(oOrderTakeawayCommodityDto);
    }

    /**
     * 修改外卖订单商品
     * @param oOrderTakeawayCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改外卖订单商品")
    @PostMapping(value = "updateOrderTakeawayCommodity", produces = "application/json;charset=utf-8")
    public Result updateOrderTakeawayCommodity(@RequestBody OrderTakeawayCommodityDto oOrderTakeawayCommodityDto) throws Exception{
        return oOrderTakeawayCommodityService.updateOrderTakeawayCommodity(oOrderTakeawayCommodityDto);
    }

    /**
     * 删除外卖订单商品
     * @param oOrderTakeawayCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除外卖订单商品")
    @PostMapping(value = "deleteOrderTakeawayCommodity", produces = "application/json;charset=utf-8")
    public Result deleteOrderTakeawayCommodity(@RequestBody OrderTakeawayCommodityDto oOrderTakeawayCommodityDto) throws Exception{
        return oOrderTakeawayCommodityService.deleteOrderTakeawayCommodity(oOrderTakeawayCommodityDto);
    }

    /**
     * 核算购物车外卖订单价格（客户）
     * @param oOrderTakeawayCommodityDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "核算购物车外卖订单价格（客户）")
    @PostMapping(value = "countCarOrderIntegral", produces = "application/json;charset=utf-8")
    public Result countCarOrderIntegral(@RequestBody OrderTakeawayCommodityDto oOrderTakeawayCommodityDto) throws Exception{
        return oOrderTakeawayCommodityService.countCarOrderIntegral(oOrderTakeawayCommodityDto);
    }

}
