package com.project.school_flate.controller.order;

import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.order.OrderDeliveryDto;
import com.project.school_flate.util.Result.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.order.OrderDelivery;
import com.project.school_flate.service.order.OrderDeliveryService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@RestController
@Tag(name = "外卖订单配送")
@RequestMapping("/orderDelivery")
public class OrderDeliveryController {

    @Autowired
    private OrderDeliveryService oOrderDeliveryService;

    /**
     * 获取外卖订单配送
     * @param oOrderDeliveryDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取外卖订单配送")
    @PostMapping(value = "getOrderDelivery", produces = "application/json;charset=utf-8")
    public Result getOrderDelivery(@RequestBody OrderDeliveryDto oOrderDeliveryDto) throws Exception{
        return oOrderDeliveryService.getOrderDelivery(oOrderDeliveryDto);
    }

    /**
     * 添加外卖订单配送
     * @param oOrderDeliveryDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "添加外卖订单配送")
    @PostMapping(value = "addOrderDelivery", produces = "application/json;charset=utf-8")
    public Result addOrderDelivery(@RequestBody OrderDeliveryDto oOrderDeliveryDto) throws Exception{
        return oOrderDeliveryService.addOrderDelivery(oOrderDeliveryDto);
    }

    /**
     * 修改外卖订单配送
     * @param oOrderDeliveryDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "修改外卖订单配送")
    @PostMapping(value = "updateOrderDelivery", produces = "application/json;charset=utf-8")
    public Result updateOrderDelivery(@RequestBody OrderDeliveryDto oOrderDeliveryDto) throws Exception{
        return oOrderDeliveryService.updateOrderDelivery(oOrderDeliveryDto);
    }

    /**
     * 删除外卖订单配送
     * @param oOrderDeliveryDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "删除外卖订单配送")
    @PostMapping(value = "deleteOrderDelivery", produces = "application/json;charset=utf-8")
    public Result deleteOrderDelivery(@RequestBody OrderDeliveryDto oOrderDeliveryDto) throws Exception{
        return oOrderDeliveryService.deleteOrderDelivery(oOrderDeliveryDto);
    }

}
