package com.project.school_flate.controller.order;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.alibaba.fastjson2.JSONObject;
import com.mybatisflex.core.paginate.Page;
import com.project.school_flate.dto.order.OrderTakeawayDto;
import com.project.school_flate.util.Result.Result;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.project.school_flate.entity.order.OrderTakeaway;
import com.project.school_flate.service.order.OrderTakeawayService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 *  控制层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@RestController
@Tag(name = "外卖订单")
@RequestMapping("/orderTakeaway")
public class OrderTakeawayController {

    @Autowired
    private OrderTakeawayService oOrderTakeawayService;

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    /**
     * 获取外卖订单
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取外卖订单")
    @PostMapping(value = "getOrderTakeaway", produces = "application/json;charset=utf-8")
    public Result getOrderTakeaway(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception{
        return oOrderTakeawayService.getOrderTakeaway(oOrderTakeawayDto);
    }

    /**
     * 添加外卖订单
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "添加外卖订单")
    @PostMapping(value = "addOrderTakeaway", produces = "application/json;charset=utf-8")
    public Result addOrderTakeaway(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception{
        return oOrderTakeawayService.addOrderTakeaway(oOrderTakeawayDto);
    }

    /**
     * 修改外卖订单
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "修改外卖订单")
    @PostMapping(value = "updateOrderTakeaway", produces = "application/json;charset=utf-8")
    public Result updateOrderTakeaway(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception{
        return oOrderTakeawayService.updateOrderTakeaway(oOrderTakeawayDto);
    }

    /**
     * 删除外卖订单
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "删除外卖订单")
    @PostMapping(value = "deleteOrderTakeaway", produces = "application/json;charset=utf-8")
    public Result deleteOrderTakeaway(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception{
        return oOrderTakeawayService.deleteOrderTakeaway(oOrderTakeawayDto);
    }

    /**
     * 店铺出餐
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "店铺出餐")
    @PostMapping(value = "diningOrderTakeawayToShop", produces = "application/json;charset=utf-8")
    public Result diningOrderTakeawayToShop(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception{
        return oOrderTakeawayService.diningOrderTakeawayToShop(oOrderTakeawayDto);
    }

    /**
     * 店铺自动出餐
     * @param oOrderTakeawayId
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @JmsListener(destination = "automaticDiningOrderTakeawayToShop", containerFactory = "queueListener")
    public void automaticDiningOrderTakeawayToShop(String oOrderTakeawayId) throws Exception{
        oOrderTakeawayService.automaticDiningOrderTakeawayToShop(oOrderTakeawayId);
    }

    /**
     * 获取外卖订单大厅（配送员）
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取外卖订单大厅（配送员）")
    @PostMapping(value = "getOrderTakeawayHallToDelivery", produces = "application/json;charset=utf-8")
    public Result getOrderTakeawayHallToDelivery(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception{
        return oOrderTakeawayService.getOrderTakeawayHallToDelivery(oOrderTakeawayDto);
    }

    /**
     * 配送员抢单
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "配送员抢单")
    @PostMapping(value = "robOrderTakeawayToDelivery", produces = "application/json;charset=utf-8")
    public Result robOrderTakeawayToDelivery(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception{
        return oOrderTakeawayService.robOrderTakeawayToDelivery(oOrderTakeawayDto);
    }

    /**
     * 配送员自动抢单
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "配送员自动抢单")
    @PostMapping(value = "automaticRobOrderTakeawayToDelivery", produces = "application/json;charset=utf-8")
    public Result automaticRobOrderTakeawayToDelivery(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception{
        return oOrderTakeawayService.automaticRobOrderTakeawayToDelivery(oOrderTakeawayDto);
    }

    /**
     * 配送员取餐
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "配送员抢单")
    @PostMapping(value = "takeOrderTakeawayToDelivery", produces = "application/json;charset=utf-8")
    public Result takeOrderTakeawayToDelivery(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception{
        return oOrderTakeawayService.takeOrderTakeawayToDelivery(oOrderTakeawayDto);
    }

    /**
     * 生成中转订单二维码
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "生成中转订单二维码")
    @PostMapping(value = "transferOrderTakeawayCode", produces = "application/json;charset=utf-8")
    public Result transferOrderTakeawayCode(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception{
        return oOrderTakeawayService.transferOrderTakeawayCode(oOrderTakeawayDto);
    }

    /**
     * 配送员中转订单
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "配送员中转订单")
    @PostMapping(value = "transferOrderTakeawayToDelivery", produces = "application/json;charset=utf-8")
    public Result transferOrderTakeawayToDelivery(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception{
        return oOrderTakeawayService.transferOrderTakeawayToDelivery(oOrderTakeawayDto);
    }

    /**
     * 配送员确认完成
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "配送员确认完成")
    @PostMapping(value = "okOrderTakeawayToDelivery", produces = "application/json;charset=utf-8")
    public Result okOrderTakeawayToDelivery(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception{
        return oOrderTakeawayService.okOrderTakeawayToDelivery(oOrderTakeawayDto);
    }

    /**
     * 获取外卖订单（配送员）
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取外卖订单（配送员）")
    @PostMapping(value = "getOrderTakeawayToDelivery", produces = "application/json;charset=utf-8")
    public Result getOrderTakeawayToDelivery(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception {
        return oOrderTakeawayService.getOrderTakeawayToDelivery(oOrderTakeawayDto);
    }

    /**
     * 获取外卖订单（商家）
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取外卖订单（商家）")
    @PostMapping(value = "getOrderTakeawayToShop", produces = "application/json;charset=utf-8")
    public Result getOrderTakeawayToShop(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception {
        return oOrderTakeawayService.getOrderTakeawayToShop(oOrderTakeawayDto);
    }

    /**
     * 微信支付外卖订单成功（回调）
     * @param jsonObject
     * @return
     * @throws Exception
     */
    @Operation(summary = "微信支付外卖订单成功（回调）")
    @PostMapping(value = "wxPayOrderTakeawayCallback", produces = "application/json;charset=utf-8")
    public Map<String, String> wxPayOrderTakeawayCallback(@RequestBody JSONObject jsonObject) throws Exception{
        return oOrderTakeawayService.wxPayOrderTakeawayCallback(jsonObject);
    }

    /**
     * 储蓄支付外卖订单成功（回调）
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "储蓄支付外卖订单成功（回调）")
    @PostMapping(value = "savePayOrderTakeawayCallback", produces = "application/json;charset=utf-8")
    public Result savePayOrderTakeawayCallback(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception{
        return oOrderTakeawayService.savePayOrderTakeawayCallback(oOrderTakeawayDto);
    }

    /**
     * 客户确认完成
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "客户确认完成")
    @PostMapping(value = "okOrderTakeawayToUser", produces = "application/json;charset=utf-8")
    public Result okOrderTakeawayToUser(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception{
        return oOrderTakeawayService.okOrderTakeawayToUser(oOrderTakeawayDto);
    }

    /**
     * 客户自动确认完成
     * @param oOrderTakeawayId
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @JmsListener(destination = "automaticOkOrderTakeawayToUser", containerFactory = "queueListener")
    public void automaticOkOrderTakeawayToUser(String oOrderTakeawayId) throws Exception{
        oOrderTakeawayService.automaticOkOrderTakeawayToUser(oOrderTakeawayId);
    }

    /**
     * 自动删除外卖订单
     * @param oOrderTakeawayId
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @JmsListener(destination = "automaticDeleteOrderTakeaway", containerFactory = "queueListener")
    public void automaticDeleteOrderTakeaway(String oOrderTakeawayId) throws Exception{
        oOrderTakeawayService.automaticDeleteOrderTakeaway(oOrderTakeawayId);
    }

    /**
     * 获取外卖订单（用户）
     * @param oOrderTakeawayDto
     * @return
     * @throws Exception
     */
    @DecryptBody(value = DecryptBodyMethod.AES)
    @EncryptBody(value = EncryptBodyMethod.AES)
    @Operation(summary = "获取外卖订单（用户）")
    @PostMapping(value = "getOrderTakeawayToUser", produces = "application/json;charset=utf-8")
    public Result getOrderTakeawayToUser(@RequestBody OrderTakeawayDto oOrderTakeawayDto) throws Exception{
        return oOrderTakeawayService.getOrderTakeawayToUser(oOrderTakeawayDto);
    }

}
