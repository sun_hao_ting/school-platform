package com.project.school_flate.controller.flow;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.service.flow.FlowTaskService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "平台任务")
@RequestMapping("/flowTask")
public class FlowTaskController {

    @Autowired
    private FlowTaskService flowTaskService;

    /**
     * 执行任务，把金额发向各个角色
     * 定时任务，间隔:每天2时执行一次
     *
     */
    @Scheduled(cron = "0 0 2 * * ?")
    public void executeFlowTask() throws Exception{
        flowTaskService.executeFlowTask();
    }
}
