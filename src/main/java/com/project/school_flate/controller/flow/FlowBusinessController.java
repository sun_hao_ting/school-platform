package com.project.school_flate.controller.flow;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.dto.flow.FlowBusinessDto;
import com.project.school_flate.dto.shop.ShopSaveUserDto;
import com.project.school_flate.service.flow.FlowBusinessService;
import com.project.school_flate.util.Result.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "业务流水")
@RequestMapping("/flowBusiness")
public class FlowBusinessController {

    @Autowired
    private FlowBusinessService oFlowBusinessService;

    /**
     * 获取业务流水
     * @param oFlowBusinessDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取业务流水")
    @PostMapping(value = "getFlowBusiness", produces = "application/json;charset=utf-8")
    public Result getFlowBusiness(@RequestBody FlowBusinessDto oFlowBusinessDto) throws Exception{
        return oFlowBusinessService.getFlowBusiness(oFlowBusinessDto);
    }

    /**
     * 获取用户在店铺储蓄消费
     * @param oFlowBusinessDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取店铺用户储蓄")
    @PostMapping(value = "getFlowMoneyConsumptionToShopSaveUser", produces = "application/json;charset=utf-8")
    public Result getFlowMoneyConsumptionToShopSaveUser(@RequestBody FlowBusinessDto oFlowBusinessDto) throws Exception{
        return oFlowBusinessService.getFlowMoneyConsumptionToShopSaveUser(oFlowBusinessDto);
    }

}
