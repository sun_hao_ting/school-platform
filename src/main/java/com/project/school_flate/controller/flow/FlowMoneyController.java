package com.project.school_flate.controller.flow;

import cn.licoy.encryptbody.annotation.decrypt.DecryptBody;
import cn.licoy.encryptbody.annotation.encrypt.EncryptBody;
import cn.licoy.encryptbody.enums.DecryptBodyMethod;
import cn.licoy.encryptbody.enums.EncryptBodyMethod;
import com.project.school_flate.dto.flow.FlowMoneyDto;
import com.project.school_flate.dto.shop.ShopSaveUserDto;
import com.project.school_flate.service.flow.FlowMoneyService;
import com.project.school_flate.util.Result.Result;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@DecryptBody(value = DecryptBodyMethod.AES)
@EncryptBody(value = EncryptBodyMethod.AES)
@RestController
@Tag(name = "平台流水")
@RequestMapping("/flowMoney")
public class FlowMoneyController {
    @Autowired
    private FlowMoneyService oFlowMoneyService;

    /**
     * 获取平台流水
     * @param oFlowMoneyDto
     * @return
     * @throws Exception
     */
    @Operation(summary = "获取平台流水")
    @PostMapping(value = "getFlowMoney", produces = "application/json;charset=utf-8")
    public Result getFlowMoney(@RequestBody FlowMoneyDto oFlowMoneyDto) throws Exception{
        return oFlowMoneyService.getFlowMoney(oFlowMoneyDto);
    }

}
