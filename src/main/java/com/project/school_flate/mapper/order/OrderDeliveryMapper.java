package com.project.school_flate.mapper.order;

import org.apache.ibatis.annotations.Mapper;
import com.mybatisflex.core.BaseMapper;
import com.project.school_flate.entity.order.OrderDelivery;

/**
 *  映射层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Mapper
public interface OrderDeliveryMapper extends BaseMapper<OrderDelivery> {

}
