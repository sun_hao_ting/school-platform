package com.project.school_flate.mapper.order;

import com.mybatisflex.core.BaseMapper;
import com.project.school_flate.entity.order.OrderTakeawaySaveCache;

public interface OrderTakeawaySaveCacheMapper extends BaseMapper<OrderTakeawaySaveCache> {
}
