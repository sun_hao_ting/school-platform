package com.project.school_flate.mapper.user;

import org.apache.ibatis.annotations.Mapper;
import com.mybatisflex.core.BaseMapper;
import com.project.school_flate.entity.user.UserAddress;

/**
 *  映射层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Mapper
public interface UserAddressMapper extends BaseMapper<UserAddress> {

}
