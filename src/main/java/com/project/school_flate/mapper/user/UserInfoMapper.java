package com.project.school_flate.mapper.user;

import com.mybatisflex.core.BaseMapper;
import com.project.school_flate.entity.user.UserInfo;

public interface UserInfoMapper extends BaseMapper<UserInfo> {
}
