package com.project.school_flate.mapper.com;

import org.apache.ibatis.annotations.Mapper;
import com.mybatisflex.core.BaseMapper;
import com.project.school_flate.entity.com.ComFileDetail;

/**
 * 文件记录表 映射层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Mapper
public interface ComFileDetailMapper extends BaseMapper<ComFileDetail> {

}
