package com.project.school_flate.mapper.com;

import com.mybatisflex.core.BaseMapper;
import com.project.school_flate.entity.com.ComRole;

public interface ComRoleMapper extends BaseMapper<ComRole> {
}
