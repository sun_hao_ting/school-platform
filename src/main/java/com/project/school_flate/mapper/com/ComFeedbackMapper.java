package com.project.school_flate.mapper.com;

import org.apache.ibatis.annotations.Mapper;
import com.mybatisflex.core.BaseMapper;
import com.project.school_flate.entity.com.ComFeedback;

/**
 *  映射层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Mapper
public interface ComFeedbackMapper extends BaseMapper<ComFeedback> {

}
