package com.project.school_flate.mapper.com;

import com.mybatisflex.core.BaseMapper;
import com.project.school_flate.entity.com.ComPayFlow;

/**
 * @Author sunht
 * @Date 2023/9/19 20:59
 * @Version v1.0.0
 * @Message
 **/
public interface ComPayFlowMapper extends BaseMapper<ComPayFlow> {
}
