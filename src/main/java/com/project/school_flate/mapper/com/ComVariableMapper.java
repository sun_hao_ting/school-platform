package com.project.school_flate.mapper.com;

import com.mybatisflex.core.BaseMapper;
import com.project.school_flate.entity.com.ComVariable;

/**
 * @Author sunht
 * @Date 2023/10/2 19:48
 * @Version v1.0.0
 * @Message
 **/
public interface ComVariableMapper extends BaseMapper<ComVariable> {
}
