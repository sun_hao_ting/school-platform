package com.project.school_flate.mapper.com;

import org.apache.ibatis.annotations.Mapper;
import com.mybatisflex.core.BaseMapper;
import com.project.school_flate.entity.com.ComImage;

/**
 *  映射层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Mapper
public interface ComImageMapper extends BaseMapper<ComImage> {

}
