package com.project.school_flate.mapper.flow;

import com.mybatisflex.core.BaseMapper;
import com.project.school_flate.entity.flow.FlowBusiness;

public interface FlowBusinessMapper extends BaseMapper<FlowBusiness> {
}
