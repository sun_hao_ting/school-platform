package com.project.school_flate.mapper.flow;

import com.mybatisflex.core.BaseMapper;
import com.project.school_flate.entity.flow.FlowMoney;

public interface FlowMoneyMapper extends BaseMapper<FlowMoney> {
}
