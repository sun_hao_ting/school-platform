package com.project.school_flate.mapper.delivery;

import org.apache.ibatis.annotations.Mapper;
import com.mybatisflex.core.BaseMapper;
import com.project.school_flate.entity.delivery.DeliveryInfo;

/**
 *  映射层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Mapper
public interface DeliveryInfoMapper extends BaseMapper<DeliveryInfo> {

}
