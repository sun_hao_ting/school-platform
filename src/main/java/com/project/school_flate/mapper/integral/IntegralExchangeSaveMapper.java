package com.project.school_flate.mapper.integral;

import org.apache.ibatis.annotations.Mapper;
import com.mybatisflex.core.BaseMapper;
import com.project.school_flate.entity.integral.IntegralExchangeSave;

/**
 *  映射层。
 *
 * @author 马维健
 * @since 2024/1/2
 */
@Mapper
public interface IntegralExchangeSaveMapper extends BaseMapper<IntegralExchangeSave> {

}
