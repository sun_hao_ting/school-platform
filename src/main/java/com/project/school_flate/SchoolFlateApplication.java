package com.project.school_flate;

import cn.licoy.encryptbody.annotation.EnableEncryptBody;
import org.dromara.x.file.storage.spring.EnableFileStorage;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableScheduling;
//开启MQ
@EnableJms
@EnableFileStorage
@EnableEncryptBody
@SpringBootApplication
@EnableScheduling
@MapperScan("com.project.school_flate.mapper")
public class SchoolFlateApplication {

    public static void main(String[] args) {
        SpringApplication.run(SchoolFlateApplication.class, args);
    }

}
